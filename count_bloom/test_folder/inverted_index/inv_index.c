#include "inv_index.h"



void read_vocab(const char * vocab_file, invIndex* inv_index)
{
	printf("This functions reads the vocabulary and also initializes the structure!!\n");

	FILE *reader;
	reader = fopen(vocab_file,"r");
	int vocab_length;
	fscanf(reader,"%d\n",&vocab_length);

	inv_index->vocab_hash = g_hash_table_new(g_str_hash, g_str_equal);									//string to int dictionary
	inv_index->vocab = (char **)malloc(vocab_length*sizeof(char *));
	inv_index->bloom_inv_ind = (bloom *)malloc(vocab_length*sizeof(bloom));
	inv_index->vocab_size = vocab_length;
	int i;
	for(i = 0; i < vocab_length; i++)
	{		
		inv_index->vocab[i] = (char *)malloc(sizeof(char)*1000);
		fscanf(reader,"%s\n",inv_index->vocab[i]);

		// printf("nn %s\n",inv_index->vocab[i] );
		g_hash_table_insert(inv_index->vocab_hash, inv_index->vocab[i], i+1);
		init(&inv_index->bloom_inv_ind[i]);

		// printf("%s -- %d\n",inv_index->vocab[i],g_hash_table_lookup(inv_index->vocab_hash, inv_index->vocab[i]));

	}
	fclose(reader);
	printf("Vocabulary reading compeleted!!\n");

}

void read_docDB(const char * docDB_file, invIndex* inv_index)
{
	printf("reading docDB!!\n");
	FILE *reader;
	reader = fopen(docDB_file,"r");
	int num_docs;
	fscanf(reader,"%d\n",&num_docs);
	inv_index->num_docs = num_docs;
	int i;
	for(i = 0; i < num_docs ;i++)
	{
		int doc_len;
		fscanf(reader,"%d ",&doc_len);

		int j;
		for( j = 0 ; j < doc_len-1 ; j++)
		{
			char *buffer = (char *)malloc(sizeof(char)*1000);
			fscanf(reader,"%s ",buffer);
			int index = g_hash_table_lookup(inv_index->vocab_hash,buffer) - 1;
			if(index<0)
			{
				printf("word %s not found in vocabulary!!\n",buffer );
				exit(0);
			}
			else
				insert(i,&inv_index->bloom_inv_ind[index]);

			// printf("%s-%d\n",buffer,index, buffer));
		}

		char *buffer = (char *)malloc(sizeof(char)*1000);
		fscanf(reader,"%s\n",buffer);
		int index = g_hash_table_lookup(inv_index->vocab_hash,buffer) - 1;
		if(index<0)
			printf("word %s not found in vocabulary!!\n",buffer );
		else
			insert(i,&inv_index->bloom_inv_ind[index]);

		// printf("%s-%d\n",buffer,index, buffer));
	}
	printf("Docs reading compeleted!!\n");
}

int* query_intersect( char ** query_terms, int query_len, invIndex* inv_index, int *size_res){

	double start_time = omp_get_wtime();

	bloom * query_docs;
	query_docs = (bloom *)malloc(sizeof(bloom *));
	init(query_docs);
	int start = 0;

	int i = 0;
	for( i = 0 ; i < query_len ; i++ ){
		
		int index = g_hash_table_lookup( inv_index->vocab_hash,query_terms[i] ) - 1;
		if( index >= 0 ){
			if(start == 0)
			{
				union_bloom(&inv_index->bloom_inv_ind[index], query_docs, query_docs);	//first needs to be taken as it is
				start = 1;
			}
			else
				intersect_bloom(&inv_index->bloom_inv_ind[index], query_docs, query_docs);
			// printf("okey %d %d \n",1,is_in(i,query_docs) );
		}
	}

	int * doc_ids = (int *)malloc(sizeof(int)*inv_index->num_docs);
	int count = 0;

	for( i = 0 ; i < inv_index->num_docs ; i++ ){
		if(is_in(i,query_docs)){
			// printf("okey %d %d \n",i,is_in(i,query_docs) );
			doc_ids[count] = i;
			// printf("ok %d\n",i);
			count++;
		}
	}
	doc_ids[count] = -2;
	*size_res = count;
	printf("Time for query_intersect:: %f\n",omp_get_wtime()-start_time);

	return doc_ids;
}

int* query_basic(char **query_terms, int query_len, invIndex* inv_index, int *size_res){

	double start_time = omp_get_wtime();

	int * doc_ids = (int *)malloc(sizeof(int)*inv_index->num_docs);
	int count,i = 0;
	for( i = 0 ; i < inv_index->num_docs ; i++ ){
		int check = 1;
		int j = 0;

		for( j = 0 ; j < query_len ; j++ ){
			int index = g_hash_table_lookup( inv_index->vocab_hash,query_terms[j] ) - 1;
			
			if(index < 0){
				printf("not found\n");
				check = 0;
				break;
			}
			if(!is_in(i,&inv_index->bloom_inv_ind[index])){
				check = 0;
				break;
			}
		}
		if(check == 1){
			doc_ids[count] = i;
			count++;
		}
	}
	doc_ids[count] = -2;
	*size_res = count;

	printf("Time for query_basic:: %f\n",omp_get_wtime()-start_time);

	return doc_ids;
}

void inv_ind_testsuite(invIndex* inv_index, int query_size){

	char **query_terms = (char **)malloc(sizeof(char *)*query_size);
	int i = 0;
	for( i = 0 ; i < query_size ; i++ ){
		int rand_index = rand()%inv_index->vocab_size;
		query_terms[i] = (char *)malloc(sizeof(char)*1000);
		sprintf(query_terms[i],"%s",inv_index->vocab[rand_index]);

		// printf("querying -- %s %d\n",query_terms[i],rand_index );

	}

	int size_1,size_2;
	printf("querying via intersection based method!!\n");
	int *docs1 = query_intersect(query_terms, query_size, inv_index, &size_1);
	printf("completed use of intersection based method!!\n");

	printf("querying via simple method!!\n");
	int *docs2 = query_basic(query_terms, query_size, inv_index, &size_2);
	printf("completed use of simple method!!\n");

	for( i = 0 ; i < inv_index->num_docs ; i++)
	{
		if(i < size_1)
			printf("%d :: query_intersect :: %d",i,docs1[i]);
		if(i < size_2)
			printf("--- query_basic :: %d",docs2[i]);
		printf("-\n");
	}
}



int main(int argc, char** argv) {
	srand(time(0));
	init_bloomParameters(1024,3,1);
	seiveInitial();
	invIndex inv_index;
	read_vocab("vocabulary.txt",&inv_index);

	read_docDB("docDB.txt",&inv_index);

	inv_ind_testsuite(&inv_index,100);

	return 0;

}


