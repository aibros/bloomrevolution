#include "count_bloom_reconstruct_mp.h"

pthread_cond_t c_overflow = PTHREAD_COND_INITIALIZER;

struct bloomNode *extractTask()
{
	pthread_mutex_lock(&m_taskQueue);
	if (rear==front) 
	{
		pthread_mutex_unlock(&m_taskQueue);
		return NULL;
	}
	struct bloomNode *t = taskQueue[front];
	front = (front+1)%QSIZE;
	pthread_mutex_unlock(&m_taskQueue);
	pthread_cond_broadcast(&c_overflow);
	return t;
}

void addTask(struct bloomNode *t)
{
	pthread_mutex_lock(&m_taskQueue);
	if ((rear+1)%QSIZE == front)
	{
		pthread_cond_wait(&c_overflow,&m_taskQueue);
		taskQueue[rear] = t;
		rear = (rear+1)%QSIZE;
		pthread_mutex_unlock(&m_taskQueue);
	}
	else if (rear==front) //empty queue
	{	
		rear = 0;
		front = 0;
		taskQueue[rear] = t;
		rear = (rear+1)%QSIZE;
		pthread_mutex_unlock(&m_taskQueue);
	}
	else
	{
		taskQueue[rear] = t;
		rear = (rear+1)%QSIZE;
		pthread_mutex_unlock(&m_taskQueue);
	}
}


void searchLeaf(struct bloomNode *t)
{
	ULONG i = 0;
	ULONG localCount = 0;
	ULONG leafID = t->leafID;
	// ULONG localMembership = 0;

	for (i=t->start;i<=t->end;i++)
	{
		// localMembership++;
		if (is_in(i,&query))
		{
			arrayLong_addElement(output[leafID],i);
			localCount++;
		}
	}
	printf("searching leaf %d %d\n",t->start,t->end);
	size_output[leafID] = localCount;
	// omp_set_lock(&m_numElements);
	// numElements += localCount;
	// omp_unset_lock(&m_numElements);

	// omp_set_lock(&m_numMembership);
	// numMembership += localMembership;
	// omp_unset_lock(&m_numMembership);
}

void descendLevels(struct bloomNode *root, UINT numLevels, UINT currLevel, double estimate)
{
	if (currLevel == numLevels)
	{
		root->prevEstimate = estimate;
		addTask(root);
	}
	else if ((root->lchild==NULL) && (root->rchild==NULL))
	{
		root->prevEstimate = estimate;
		addTask(root);
	}
	else
	{
		descendLevels(root->lchild,numLevels,currLevel+1,estimate);
		descendLevels(root->rchild,numLevels,currLevel+1,estimate);
	}
}


void *executeTask(void *x)
{
	while (1)
	{
		struct bloomNode *t = extractTask();
		if (t == NULL)
		{
			omp_set_lock(&m_totalRange);
			ULONG tR = totalRange;
			omp_unset_lock(&m_totalRange);
			if (tR>=nVertices)
				break;
		}
		else
		{
			if (t->level >= levelThreshold)
			{
				searchLeaf(t);

				omp_set_lock(&m_totalRange);
				totalRange += (t->end - t->start + 1);
				omp_unset_lock(&m_totalRange);

				continue;
			}

			printf("level at which stuck -- %d  %d\n",t->level,levelThreshold );
			double estimate = elemsIntersection(&(t->lchild->filter),&query,t->nOnes,k_ones_query);
			int flag = (estimate>overlapThreshold)?1:0;
			if (flag>0)
			{
				double ratio = estimate/t->prevEstimate;											//prevEstimate tells how much was found at parent and then check what fraction is there in the children as compared to their parent
				int numLevels;

				if (ratio <= 0.25) numLevels = 1;
				else if (ratio <= 0.5) numLevels = 2;
				else if (ratio <= 0.75) numLevels = 3;
				else numLevels = 4;

				descendLevels(t,numLevels,0,estimate);
			}
			else
			{
				omp_set_lock(&m_totalRange);
                totalRange += (t->end - t->start + 1);
                omp_unset_lock(&m_totalRange);
			}
		}
	}
}



/* Assuming bloomSampleTree is initialised with range being O-M
*  Need
*  nNodes : Number of nodes in query
*/
void reconstructSet(UINT nNodes, struct bloomTree* a, bloom* arg_query , my_array_long** return_array, UINT* return_array_size)
{

	k_ones_query = num_ones(arg_query);
	query = *arg_query;
	int rc;
	(a->left)->prevEstimate = 1.0*nNodes;
	(a->right)->prevEstimate = 1.0*nNodes;

	printf("%d %d %d\n",a->left->start,a->left->end,a->left->filter );
	printf("%d %d %d\n",a->right->start,a->right->end, a->right->filter );

	addTask(a->left);
	addTask(a->right);

	//initialising numLeaves and output arrays corresponding to each leaves (may try to do per thread)

	ULONG M = nVertices;
	numLeaves = 1;
	while (M>THRESH)
	{
		M /= 2;
		numLeaves *= 2;
	}
	output 		= (my_array_long**)malloc(sizeof(my_array_long*)*numLeaves);
	size_output = (UINT*)malloc(sizeof(UINT)*numLeaves);

	ULONG i;
	for (i=0;i<numLeaves;i++)
	{
		output[i] 		= (my_array_long*)malloc(sizeof(my_array_long)*THRESH);
		arrayLong_init(output[i]);
		size_output[i] 	=  0;
	}

	// omp_init_lock(&m_numElements);
	pthread_mutex_init(&m_taskQueue, NULL);
	omp_init_lock(&m_totalRange);
	// omp_init_lock(&m_numIntersections);
	// omp_init_lock(&m_numMembership);

	// the parallel region uniformly executes the executeTask
	// and spawns the number of threads as mentioned but in this 
	// case setting it to default or no. of cores

	/// omp_set_num_threads(NUM_THREADS);
	int num_threads = omp_get_num_threads();
	double start = omp_get_wtime();
	#pragma omp parallel
	{
		int a = omp_get_thread_num();
		executeTask(a);
	}
	double elapsed = omp_get_wtime()-start;

	// printf("RECONSTRUCTED SET  WITH PRECISION %lf IN TIME %lf sec, INTERSECTIONS=%d,MEMBERSHIP=%d\n",(1.0*nNodes)/numElements,elapsed,numIntersections,numMembership-1);
		

	// omp_destroy_lock(&m_numElements);
	// omp_destroy_lock(&m_taskQueue);
	omp_destroy_lock(&m_totalRange);
	// omp_destroy_lock(&m_numIntersections);
	// omp_destroy_lock(&m_numMembership);
	printf("done\n");
	// Now combine output of each leaf node into a single array of output values 
	my_array_long* final_array;
	final_array = (my_array_long*)malloc(sizeof(my_array_long));
	arrayLong_init(final_array);
	UINT j;
	UINT ele_ctr = 0;
	for (i = 0; i < numLeaves; ++i)
	{
		for (j = 0; j < size_output[i]; ++j)
		{
			printf("%d\n",output[i]->elements[j] );
			arrayLong_addElement(final_array,output[i]->elements[j]);
			ele_ctr += 1;
		}
	}
	*return_array = final_array;
	*return_array_size = ele_ctr;

	//freeing the memory
	for (i = 0; i < numLeaves; ++i)
		free(output[i]->elements);
	free(output);
	free(size_output);
}



void init_reconstruct(UINT arg_levelThreshold, UINT  arg_overlapThreshold , ULONG arg_nVertices)
{
	levelThreshold 		= arg_levelThreshold;
	overlapThreshold 	= arg_overlapThreshold;
	nVertices 			= arg_nVertices;
}
