#include "count_bloom_sample.h"

UINT  THRESH = 200;
ULONG leafID = 0;
UINT  levelThreshold_sample = 6;
UINT nMembership = 0;  // Not used in our code

struct bloomTree *getBloomTree(ULONG startVertex, ULONG endVertex)
{
	ULONG mid = (endVertex + startVertex)/2;
	struct bloomTree *bT = (struct bloomTree*)malloc(sizeof(struct bloomTree));
	bT->left 	= createBloomTree(startVertex,mid,1);
	bT->right 	= createBloomTree(mid+1,endVertex,1);
	return bT;
}

struct bloomNode *createBloomTree(ULONG startVertex, ULONG endVertex, UINT level)
{
	struct bloomNode *r = (struct bloomNode*)malloc(sizeof(struct bloomNode));
	r->level = level;
	r->start = startVertex;								
	r->end = endVertex;
	r->lchild = NULL;
	r->rchild = NULL;
	if (level <= levelThreshold_sample)
	{
		//redundant as done in init// r->filter.flag_array = (int*)malloc(sizeof(int)*(SIZE_BLOOM/NUM_BITS + 1));
		init(&r->filter);
		r->trueValue = 0;	//TO BE DELETED
		// ULONG i;
		/*not Inserting actual values in the bloom filter just setting up for the insertion of edges*/
		// for (i=startVertex;i<=endVertex;i++){
		// 	insert(i,&r->filter);
		// }
		r->nOnes = 0;///num_ones(&(r->filter));
		// setSignature(&(r->filter));
	}
	if (level < levelThreshold_sample)
	{
		ULONG mid = ((endVertex + startVertex)/2);
		r->lchild = createBloomTree(startVertex,mid,level+1);
		r->rchild = createBloomTree(mid+1,endVertex,level+1);
	}
	else
	{
		r->leafID = leafID;
		leafID++;
	}
	return r;
}

void insert_bloom_tree( struct bloomTree *tree, ULONG elem)
{
	struct  bloomNode * templ = tree->left;
	struct  bloomNode * tempr = tree->right;

	if(elem <= tempr->end && elem >= tempr->start)
	{
		while(tempr!=NULL)
		{
			// printf("right\n");
			if(!is_in(elem,&tempr->filter))
				insert(elem,&tempr->filter);
			if(tempr->lchild == NULL && tempr->rchild==NULL)
			{
				tempr = NULL;
				break;
			}
			if( elem <= tempr->lchild->end && elem >= tempr->lchild->start)
				tempr = tempr->lchild;
			else 
				tempr = tempr->rchild;
		}
	}
	else if(elem <= templ->end && elem >= templ->start)
	{
		while(templ!=NULL)
		{
			// printf("left\n");
			if(!is_in(elem,&tempr->filter))
				insert(elem,&templ->filter);
			if(templ->lchild == NULL && templ->rchild==NULL)
			{
				templ = NULL;
				break;
			}
			if(elem <= templ->lchild->end && elem >= templ->lchild->start)
				templ = templ->lchild;
			else
				templ = templ->rchild;
		}
	}

	// printf("insertion complete -- %d \n",elem );
}

void update_nones(struct bloomNode *tree)
{
	tree->nOnes = num_ones(&(tree->filter));
	struct  bloomNode * templ = tree->lchild;
	struct  bloomNode * tempr = tree->rchild;
	if(templ!=NULL)
		update_nones(templ);
	if(tempr!=NULL)
		update_nones(tempr);
}

/*Count the number of ones in the intersection of r and k*/
ULONG intersectNode(struct bloomNode *r,bloom *k)
{
	ULONG flag = 0;
	flag = numOnes_intersection(&r->filter,k);
	return flag;
}

//issue both left and right containing all elements
long sampleFromTree(bloom *k, struct bloomTree *a)
{
	//// T_sample = thresh;	//present in init_main
	//// levelThreshold_sample = levelThresh;
	//no recursive call so assigning here only
	// bloom * k = (bloom *)malloc(sizeof(bloom));
	// init(k);
	// insert(30,k);
	// insert(31,k);
	// insert(32,k);
	// insert(600,k);
	// enumerate_all(k,a);
	// exit(0);
	
	// printf("sampleFromTree()\t:: left-start::%d  left-end::%d  \n",a->left->start,a->left->end);
	// printf("sampleFromTree()\t:: right-start::%d right-end::%d \n",a->right->start,a->right->end);
	ULONG k1 =  num_ones(k);
	ULONG elems = count_elems(k);
	// printf("sampleFromTree()\t:: Number of elems in query::%d\t%d \n",elems,k1);

	// refer the reference in paper[21] to understand

	//
	// int i , j = 0;
	// for(i=0;i< (SIZE_BLOOM/NUM_BITS);i++)
	// {
	// 	int temp,temp2 = 0;
	// 	for( j = 0 ; j < NUM_BITS/COUNTER_CHUNK ; j++)
	// 	{
	// 		int new_map = BIT_MAP<<(j*COUNTER_CHUNK);
	// 		temp = a->left->filter.flag_array[i]&new_map;
	// 		if(!temp)
	// 			printf("okay somethings not there1 - %d\n",i);
	// 		temp2 = a->right->filter.flag_array[i]&new_map;
	// 		if(!temp2)
	// 			printf("okay somethings not there2 - %d\n",i);
	// 	}
	// }
	//
	// printf("%d ::l:%d:r:%d-- %d ::l:%d:r:%d::\n",numOnes_intersection(&(a->left->filter),k),a->left->start,a->left->end,numOnes_intersection(&(a->right->filter),k),a->right->start,a->right->end );
	// exit(0);

	double lElems = elemsIntersection(&(a->left->filter),k,a->left->nOnes,k1);
	double rElems = elemsIntersection(&(a->right->filter),k,a->right->nOnes,k1);
	// printf("sampleFromTree()\t:: lElems::%f rElems::%f\n",lElems,rElems);
	int flagL = (lElems > 0.005)?1:0;	//since if greater than zero then proceed
	int flagR = (rElems > 0.005)?1:0;

	// printf("sampleFromTree()\t:: flagL::%d flagR::%d\n",flagL,flagR );
	if ((flagL)&&(!flagR))
	{
		ULONG sample = sampleValue(k,a->left,k1);
		if (sample < 0)
		{
			printf("sampleFromTree()::\tReturning -1 as lSample is negative(No element in right)\n");
			printf("%d\n",1/0);
		}
		return sample;
	} 
	if ((!flagL)&&(flagR))
	{
		ULONG sample = sampleValue(k,a->right,k1);
		if (sample < 0)
		{
			printf("sampleFromTree()::\tReturning -1 as lSample is negative(No element in right)\n");
			printf("%d\n",1/0);
		}
		return sample;	
	} 
	if ((!flagL)&&(!flagR))
	{
		printf("sampleFromTree()::\t Both flags are false\n");
		printf("%d\n",1/0);
		return -1;
	}

	// printf("sampleFromTree()\t:: SampleLeft:%d SampleRight%d\n",sampleValue(k,a->left,k1),sampleValue(k,a->right,k1) );
	// srand(time(NULL));
	// if both sides have members in the set then randomly choose based on size
	double z = ((double)rand())/((double)RAND_MAX)*(lElems+rElems);
	// printf("sampleFromTree()\t:: Z::%f\n",z);
	if (z<lElems)
	{	
		// printf("sampleFromTree()\t:: Sampling from left subtree\n");
		long lSample = sampleValue(k,a->left,k1);
		if (lSample<0) 
		{
			// printf("sampleFromTree()\t:: Sampling from left failed, going to right\n");
			long sample = sampleValue(k,a->right,k1);
			if (sample < 0)
			{
				printf("sampleFromTree()::\tReturning -1 as lSample and rSample are negative\n");
				printf("%d\n",1/0);
			}
			return sample;
		}
		// printf("sampleFromTree()\t:: Sample::%d\n",lSample);
		return lSample;
	}
	else
	{
		// printf("sampleFromTree()\t:: Sampling from right subtree\n");
		long rSample = sampleValue(k,a->right,k1);
		if (rSample<0) 
		{
			// printf("sampleFromTree()\t:: Sampling from right failed, going to left\n");
			long sample = sampleValue(k,a->left,k1);
			if (sample < 0)
			{
				printf("sampleFromTree()::\tReturning -1 as rSample and lSample are negative\n");
				printf("%d\n",1/0);
			}
			return sample;
		}
		// printf("sampleFromTree()\t:: Sample::%d\n",rSample);
		return rSample;
	}
	printf("sampleFromTree()::\tReturning -1\n");
	printf("%d\n",1/0);
	return -1;
}


long sampleValue(bloom *k, struct bloomNode *r, ULONG k1)
{
	if ((!r->lchild)&&(!r->rchild))
	{
	//if (r->level >= levelThreshold_sample){
		ULONG i=0,j;
		ULONG numValues = r->end - r->start + 1;
		// printf("sampleValue()\t\t:: No child for this node::numValues::%d\n",numValues);
		ULONG *values = (ULONG*)malloc(sizeof(ULONG)*numValues);
		for (j=r->start;j<=r->end;j++)
		{
			nMembership++;
			if (is_in(j,k))
			{
				// printf("sampleValue()\t\t\t\t:: Found in leaf::%d\n",j);
				values[i] = j;
				i++;
			}
		}
		if (i==0)
		{
			// printf("sampleValue()\t\t:: NO element present::\n");	
			// printf("%d\n",1/0);	
			free(values);
			return -1;
		}
		// srand(time(NULL));
		ULONG index = rand()%i;
		// printf("Index::%u\n",index);
		ULONG z = values[index];
		free(values);
		// printf("sampleValue()\t\t:: Number of elements present::%d::%d\n",index,i);	
		return z;
	}
	else
	{
	/*		int flagL = intersectNode(r->lchild,k);
			int flagR = intersectNode(r->rchild,k);
			double lElems = -1.0*log(1 - ((double) flagL)/SIZE_BLOOM);
			double rElems = -1.0*log(1 - ((double) flagR)/SIZE_BLOOM);
	*/
		double lElems = elemsIntersection(&(r->lchild->filter), k, r->lchild->nOnes ,k1);
		double rElems = elemsIntersection(&(r->rchild->filter), k, r->rchild->nOnes, k1);
		// printf("sampleValue()\t\t:: lElems = %lf, rElems = %lf, lTrue = %d, rTrue = %d\n",lElems,rElems,r->lchild->trueValue,r->rchild->trueValue);
		//int flagL = intersectNode(r->lchild,k);
		//int flagR = intersectNode(r->rchild,k);

		// int flagL = (lElems > T_sample)?1:0;
		// int flagR = (rElems > T_sample)?1:0;
		int flagL = (lElems > 0.0001)?1:0;
		int flagR = (rElems > 0.0001)?1:0;

		if ((flagL)&&(!flagR))
		{
			long sample =  sampleValue(k,r->lchild,k1);
			if (sample < 0)
			{
				// printf("sampleValue()::\tReturning -1 as lSample is negative(No element in right)\n");
				// printf("%d\n",1/0);
			}
			return sample;
		}
		if ((!flagL)&&(flagR)) 
		{
			long sample =  sampleValue(k,r->rchild,k1);
			if (sample < 0)
			{
				// printf("sampleValue()::\tReturning -1 as lSample is negative(No element in right)\n");
				// printf("%d\n",1/0);
			}
			return sample;
		}
		if ((!flagL)&&(!flagR)) 
		{
			// printf("sampleValue()::\t Both flags are false\n");
			// printf("%d\n",1/0);
			return -1;
		}
		// srand(time(NULL));
		double z = ((double)rand())/((double)RAND_MAX)*(lElems+rElems);
		if (z < lElems)
		{
			long lSample = sampleValue(k,r->lchild,k1);
			if (lSample < 0)
			{
				long sample = sampleValue(k,r->rchild,k1);
				if (sample < 0)
				{
					// printf("sampleValue()::\tReturning -1 as lSample and rSample are negative\n");
					// printf("%d\n",1/0);
				}
				return sample;
			}
			return lSample;
		}
		else
		{
			long rSample = sampleValue(k,r->rchild,k1);
			if (rSample < 0)
			{
				long sample = sampleValue(k,r->lchild,k1);
				if (sample < 0)
				{
					// printf("sampleValue()::\tReturning -1 as rSample and lSample are negative\n");
					// printf("%d\n",1/0);
				}
				return sample;
			} 
			return rSample;
		}
	}
	// printf("sampleValue()::\tReturning -1\n");
	// printf("%d\n",1/0);	
	return -1;
}

ULONG enumerate_all_fresh(bloom* k, ULONG start, ULONG end)
{
	ULONG ctr = 0;
	ULONG j = 0;
	for (j=start;j<=end;j++){
		if (is_in(j,k)){
			// int n1,n2;
			// invert_pairing(j,&n1,&n2);
			// printf("sampleValue()\t\t\t\t:: Found in leaf left::%d::%d,%d\n",j,n1,n2);
			ctr += 1;
		}
	}
	return ctr;
}

ULONG enumerate_all(bloom* k,struct bloomTree*a)
{
	ULONG ctr = 0;
	ULONG j = 0;
	for (j=a->left->start;j<=a->left->end;j++){
		if (is_in(j,k)){
			// int n1,n2;
			// invertPairing(j,&n1,&n2);
			// printf("enumerate\t\t\t\t:: Found in leaf left::%d::%d,%d\n",j,n1,n2);
			ctr += 1;
		}
	}
	j = 0;
	for (j=a->right->start;j<=a->right->end;j++){
		if (is_in(j,k)){
			// int n1,n2;
			// invertPairing(j,&n1,&n2);
			// printf("enumerate\t\t\t\t:: Found in leaf right::%d::%d,%d\n",j,n1,n2);
			ctr += 1;
		}
	}
	return ctr;
}

ULONG enumerate_all_vertices(bloom* k,struct bloomTree*a)
{
	ULONG ctr = 0;
	ULONG j = 0;
	for (j=a->left->start;j<=a->left->end;j++)
	{
		if (is_in(j,k))
		{
			ctr += 1;
		}
	}
	j = 0;
	for (j=a->right->start;j<=a->right->end;j++)
	{
		if (is_in(j,k))
		{
			ctr += 1;
		}
	}
	return ctr;
}


// Not used anywhere
// void traverseTree(bloom *k, struct bloomNode *r)
// {
// 	ULONG flag = intersectNode(r,k);
// 	printf("(%lu,%lu,%lu)\n",r->start,r->end,flag);
// 	if (r->lchild)
// 		traverseTree(k,r->lchild);
// 	if (r->rchild)
// 		traverseTree(k,r->rchild);
// }

// NOT USED
// UINT belongsInArr1(UINT val, UINT *arr, UINT size)
// {
// 	UINT j;
// 	for (j=0;j<size;j++)
// 		if (arr[j]==val)
// 			return j;
// 	return -1;
// }

// NOT USED
// //this function takes in a value asorted_datand updates the count of each node in tree that has this val in its range
// void updateTrueValue(struct bloomNode *a, ULONG value)
// {
// 	if (a==NULL) 
// 		return;
// 	if ((a->start<=value)&&(a->end>=value)) 
// 		a->trueValue++;

// 	updateTrueValue(a->lchild,value);
// 	updateTrueValue(a->rchild,value);
// }

// NOT USED
// void updateFlags(struct bloomNode *a, bloom *q)
// {
// 	if (a==NULL) 
// 		return;

// 	a->flag = intersectNode(a,q);
// 	updateFlags(a->lchild,q);
// 	updateFlags(a->rchild,q);
// }
