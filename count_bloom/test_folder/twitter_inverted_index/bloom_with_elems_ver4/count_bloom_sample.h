#ifndef _BLOOM_TREE_SAMPLE_H
#define _BLOOM_TREE_SAMPLE_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>

#include "../../../count_bloom.h"

extern UINT THRESH;
extern ULONG leafID;
extern UINT levelThreshold_sample;
extern UINT nMembership;  // Not used in our code, used in original bloom sample


struct bloomNode{
	bloom filter;
	ULONG start;		/*starting vertex for this node*/
	ULONG end;		/*ending vertex for this node*/
	struct bloomNode *lchild;
	struct bloomNode *rchild;
	ULONG trueValue;	//TO BE DELETED
	int flag;
	ULONG leafID;
    ULONG sample;
    double prevEstimate;
	ULONG nOnes;
	ULONG level;
};


struct bloomTree{		/*The root level*/
	struct bloomNode* left;
	struct bloomNode* right;
};

/*
* INPUT : Range of elements that the bloomTree will contain
* WORK	: Creates an empty bloomTree , with as many levels as required for the given range
* OUTPUT: Returns the empty BloomTree
*/
struct bloomTree *getBloomTree(ULONG startVertex, ULONG endVertex);

/*
*  NOTE: modifying to take in data not as contiguous range but in an online manner , where the cantor 
*  functions of the edges will be calculated and the edges will be inserted one by one to enable 
*  scaling and at the same adapt to dynamic situations.
*/
struct bloomNode *createBloomTree(ULONG startVertex, ULONG endVertex, UINT level);

/*
* INPUT : edgeFileName, start and end range of BloomTree
* WORK	: Reads the edge file , creates an empty bloomTree,then inserts the edges one by one in the tree
* OUTPUT: Returns the update BloomTree
*/
struct bloomTree* setupTree(char* edge_file_name, ULONG start , ULONG end);

/*
* INPUT : bloomtree,element
* WORK	: Inserting an element in the bloom tree
* OUTPUT: None
*/
void insert_bloom_tree( struct bloomTree *tree, ULONG elem);

/*
* INPUT : bloomNode
* WORK	: updating the nOnes of whole subtree
* OUTPUT: 
* NOTE  : make sure to call it after inserting all the edges or any set of new edges to update 
* the nones of each node
*/
void update_nones(struct bloomNode *node);


/*
INPUT 	: Node of bloomTree and a bloomFilter
OUTPUT	: Count the number of ones in the intersection of r and k 
*/
ULONG intersectNode(struct bloomNode *r,bloom *k);


/*
* INPUT : bloom filter to sample from, bloom tree (already setup)
* WORK	: samples from tree and returns the sample
* OUTPUT: sample obtained
* NOTE 	: the function wraps up the assignment of specific global values.
*/
long sampleFromTree(bloom *k, struct bloomTree *a);

/*
* INPUT : bloom filter to sample from and bloomNode, and numOnes in query bloomFilter 
*			numOnes_K means sum of counters at all positions in K
* WORK	: Returns a sample from this node or -1 if sampling fails
* OUTPUT: sample obtained
*/
long sampleValue(bloom *k, struct bloomNode *r, ULONG numOnes_K);

/*
INPUT	: BloomFilter and BloomTree
WORK	: Enumerate all elements present in BloomFitler using range specified using BloomTree
OUTPUT	: Number of elements enumerated
NOTE	: It assumes elements present in bloomFilter are edges and hence can attempt to invert them
		to get vertices
*/
ULONG enumerate_all(bloom* k,struct bloomTree *a);

/*
INPUT	: BloomFilter and BloomTree
WORK	: Enumerate all elements present in BloomFitler betwenn start and end
OUTPUT	: Number of elements enumerated
*/
ULONG enumerate_all_fresh(bloom* k, ULONG start, ULONG end);

/*
INPUT	: BloomFilter and BloomTree
WORK	: Enumerate all elements present in BloomFitler using range specified using BloomTree
OUTPUT	: Number of elements enumerated
NOTE	; unlike enumerate_all() , it does not attempt to invert enumerated element
		So this is used to enumerate elements of bloomFilters which contain vertices and not edges
*/
ULONG enumerate_all_vertices(bloom* k,struct bloomTree*a);

#endif