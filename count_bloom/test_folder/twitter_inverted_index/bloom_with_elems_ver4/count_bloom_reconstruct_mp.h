#ifndef BLOOM_RECONSTRUCT_H
#define BLOOM_RECONSTRUCT_H

#include <stdio.h>
#include "../../../count_bloom.h"
#include "dynamic_array/array.h"
#include "count_bloom_sample.h"				
#include <pthread.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>


#define NUMTHREADS 48
#define QSIZE 100000

// int K;
// int SIZE_BLOOM;
/*Add conditions on queue(for correctness), and on mutexes (for performance)*/

UINT levelThreshold;
UINT overlapThreshold;

// UINT numElements = 0;
// omp_lock_t m_numElements;

ULONG totalRange = 0;
omp_lock_t m_totalRange;

// UINT numIntersections = 0;
// omp_lock_t m_numIntersections;

// UINT numMembership = 0;
// omp_lock_t m_numMembership;

UINT k_ones_query;	//Number of 1s in the query bloom filter

FILE *foutput;				
omp_lock_t m_output;

my_array_long **output;
ULONG numLeaves;
UINT *size_output;


ULONG nVertices;

bloom query;

struct task{
	struct bloomNode *root;
	struct task *next;
};

struct bloomNode *taskQueue[QSIZE];
pthread_mutex_t m_taskQueue;


int rear = -1;	//rear == front indicates empty queue
int front = -1;

// pthread_t ids[NUMTHREADS];

struct bloomNode* extractTask();

void searchLeaf(struct bloomNode *t);

void addTask(struct bloomNode *t);

void descendLevels(struct bloomNode *root, UINT numLevels, UINT currLevel, double estimate);

void *executeTask(void *x);

void reconstructSet(UINT nNodes,struct bloomTree* a, bloom *arg_query , my_array_long** return_array, UINT* return_array_size);

void init_reconstruct(UINT arg_levelThreshold, UINT arg_overlapThreshold , ULONG arg_nVertices);

#endif