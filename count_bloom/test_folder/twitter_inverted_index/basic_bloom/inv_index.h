#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include "../../../count_bloom.h"
#include <omp.h>

typedef long int LONG;

int query_max_len;

typedef struct invIndex
{
	GHashTable* vocab_hash;    // hashtag to index hash table 
	bloom* bloom_inv_ind;      // bloom filters storing uids for each hashtag
	LONG vocab_size;
	LONG uid_min;
	LONG uid_max;
}invIndex;



void read_data(const char * data_file, invIndex* inv_index);

void query_intersect( char ** query_terms, int query_len, invIndex* inv_index, int *size_res, const char* output_file);

void query_basic(char **query_terms, int query_len, invIndex* inv_index, int *size_res, const char* output_file);

void test_suite(invIndex *inv_index, const char *query_file);

