#ifndef ARRAY_H
#define ARRAY_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "../temp/conf.h"
#include "../temp/bitpack.h"
#include "../temp/vint.h"		
#include "../temp/bitutil.h"
#include "../temp/vp4.h"

#include "../LittleIntPacker/include/bitpacking.h"
#include "../MaskedVByte/include/varintdecode.h"
#include "../MaskedVByte/include/varintencode.h"

typedef uint32_t LONG;

typedef struct my_array
{
	unsigned int* elements;
	unsigned int curr_size;
	unsigned int max_size;
}my_array;

typedef struct my_array_long
{
	LONG* elements;
	unsigned int curr_size;
	unsigned int max_size;
}my_array_long;

typedef struct comp_array_long
{
	unsigned char *comp_elements;
	unsigned int size;
}comp_array_long;

static __m128i shuffle_mask[16]; // precomputed dictionary

int cmpfunc (const void *a, const void *b);

void prepare_shuffling_dictionary();

void arrayInt_init(my_array* array);
void arrayInt_addElement(my_array* array,unsigned int element);
void arrayInt_free(my_array* array);
int arrayInt_find(my_array* array,unsigned int element);
void print_elems(my_array* array);

void arrayLong_init(my_array_long* array);
void arrayLong_addElement(my_array_long* array,LONG element);
void arrayLong_free(my_array_long* array);
int arrayLong_find(my_array_long* array,LONG element);
void arrayLong_extend(my_array_long *array, int size);

int compress_elems_long(my_array_long* array, comp_array_long* c_array);
int decompress_elems_long(comp_array_long *c_array, my_array_long *array);

#endif