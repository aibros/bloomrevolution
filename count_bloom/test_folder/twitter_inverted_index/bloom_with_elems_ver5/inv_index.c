#include "inv_index.h"



void read_data(const char * data_file, invIndex* inv_index)
{
	double start_time = omp_get_wtime();

	printf("reading data file : %s !!\n",data_file);
	inv_index->vocab_hash = g_hash_table_new(g_str_hash, g_str_equal);								//string to int dictionary


	FILE *reader;
	reader = fopen(data_file,"r");

	uint32_t vocab_len;
	fscanf(reader,"%u\n",&vocab_len);

	// inv_index->bloom_inv_ind = (bloom *)malloc(vocab_len*sizeof(bloom));
	inv_index->vocab_size = vocab_len;

	my_array_long **temp_store = (my_array_long **)malloc(vocab_len*sizeof(my_array_long *));

	
	int i;
	for(i = 0 ; i < vocab_len ; i++){
		// init(&inv_index->bloom_inv_ind[i]);
		temp_store[i] = (my_array_long *)malloc(sizeof(my_array_long));
		arrayLong_init(temp_store[i]);
	}

	LONG_INT counter = 0;
	while(1)
	{
		char *hash_tag = (char *)malloc(sizeof(char)*256);
		LONG_INT time_stamp = 0;
		LONG_INT uid = 0;
		int ret;
		if(sizeof(LONG_INT)==8)
			ret = fscanf(reader, "%s\t%ld\t%ld\n", hash_tag, &time_stamp, &uid);
		else if(sizeof(LONG_INT)==4)
			ret = fscanf(reader, "%s\t%u\t%u\n", hash_tag, &time_stamp, &uid);

		if(ret < 1)																// fscanf returns the number of objects read
			break;
		// if(uid > 2147483645)
		// 	continue;
		int index = g_hash_table_lookup(inv_index->vocab_hash,hash_tag) - 1;	//lookup returns 0 if not found

		if(index < 0)
		{
			g_hash_table_insert(inv_index->vocab_hash, hash_tag, counter+1);
			index = counter;

			counter++;
			// if(!is_in(uid,&inv_index->bloom_inv_ind[index]))
			// {
				// insert(uid ,&inv_index->bloom_inv_ind[index]);
				arrayLong_addElement(temp_store[index], uid );
			// }
		}
		else if(index >= 0)
		{
			// if(!is_in(uid,&inv_index->bloom_inv_ind[index]))
			// {
				// insert(uid ,&inv_index->bloom_inv_ind[index]);
				arrayLong_addElement(temp_store[index], uid );
			// }
			free(hash_tag);												//don't free in if case as it needs to be stored in hashtable
		}

	}

	inv_index->vocab_current = counter;

	LONG_INT compressed_bytes = 0;
	LONG_INT uncompressed_bytes = 0;

	inv_index->elems = (comp_array_long **)malloc(vocab_len*sizeof(comp_array_long *));

	for( i = 0 ; i < vocab_len ; i++ )
	{

		// print_elems_long(temp_store[i]);
		inv_index->elems[i] = (comp_array_long *)malloc(sizeof(comp_array_long));
		
		// printf("iter -- %d  size -- %d\n",i,temp_store[i]->curr_size );
		LONG_INT c_size = compress_elems_long(temp_store[i],inv_index->elems[i]);
		compressed_bytes += c_size;
		uncompressed_bytes += sizeof(LONG_INT)*temp_store[i]->curr_size;

		arrayLong_free(temp_store[i]);
	}

	free(temp_store);

	printf("uncompressed_bytes - %ld compressed_bytes - %ld\n",uncompressed_bytes,compressed_bytes );

	printf("Time for index creation:: %f\n",omp_get_wtime()-start_time);

	printf("data file read and twitter index created: %s with total number of hashtags : %d %d!!\n", data_file, inv_index->vocab_size,inv_index->vocab_current);
}



void init_updates(invIndex *inv_index, int length)
{
	inv_index->updates = (indUpdate *)malloc(sizeof(indUpdate)*length);
	inv_index->update_len_current = 0;
	inv_index->update_len_max = length;
}



void update_zipper(const char *data_file, invIndex *inv_index)
{
	double start_time = omp_get_wtime();

	LONG_INT vocab_len;

	FILE *reader;
	reader = fopen(data_file,"r");
	fscanf(reader,"%u\n",&vocab_len);
	if(vocab_len != inv_index->vocab_size)
		printf("vocabulary sizes are not matching\n");
	vocab_len = inv_index->vocab_size;

	my_array_long **temp_store = (my_array_long **)malloc(vocab_len*sizeof(my_array_long *));

	int i;
	for(i = 0 ; i < inv_index->vocab_size ; i++){
		temp_store[i] = NULL;
	}

	LONG_INT counter = inv_index->vocab_current;
	
	while(1)
	{
		char *hash_tag = (char *)malloc(sizeof(char)*256);
		LONG_INT time_stamp = 0;
		LONG_INT uid = 0;
		int ret;
		if(sizeof(LONG_INT)==8)
			ret = fscanf(reader, "%s\t%ld\t%ld\n", hash_tag, &time_stamp, &uid);
		else if(sizeof(LONG_INT)==4)
			ret = fscanf(reader, "%s\t%u\t%u\n", hash_tag, &time_stamp, &uid);

		if(ret < 1)																// fscanf returns the number of objects read
			break;

		int index = g_hash_table_lookup(inv_index->vocab_hash,hash_tag) - 1;	//lookup returns 0 if not found

	
		if(index < 0)
		{
			g_hash_table_insert(inv_index->vocab_hash, hash_tag, counter+1);
			index = counter;
			counter++;
			// if(!is_in(uid,&inv_index->bloom_inv_ind[index]))
			// {
				// insert(uid ,&inv_index->bloom_inv_ind[index]);
				if(temp_store[index] == NULL)
				{
					temp_store[index] = (my_array_long *)malloc(sizeof(my_array_long));
					arrayLong_init(temp_store[index]);
					// decompress_elems_long(inv_index->elems[index], temp_store[index]);
				}
				arrayLong_addElement(temp_store[index], uid );
			// }
		}
		else if(index >= 0)
		{
			// if(!is_in(uid,&inv_index->bloom_inv_ind[index]))
			// {
				if(temp_store[index] == NULL)
				{
					temp_store[index] = (my_array_long *)malloc(sizeof(my_array_long));
					decompress_elems_long(inv_index->elems[index], temp_store[index]);
				}
			// printf("%d %s %d %d\n",index,hash_tag,temp_store[index]->curr_size,temp_store[index]->max_size );

				// insert(uid ,&inv_index->bloom_inv_ind[index]);
				arrayLong_addElement(temp_store[index], uid );
			// }
			free(hash_tag);
		}

	}

	inv_index->vocab_current = counter;

	LONG_INT compressed_bytes = 0;
	LONG_INT uncompressed_bytes = 0;

	// int current = inv_index->update_len_current;
	// inv_index->elems = (comp_array_long **)malloc(vocab_len*sizeof(comp_array_long *));

	for( i = 0 ; i < vocab_len ; i++ )
	{
		if(temp_store[i] != NULL)
		{
			free(inv_index->elems[i]);
			inv_index->elems[i] = (comp_array_long *)malloc(sizeof(comp_array_long));

			LONG_INT c_size = compress_elems_long(temp_store[i],inv_index->elems[i]);
			compressed_bytes += c_size;
			uncompressed_bytes += sizeof(LONG_INT)*temp_store[i]->curr_size;

			arrayLong_free(temp_store[i]);
		}
	}

	free(temp_store);

	printf("uncompressed_bytes of elements to be inserted - %ld compressed_bytes of elements to be inserted- %ld\n",uncompressed_bytes,compressed_bytes );

	printf("Time for index creation:: %f\n",omp_get_wtime()-start_time);
}


void query_zipper( char ** query_terms, LONG_INT query_len, invIndex* inv_index, LONG_INT *size_res, const char* output_file )
{
	double start_time = omp_get_wtime();

	LONG_INT start = 0;

	LONG_INT min_sized_ind = 999999999;
	LONG_INT min_size = 999999999;

	LONG_INT i = 0;
	for( i = 0 ; i < query_len ; i++ ){
		LONG_INT index = g_hash_table_lookup( inv_index->vocab_hash,query_terms[i] ) - 1;
		if( index >= 0 ){
			if(inv_index->elems[index]->size < min_size){
				min_size = inv_index->elems[index]->size;
				min_sized_ind = index;
			}
		}
		else
		{
			printf("query term not present, empty output so output file not created: %s\n",query_terms[i] );
			return;
		}
	}

	my_array_long * current = (my_array_long *)malloc(sizeof(my_array_long));
	decompress_elems_long(inv_index->elems[min_sized_ind],current);

	my_array_long * old = (my_array_long *)malloc(sizeof(my_array_long));
	arrayLong_init(old);

	for( i = 0; i < query_len ; i++)
	{

		if(i == min_sized_ind)
			continue;
		LONG_INT index = g_hash_table_lookup( inv_index->vocab_hash,query_terms[i] ) - 1;
		// printf("ind -- %u \n",index );
		my_array_long * temp = (my_array_long *)malloc(sizeof(my_array_long));
		decompress_elems_long(inv_index->elems[index], temp);
		// print_elems_long(temp);

		intersect_array_long(temp, current, old);
		// printf("%d %s -- %d -- %u\n",i,query_terms[i],old->curr_size,old->elements[0] );
		// print_elems_long(old);
		// printf("-------next------------\n");
		my_array_long * swap = old;
		old = current;
		current = swap;

		arrayLong_free(temp);

	}

	FILE* writer = fopen(output_file,"a");
	fprintf(writer, "********\n\n" );

	for( i = 0 ; i < current->curr_size ; i++)
		fprintf(writer,"%ld\n",current->elements[i]);

	fclose(writer);
	arrayLong_free(current);
	arrayLong_free(old);
	printf("Time for query_zipper of size %d:: %f\n" ,current->curr_size ,omp_get_wtime()-start_time);
}

// void query_generator()

void handle_query(invIndex *inv_index, const char *query_file)
{
	printf("starting the queries from file ::%s \n",query_file );

	FILE *query_reader;
	query_reader = fopen(query_file,"r");

	LONG_INT num_queries = 0;
	fscanf(query_reader,"%d\n",&num_queries);

	LONG_INT i,j = 0;
	for( i = 0 ; i < num_queries ; i++ ){

		LONG_INT num_terms = 0;
		fscanf(query_reader,"%d ", &num_terms);

		char **query_terms = (char **)malloc( sizeof(char *)*num_terms );

		for( j = 0 ; j < num_terms-1 ; j++){
			query_terms[j] = (char *)malloc(sizeof(char)*256);
			fscanf(query_reader,"%s ",query_terms[j]);
			// printf("%s\n",query_terms[j] );
		}

		query_terms[j] = (char *)malloc(sizeof(char)*256);
		fscanf(query_reader, "%s\n", query_terms[j]);
		// printf("%s\n",query_terms[j] );

		LONG_INT size1,size2 = 0;
		printf("------------**************\n");
		query_zipper(query_terms, num_terms, inv_index, &size2,"output/query_zipper.txt" );

	}

	fclose(query_reader);
	printf("all the queries are done\n");
	
}

void free_inv_index(invIndex *inv_index)
{
	LONG_INT i;
	for( i = 0 ; i < inv_index->vocab_size ; i++ )
	{
		g_array_free(inv_index->elems[i],TRUE);
	}
}


int main(LONG_INT argc, char** argv) {
	prepare_shuffling_dictionary();

	srand(time(0));
	init_bloomParameters(1024,3,1);
	seiveInitial();
	invIndex inv_index;
	// read_data("../../../../doc_dataset/sample_complete",&inv_index);

	// my_array_long * current = (my_array_long *)malloc(sizeof(my_array_long));
	// decompress_elems_long(inv_index.elems[189187],current);
	// qsort(current->elements, current->curr_size, sizeof(LONG_INT), cmpfunc);

	// print_elems_long(current);
	// 189187
	read_data("../../../../doc_dataset/chunk1",&inv_index);
	// init_updates(&inv_index,8);

	update_zipper("../../../../doc_dataset/chunk2",&inv_index);
	update_zipper("../../../../doc_dataset/chunk3",&inv_index);
	update_zipper("../../../../doc_dataset/chunk4",&inv_index);
	update_zipper("../../../../doc_dataset/chunk5",&inv_index);

	handle_query(&inv_index,"../../../../doc_dataset/query_random");
	handle_query(&inv_index,"../../../../doc_dataset/query_maxuid");
	handle_query(&inv_index,"../../../../doc_dataset/query_maxuid_inter");
}























// 	//experiment compressing

// 	// printf("%d %d\n",sizeof(GLONG_INT),sizeof(LONG_INT) );

// 	// LONG_INT *array = (LONG_INT *)malloc(sizeof(LONG_INT)*37);
// 	// array[0] = 24520120;
// 	// array[1] = 29620120;
// 	// array[2] = 42420120;
// 	// array[3] = 20124222;
// 	// array[4] = 4294967295;

// 	// //5 elements of 8 bytes -- CSIZE = 7 

// 	// // unsigned char* out = (unsigned char *)malloc(15); // SIGABRT raised
// 	// // unsigned char* out = (unsigned char *)malloc(48); //-- assuming 37 elements CSIZE = 39  - still SIGABRT
// 	// unsigned char* out = (unsigned char *)malloc(15*10); //-- works fine

// 	// unsigned char * op = p4enc64(array, 5, out);
// 	// printf("%d\n",(int)(op-out) );
	

// 	// LONG_INT *capacity = (LONG_INT *)malloc(sizeof(LONG_INT)*37);
// 	// unsigned char * op2 =  p4dec64(out, 5, capacity);
// 	// printf("%d\n",(int)(op2-out) );


// 	// printf("%ld %ld %ld %ld %ld\n",capacity[0],capacity[1],capacity[2],capacity[3],capacity[4] );


// 	return 0;
// }



    // size_t compressed_size = encode( unsigned *in, size_t n, char *out)
    // compressed_size : number of bytes written into compressed output buffer out

    // size_t compressed_size = decode( char *in, size_t n, unsigned *out)
