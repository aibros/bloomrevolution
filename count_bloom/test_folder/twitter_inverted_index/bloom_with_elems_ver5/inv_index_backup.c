#include "inv_index.h"


void read_data(const char * data_file, invIndex* inv_index, int vocab_len)
{
	double start_time = omp_get_wtime();

	printf("reading data file : %s !!\n",data_file);
	FILE *reader;
	reader = fopen(data_file,"r");
	long int vocab_len = 0;

	inv_index->vocab_hash = g_hash_table_new(g_str_hash, g_str_equal);								//string to int dictionary
	
	// while(1)		//creating the dictionary and obtaining the size of vocabulary of hashtags
	// {
	// 	char *hash_tag = (char *)malloc(sizeof(char)*256);
	// 	LONG_INT time_stamp = 0;
	// 	LONG_INT uid = 0;
	// 	int ret = fscanf(reader, "%s\t%ld\t%ld\n", hash_tag, &time_stamp, &uid);

	// 	if(ret < 1)																// fscanf returns the number of objects read
	// 		break;
	// 	int index = g_hash_table_lookup(inv_index->vocab_hash,hash_tag) - 1;	//lookup returns 0 if not found
	// 	if(index < 0)
	// 	{
	// 		g_hash_table_insert(inv_index->vocab_hash, hash_tag, vocab_len+1);
	// 		vocab_len += 1;
	// 	}
	// 	else
	// 		free(hash_tag);
	// }
	printf("Time for hashtable creation:: %f\n",omp_get_wtime()-start_time);

	fclose(reader);
	reader = fopen(data_file,"r");
	inv_index->bloom_inv_ind = (bloom *)malloc(vocab_len*sizeof(bloom));
	inv_index->vocab_size = vocab_len;

	my_array_long **temp_store = (my_array_long **)malloc(vocab_len*sizeof(my_array_long *));

	
	int i;
	for(i = 0 ; i < vocab_len ; i++){

		init(&inv_index->bloom_inv_ind[i]);
		temp_store[i] = (my_array_long *)malloc(sizeof(my_array_long));
		arrayLong_init(temp_store[i]);
	}

	while(1)
	{
		char *hash_tag = (char *)malloc(sizeof(char)*256);
		LONG_INT time_stamp = 0;
		LONG_INT uid = 0;
		int ret;
		if(sizeof(LONG_INT)==8)
			ret = fscanf(reader, "%s\t%ld\t%ld\n", hash_tag, &time_stamp, &uid);
		else if(sizeof(LONG_INT)==4)
			ret = fscanf(reader, "%s\t%u\t%u\n", hash_tag, &time_stamp, &uid);

		if(ret < 1)																// fscanf returns the number of objects read
			break;

		int index = g_hash_table_lookup(inv_index->vocab_hash,hash_tag) - 1;	//lookup returns 0 if not found

		if(index < 0)
		{
			g_hash_table_insert(inv_index->vocab_hash, hash_tag, vocab_len+1);
		}
		else if(index >= 0)
		{
			if(!is_in(uid,&inv_index->bloom_inv_ind[index]))
			{
				insert(uid ,&inv_index->bloom_inv_ind[index]);
				arrayLong_addElement(temp_store[index], uid );
			}
		}

		free(hash_tag);
	}

	LONG_INT compressed_bytes = 0;
	LONG_INT uncompressed_bytes = 0;

	inv_index->elems = (comp_array_long **)malloc(vocab_len*sizeof(comp_array_long *));

	for( i = 0 ; i < vocab_len ; i++ )
	{

		// print_elems_long(temp_store[i]);
		inv_index->elems[i] = (comp_array_long *)malloc(sizeof(comp_array_long));
		
		// printf("iter -- %d  size -- %d\n",i,temp_store[i]->curr_size );
		LONG_INT c_size = compress_elems_long(temp_store[i],inv_index->elems[i]);
		compressed_bytes += c_size;
		uncompressed_bytes += sizeof(LONG_INT)*temp_store[i]->curr_size;

		arrayLong_free(temp_store[i]);
	}

	free(temp_store);

	printf("uncompressed_bytes - %ld compressed_bytes - %ld\n",uncompressed_bytes,compressed_bytes );

	printf("Time for index creation:: %f\n",omp_get_wtime()-start_time);

	printf("data file read and twitter index created: %s with total number of hashtags : %d!!\n", data_file, vocab_len);
}

// plan 
// have another update structure to store the update and have a general decompress functions 
// now merging is expensive so how do we get the elements together for intersection -- we do it the expensive way
// now for the worries regarding compression don't maintain any batch


void init_updates(invIndex *inv_index, int length)
{
	inv_index->updates = (indUpdate *)malloc(sizeof(indUpdate)*length);
	inv_index->update_len_current = 0;
	inv_index->update_len_max = length;
}

void update(invIndex * inv_index, const char * data_file)
{
	FILE *reader = fopen(data_file,"r");
	my_array_long **temp_store = (my_array_long **)malloc(vocab_len*sizeof(my_array_long *));

	
	int i;
	for(i = 0 ; i < inv_index->vocab_size ; i++){
		temp_store[i] = (my_array_long *)malloc(sizeof(my_array_long));
		arrayLong_init(temp_store[i]);
	}

	while(1)
	{
		char *hash_tag = (char *)malloc(sizeof(char)*256);
		LONG_INT time_stamp = 0;
		LONG_INT uid = 0;
		int ret;
		if(sizeof(LONG_INT)==8)
			ret = fscanf(reader, "%s\t%ld\t%ld\n", hash_tag, &time_stamp, &uid);
		else if(sizeof(LONG_INT)==4)
			ret = fscanf(reader, "%s\t%u\t%u\n", hash_tag, &time_stamp, &uid);

		if(ret < 1)																// fscanf returns the number of objects read
			break;

		int index = g_hash_table_lookup(inv_index->vocab_hash,hash_tag) - 1;	//lookup returns 0 if not found
		if(uid > 2147483645)
			continue;

		if(index >= 0)
		{
			if(!is_in(uid,&inv_index->bloom_inv_ind[index]))
			{
				insert(uid ,&inv_index->bloom_inv_ind[index]);
				arrayLong_addElement(temp_store[index], uid );
			}
		}
		else
			printf("hashtag not found in the hashtable , issue in reading vocab!!\n");

		free(hash_tag);
	}

	LONG_INT compressed_bytes = 0;
	LONG_INT uncompressed_bytes = 0;

	inv_index->elems = (comp_array_long **)malloc(vocab_len*sizeof(comp_array_long *));

	int current = inv_index->update_len_current;

	for( i = 0 ; i < vocab_len ; i++ )
	{
		inv_index->updates[current]->elems[i] = (comp_array_long *)malloc(sizeof(comp_array_long));
		
		// printf("iter -- %d  size -- %d\n",i,temp_store[i]->curr_size );
		LONG_INT c_size = compress_elems_long(temp_store[i],inv_index->updates[current]->elems[i]);
		compressed_bytes += c_size;
		uncompressed_bytes += sizeof(LONG_INT)*temp_store[i]->curr_size;

		arrayLong_free(temp_store[i]);
	}

	free(temp_store);
}


// void init_inv_index(invIndex *inv_index, LONG_INT vocab_size )
// {
// 		inv_index->vocab_hash = g_hash_table_new(g_str_hash, g_str_equal);								//string to int dictionary
// 	inv_index->bloom_inv_ind = (bloom *)malloc(vocab_len*sizeof(bloom));
// 	inv_index->vocab_size = vocab_len;
// }

void query_intersect( char ** query_terms, LONG_INT query_len, invIndex* inv_index, LONG_INT *size_res, const char* output_file){
	double start_time = omp_get_wtime();

	bloom * query_bloom;
	bloom * temp_bloom;
	query_bloom = (bloom *)malloc(sizeof(bloom));
	init(query_bloom);

	LONG_INT start = 0;

	LONG_INT min_sized_ind = 999999999;
	LONG_INT min_size = 999999999;

	LONG_INT i = 0;
	for( i = 0 ; i < query_len ; i++ ){
		LONG_INT index = g_hash_table_lookup( inv_index->vocab_hash,query_terms[i] ) - 1;
		if( index >= 0 ){
			if(start == 0){
				// union_bloom(&inv_index->bloom_inv_ind[index], query_bloom, query_bloom);	//first needs to be taken as it is
				temp_bloom = &inv_index->bloom_inv_ind[index];
				start = 1;
			}
			else if(start == 1)
			{
				intersect_bloom(&inv_index->bloom_inv_ind[index], temp_bloom, query_bloom);
				start = 2;
			}
			else
				intersect_bloom(&inv_index->bloom_inv_ind[index], query_bloom, query_bloom);

			// printf("--%d\n",inv_index->elems[index]->size );
			if(inv_index->elems[index]->size < min_size){
				min_size = inv_index->elems[index]->size;
				min_sized_ind = index;
			}
		}
		else
		{
			printf("query term not present, empty output so output file not created: %s\n",query_terms[i] );
			return;
		}
	}

	// printf("*--%d\n",inv_index->elems[min_sized_ind]->size );


	// printf("Time for intersection of the bloom filters for the query terms:: %f\n",omp_get_wtime()-start_time);
	// double time_before_dict_attack = omp_get_wtime();

	//dictionary attack
	FILE* writer = fopen(output_file,"a");
	fprintf(writer, "********\n\n" );
	LONG_INT count = 0;
	LONG_INT j = 0;

	my_array_long * temp_array = (my_array_long *)malloc(sizeof(my_array_long));
	decompress_elems_long(inv_index->elems[min_sized_ind],temp_array);

	// printf("Time for decompression:: %f\n",omp_get_wtime()-time_before_dict_attack);

	for( j = 0 ; j <= min_size ; j++ ){
		LONG_INT elem = temp_array->elements[j];

		if(is_in(elem,query_bloom)){
			fprintf(writer,"%ld\n",elem);
			count++;
		}
	}
	*size_res = count;
	fclose(writer);
	free(temp_array);
	// printf("Time for dictionary attack and decompression on resultant bloom filter:: %f %f\n",omp_get_wtime()-time_before_dict_attack,omp_get_wtime()-start_time);
	printf("Time for query_intersect of size %d:: %f\n",count,omp_get_wtime()-start_time);

	// fprintf(writer, "%f\n",omp_get_wtime()-start_time);
	// printf("Time for query_intersect of size %d:: %f\n",count,omp_get_wtime()-start_time);
}


void query_zipper( char ** query_terms, LONG_INT query_len, invIndex* inv_index, LONG_INT *size_res, const char* output_file )
{
	double start_time = omp_get_wtime();

	LONG_INT start = 0;

	LONG_INT min_sized_ind = 999999999;
	LONG_INT min_size = 999999999;

	LONG_INT i = 0;
	for( i = 0 ; i < query_len ; i++ ){
		LONG_INT index = g_hash_table_lookup( inv_index->vocab_hash,query_terms[i] ) - 1;
		if( index >= 0 ){
			if(inv_index->elems[index]->size < min_size){
				min_size = inv_index->elems[index]->size;
				min_sized_ind = index;
			}
		}
		else
		{
			printf("query term not present, empty output so output file not created: %s\n",query_terms[i] );
			return;
		}
	}

	my_array_long * current = (my_array_long *)malloc(sizeof(my_array_long));
	decompress_elems_long(inv_index->elems[min_sized_ind],current);

	my_array_long * old = (my_array_long *)malloc(sizeof(my_array_long));
	arrayLong_init(old);

	for( i = 0; i < query_len ; i++)
	{
		if(i == min_sized_ind)
			continue;
		LONG_INT index = g_hash_table_lookup( inv_index->vocab_hash,query_terms[i] ) - 1;

		my_array_long * temp = (my_array_long *)malloc(sizeof(my_array_long));
		decompress_elems_long(inv_index->elems[index], temp);

		
		intersect_array_long(temp, current, old);

		my_array_long * swap = old;
		old = current;
		current = swap;

		arrayLong_free(temp);
	}

	FILE* writer = fopen(output_file,"a");
	fprintf(writer, "********\n\n" );

	for( i = 0 ; i < current->curr_size ; i++)
		fprintf(writer,"%ld\n",current->elements[i]);

	fclose(writer);
	arrayLong_free(current);
	arrayLong_free(old);
	printf("Time for query_zipper of size %d:: %f\n" ,current->curr_size ,omp_get_wtime()-start_time);
}

// void query_generator()

void test_suite(invIndex *inv_index, const char *query_file)
{
	printf("starting the queries from file ::%s \n",query_file );

	FILE *query_reader;
	query_reader = fopen("query","r");

	LONG_INT num_queries = 0;
	fscanf(query_reader,"%d\n",&num_queries);

	LONG_INT i,j = 0;
	for( i = 0 ; i < num_queries ; i++ ){

		LONG_INT num_terms = 0;
		fscanf(query_reader,"%d ", &num_terms);

		char **query_terms = (char **)malloc( sizeof(char *)*num_terms );

		for( j = 0 ; j < num_terms-1 ; j++){
			query_terms[j] = (char *)malloc(sizeof(char)*256);
			fscanf(query_reader,"%s ",query_terms[j]);
			// printf("%s\n",query_terms[j] );
		}

		query_terms[j] = (char *)malloc(sizeof(char)*256);
		fscanf(query_reader, "%s\n", query_terms[j]);
		// printf("%s\n",query_terms[j] );

		LONG_INT size1,size2 = 0;
		printf("------------**************\n");
		query_intersect(query_terms, num_terms, inv_index, &size1,"output/query_intersect.txt" );
		query_zipper(query_terms, num_terms, inv_index, &size2,"output/query_zipper.txt" );

	}

	fclose(query_reader);
	printf("all the queries are done\n");
}

void free_inv_index(invIndex *inv_index)
{
	LONG_INT i;
	for( i = 0 ; i < inv_index->vocab_size ; i++ )
	{
		g_array_free(inv_index->elems[i],TRUE);
	}
}


int main(LONG_INT argc, char** argv) {
	srand(time(0));
	init_bloomParameters(1024,3,1);
	seiveInitial();
	invIndex inv_index;
	read_data("../../../../doc_dataset/sample_dif_timeline1s",&inv_index);
	test_suite(&inv_index,"query");
}























// 	//experiment compressing

// 	// printf("%d %d\n",sizeof(GLONG_INT),sizeof(LONG_INT) );

// 	// LONG_INT *array = (LONG_INT *)malloc(sizeof(LONG_INT)*37);
// 	// array[0] = 24520120;
// 	// array[1] = 29620120;
// 	// array[2] = 42420120;
// 	// array[3] = 20124222;
// 	// array[4] = 4294967295;

// 	// //5 elements of 8 bytes -- CSIZE = 7 

// 	// // unsigned char* out = (unsigned char *)malloc(15); // SIGABRT raised
// 	// // unsigned char* out = (unsigned char *)malloc(48); //-- assuming 37 elements CSIZE = 39  - still SIGABRT
// 	// unsigned char* out = (unsigned char *)malloc(15*10); //-- works fine

// 	// unsigned char * op = p4enc64(array, 5, out);
// 	// printf("%d\n",(int)(op-out) );
	

// 	// LONG_INT *capacity = (LONG_INT *)malloc(sizeof(LONG_INT)*37);
// 	// unsigned char * op2 =  p4dec64(out, 5, capacity);
// 	// printf("%d\n",(int)(op2-out) );


// 	// printf("%ld %ld %ld %ld %ld\n",capacity[0],capacity[1],capacity[2],capacity[3],capacity[4] );


// 	return 0;
// }



    // size_t compressed_size = encode( unsigned *in, size_t n, char *out)
    // compressed_size : number of bytes written into compressed output buffer out

    // size_t compressed_size = decode( char *in, size_t n, unsigned *out)
