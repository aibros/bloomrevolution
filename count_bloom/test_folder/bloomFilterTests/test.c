#include "test.h"

char* bloomTestingPath;
char* logsPath;
char* datasetPath;

// Reads data from file and inserts them in the bloom filter
void read_and_insert(char * filename, bloom* filter)
{
	FILE * reader;
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,filename);
	reader = fopen(fullFilename,"r");
	UINT i,num_elems = 0,namespace = 1;
	fscanf(reader,"%d",&num_elems);
	fscanf(reader,"%d",&namespace);
	for(i = 0 ; i < num_elems ; i++)
	{
		int elem = 0;
		fscanf(reader,"%d",&elem);
		insert(elem,filter);
	}
	fclose(reader);
}

// Takes a bloom filter and estimates number of false positives in it.
void estimate_false_positive(char* filename,bloom* filter, FILE* logger)
{
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,filename);
	FILE * reader = fopen(fullFilename,"r");
	int num_elems,namespace;
	fscanf(reader,"%d",&num_elems);
	fscanf(reader,"%d",&namespace);
	fclose(reader);
	int i;
	int numPresent = 0;
	for (i = 1; i <= namespace; ++i)
	{
		if(is_in(i,filter))
		{
			numPresent++;
		}
	}
	fprintf(logger,"%f,",(numPresent - num_elems)/(float)namespace);
}

// This function reads bloom filter size from a file which contain results from dynamic_partition so that we can
// allocate a static bloom filter of the same size so that we can compare the performance
// unitSize is the size of unit bloom filter used in dynPartition bloom filter
void run_set_creation_tests_sameMemUsage(UINT K, int unitSize)
{
	// UINT K 				= 3;
	// It is important for referenceFileName to be tab separated data, somehow it makes things easier to parse
	char directoryName[1000],dynPartitionDirName[1000],referenceFilename[1000], temp[1000], nullStr[100],logFilename[1000];;
	char* filename;
	FILE* refReader;
	FILE* logger;
	int size,numBFs,memUsage, tempUnitSize;
	UINT COUNTER_SIZE 	= 1;
	float fpRatio,tempTime;
	struct timeval start,end;
	
	sprintf(directoryName,"%sSetCreationUnitSizeAnalysis_Static",logsPath);
	sprintf(dynPartitionDirName,"%sSetCreationUnitSizeAnalysis_DynPart",logsPath);
	mkdir(directoryName,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	
	sprintf(referenceFilename,"%s/%d.tsv",dynPartitionDirName,unitSize); // Name of file containing dynPartition's output
	refReader = fopen(referenceFilename,"r");
	fscanf(refReader,"%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s",temp,temp,temp,temp,temp,temp,temp,temp,temp); // Reading header out

	sprintf(logFilename,"%s/%d.csv",directoryName,unitSize);
	logger = fopen(logFilename,"w");
	fprintf(logger,"%s","FileName,NumElems,UnitSize,EffMemUsage,FP_Ratio,TimeTaken,MembershipQueryTime,NULL");

	while(!feof(refReader))
	{
		fprintf(logger, "\n");
		filename 	= (char*)malloc(1000*sizeof(char));
		fscanf(refReader,"%s\t%d\t%d\t%d\t%d\t%f\t%f\t%f\t%s",filename,&size,&tempUnitSize,&numBFs,&memUsage,&fpRatio,&tempTime,&tempTime,nullStr);
		
		if(tempUnitSize != unitSize)
		{
			printf("We are reading wrong file, unitSizes do not match::%d!=%d\n",tempUnitSize,unitSize );
			exit(0);
		}
		init_bloomParameters(memUsage,K,COUNTER_SIZE); // Sets SIZE_BLOOM, K, and COUNTER_SIZE

		///////////////////////////////Read file and insert in bloom filter///////////////////////////////

		gettimeofday(&start,NULL);
		bloom* filter = (bloom*)malloc(sizeof(bloom));
		init(filter);
		read_and_insert(filename,filter);
		gettimeofday(&end,NULL);
		float creationTime = get_time_diff(start,end);


		char fullFilename[1000];
		sprintf(fullFilename,"%s%s",datasetPath,filename);
		FILE * reader = fopen(fullFilename,"r");
		int num_elems,namespace;
		fscanf(reader,"%d",&num_elems);
		fscanf(reader,"%d",&namespace);
		fclose(reader);

		fprintf(logger, "%s,",filename);
		fprintf(logger, "%d,",num_elems);
		fprintf(logger, "%d,",unitSize);
		fprintf(logger, "%d,",bloomParam->SIZE_BLOOM);
		gettimeofday(&start,NULL);
		estimate_false_positive(filename,filter,logger);
		gettimeofday(&end,NULL);
		float memQueryTime = get_time_diff(start,end);

		fprintf(logger, "%f,",creationTime);
		fprintf(logger, "%f,",memQueryTime);
		fprintf(logger, "NULL");
		///////////////////////////////////////////////////////////////////////////////////////////////
		free_bloom(filter);
		free(filename);
	}
	fclose(refReader);
	fclose(logger);
}

// This function reads bloom filter size from a file which contain results from dynamic_partition so that we can
// allocate a static bloom filter of the same size so that we can compare the performance
// unitSize is the size of unit bloom filter used in dynPartition bloom filter
void run_set_creation_tests_sameFP(UINT K, int unitSize, float falsePstvProb)
{
	// It is important for referenceFileName to be tab separated data, somehow it makes things easier to parse
	char directoryName[1000],dynPartitionDirName[1000],referenceFilename[1000], temp[1000], nullStr[100],logFilename[1000];;
	char* filename;
	FILE* refReader;
	FILE* logger;
	int size,numBFs,memUsage, tempUnitSize;
	UINT COUNTER_SIZE 	= 1;
	float fpRatio,tempTime;
	struct timeval start,end;
	
	sprintf(directoryName,"%sSetCreationUnitSizeAnalysis_Static",logsPath);
	sprintf(dynPartitionDirName,"%sSetCreationUnitSizeAnalysis_DynPart",logsPath);
	mkdir(directoryName,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	
	sprintf(referenceFilename,"%s/%d.tsv",dynPartitionDirName,unitSize); // Name of file containing dynPartition's output
	refReader = fopen(referenceFilename,"r");
	fscanf(refReader,"%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s",temp,temp,temp,temp,temp,temp,temp,temp,temp); // Reading header out

	sprintf(logFilename,"%s/%d.csv",directoryName,unitSize);
	logger = fopen(logFilename,"w");
	fprintf(logger,"%s","FileName,NumElems,UnitSize,EffMemUsage,FP_Ratio,TimeTaken,MembershipQueryTime,NULL");

	while(!feof(refReader))
	{
		fprintf(logger, "\n");
		filename 	= (char*)malloc(1000*sizeof(char));
		fscanf(refReader,"%s\t%d\t%d\t%d\t%d\t%f\t%f\t%f\t%s",filename,&size,&tempUnitSize,&numBFs,&memUsage,&fpRatio,&tempTime,&tempTime,nullStr);
		
		if(tempUnitSize != unitSize)
		{
			printf("We are reading wrong file, unitSizes do not match::%d!=%d\n",tempUnitSize,unitSize );
			exit(0);
		}

		// Calculating size of BF to match the FP Ratio achieved by dynamic partition BF
		float tempBFSize;
		tempBFSize = -1*((int)K)*size;
		tempBFSize =  tempBFSize/(log(1 - pow(falsePstvProb, 1.0/K)));
		tempBFSize = tempBFSize/(8*sizeof(int));
		memUsage = (int)tempBFSize;
		memUsage *= (8*sizeof(int));
		init_bloomParameters(memUsage,K,COUNTER_SIZE); // Sets SIZE_BLOOM, K, and COUNTER_SIZE

		///////////////////////////////Read file and insert in bloom filter///////////////////////////////

		gettimeofday(&start,NULL);
		bloom* filter = (bloom*)malloc(sizeof(bloom));
		init(filter);
		read_and_insert(filename,filter);
		gettimeofday(&end,NULL);
		float creationTime = get_time_diff(start,end);


		char fullFilename[1000];
		sprintf(fullFilename,"%s%s",datasetPath,filename);
		FILE * reader = fopen(fullFilename,"r");
		int num_elems,namespace;
		fscanf(reader,"%d",&num_elems);
		fscanf(reader,"%d",&namespace);
		fclose(reader);

		fprintf(logger, "%s,",filename);
		fprintf(logger, "%d,",num_elems);
		fprintf(logger, "%d,",unitSize);
		fprintf(logger, "%d,",bloomParam->SIZE_BLOOM);
		gettimeofday(&start,NULL);
		estimate_false_positive(filename,filter,logger);
		gettimeofday(&end,NULL);
		float memQueryTime = get_time_diff(start,end);

		fprintf(logger, "%f,",creationTime);
		fprintf(logger, "%f,",memQueryTime);
		fprintf(logger, "NULL");
		///////////////////////////////////////////////////////////////////////////////////////////////
		free_bloom(filter);
		free(filename);
	}
	fclose(refReader);
	fclose(logger);
}

void run_intersection_varying_overlap()
{
	UINT K 				= 3;
	UINT COUNTER_SIZE 	= 1;
	UINT namespace    	= 100000;
	UINT unitSize;
	int filesizes[]   = {10,100,200,500,1000,2000};
	int num_filesizes = 6;
	int num_samples   = 2; 
	int num_elems_A,num_elems_B, num_elems_intersect, intJunk, memUsage;
	float floatJunk;

	char directoryName[1000];
	sprintf(directoryName,"%sintersection_Static_overlap",logsPath);
	mkdir(directoryName,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

	int i,j,l;
	float overlap;
	for(i = 0 ; i < num_filesizes ; i++)
	{
		for(j = i ; j < num_filesizes ; j++)
		{
			char referenceFilename[1000];
			char temp[1000];
			sprintf(referenceFilename,"%s/%d_%d.tsv",directoryName,filesizes[i],filesizes[j]); // Name of file containing dynPartition's output

			FILE* refReader;
			refReader = fopen(referenceFilename,"r");
			fscanf(refReader,"%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s",temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp); // Reading header out

			FILE* logger;
			char logFilename[1000];
			sprintf(logFilename,"%s/%d_%d.csv",directoryName,filesizes[i],filesizes[j]);
			logger = fopen(logFilename,"w");
			fprintf(logger,"%s","FileA,FileB,ResultFile,Overlap,Operation,Namespace,NumHashFunc,UnitSize,NumElemsA,NumElemsB,NumElemsResult,EffMemUsage,FP_Ratio,NULL");

			while(!feof(refReader))
			{
				char* file_A,*file_B;
				char * intersect_filename;

				file_A = (char*)malloc(1000*sizeof(char));
				file_B = (char*)malloc(1000*sizeof(char));
				intersect_filename = (char*)malloc(1000*sizeof(char));

				fscanf(refReader,"%s\t%s\t%s\t%f\t%s\t%d\t%d\t%d\t%f\t%f\t%d\t%d\t%d\t%d\t%d\t%f\t%s",file_A,file_B,intersect_filename,&overlap,temp,&namespace,&K,&unitSize,&floatJunk,&floatJunk,&num_elems_A,&num_elems_B,&num_elems_intersect,&intJunk,&memUsage,&floatJunk,temp);

				init_bloomParameters(memUsage,K,COUNTER_SIZE); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
			
				bloom* filter_1 		= (bloom*)malloc(sizeof(bloom));
				bloom* filter_2 		= (bloom*)malloc(sizeof(bloom));
				bloom* filter_intersect = (bloom*)malloc(sizeof(bloom));
				init(filter_1);
				init(filter_2);
				init(filter_intersect);

				FILE * reader;
				int namespace;
				int num_elems_1,num_elems_2, num_elems_union, num_elems_intersect;

				fprintf(logger, "\n%s,", file_A);
				fprintf(logger, "%s,", file_B);
				fprintf(logger, "%s,", intersect_filename);
				fprintf(logger, "%.5f,", overlap);
				fprintf(logger, "Intersection,");

				fprintf(logger, "%d,",namespace);
				fprintf(logger, "%d,",bloomParam->K);
				fprintf(logger, "%d,",unitSize);
				
				read_and_insert(file_A,filter_1);
				read_and_insert(file_B,filter_2);
				intersect_bloom(filter_1,filter_2,filter_intersect);

				// Details for set A
				fprintf(logger, "%d,",num_elems_A );

				// Details for set B
				fprintf(logger, "%d,",num_elems_B );

				// Details for set (A intersect B)
				fprintf(logger, "%d,",num_elems_intersect );
				fprintf(logger, "%d,",bloomParam->SIZE_BLOOM);
				estimate_false_positive(intersect_filename,filter_intersect,logger);

				fprintf(logger, "NULL");
				free(file_A);
				free(file_B);
				free(intersect_filename);
				free_bloom(filter_1);
				free_bloom(filter_2);
				free_bloom(filter_intersect);
				free(filter_1);
				free(filter_2);
				free(filter_intersect);
			}

			fclose(refReader);
			fclose(logger);
		}
	}
}

void run_union_varying_overlap()
{
	UINT K 				= 3;
	UINT COUNTER_SIZE 	= 1;
	UINT namespace    	= 100000;
	UINT unitSize;
	int filesizes[]   = {10,100,200,500,1000,2000};
	int num_filesizes = 6;
	int num_samples   = 20; 
	int num_elems_A, num_elems_B, num_elems_union, intJunk, memUsage;
	float floatJunk;

	char directoryName[1000];
	sprintf(directoryName,"%sunion_Static_overlap",logsPath);
	mkdir(directoryName,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

	int i,j,l;
	float overlap;
	for(i = 0 ; i < num_filesizes ; i++)
	{
		for(j = i ; j < num_filesizes ; j++)
		{
			char referenceFilename[1000];
			char temp[1000];
			sprintf(referenceFilename,"%s/%d_%d.tsv",directoryName,filesizes[i],filesizes[j]); // Name of file containing dynPartition's output

			FILE* refReader;
			refReader = fopen(referenceFilename,"r");
			fscanf(refReader,"%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s",temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp); // Reading header out

			FILE* logger;
			char logFilename[1000];
			sprintf(logFilename,"%s/%d_%d.csv",directoryName,filesizes[i],filesizes[j]);
			logger = fopen(logFilename,"w");
			fprintf(logger,"%s","FileA,FileB,ResultFile,Overlap,Operation,Namespace,NumHashFunc,UnitSize,NumElemsA,NumElemsB,NumElemsResult,EffMemUsage,FP_Ratio,NULL");

			while(!feof(refReader))
			{
				char* file_A,*file_B;
				char * union_filename;

				file_A = (char*)malloc(1000*sizeof(char));
				file_B = (char*)malloc(1000*sizeof(char));
				union_filename = (char*)malloc(1000*sizeof(char));

				fscanf(refReader,"%s\t%s\t%s\t%f\t%s\t%d\t%d\t%d\t%f\t%f\t%d\t%d\t%d\t%d\t%d\t%f\t%s",file_A,file_B,union_filename,&overlap,temp,&namespace,&K,&unitSize,&floatJunk,&floatJunk,&num_elems_A,&num_elems_B,&num_elems_union,&intJunk,&memUsage,&floatJunk,temp);

				init_bloomParameters(memUsage,K,COUNTER_SIZE); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
			
				bloom* filter_1 		= (bloom*)malloc(sizeof(bloom));
				bloom* filter_2 		= (bloom*)malloc(sizeof(bloom));
				bloom* filter_union 	= (bloom*)malloc(sizeof(bloom));
				init(filter_1);
				init(filter_2);
				init(filter_union);

				FILE * reader;
				int namespace;
				int num_elems_1,num_elems_2, num_elems_union;

				fprintf(logger, "\n%s,", file_A);
				fprintf(logger, "%s,", file_B);
				fprintf(logger, "%s,", union_filename);
				fprintf(logger, "%.5f,", overlap);
				fprintf(logger, "Union,");

				fprintf(logger, "%d,",namespace);
				fprintf(logger, "%d,",bloomParam->K);
				fprintf(logger, "%d,",unitSize);
				
				read_and_insert(file_A,filter_1);
				read_and_insert(file_B,filter_2);
				union_bloom(filter_1,filter_2,filter_union);

				// Details for set A
				fprintf(logger, "%d,",num_elems_A );

				// Details for set B
				fprintf(logger, "%d,",num_elems_B );

				// Details for set (A intersect B)
				fprintf(logger, "%d,",num_elems_union );
				fprintf(logger, "%d,",bloomParam->SIZE_BLOOM);
				estimate_false_positive(union_filename,filter_union,logger);

				fprintf(logger, "NULL");
				free(file_A);
				free(file_B);
				free(union_filename);
				free_bloom(filter_1);
				free_bloom(filter_2);
				free_bloom(filter_union);
				free(filter_1);
				free(filter_2);
				free(filter_union);
			}
			fclose(refReader);
			fclose(logger);
		}
	}
}

// Run test where we have fixed size of overlap between A & B but vary size of symmetric difference
// overlapList		: List of overlap sizes for which to run test, 
// numOverlapVals	: Size of overlapList
// numSets 			: number of sets generated for performing union/intersection for a particular value of overlap
// K 				: Number of hash functions
// FALSE_PSTV_PROB 	: False positive probability
void run_intersection_fixed_overlap(int* overlapList,int numOverlapVals,int numSets,int K,float FALSE_PSTV_PROB)
{

	UINT COUNTER_SIZE 	= 1;
	UINT unitSize;
	int num_elems_A,num_elems_B, num_elems_intersect,num_elems_symDiff, intJunk, memUsage, namespace, overlap;
	float floatJunk;

	char directoryName[1000], dynPartitionDirName[1000];
	sprintf(directoryName,"%sintersection_Static_fixedOverlap",logsPath);
	sprintf(dynPartitionDirName,"%sintersection_dynPartition_fixedOverlap",logsPath);	
	mkdir(directoryName,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	
	for (int i = 0; i < numOverlapVals; ++i)
	{
		char referenceFilename[1000];
		char temp[1000];
		sprintf(referenceFilename,"%s/%d.tsv",dynPartitionDirName,overlapList[i]); // Name of file containing dynPartition's output

		FILE* refReader;
		refReader = fopen(referenceFilename,"r");
		fscanf(refReader,"%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s",temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp); // Reading header out

		FILE* logger;
		char logFilename[1000];
		sprintf(logFilename,"%s/%d.csv",directoryName,overlapList[i]);
		logger = fopen(logFilename,"w");
		fprintf(logger,"%s","FileA,FileB,ResultFile,Overlap,Operation,Namespace,NumHashFunc,UnitSize,NumElemsA,NumElemsB,NumElemsResult,SymmetricDiffSize,EffMemUsage,FP_Ratio,NULL");

		while(!feof(refReader))
		{
			char* file_A,*file_B;
			char * intersect_filename;

			file_A = (char*)malloc(1000*sizeof(char));
			file_B = (char*)malloc(1000*sizeof(char));
			intersect_filename = (char*)malloc(1000*sizeof(char));

			fscanf(refReader,"%s\t%s\t%s\t%d\t%s\t%d\t%d\t%d\t%f\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%f\t%s",file_A,file_B,intersect_filename,&overlap,temp,&namespace,&K,&unitSize,&floatJunk,&floatJunk,&num_elems_A,&num_elems_B,&num_elems_intersect,&num_elems_symDiff,&intJunk,&memUsage,&floatJunk,temp);

			// printf("file_A::%s\n",file_A );
			// printf("file_B::%s\n",file_B );
			// printf("union_filename::%s\n",intersect_filename );
			// printf("overlap::%d\n",overlap );
			// printf("temp::%s\n",temp );
			// printf("namespace::%d\n",namespace );
			// printf("K::%d\n",K );
			// printf("unitSize::%d\n",unitSize );
			// printf("floatJunk::%f\n",floatJunk );
			// printf("floatJunk::%f\n",floatJunk );
			// printf("num_elems_A::%d\n",num_elems_A );
			// printf("num_elems_B::%d\n",num_elems_B );
			// printf("num_elems_union::%d\n",num_elems_intersect );
			// printf("num_elems_symDiff::%d\n",num_elems_symDiff );
			// printf("intJunk::%d\n",intJunk );
			// printf("memUsage::%d\n",memUsage );
			// printf("floatJunk::%f\n",floatJunk );
			// printf("temp::%s\n",temp );

			init_bloomParameters(memUsage,K,COUNTER_SIZE); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
		
			bloom* filter_1 		= (bloom*)malloc(sizeof(bloom));
			bloom* filter_2 		= (bloom*)malloc(sizeof(bloom));
			bloom* filter_intersect = (bloom*)malloc(sizeof(bloom));
			init(filter_1);
			init(filter_2);
			init(filter_intersect);

			FILE * reader;
			fprintf(logger, "\n%s,", file_A);
			fprintf(logger, "%s,", file_B);
			fprintf(logger, "%s,", intersect_filename);
			fprintf(logger, "%d,", overlap);
			fprintf(logger, "Intersection,");

			fprintf(logger, "%d,",namespace);
			fprintf(logger, "%d,",bloomParam->K);
			fprintf(logger, "%d,",unitSize);
			
			read_and_insert(file_A,filter_1);
			read_and_insert(file_B,filter_2);
			intersect_bloom(filter_1,filter_2,filter_intersect);

			// Details for set A
			fprintf(logger, "%d,",num_elems_A );

			// Details for set B
			fprintf(logger, "%d,",num_elems_B );

			// Details for set (A intersect B)
			fprintf(logger, "%d,",num_elems_intersect );
			fprintf(logger, "%d,",num_elems_symDiff );
			fprintf(logger, "%d,",bloomParam->SIZE_BLOOM);
			estimate_false_positive(intersect_filename,filter_intersect,logger);
			
			fprintf(logger, "NULL");
			free(file_A);
			free(file_B);
			free(intersect_filename);
			free_bloom(filter_1);
			free_bloom(filter_2);
			free_bloom(filter_intersect);
			free(filter_1);
			free(filter_2);
			free(filter_intersect);
		}

		fclose(refReader);
		fclose(logger);
	}
}

// Run test where we have fixed size of overlap between A & B but vary size of symmetric difference
// overlapList		: List of overlap sizes for which to run test, 
// numOverlapVals	: Size of overlapList
// numSets 			: number of sets generated for performing union/intersection for a particular value of overlap
// K 				: Number of hash functions
// FALSE_PSTV_PROB 	: False positive probability
void run_union_fixed_overlap(int* overlapList,int numOverlapVals,int numSets,int K,float FALSE_PSTV_PROB)
{
	UINT COUNTER_SIZE 	= 1;	
	UINT unitSize;
	
	int num_elems_A, num_elems_B, num_elems_union,num_elems_symDiff, intJunk, memUsage, namespace, overlap;
	float floatJunk;

	char directoryName[1000],dynPartitionDirName[1000];
	sprintf(directoryName,"%sunion_Static_fixedOverlap",logsPath);
	sprintf(dynPartitionDirName,"%sunion_dynPartition_fixedOverlap",logsPath);
	mkdir(directoryName,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

	for(int i = 0 ; i < numOverlapVals ; i++)
	{
		char referenceFilename[1000];
		char temp[1000];
		sprintf(referenceFilename,"%s/%d.tsv",dynPartitionDirName,overlapList[i]); // Name of file containing dynPartition's output

		FILE* refReader;
		refReader = fopen(referenceFilename,"r");
		fscanf(refReader,"%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s",temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp); // Reading header out

		FILE* logger;
		char logFilename[1000];
		sprintf(logFilename,"%s/%d.csv",directoryName,overlapList[i]);
		logger = fopen(logFilename,"w");
		fprintf(logger,"%s","FileA,FileB,ResultFile,Overlap,Operation,Namespace,NumHashFunc,UnitSize,NumElemsA,NumElemsB,NumElemsResult,SymmetricDiffSize,EffMemUsage,FP_Ratio,NULL");

		while(!feof(refReader))
		{
			char* file_A,*file_B;
			char * union_filename;

			file_A = (char*)malloc(1000*sizeof(char));
			file_B = (char*)malloc(1000*sizeof(char));
			union_filename = (char*)malloc(1000*sizeof(char));

			fscanf(refReader,"%s\t%s\t%s\t%d\t%s\t%d\t%d\t%d\t%f\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%f\t%s",file_A,file_B,union_filename,&overlap,temp,&namespace,&K,&unitSize,&floatJunk,&floatJunk,&num_elems_A,&num_elems_B,&num_elems_union,&num_elems_symDiff,&intJunk,&memUsage,&floatJunk,temp);

			// printf("file_A::%s\n",file_A );
			// printf("file_B::%s\n",file_B );
			// printf("union_filename::%s\n",union_filename );
			// printf("overlap::%d\n",overlap );
			// printf("temp::%s\n",temp );
			// printf("namespace::%d\n",namespace );
			// printf("K::%d\n",K );
			// printf("unitSize::%d\n",unitSize );
			// printf("floatJunk::%f\n",floatJunk );
			// printf("floatJunk::%f\n",floatJunk );
			// printf("num_elems_A::%d\n",num_elems_A );
			// printf("num_elems_B::%d\n",num_elems_B );
			// printf("num_elems_union::%d\n",num_elems_union );
			// printf("num_elems_symDiff::%d\n",num_elems_symDiff );
			// printf("intJunk::%d\n",intJunk );
			// printf("memUsage::%d\n",memUsage );
			// printf("floatJunk::%f\n",floatJunk );
			// printf("temp::%s\n",temp );

			init_bloomParameters(memUsage,K,COUNTER_SIZE); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
		
			bloom* filter_1 		= (bloom*)malloc(sizeof(bloom));
			bloom* filter_2 		= (bloom*)malloc(sizeof(bloom));
			bloom* filter_union 	= (bloom*)malloc(sizeof(bloom));
			init(filter_1);
			init(filter_2);
			init(filter_union);

			FILE * reader;

			fprintf(logger, "\n%s,", file_A);
			fprintf(logger, "%s,", file_B);
			fprintf(logger, "%s,", union_filename);
			fprintf(logger, "%d,", overlap);
			fprintf(logger, "Union,");

			fprintf(logger, "%d,",namespace);
			fprintf(logger, "%d,",bloomParam->K);
			fprintf(logger, "%d,",unitSize);
			
			read_and_insert(file_A,filter_1);
			read_and_insert(file_B,filter_2);
			union_bloom(filter_1,filter_2,filter_union);

			// Details for set A
			fprintf(logger, "%d,",num_elems_A );

			// Details for set B
			fprintf(logger, "%d,",num_elems_B );

			// Details for set (A intersect B)
			fprintf(logger, "%d,",num_elems_union );
			fprintf(logger, "%d,",num_elems_symDiff );
			fprintf(logger, "%d,",bloomParam->SIZE_BLOOM);
			estimate_false_positive(union_filename,filter_union,logger);

			fprintf(logger, "NULL");
			free(file_A);
			free(file_B);
			free(union_filename);
			free_bloom(filter_1);
			free_bloom(filter_2);
			free_bloom(filter_union);
			free(filter_1);
			free(filter_2);
			free(filter_union);
		}
		fclose(refReader);
		fclose(logger);
	}
}

static inline float get_time_diff(struct timeval t1, struct timeval t2)
{
	return t2.tv_sec - t1.tv_sec + 1e-6 * (t2.tv_usec - t1.tv_usec);
}

int main(int argc, char** argv)
{
	bloomTestingPath 	= (char*)malloc(1000*sizeof(char));
	datasetPath 		= (char*)malloc(1000*sizeof(char));
	logsPath 			= (char*)malloc(1000*sizeof(char));
	strcpy(bloomTestingPath,"../../../bloomTesting/");
	strcpy(datasetPath,"../../../bloomTesting/datasets/");
	strcpy(logsPath,"../../../bloomTesting/logs/");
	time_t t;
	srand((unsigned) time(&t));

	// Call csv to tsv converted here to fully automate things
	if (argc >=2)
	{
		int testId = atoi(argv[1]);
		if ( ((testId == 3) || (testId == 4)) && (argc >= 6))
		{
			// Call csv to tsv converted here to fully automate things
			char* command = (char*)malloc(10000*sizeof(char));
			sprintf(command,"python %scsvTotsv.py %slogs/SetCreationUnitSizeAnalysis_DynPart",bloomTestingPath,bloomTestingPath);
			system(command);
			free(command);
			
			UINT K 				= atoi(argv[2]);
			float falsePstvProb = atof(argv[3]);
			int numUnitSize 	= atoi(argv[4]);
			int* unitSizeVals 	= (int*)malloc(numUnitSize*sizeof(int));

			if (argc < numUnitSize + 5)
			{
				printf("Not enough unit size values specified\n");
				exit(0);			
			}

			for (int i = 0; i < numUnitSize; ++i)
				unitSizeVals[i] = atoi(argv[5 + i]);

			for (int i = 0; i < numUnitSize; ++i)
			{
				if (testId == 3)
					run_set_creation_tests_sameMemUsage(K, unitSizeVals[i]);
				else if (testId == 4)
					run_set_creation_tests_sameFP(K, unitSizeVals[i],falsePstvProb);	
			}

			// Remove temporary .tsv files created
			command = (char*)malloc(10000*sizeof(char));
			sprintf(command,"rm %slogs/SetCreationUnitSizeAnalysis_DynPart/*.tsv",bloomTestingPath);
			system(command);
			free(command);
		}
		else if ((testId == 2) && (argc >= 7))
		{
			// Call csv to tsv converted here to fully automate things
			char* command = (char*)malloc(1000*sizeof(char));
			sprintf(command,"python %scsvTotsv.py %slogs/intersection_dynPartition_fixedOverlap",bloomTestingPath,bloomTestingPath);
			system(command);
			free(command);
			command = (char*)malloc(1000*sizeof(char));
			sprintf(command,"python %scsvTotsv.py %slogs/union_dynPartition_fixedOverlap",bloomTestingPath,bloomTestingPath);
			system(command);
			free(command);
			
			UINT K 				= atoi(argv[2]);
			float falsePstvProb = atof(argv[3]);
			int numSets 		= atoi(argv[4]);
			int numOverlapVals 	= atoi(argv[5]);
			int* overlapList 	= (int*)malloc(numOverlapVals*sizeof(int));

			if (argc < numOverlapVals + 6)
			{
				printf("Not enough unit size values specified\n");
				exit(0);			
			}

			for (int i = 0; i < numOverlapVals; ++i)
				overlapList[i] = atoi(argv[6 + i]);
		
			run_intersection_fixed_overlap(overlapList,numOverlapVals,numSets,K,falsePstvProb);
			run_union_fixed_overlap(overlapList,numOverlapVals,numSets,K,falsePstvProb);

			// Remove temporary .tsv files created
			command = (char*)malloc(10000*sizeof(char));
			sprintf(command,"rm %slogs/intersection_dynPartition_fixedOverlap/*.tsv",bloomTestingPath);
			system(command);
			free(command);

			command = (char*)malloc(10000*sizeof(char));
			sprintf(command,"rm %slogs/union_dynPartition_fixedOverlap/*.tsv",bloomTestingPath);
			system(command);
			free(command);
		}
		else
		{
			printf("Error:: Not enough arguments after testId\n");
			printf("Usage:: ./test <testId> <other parameters>\n");
			printf("testId = 2 for set intersection/union tests with fixed overlap\n");
			printf("testId = 3 for set creation tests (with varying size of unit Bloom filter)(with same Memory as DPBF)\n");
			printf("testId = 4 for set creation tests (with varying size of unit Bloom filter)(with bounded FP Rate)\n");
			printf("For testId = 2 Usage:: ./test 2 <K> <False Positive Prob> <numSets> <number of overlap values(n)>  <overlap value_1> ... <overlap value_n>\n");
			printf("For testId = 3 or 4 Usage:: ./test 1 <K> <False Positive Prob> <number of unitSizes(n)>  <unit size value_1> ... <unit size value_n>\n");
		}
	}
	else
	{
		printf("Error:: Not enough arguments after testId\n");
		printf("Usage:: ./test <testId> <other parameters>\n");
		printf("testId = 2 for set intersection/union tests with fixed overlap\n");
		printf("testId = 3 for set creation tests (with varying size of unit Bloom filter)(with same Memory as DPBF)\n");
		printf("testId = 4 for set creation tests (with varying size of unit Bloom filter)(with bounded FP Rate)\n");
		printf("For testId = 2 Usage:: ./test 2 <K> <False Positive Prob> <numSets> <number of overlap values(n)>  <overlap value_1> ... <overlap value_n>\n");
		printf("For testId = 3 or 4 Usage:: ./test 1 <K> <False Positive Prob> <number of unitSizes(n)>  <unit size value_1> ... <unit size value_n>\n");
	}
}
