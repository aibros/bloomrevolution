#include "../../count_bloom.h"
#include "sys/time.h"

void read_and_insert(char * filename, bloom* filter);
void estimate_false_positive(char* filename,bloom* filter, FILE* logger);
void run_set_creation_tests_sameMemUsage(UINT K, int unitSize);
void run_set_creation_tests_sameFP(UINT K, int unitSize, float falsePstvProb);
void run_intersection_varying_overlap();
void run_union_varying_overlap();
void run_intersection_fixed_overlap(int* overlapList,int numOverlapVals,int numSets,int K,float FALSE_PSTV_PROB);
void run_union_fixed_overlap(int* overlapList,int numOverlapVals,int numSets,int K,float FALSE_PSTV_PROB);
static inline float get_time_diff(struct timeval t1, struct timeval t2);

extern char* bloomTestingPath;
extern char* logsPath;
extern char* datasetPath;