import random
import sys

def get_vocabsize(filename):
	vocab = set()
	file = open(filename);
	line = file.readline();
	file_len = 1;
	while(line != ""):
		htag, tstamp, uid = line.split();
		vocab.add(htag)
		line = file.readline();
		file_len+=1;
	file.close()
	return len(vocab),file_len-1;


def create_chunk(file_handle, chunk_size, chunk_id, vocab_size, filename):
	chunk_file = open("chunk"+str(chunk_id),"w");
	chunk_file.write(str(vocab_size+1)+"\n");
	for i in range(chunk_size):
		chunk_file.write(file_handle.readline());
	chunk_file.close();

#chunksizes and filenames are predefined or hardcoded for now
def create_chunks(filename,file_len, vocab_size):
	file = open(filename);
	create_chunk(file, int(0.4*file_len), 1, vocab_size, filename);
	print("chunk 1 created")
	create_chunk(file, int(0.15*file_len), 2, vocab_size, filename);
	print("chunk 2 created")
	create_chunk(file, int(0.15*file_len), 3, vocab_size, filename);
	print("chunk 3 created")
	create_chunk(file, int(0.15*file_len), 4, vocab_size, filename);
	print("chunk 4 created")



	chunk_file = open("chunk5","w");
	chunk_file.write(str(vocab_size+1)+"\n");
	line = file.readline()
	while(line!=""):
		chunk_file.write(line);
		line = file.readline()
	chunk_file.close();
	print("chunk 5 created")

# there would be 3 kinds of queries
# random queries - 20 - len picked randomly from 25-100
# max htag queries - 20 - top 20
# some intersection queries - 20 - (intersection of 5 from top 100)
def query_creator(filename, query_folder):
	file = open(filename);
	line = file.readline();
	reverse_dict = {};
	htags = set();
	while(line != ""):
		htag, tstamp, uid = line.split();
		htags.add(htag);
		if uid in reverse_dict:
			reverse_dict[uid].add(htag);
		else:
			reverse_dict[uid] = {htag};
		line = file.readline();
	file.close();

	rev_values = reverse_dict.values();
	rev_values.sort(key=len);

	# random 20 queries of random lengths in(10,50) 

	file = open(query_folder+"query_random","w");
	file.write("5000\n");
	htags_len = len(htags);
	htags = list(htags)
	for query_size in range(50,100):
		for i in xrange(100):
			x = query_size #random.randint(10,50)
			file.write(str(x)+" ");

			for j in range(x):
				ind = random.randint(0,htags_len-1);
				if(j == x-1):
					file.write(str(htags[ind])+"\n");
					continue;
				file.write(str(htags[ind])+" ");
	file.close();
	print "random queries created"

	#max hashtag - top 20 uids with max hashtags(these hashtags forming queries)
	

	file = open(query_folder+"query_maxuid","w");
	file.write("5\n");
	rev_len = len(rev_values);
	for i in range(5):
		q = list(rev_values[rev_len-i-1]);
		file.write(str(len(q))+" "+" ".join(q)+"\n");
	file.close();

	print "max uid based queries created"



	#max intersection queries - intersection of 2 random from
	#top 50 uids with largest htag sizes and of nonempty size
	file = open(query_folder+"query_maxuid_inter","w");
	file.write("1000\n")
	count = 0;
	dict_check = {}
	# while(count < 10000):
	# 	q = set([])
	# 	p = random.randint(2,4)
	# 	for j in xrange(p):
	# 		rand = random.randint(1,71);
	# 		if(j==0):
	# 			q = q.union(rev_values[rev_len-rand]);
	# 		else:
	# 			q = q.intersection(rev_values[rev_len-rand])
	# 	if(len(q) > 1):
	# 		if(str(q) in dict_check):
	# 			continue;
	# 		file.write(str(len(q))+" "+" ".join(q)+"\n");
	# 		dict_check[str(q)] = 1
	# 		count += 1;
	# file.close()


	# for query_size in range(1,200,5):
	# 	print query_size
	# 	count = 0;
	# 	while(count < 25):
	# 		q = set([])
	# 		p = random.randint(2,4)
	# 		for j in xrange(p):
	# 			rand = random.randint(1,100);
	# 			if(j==0):
	# 				q = q.union(rev_values[rev_len-rand]);
	# 			else:
	# 				q = q.intersection(rev_values[rev_len-rand])
	# 		if(len(q) > query_size):
	# 			q = list(q)
	# 			if(str(q) in dict_check):
	# 				continue;
	# 			dict_check[str(q)] = 1
	# 			random.shuffle(q);
	# 			q = q[:query_size];
	# 			file.write(str(len(q))+" "+" ".join(q)+"\n");
	# 			count += 1;


	# file.close()
	print "maxuid intersection based queries created"







filename = sys.argv[1]#"sample_dif_timeline1s";
print("Splitting dataset!!")
vocab,file_len = get_vocabsize(filename)
# create_chunks(filename,file_len, vocab)
query_creator(filename,"")
print("all the separate chunks created from the dataset for experiment!!")


# todo
# run on queries
# create graphs for 
# size of result query vs time
# memory overhead -- 