import matplotlib.pyplot as plt
import matplotlib

def mapper(filename,X):
	res_to_time = {};
	term_to_time = {};
	file = open(filename);
	data = file.read().strip().split("\n");
	memory = 0;
	t_create = 0;
	for i in range(len(data)):
		if(i==0):
			a,t_create,b,memory = data[i].split(",");
			continue;
		else:
			a,t,b,res,c,terms = data[i].split(",");
			print i-1,len(X)
			res = X[i-1]
			if(res in res_to_time):
				res_to_time[int(res)] += [float(t)]
			else:
				res_to_time[int(res)] = [float(t)]

			if(terms in term_to_time):
				term_to_time[int(terms)] += [float(t)]
			else:
				term_to_time[int(terms)] = [float(t)]

	# print res_to_time
	# print term_to_time

	X_res  = res_to_time.keys()
	X_res.sort()
	X_term = term_to_time.keys()
	X_term.sort()
	Y_res  = []
	Y_term = []

	for ind in range(len(X_res)):
		temp = res_to_time[X_res[ind]]
		avg = sum(temp)/float(len(temp));
		Y_res += [avg]

	for ind in range(len(X_term)):
		temp = term_to_time[X_term[ind]]
		avg = sum(temp)/float(len(temp));
		Y_term += [avg]

	print Y_term
	print Y_res

	return t_create,memory,(X_res,Y_res),(X_term,Y_term)


def get_response(filename):
	file = open(filename);
	data = file.read().strip().split("\n");
	ret = [];
	for i in range(len(data)):
		if(i==0):
			a,t_create,b,memory = data[i].split(",");
			continue;
		else:
			a,t,b,res,c,terms = data[i].split(",");
			ret += [res];
	return ret;



def draw_graph( title, X_label, Y_label, graph_instances, instance_labels,directory,name):
	fig = plt.figure(figsize=(16, 9), dpi=80);
	ax = fig.add_subplot(111)

	for ind in range(len(graph_instances)):
		(X,Y) = graph_instances[ind];		
		plt.errorbar(X,Y,marker='o',ls='-');
		plt.xticks(X)
		# for xy in zip(X, Y):                                       
		# 	ax.annotate('(%s, %s)' % xy, xy=xy, textcoords='data')

	plt.xscale('log')
	plt.legend(instance_labels, loc='upper left')
	plt.suptitle(title);
	plt.xlabel(X_label);
	plt.ylabel(Y_label);
	# plt.show();
	fig.savefig(name+".png")
	# fig.savefig(directory+"/"+name+".png")


def plot(filenames):
	res_to_time_plots = [];
	terms_to_time_plots = [];
	# instances = ["Intersection based", "Membership queries based", "Standard inverted index"]
	# instances = ["simple intersection", "intersection with compression in between", "standard invindex","k_intersect with compression","intersection with compression at end","k_intersect with no compression"]
	instances = ["simple intersection", "k_intersect with no compression", "standard invindex"]
	# instances = ["1", "0", "5"]

	X = get_response(filenames[2]);
	print X
	for ind in range(len(filenames)):
		name = filenames[ind]
		t_create,memory,R,T = mapper(name,X);
		res_to_time_plots += [R]
		terms_to_time_plots += [T]
	draw_graph( "Number of terms in query vs Query response Time", "Number of terms in query", "Query response Time", terms_to_time_plots, instances,"","term_time");
	draw_graph( "Size of query response vs Query response Time", "size of query response", "Query response Time",res_to_time_plots, instances,"","res_time");


# plot(["record_1","record_2","record_5","record_3","record_4","record_0"])
plot(["record_1","record_0","record_5"])


# draw_graph("Total size of all DPBFs vs unit bloom filter size ", "unit bloom filter size", "total size of all DPBFs used", [([128,256,512,1024],[6043472,9484288,16142592,29585792]),([128,256,512,1024],[9293439,9293439,9293439,9293439])], ["total bloom filter memory","memory taken by compressed lists for reference"],"","okay");



# 16142592
# 29585792
