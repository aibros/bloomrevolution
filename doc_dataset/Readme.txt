There are two scripts in this folder:

run_tests : sets up and does all experiments
clean : cleans up all the files created during experiments during record

* Place the dataset file "sample_dif_timeline1s" in the folder doc_dataset(current)
* If there is a different name for the file then please change the "DATA" variable in "run_tests" accordingly
* run "bash run_tests"

* It will clean and compile all the suitable files and also split the dataset into chunks and then execute experiments
* there are five chunks (with names such as chunk1,chunk2 and so on), with last 4 forming updates

* there are three query files generated which are for experiments
* query_random - 20 queries with randomly picked hashtags of a random count between (25,75)
* query_maxuid - 20 queries based on top 20 uids with max number of hashtags(these hashtags form queries)
* query_random - 10 queries using intersection of hashtag lists of 2 randomly picked uids(using reverse map) from top 50 uids
* for any changes in queries please refer to process.py
* if you wish to fix queries, then comment the line calling "python process.py" in "run_tests"

* The results are stored in "records/" - record4(ver4) -- record5(ver5)
* run "bash clean" to remove any temporary files
