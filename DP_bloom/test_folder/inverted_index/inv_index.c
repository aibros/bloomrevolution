#include "inv_index.h"



void read_vocab(const char * vocab_file, invIndex* inv_index)
{
	printf("This functions reads the vocabulary and also initializes the structure!!\n");

	FILE *reader;
	reader = fopen(vocab_file,"r");
	int vocab_length;
	fscanf(reader,"%d\n",&vocab_length);

	inv_index->vocab_hash = g_hash_table_new(g_str_hash, g_int_equal);									//string to int dictionary
	inv_index->vocab = (char **)malloc(vocab_length*sizeof(char *));
	inv_index->bloom_inv_ind = (b_node *)malloc(vocab_length*sizeof(b_node));

	inv_index->vocab_size = vocab_length;
	int i;
	for(i = 0; i < vocab_length; i++)
	{		
		char line[1000];
		fscanf(reader,"%s\n",line);

		inv_index->vocab[i] = line;
		g_hash_table_insert(inv_index->vocab_hash, line, i+1);
		init(&inv_index->bloom_inv_ind[i]);
		grow_full_tree(&inv_index->bloom_inv_ind[i],0);
		// printf("%s -- %d\n",line,g_hash_table_lookup(inv_index->vocab_hash, line));
	}
	fclose(reader);
}

void read_docDB(const char * docDB_file, invIndex* inv_index)
{
	printf("reading docDB!!\n");
	FILE *reader;
	reader = fopen(docDB_file,"r");
	int num_docs;
	fscanf(reader,"%d\n",&num_docs);
	inv_index->num_docs = num_docs;
	int i;
	for(i = 0; i < num_docs ;i++)
	{
		int doc_len;
		fscanf(reader,"%d ",&doc_len);

		int j;
		for( j = 0 ; j < doc_len-1 ; j++)
		{
			char buffer[1000];
			fscanf(reader,"%s ",buffer);
			int index = g_hash_table_lookup(inv_index->vocab_hash,buffer) - 1;
			if(index<0)
				printf("word %s not found in vocabulary!!\n",buffer );
			else
				insert_new(i,&inv_index->bloom_inv_ind[index],0);

			// printf("%s-%d\n",buffer,index, buffer));
		}

		char buffer[1000];
		fscanf(reader,"%s\n",buffer);
		int index = g_hash_table_lookup(inv_index->vocab_hash,buffer) - 1;
		if(index<0)
			printf("word %s not found in vocabulary!!\n",buffer );
		else
			insert_new(i,&inv_index->bloom_inv_ind[index],0);

		// printf("%s-%d\n",buffer,index, buffer));
	}
	int k;
	for( k = 0 ; k < inv_index->vocab_size ; k++)
		finish_insertion(&inv_index->bloom_inv_ind[k]);
}



int main(int argc, char** argv) 
{
	init_bloomParameters(1024,3,1,10,0.001);
	seiveInitial();
	invIndex inv_index;
	read_vocab("vocabulary.txt",&inv_index);
	read_docDB("docDB.txt",&inv_index);
	return 0;
}


