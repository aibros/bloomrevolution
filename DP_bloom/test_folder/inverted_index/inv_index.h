#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include "../../count_bloom.h"

typedef struct invIndex
{
	GHashTable* vocab_hash;
	char** vocab;
	b_node* bloom_inv_ind;
	int vocab_size;
	int num_docs;
}invIndex;


void read_vocab(const char * vocab_file, invIndex* inv_index);

void read_docDB(const char * docDB_file, invIndex* inv_index);
