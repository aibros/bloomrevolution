#include "inv_index.h"


void read_data(const char * data_file, invIndex* inv_index)
{
	double start_time = omp_get_wtime();

	printf("reading data file : %s !!\n",data_file);
	FILE *reader;
	reader = fopen(data_file,"r");
	LONG vocab_len = 0;

	inv_index->vocab_hash = g_hash_table_new(g_str_hash, g_str_equal);								//string to int dictionary
	
	while(1)		//creating the dictionary and obtaining the size of vocabulary of hashtags
	{
		char *hash_tag = (char *)malloc(sizeof(char)*256);
		LONG time_stamp = 0;
		LONG uid = 0;
		int ret = fscanf(reader, "%s\t%ld\t%ld\n", hash_tag, &time_stamp, &uid);

		if(ret < 1)																// fscanf returns the number of objects read
			break;
		int index = g_hash_table_lookup(inv_index->vocab_hash,hash_tag) - 1;	//lookup returns 0 if not found

		if(index < 0)
		{
			g_hash_table_insert(inv_index->vocab_hash, hash_tag, vocab_len+1);
			vocab_len++;
		}
		else
			free(hash_tag);
	}

	printf("Time for hashtable creation:: %f\n",omp_get_wtime()-start_time);

	fclose(reader);
	reader = fopen(data_file,"r");
	inv_index->bloom_inv_ind = (b_node *)malloc(vocab_len*sizeof(b_node));
	inv_index->vocab_size = vocab_len;
	inv_index->elems = (GArray **)malloc(vocab_len*sizeof(GArray *));


	int i;
	for(i = 0 ; i < vocab_len ; i++){
		init(&inv_index->bloom_inv_ind[i]);
		grow_full_tree(&inv_index->bloom_inv_ind[i],0);
		// printf("%f\n", omp_get_wtime()-start_time);
		inv_index->elems[i] = g_array_new(FALSE, FALSE, sizeof(glong));
	}
	
	
	while(1)
	{
		char *hash_tag = (char *)malloc(sizeof(char)*256);
		LONG time_stamp = 0;
		LONG uid = 0;
		int ret = fscanf(reader, "%s\t%ld\t%ld\n", hash_tag, &time_stamp, &uid);

		if(ret < 1)																// fscanf returns the number of objects read
			break;

		int index = g_hash_table_lookup(inv_index->vocab_hash,hash_tag) - 1;	//lookup returns 0 if not found
		if(index >= 0)
		{
			insert_new(uid,&inv_index->bloom_inv_ind[index],0);
			g_array_append_val(inv_index->elems[index] , uid);
		}
		else
			printf("hashtag not found in the hashtable , issue in reading vocab!!\n");

		free(hash_tag);
	}

	int k;
	for( k = 0 ; k < inv_index->vocab_size ; k++)
		finish_insertion(&inv_index->bloom_inv_ind[k]);
	printf("Time for index creation:: %f\n",omp_get_wtime()-start_time);

	printf("data file read and twitter index created: %s with total number of hashtags : %d!!\n", data_file, vocab_len);
}

void query_intersect( char ** query_terms, int query_len, invIndex* inv_index, int *size_res, const char* output_file){
	double start_time = omp_get_wtime();

	b_node * query_bloom;
	b_node * temp_bloom;
	query_bloom = (b_node *)malloc(sizeof(b_node));
	init(query_bloom);

	int start = 0;

	int min_sized_ind = 999999999;
	int min_size = 999999999;

	int i = 0;
	for( i = 0 ; i < query_len ; i++ ){
		int index = g_hash_table_lookup( inv_index->vocab_hash,query_terms[i] ) - 1;
		if( index >= 0 ){
			if(start == 0){
				temp_bloom = &inv_index->bloom_inv_ind[index];
				start = 1;
			}
			else if(start == 1)
			{
				intersect_sans_expansion(&inv_index->bloom_inv_ind[index], temp_bloom, query_bloom);
				start = 2;
			}
			else
				intersect_sans_expansion(&inv_index->bloom_inv_ind[index], query_bloom, query_bloom);


			if(inv_index->elems[i]->len < min_size){
				min_size = inv_index->elems[index]->len;
				min_sized_ind = index;
			}
		}
		else
		{
			printf("query term not present, empty output so output file not created: %s\n",query_terms[i] );
			return;
		}
	}

	printf("Time for intersection of the bloom filters for the query terms:: %f\n",omp_get_wtime()-start_time);

	double time_before_dict_attack = omp_get_wtime();
	//dictionary attack
	FILE* writer = fopen(output_file,"a");
	fprintf(writer, "\n\n" );
	LONG count = 0;
	LONG j = 0;

	for( j = 0 ; j <= min_size ; j++ ){
		LONG elem = g_array_index (inv_index->elems[min_sized_ind], glong, j);
		if(is_in(elem,query_bloom)){
			// printf("okey %d %d \n",i,is_in(i,query_bloom) );
			fprintf(writer,"%ld\n",elem);
			count++;
		}
	}
	printf("Time for dictionary attack on resultant bloom filter:: %f\n",omp_get_wtime()-time_before_dict_attack);

	*size_res = count;
	fprintf(writer, "%f\n",omp_get_wtime()-start_time);
	fclose(writer);
	printf("Time for query_intersect:: %f\n",omp_get_wtime()-start_time);
}

void query_basic(char **query_terms, int query_len, invIndex* inv_index, int *size_res, const char* output_file){

	double start_time = omp_get_wtime();


	int min_sized_ind = 999999999;
	int min_size = 999999999;

	int i = 0;
	for( i = 0 ; i < query_len ; i++ ){
		int index = g_hash_table_lookup( inv_index->vocab_hash,query_terms[i] ) - 1;
		if(index > 0){
			if(inv_index->elems[i]->len < min_size){
				min_size = inv_index->elems[index]->len;
				min_sized_ind = index;
			}
		}
	}



	FILE* writer = fopen(output_file,"a");
	fprintf(writer, "\n\n" );
	LONG count = 0;


	for( i = 0 ; i <= min_size ; i++ ){			//simple and queries based dictionary attack
		int check = 1;
		int j = 0;
		LONG elem = g_array_index(inv_index->elems[min_sized_ind], glong, i);

		for( j = 0 ; j < query_len ; j++ ){
			int index = g_hash_table_lookup( inv_index->vocab_hash,query_terms[j] ) - 1;
			
			if(index < 0){
				check = 0;
				break;
			}
			if(!is_in(elem,&inv_index->bloom_inv_ind[index])){
				check = 0;
				break;
			}
		}

		if(check == 1){
			fprintf(writer, "%ld\n",elem );
			count++;
		}
	}

	*size_res = count;
	fprintf(writer, "%f\n",omp_get_wtime()-start_time);
	fclose(writer);
	printf("Time for query_basic:: %f\n",omp_get_wtime()-start_time);
}

void test_suite(invIndex *inv_index, const char *query_file)
{
	printf("starting the queries from file ::%s \n",query_file );

	FILE *query_reader;
	query_reader = fopen("query","r");

	int num_queries = 0;
	fscanf(query_reader,"%d\n",&num_queries);

	int i,j = 0;
	for( i = 0 ; i < num_queries ; i++ ){

		int num_terms = 0;
		fscanf(query_reader,"%d ", &num_terms);

		char **query_terms = (char **)malloc( sizeof(char *)*num_terms );

		for( j = 0 ; j < num_terms-1 ; j++){
			query_terms[j] = (char *)malloc(sizeof(char)*256);
			fscanf(query_reader,"%s ",query_terms[j]);
			// printf("%s\n",query_terms[j] );
		}

		query_terms[j] = (char *)malloc(sizeof(char)*256);
		fscanf(query_reader, "%s\n", query_terms[j]);
		// printf("%s\n",query_terms[j] );

		int size1,size2 = 0;
		query_intersect(query_terms, num_terms, inv_index, &size1,"output/query_intersect.txt" );
		query_basic(query_terms, num_terms, inv_index, &size2,"output/query_basic.txt" );

	}

	fclose(query_reader);
	printf("all the queries are done\n");
}

void free_inv_index(invIndex *inv_index)
{
	LONG i;
	for( i = 0 ; i < inv_index->vocab_size ; i++ )
	{
		g_array_free(inv_index->elems[i],TRUE);
	}
}

int main(int argc, char** argv) {
	srand(time(0));
	init_bloomParameters(1024,3,1,7,0.001);
	seiveInitial();
	invIndex inv_index;
	read_data("../../../../doc_dataset/sample_dif_timeline1s",&inv_index);
	test_suite(&inv_index,"query");

	return 0;
}


