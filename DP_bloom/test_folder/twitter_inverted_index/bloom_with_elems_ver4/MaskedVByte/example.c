#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "varintencode.h"
#include "varintdecode.h"


int cmpfunc (const void * a, const void * b)
{
   return ( *(int*)a - *(int*)b );
}


int main() {
	int N = 579;
	uint32_t * datain = malloc(N * sizeof(uint32_t));
	uint8_t * compressedbuffer = malloc(2*N * sizeof(uint32_t));
	uint32_t * recovdata = malloc(N * sizeof(uint32_t));


	// for (int k = 0; k < N; ++k)
	// 	datain[k] = 4247483647;
	FILE *reader;
	reader = fopen("out.txt","r");
	int count = 0;
	while(1)
	{
		int uid;
		int ret = fscanf(reader,"%d\n",&uid);
		// printf("%d -- %d\n",count,uid );
		datain[count] = uid;
		count++;
		if(ret < 0)
			break;
	}

  	qsort(datain, 577, sizeof(uint32_t), cmpfunc);



	size_t compsize = vbyte_encode_delta(datain, 579, compressedbuffer,0); // encoding

	printf("%ld\n",compsize );
	// here the result is stored in compressedbuffer using compsize bytes
	size_t compsize2 = masked_vbyte_decode_delta(compressedbuffer, recovdata,
					N,0); // decoding (fast)

  	while(count-->0)
  		printf("%d -- %d\n",count,recovdata[count] );

	printf("%ld\n",compsize2 );

	assert(compsize == compsize2);
	free(datain);
	free(compressedbuffer);
	free(recovdata);
	printf("Compressed %d integers down to %d bytes.\n",N,(int) compsize);
	return 0;
}

