#ifndef ARRAY_EXTEN_H
#define ARRAY_EXTEN_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

// #include "../temp/conf.h"
// #include "../temp/bitpack.h"
// #include "../temp/vint.h"		
// #include "../temp/bitutil.h"
// #include "../temp/vp4.h"

#include "../LittleIntPacker/include/bitpacking.h"
#include "../MaskedVByte/include/varintdecode.h"
#include "../MaskedVByte/include/varintencode.h"

typedef uint32_t LONG;

typedef struct my_array_extended
{
	unsigned int* elements;
	unsigned int curr_size;
	unsigned int max_size;
}my_array_extended;

typedef struct my_array_long_extended
{
	LONG* elements;
	unsigned int curr_size;
	unsigned int max_size;
}my_array_long_extended;

typedef struct comp_array_long
{
	unsigned char *comp_elements;
	unsigned int size;
}comp_array_long;

// static __m128i shuffle_mask[16]; // precomputed dictionary

void arrayInt_init_extended(my_array_extended* array);
void arrayInt_addElement_extended(my_array_extended* array,unsigned int element);
void arrayInt_free_extended(my_array_extended* array);
int arrayInt_find_extended(my_array_extended* array,unsigned int element);
void print_elems_extended(my_array_extended* array);

void arrayLong_init_extended(my_array_long_extended* array);
void arrayLong_addElement_extended(my_array_long_extended* array,LONG element);
void arrayLong_free_extended(my_array_long_extended* array);
int arrayLong_find_extended(my_array_long_extended* array,LONG element);
void arrayLong_extend_extended(my_array_long_extended *array, int size);

int compress_elems_long(my_array_long_extended* array, comp_array_long* c_array);
int decompress_elems_long(comp_array_long *c_array, my_array_long_extended *array);

#endif