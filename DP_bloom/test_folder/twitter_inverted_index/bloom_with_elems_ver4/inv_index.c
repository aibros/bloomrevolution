#include "inv_index.h"


LONG_INT MAX_UID = 0;

FILE *record;

char* concat(const char *s1, const char *s2)
{
    char *result = malloc(strlen(s1)+strlen(s2)+1);//+1 for the zero-terminator
    //in real code you would check for errors in malloc here
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

void read_data(const char * data_file, invIndex* inv_index)
{
	double start_time = omp_get_wtime();

	// printf("reading data file : %s !!\n",data_file);
	inv_index->vocab_hash = g_hash_table_new(g_str_hash, g_str_equal);								//string to int dictionary
	LONG_INT num_leaves = 0;

	FILE *reader;
	reader = fopen(data_file,"r");

	uint32_t vocab_len;
	fscanf(reader,"%u\n",&vocab_len);

	inv_index->bloom_inv_ind = (b_node *)malloc(vocab_len*sizeof(b_node));
	inv_index->vocab_size = vocab_len;

	my_array_long_extended **temp_store = (my_array_long_extended **)malloc(vocab_len*sizeof(my_array_long_extended *));

	// printf("initiating  bloom filters\n");
	
	int i;
	for(i = 0 ; i < vocab_len ; i++){
		init_without_bloom(&inv_index->bloom_inv_ind[i]);
		temp_store[i] = (my_array_long_extended *)malloc(sizeof(my_array_long_extended));
		arrayLong_init_extended(temp_store[i]);
	}

	LONG_INT counter = 0;
	while(1)
	{
		char *hash_tag = (char *)malloc(sizeof(char)*256);
		LONG_INT time_stamp = 0;
		LONG_INT uid = 0;
		int ret;
		if(sizeof(LONG_INT)==8)
			ret = fscanf(reader, "%s\t%ld\t%ld\n", hash_tag, &time_stamp, &uid);
		else if(sizeof(LONG_INT)==4)
			ret = fscanf(reader, "%s\t%u\t%u\n", hash_tag, &time_stamp, &uid);

		if(ret < 1)																// fscanf returns the number of objects read
			break;

		if(uid > MAX_UID)
			MAX_UID = uid;
		// if(uid > 2147483645)
		// 	continue;
		int index = g_hash_table_lookup(inv_index->vocab_hash,hash_tag) - 1;	//lookup returns 0 if not found

		if(index < 0)
		{
			g_hash_table_insert(inv_index->vocab_hash, hash_tag, counter+1);
			index = counter;

			counter++;
			// if(!is_in(uid,&inv_index->bloom_inv_ind[index]))
			// {
				// insert(uid ,&inv_index->bloom_inv_ind[index]);
				arrayLong_addElement_extended(temp_store[index], uid );
			// }
		}
		else if(index >= 0)
		{
			// if(!is_in(uid,&inv_index->bloom_inv_ind[index]))
			// {
				// insert(uid ,&inv_index->bloom_inv_ind[index]);
				arrayLong_addElement_extended(temp_store[index], uid );
			// }
			free(hash_tag);												//don't free in if case as it needs to be stored in hashtable
		}
	}

	inv_index->vocab_current = counter;

	LONG_INT compressed_bytes = 0;
	LONG_INT uncompressed_bytes = 0;

	inv_index->elems = (comp_array_long **)malloc(vocab_len*sizeof(comp_array_long *));

	//handling bloom filters here
	// printf("setting up bloom filters\n");

	for( i = 0 ; i < vocab_len ; i++ )
	{
		if(temp_store[i]->curr_size > 0)
		{
			LONG_INT num_elems = temp_store[i]->curr_size;
			LONG_INT maxLevel = num_elems/bloomParam->FILL_THRESHOLD;
			if (maxLevel > bloomParam->NUM_PARTITION)
				maxLevel = bloomParam->NUM_PARTITION;
			grow_full_tree_upto_level(&inv_index->bloom_inv_ind[i],0,maxLevel);
			LONG_INT j = 0;
			for( j = 0 ; j < num_elems ; j++)
				insert_at_level(temp_store[i]->elements[j],&inv_index->bloom_inv_ind[i],0,maxLevel);
			finish_insertion(&inv_index->bloom_inv_ind[i]);
			num_leaves += count_leaves(&inv_index->bloom_inv_ind[i]);
		}


		// print_elems_long(temp_store[i]);
		inv_index->elems[i] = (comp_array_long *)malloc(sizeof(comp_array_long));
		
		// printf("iter -- %d  size -- %d\n",i,temp_store[i]->curr_size );
		LONG_INT c_size = compress_elems_long(temp_store[i],inv_index->elems[i]);
		compressed_bytes += c_size;
		uncompressed_bytes += sizeof(LONG_INT)*temp_store[i]->curr_size;

		arrayLong_free_extended(temp_store[i]);
	}
	
	free(temp_store);

	LONG_INT memory = compressed_bytes + num_leaves*(bloomParam->SIZE_BLOOM/8);
	fprintf(record, "time,%f,memory,%lld\n",omp_get_wtime()-start_time,memory);


	// printf("uncompressed_bytes - %ld compressed_bytes - %ld num_leaves - %ld bloom_memory - %ld\n",uncompressed_bytes,compressed_bytes, num_leaves, num_leaves*(bloomParam->SIZE_BLOOM/8));

	// printf("Time for index creation:: %f\n",omp_get_wtime()-start_time);

	// printf("data file read and twitter index created: %s with total number of hashtags : %d %d!!\n", data_file, inv_index->vocab_size,inv_index->vocab_current);
}

// things to try
// removing redundant in actual
// having a look at intersection code

void init_updates(invIndex *inv_index, int length)
{
	inv_index->updates = (indUpdate *)malloc(sizeof(indUpdate)*length);
	inv_index->update_len_current = 0;
	inv_index->update_len_max = length;
}

void update( const char * data_file ,invIndex * inv_index)
{
	if(inv_index->update_len_current == inv_index->update_len_max)
	{
		printf("no more space for further updates!! max updates - %d(to be fixed in future)\n", inv_index->update_len_max);
		exit(0);
	}

	double start_time = omp_get_wtime();
	LONG_INT num_leaves = 0;
	LONG_INT vocab_len;

	FILE *reader;
	reader = fopen(data_file,"r");
	fscanf(reader,"%u\n",&vocab_len);
	if(vocab_len != inv_index->vocab_size)
		printf("vocabulary sizes are not matching\n");
	vocab_len = inv_index->vocab_size;

	my_array_long_extended **temp_store = (my_array_long_extended **)malloc(vocab_len*sizeof(my_array_long_extended *));

	int i;
	for(i = 0 ; i < inv_index->vocab_size ; i++){
		temp_store[i] = (my_array_long_extended *)malloc(sizeof(my_array_long_extended));
		arrayLong_init_extended(temp_store[i]);
	}

	LONG_INT counter = inv_index->vocab_current;
	while(1)
	{
		char *hash_tag = (char *)malloc(sizeof(char)*256);
		LONG_INT time_stamp = 0;
		LONG_INT uid = 0;
		int ret;
		if(sizeof(LONG_INT)==8)
			ret = fscanf(reader, "%s\t%ld\t%ld\n", hash_tag, &time_stamp, &uid);
		else if(sizeof(LONG_INT)==4)
			ret = fscanf(reader, "%s\t%u\t%u\n", hash_tag, &time_stamp, &uid);

		if(ret < 1)																// fscanf returns the number of objects read
			break;

		long long int index = g_hash_table_lookup(inv_index->vocab_hash,hash_tag) - 1;	//lookup returns 0 if not found

	
		if(index < 0)
		{
			g_hash_table_insert(inv_index->vocab_hash, hash_tag, counter+1);
			index = counter;
			counter++;
			// if(!is_in(uid,&inv_index->bloom_inv_ind[index]))
			// {
				// insert(uid ,&inv_index->bloom_inv_ind[index]);
				arrayLong_addElement_extended(temp_store[index], uid );
			// }
		}
		else if(index >= 0)
		{
			// if(!is_in(uid,&inv_index->bloom_inv_ind[index]))
			// {
				// insert(uid ,&inv_index->bloom_inv_ind[index]);
				arrayLong_addElement_extended(temp_store[index], uid );
			// }
			free(hash_tag);
		}

	}

	inv_index->vocab_current = counter;

	LONG_INT compressed_bytes = 0;
	LONG_INT uncompressed_bytes = 0;


	int current = inv_index->update_len_current;
	inv_index->updates[current].elems = (comp_array_long **)malloc(vocab_len*sizeof(comp_array_long *));

	for( i = 0 ; i < vocab_len ; i++ )
	{

		if(temp_store[i]->curr_size > 0)
		{
			LONG_INT num_elems = temp_store[i]->curr_size;
			LONG_INT maxLevel = num_elems/bloomParam->FILL_THRESHOLD;
			if (maxLevel > bloomParam->NUM_PARTITION)
				maxLevel = bloomParam->NUM_PARTITION;

			if(inv_index->bloom_inv_ind[i].bf == NULL)
			{
				grow_full_tree_upto_level(&inv_index->bloom_inv_ind[i],0,maxLevel);
				LONG_INT j = 0;
				for( j = 0 ; j < num_elems ; j++)
					insert_at_level(temp_store[i]->elements[j],&inv_index->bloom_inv_ind[i],0,maxLevel);

				finish_insertion(&inv_index->bloom_inv_ind[i]);
				num_leaves += count_leaves(&inv_index->bloom_inv_ind[i]);
			}
			else
			{
				LONG_INT j = 0;
				for( j = 0 ; j < num_elems ; j++)
					insert(temp_store[i]->elements[j],&inv_index->bloom_inv_ind[i]);
			}

		}

		inv_index->updates[current].elems[i] = (comp_array_long *)malloc(sizeof(comp_array_long));
		
		// printf("iter -- %d  size -- %d\n",i,temp_store[i]->curr_size );
		LONG_INT c_size = compress_elems_long(temp_store[i],inv_index->updates[current].elems[i]);
		compressed_bytes += c_size;
		uncompressed_bytes += sizeof(LONG_INT)*temp_store[i]->curr_size;

		arrayLong_free_extended(temp_store[i]);
	}

	free(temp_store);

	inv_index->update_len_current++;

	printf("uncompressed_bytes of elements to be inserted - %ld compressed_bytes of elements to be inserted- %ld\n",uncompressed_bytes,compressed_bytes );

	printf("Time for index creation:: %f\n",omp_get_wtime()-start_time);

	LONG_INT memory = compressed_bytes + num_leaves*(bloomParam->SIZE_BLOOM/8);
	fprintf(record, "update: time :: %f extra-memory :: %lld\n",omp_get_wtime()-start_time,memory);
}

void query_intersect( char ** query_terms, LONG_INT query_len, invIndex* inv_index, LONG_INT *size_res, const char* output_file){
	double start_time = omp_get_wtime();

	b_node * query_bloom;
	b_node * temp_bloom;
	query_bloom = (b_node *)malloc(sizeof(b_node));
	init(query_bloom);

	LONG_INT start = 0;

	LONG_INT min_sized_ind = 999999999;
	LONG_INT min_size = 999999999;

	LONG_INT i = 0;
	for( i = 0 ; i < query_len ; i++ ){
		long long int index = g_hash_table_lookup( inv_index->vocab_hash,query_terms[i] ) - 1;

		if( index >= 0 ){
			if(start == 0){
				// union_bloom(&inv_index->bloom_inv_ind[index], query_bloom, query_bloom);	//first needs to be taken as it is
				temp_bloom = &inv_index->bloom_inv_ind[index];
				// printf("--%d\n",count_elems(temp_bloom) );

				start = 1;
			}
			else if(start == 1)
			{

				intersect_sans_expansion(&inv_index->bloom_inv_ind[index], temp_bloom, query_bloom);
				// intersection_with_compression(&inv_index->bloom_inv_ind[index], temp_bloom, query_bloom);
				// printf("--%d\n",count_elems(query_bloom) );
				
				start = 2;
			}
			else
			{
				b_node *temp = (b_node *)malloc(sizeof(b_node));
				init_without_bloom(temp);
				intersect_sans_expansion(&inv_index->bloom_inv_ind[index], query_bloom, temp);
				// intersection_with_compression(&inv_index->bloom_inv_ind[index], query_bloom, temp);
				// printf("--%d\n",count_elems(temp) );

				free_bloom(query_bloom);
				query_bloom = temp;

			}
			// finish_insertion(query_bloom);
			// printf("--%d\n",inv_index->elems[index]->size );
			LONG_INT size_ind = inv_index->elems[index]->size;
			int k;
			for(k = 0; k < inv_index->update_len_current ; k++)
				size_ind += inv_index->updates[k].elems[index]->size;	

			if(size_ind < min_size){
				min_size = size_ind;
				min_sized_ind = index;
			}
		}
		else
		{
			printf("query term not present, empty output so output file not created: %s\n",query_terms[i] );
			return;
		}
	}
	if(query_len == 1)
		query_bloom = temp_bloom;


	// printf("Time for intersection of the bloom filters for the query terms:: %f\n",omp_get_wtime()-start_time);
	// double time_before_dict_attack = omp_get_wtime();

	//dictionary attack
	FILE* writer = fopen(output_file,"a");
	fprintf(writer, "********\n\n" );
	LONG_INT count = 0;
	LONG_INT j = 0;

	my_array_long_extended * temp_array = (my_array_long_extended *)malloc(sizeof(my_array_long_extended));
	decompress_elems_long(inv_index->elems[min_sized_ind],temp_array);

	// printf("Time for decompression:: %f\n",omp_get_wtime()-time_before_dict_attack);

	for( j = 0 ; j <= temp_array->curr_size ; j++ ){
		LONG_INT elem = temp_array->elements[j];
		if(is_in(elem,query_bloom)){
			fprintf(writer,"%ld\n",elem);
			// printf("%ld\n",elem);
			count++;
		}
	}
	int k;
	for( k = 0 ; k < inv_index->update_len_current ; k++)
	{
		my_array_long_extended * upd_array = (my_array_long_extended *)malloc(sizeof(my_array_long_extended));
		decompress_elems_long(inv_index->updates[k].elems[min_sized_ind],upd_array);

		for( j = 0 ; j <= upd_array->curr_size ; j++ ){
			LONG_INT elem = upd_array->elements[j];

			if(is_in(elem,query_bloom)){
				fprintf(writer,"%ld\n",elem);
				count++;
			}
		}
	}

	*size_res = count;
	fclose(writer);
	free(temp_array);
	// printf("Time for dictionary attack and decompression on resultant bloom filter:: %f %f\n",omp_get_wtime()-time_before_dict_attack,omp_get_wtime()-start_time);
	// printf("Time for query_intersect of size %d:: %f\n",count,omp_get_wtime()-start_time);
	fprintf(record,"time,%f,query_result_size,%llu,terms,%lld\n", omp_get_wtime()-start_time,count,query_len);
	// printf("query : time : %f query_result_size : %llu\n", omp_get_wtime()-start_time,count);
	// fprintf(writer, "%f\n",omp_get_wtime()-start_time);
	// printf("Time for query_intersect of size %d:: %f\n",count,omp_get_wtime()-start_time);
}

void query_intersect2( char ** query_terms, LONG_INT query_len, invIndex* inv_index, LONG_INT *size_res, const char* output_file){
	double start_time = omp_get_wtime();

	b_node * query_bloom;
	b_node * temp_bloom;
	query_bloom = (b_node *)malloc(sizeof(b_node));
	init(query_bloom);

	LONG_INT start = 0;

	LONG_INT min_sized_ind = 999999999;
	LONG_INT min_size = 999999999;

	LONG_INT i = 0;
	for( i = 0 ; i < query_len ; i++ ){
		long long int index = g_hash_table_lookup( inv_index->vocab_hash,query_terms[i] ) - 1;

		if( index >= 0 ){
			if(start == 0){
				// union_bloom(&inv_index->bloom_inv_ind[index], query_bloom, query_bloom);	//first needs to be taken as it is
				temp_bloom = &inv_index->bloom_inv_ind[index];
				// printf("--%d\n",count_elems(temp_bloom) );

				start = 1;
			}
			else if(start == 1)
			{

				// intersect_sans_expansion(&inv_index->bloom_inv_ind[index], temp_bloom, query_bloom);
				intersection_with_compression(&inv_index->bloom_inv_ind[index], temp_bloom, query_bloom);
				// printf("--%d\n",count_elems(query_bloom) );
				
				start = 2;
			}
			else
			{
				b_node *temp = (b_node *)malloc(sizeof(b_node));
				init_without_bloom(temp);
				// intersect_sans_expansion(&inv_index->bloom_inv_ind[index], query_bloom, temp);
				intersection_with_compression(&inv_index->bloom_inv_ind[index], query_bloom, temp);
				// printf("--%d\n",count_elems(temp) );

				free_bloom(query_bloom);
				query_bloom = temp;

			}
			// finish_insertion(query_bloom);
			// printf("--%d\n",inv_index->elems[index]->size );
			LONG_INT size_ind = inv_index->elems[index]->size;
			int k;
			for(k = 0; k < inv_index->update_len_current ; k++)
				size_ind += inv_index->updates[k].elems[index]->size;	

			if(size_ind < min_size){
				min_size = size_ind;
				min_sized_ind = index;
			}
		}
		else
		{
			printf("query term not present, empty output so output file not created: %s\n",query_terms[i] );
			return;
		}
	}
	if(query_len == 1)
		query_bloom = temp_bloom;


	// printf("Time for intersection of the bloom filters for the query terms:: %f\n",omp_get_wtime()-start_time);
	// double time_before_dict_attack = omp_get_wtime();

	//dictionary attack
	FILE* writer = fopen(output_file,"a");
	fprintf(writer, "********\n\n" );
	LONG_INT count = 0;
	LONG_INT j = 0;

	my_array_long_extended * temp_array = (my_array_long_extended *)malloc(sizeof(my_array_long_extended));
	decompress_elems_long(inv_index->elems[min_sized_ind],temp_array);

	// printf("Time for decompression:: %f\n",omp_get_wtime()-time_before_dict_attack);

	for( j = 0 ; j <= temp_array->curr_size ; j++ ){
		LONG_INT elem = temp_array->elements[j];
		if(is_in(elem,query_bloom)){
			fprintf(writer,"%ld\n",elem);
			// printf("%ld\n",elem);
			count++;
		}
	}
	int k;
	for( k = 0 ; k < inv_index->update_len_current ; k++)
	{
		my_array_long_extended * upd_array = (my_array_long_extended *)malloc(sizeof(my_array_long_extended));
		decompress_elems_long(inv_index->updates[k].elems[min_sized_ind],upd_array);

		for( j = 0 ; j <= upd_array->curr_size ; j++ ){
			LONG_INT elem = upd_array->elements[j];

			if(is_in(elem,query_bloom)){
				fprintf(writer,"%ld\n",elem);
				count++;
			}
		}
	}

	*size_res = count;
	fclose(writer);
	free(temp_array);
	// printf("Time for dictionary attack and decompression on resultant bloom filter:: %f %f\n",omp_get_wtime()-time_before_dict_attack,omp_get_wtime()-start_time);
	// printf("Time for query_intersect of size %d:: %f\n",count,omp_get_wtime()-start_time);
	fprintf(record,"time,%f,query_result_size,%llu,terms,%lld\n", omp_get_wtime()-start_time,count,query_len);
	// printf("query : time : %f query_result_size : %llu\n", omp_get_wtime()-start_time,count);
	// fprintf(writer, "%f\n",omp_get_wtime()-start_time);
	// printf("Time for query_intersect of size %d:: %f\n",count,omp_get_wtime()-start_time);
}

void query_intersect3( char ** query_terms, LONG_INT query_len, invIndex* inv_index, LONG_INT *size_res, const char* output_file){
	double start_time = omp_get_wtime();

	b_node * query_bloom;
	b_node ** temp_bloom = (b_node **)malloc(query_len*sizeof(b_node *));
	query_bloom = (b_node *)malloc(sizeof(b_node));
	init(query_bloom);

	LONG_INT start = 0;

	LONG_INT min_sized_ind = 999999999;
	LONG_INT min_size = 999999999;

	LONG_INT i = 0;
	for( i = 0 ; i < query_len ; i++ ){
		long long int index = g_hash_table_lookup( inv_index->vocab_hash,query_terms[i] ) - 1;
		if( index >= 0 ){
			temp_bloom[i] = &inv_index->bloom_inv_ind[index];
			if(temp_bloom[i] == NULL)
				printf("okay\n");

			LONG_INT size_ind = inv_index->elems[index]->size;
			int k;
			for(k = 0; k < inv_index->update_len_current ; k++)
				size_ind += inv_index->updates[k].elems[index]->size;	

			if(size_ind < min_size){
				min_size = size_ind;
				min_sized_ind = index;
			}
		}
		else
		{
			printf("query term not present, empty output so output file not created: %s\n",query_terms[i] );
			return;
		}
	}

	
	// printf("okay what is this  %d\n",temp_bloom[8] );
	// exit(0);
	intersect_k(temp_bloom, query_len, query_bloom);


	// printf("Time for intersection of the bloom filters for the query terms:: %f\n",omp_get_wtime()-start_time);
	// double time_before_dict_attack = omp_get_wtime();

	//dictionary attack
	FILE* writer = fopen(output_file,"a");
	fprintf(writer, "********\n\n" );
	LONG_INT count = 0;
	LONG_INT j = 0;

	my_array_long_extended * temp_array = (my_array_long_extended *)malloc(sizeof(my_array_long_extended));
	decompress_elems_long(inv_index->elems[min_sized_ind],temp_array);

	// printf("Time for decompression:: %f\n",omp_get_wtime()-time_before_dict_attack);

	for( j = 0 ; j <= temp_array->curr_size ; j++ ){
		LONG_INT elem = temp_array->elements[j];
		if(is_in(elem,query_bloom)){
			fprintf(writer,"%ld\n",elem);
			// printf("%ld\n",elem);
			count++;
		}
	}
	int k;
	for( k = 0 ; k < inv_index->update_len_current ; k++)
	{
		my_array_long_extended * upd_array = (my_array_long_extended *)malloc(sizeof(my_array_long_extended));
		decompress_elems_long(inv_index->updates[k].elems[min_sized_ind],upd_array);

		for( j = 0 ; j <= upd_array->curr_size ; j++ ){
			LONG_INT elem = upd_array->elements[j];

			if(is_in(elem,query_bloom)){
				fprintf(writer,"%ld\n",elem);
				count++;
			}
		}
	}

	*size_res = count;
	fclose(writer);
	free(temp_array);
	// printf("Time for dictionary attack and decompression on resultant bloom filter:: %f %f\n",omp_get_wtime()-time_before_dict_attack,omp_get_wtime()-start_time);
	// printf("Time for query_intersect of size %d:: %f\n",count,omp_get_wtime()-start_time);
	fprintf(record,"time,%f,query_result_size,%llu,terms,%lld\n", omp_get_wtime()-start_time,count,query_len);
	// printf("query : time : %f query_result_size : %llu\n", omp_get_wtime()-start_time,count);
	// fprintf(writer, "%f\n",omp_get_wtime()-start_time);
	// printf("Time for query_intersect of size %d:: %f\n",count,omp_get_wtime()-start_time);
}

void query_intersect4( char ** query_terms, LONG_INT query_len, invIndex* inv_index, LONG_INT *size_res, const char* output_file){
	double start_time = omp_get_wtime();

	b_node * query_bloom;
	b_node * temp_bloom;
	query_bloom = (b_node *)malloc(sizeof(b_node));
	init(query_bloom);

	LONG_INT start = 0;

	LONG_INT min_sized_ind = 999999999;
	LONG_INT min_size = 999999999;

	LONG_INT i = 0;
	for( i = 0 ; i < query_len ; i++ ){
		long long int index = g_hash_table_lookup( inv_index->vocab_hash,query_terms[i] ) - 1;

		if( index >= 0 ){
			if(start == 0){
				// union_bloom(&inv_index->bloom_inv_ind[index], query_bloom, query_bloom);	//first needs to be taken as it is
				temp_bloom = &inv_index->bloom_inv_ind[index];
				// printf("--%d\n",count_elems(temp_bloom) );

				start = 1;
			}
			else if(start == 1)
			{

				intersect_sans_expansion(&inv_index->bloom_inv_ind[index], temp_bloom, query_bloom);
				// intersection_with_compression(&inv_index->bloom_inv_ind[index], temp_bloom, query_bloom);
				// printf("--%d\n",count_elems(query_bloom) );
				
				start = 2;
			}
			else
			{
				b_node *temp = (b_node *)malloc(sizeof(b_node));
				init_without_bloom(temp);
				intersect_sans_expansion(&inv_index->bloom_inv_ind[index], query_bloom, temp);
				// intersection_with_compression(&inv_index->bloom_inv_ind[index], query_bloom, temp);
				// printf("--%d\n",count_elems(temp) );

				free_bloom(query_bloom);
				query_bloom = temp;

			}
			finish_insertion(query_bloom);
			// printf("--%d\n",inv_index->elems[index]->size );
			LONG_INT size_ind = inv_index->elems[index]->size;
			int k;
			for(k = 0; k < inv_index->update_len_current ; k++)
				size_ind += inv_index->updates[k].elems[index]->size;	

			if(size_ind < min_size){
				min_size = size_ind;
				min_sized_ind = index;
			}
		}
		else
		{
			printf("query term not present, empty output so output file not created: %s\n",query_terms[i] );
			return;
		}
	}
	if(query_len == 1)
		query_bloom = temp_bloom;


	// printf("Time for intersection of the bloom filters for the query terms:: %f\n",omp_get_wtime()-start_time);
	// double time_before_dict_attack = omp_get_wtime();

	//dictionary attack
	FILE* writer = fopen(output_file,"a");
	fprintf(writer, "********\n\n" );
	LONG_INT count = 0;
	LONG_INT j = 0;

	my_array_long_extended * temp_array = (my_array_long_extended *)malloc(sizeof(my_array_long_extended));
	decompress_elems_long(inv_index->elems[min_sized_ind],temp_array);

	// printf("Time for decompression:: %f\n",omp_get_wtime()-time_before_dict_attack);

	for( j = 0 ; j <= temp_array->curr_size ; j++ ){
		LONG_INT elem = temp_array->elements[j];
		if(is_in(elem,query_bloom)){
			fprintf(writer,"%ld\n",elem);
			// printf("%ld\n",elem);
			count++;
		}
	}
	int k;
	for( k = 0 ; k < inv_index->update_len_current ; k++)
	{
		my_array_long_extended * upd_array = (my_array_long_extended *)malloc(sizeof(my_array_long_extended));
		decompress_elems_long(inv_index->updates[k].elems[min_sized_ind],upd_array);

		for( j = 0 ; j <= upd_array->curr_size ; j++ ){
			LONG_INT elem = upd_array->elements[j];

			if(is_in(elem,query_bloom)){
				fprintf(writer,"%ld\n",elem);
				count++;
			}
		}
	}

	*size_res = count;
	fclose(writer);
	free(temp_array);
	// printf("Time for dictionary attack and decompression on resultant bloom filter:: %f %f\n",omp_get_wtime()-time_before_dict_attack,omp_get_wtime()-start_time);
	// printf("Time for query_intersect of size %d:: %f\n",count,omp_get_wtime()-start_time);
	fprintf(record,"time,%f,query_result_size,%llu,terms,%lld\n", omp_get_wtime()-start_time,count,query_len);
	// printf("query : time : %f query_result_size : %llu\n", omp_get_wtime()-start_time,count);
	// fprintf(writer, "%f\n",omp_get_wtime()-start_time);
	// printf("Time for query_intersect of size %d:: %f\n",count,omp_get_wtime()-start_time);
}

void query_intersect0( char ** query_terms, LONG_INT query_len, invIndex* inv_index, LONG_INT *size_res, const char* output_file){
	double start_time = omp_get_wtime();

	b_node * query_bloom;
	b_node ** temp_bloom = (b_node **)malloc(query_len*sizeof(b_node *));
	query_bloom = (b_node *)malloc(sizeof(b_node));
	init(query_bloom);

	LONG_INT start = 0;

	LONG_INT min_sized_ind = 999999999;
	LONG_INT min_size = 999999999;

	LONG_INT i = 0;
	for( i = 0 ; i < query_len ; i++ ){
		long long int index = g_hash_table_lookup( inv_index->vocab_hash,query_terms[i] ) - 1;
		if( index >= 0 ){
			temp_bloom[i] = &inv_index->bloom_inv_ind[index];
			if(temp_bloom[i] == NULL)
				printf("okay\n");

			LONG_INT size_ind = inv_index->elems[index]->size;
			int k;
			for(k = 0; k < inv_index->update_len_current ; k++)
				size_ind += inv_index->updates[k].elems[index]->size;	

			if(size_ind < min_size){
				min_size = size_ind;
				min_sized_ind = index;
			}
		}
		else
		{
			printf("query term not present, empty output so output file not created: %s\n",query_terms[i] );
			return;
		}
	}

	
	// printf("okay what is this  %d\n",temp_bloom[8] );
	// exit(0);
	intersect_k_sans_compression(temp_bloom, query_len, query_bloom);


	// printf("Time for intersection of the bloom filters for the query terms:: %f\n",omp_get_wtime()-start_time);
	// double time_before_dict_attack = omp_get_wtime();

	//dictionary attack
	FILE* writer = fopen(output_file,"a");
	fprintf(writer, "********\n\n" );
	LONG_INT count = 0;
	LONG_INT j = 0;

	my_array_long_extended * temp_array = (my_array_long_extended *)malloc(sizeof(my_array_long_extended));
	decompress_elems_long(inv_index->elems[min_sized_ind],temp_array);

	// printf("Time for decompression:: %f\n",omp_get_wtime()-time_before_dict_attack);

	for( j = 0 ; j <= temp_array->curr_size ; j++ ){
		LONG_INT elem = temp_array->elements[j];
		if(is_in(elem,query_bloom)){
			fprintf(writer,"%ld\n",elem);
			// printf("%ld\n",elem);
			count++;
		}
	}
	int k;
	for( k = 0 ; k < inv_index->update_len_current ; k++)
	{
		my_array_long_extended * upd_array = (my_array_long_extended *)malloc(sizeof(my_array_long_extended));
		decompress_elems_long(inv_index->updates[k].elems[min_sized_ind],upd_array);

		for( j = 0 ; j <= upd_array->curr_size ; j++ ){
			LONG_INT elem = upd_array->elements[j];

			if(is_in(elem,query_bloom)){
				fprintf(writer,"%ld\n",elem);
				count++;
			}
		}
	}

	*size_res = count;
	fclose(writer);
	free(temp_array);
	// printf("Time for dictionary attack and decompression on resultant bloom filter:: %f %f\n",omp_get_wtime()-time_before_dict_attack,omp_get_wtime()-start_time);
	// printf("%d Time for query_intersect of size -- %d:: %f\n",query_len,count,omp_get_wtime()-start_time);
	fprintf(record,"time,%f,query_result_size,%llu,terms,%lld\n", omp_get_wtime()-start_time,count,query_len);
	// printf("query : time : %f query_result_size : %llu\n", omp_get_wtime()-start_time,count);
	// fprintf(writer, "%f\n",omp_get_wtime()-start_time);
	// printf("Time for query_intersect of size %d:: %f\n",count,omp_get_wtime()-start_time);
}

void query_basic(char **query_terms, int query_len, invIndex* inv_index, int *size_res, const char* output_file){

	double start_time = omp_get_wtime();


	int min_sized_ind = 999999999;
	int min_size = 999999999;

	int i = 0;
	for( i = 0 ; i < query_len ; i++ ){
		int index = g_hash_table_lookup( inv_index->vocab_hash,query_terms[i] ) - 1;
		if(index > 0){
			// if(inv_index->elems[i]->len < min_size){
			// 	min_size = inv_index->elems[index]->len;
			// 	min_sized_ind = index;
			// }

			LONG_INT size_ind = inv_index->elems[index]->size;
			int k;
			for(k = 0; k < inv_index->update_len_current ; k++)
				size_ind += inv_index->updates[k].elems[index]->size;	

			if(size_ind < min_size){
				min_size = size_ind;
				min_sized_ind = index;
			}
		}
	}



	FILE* writer = fopen(output_file,"a");
	fprintf(writer, "\n\n" );
	LONG_INT count = 0;
	LONG_INT j = 0;

	my_array_long_extended * temp_array = (my_array_long_extended *)malloc(sizeof(my_array_long_extended));
	decompress_elems_long(inv_index->elems[min_sized_ind],temp_array);

	// printf("Time for decompression:: %f\n",omp_get_wtime()-time_before_dict_attack);

	for( j = 0 ; j <= temp_array->curr_size ; j++ ){
		LONG_INT elem = temp_array->elements[j];

		int check = 1;
		int p = 0;

		for( p = 0 ; p < query_len ; p++ ){
			int index = g_hash_table_lookup( inv_index->vocab_hash,query_terms[p] ) - 1;
			
			if(index < 0){
				check = 0;
				break;
			}
			if(!is_in(elem,&inv_index->bloom_inv_ind[index])){
				check = 0;
				break;
			}
		}
		if(check == 1){
			fprintf(writer, "%ld\n",elem );
			count++;
		}

	}
	int k;
	for( k = 0 ; k < inv_index->update_len_current ; k++)
	{
		my_array_long_extended * upd_array = (my_array_long_extended *)malloc(sizeof(my_array_long_extended));
		decompress_elems_long(inv_index->updates[k].elems[min_sized_ind],upd_array);

		for( j = 0 ; j <= upd_array->curr_size ; j++ )
		{
			LONG_INT elem = upd_array->elements[j];
		
			int check = 1;
			int p = 0;

			for( p = 0 ; p < query_len ; p++ ){
				int index = g_hash_table_lookup( inv_index->vocab_hash,query_terms[p] ) - 1;
				
				if(index < 0){
					check = 0;
					break;
				}
				if(!is_in(elem,&inv_index->bloom_inv_ind[index])){
					check = 0;
					break;
				}
			}
			if(check == 1){
				fprintf(writer, "%ld\n",elem );
				count++;
				}
		}
	}


	free(temp_array);


	*size_res = count;
	fprintf(writer, "%f\n",omp_get_wtime()-start_time);
	fclose(writer);
	fprintf(record,"time,%f,query_result_size,%llu,terms,%lld\n", omp_get_wtime()-start_time,count,query_len);

	// printf("%d Time for query_basic of size %d:: %f\n",query_len,count,omp_get_wtime()-start_time);
}

// void query_generator()

void handle_query(invIndex *inv_index, const char * query_file, int version)
{
	// printf("starting the queries from file :: %s \n",query_file );

	FILE *query_reader;
	query_reader = fopen(query_file,"r");

	LONG_INT num_queries = 0;
	fscanf(query_reader,"%d\n",&num_queries);
	// exit(0);
	LONG_INT i,j = 0;
	for( i = 0 ; i < num_queries ; i++ ){

		LONG_INT num_terms = 0;
		fscanf(query_reader,"%d ", &num_terms);

		char **query_terms = (char **)malloc( sizeof(char *)*num_terms );

		for( j = 0 ; j < num_terms-1 ; j++){
			query_terms[j] = (char *)malloc(sizeof(char)*256);
			fscanf(query_reader,"%s ",query_terms[j]);
			// printf("%s\n",query_terms[j] );
		}

		query_terms[j] = (char *)malloc(sizeof(char)*256);
		fscanf(query_reader, "%s\n", query_terms[j]);
		// printf("%s\n",query_terms[j] );

		LONG_INT size1,size2 = 0;
		// printf("------------**************\n");
		if(version == 1)
			query_intersect(query_terms, num_terms, inv_index, &size1,"output/query_intersect.txt" );
		else if(version == 2)
			query_intersect2(query_terms, num_terms, inv_index, &size1,"output/query_intersect.txt" );
		else if(version == 3)
			query_intersect3(query_terms, num_terms, inv_index, &size1,"output/query_intersect.txt" );
		else if(version == 4)
			query_intersect4(query_terms, num_terms, inv_index, &size1,"output/query_intersect.txt" );
		else if(version == 0)
			query_intersect0(query_terms, num_terms, inv_index, &size1,"output/query_intersect.txt" );
		else if(version == 6)
			query_basic(query_terms, num_terms, inv_index, &size1,"output/query_intersect.txt" );


			
			
		// query_intersect2(query_terms, num_terms, inv_index, &size1,"output/query_intersect.txt" );
		// query_intersect3(query_terms, num_terms, inv_index, &size1,"output/query_intersect.txt" );
		// query_basic(query_terms, num_terms, inv_index, &size1,"output/query_intersect.txt" );
		// query_zipper(query_terms, num_terms, inv_index, &size2,"output/query_zipper.txt" );

	}

	fclose(query_reader);
	// printf("all the queries are done\n");
}

void free_inv_index(invIndex *inv_index)
{
	LONG_INT i;
	for( i = 0 ; i < inv_index->vocab_size ; i++ )
	{
		arrayLong_free_extended(inv_index->elems[i]);
		// free_bloom(&inv_index->bloom_inv_ind[i]);
	}
	free(inv_index->elems);
	free(inv_index->bloom_inv_ind);
	g_hash_table_destroy(inv_index->vocab_hash);

}

void experiments(char *file_list, char *query_folder, char *reply_folder)
{
	invIndex *inv_index = (invIndex *)malloc(sizeof(invIndex));
	char * memrec = concat(query_folder,"memrec");
	record = fopen(memrec,"a");
	char *db = concat(query_folder,"database");
	read_data(db,inv_index);
	init_updates(inv_index,8);
	free(db);
	free(memrec);
	fclose(record);


	FILE * filelist = fopen(file_list,"r");
	char * buffer = (char *)malloc(sizeof(char)*512);
	int count = 0;
	fscanf(filelist,"%d\n",&count);
	// char * reply_folder = concat("response_",query_folder);
	// printf("check 1\n");
	while(count--)
	{
		fscanf(filelist,"%s\n",buffer);
		char *filename = concat(query_folder,buffer);
		char *resultfile = concat(reply_folder,buffer);

		char * ver1 = concat(resultfile, "#simple_intersection");
		record = fopen(ver1,"w");
		handle_query(inv_index,filename,1);
		fclose(record);
		free(ver1);

		char * ver2 = concat(resultfile, "#simple_k_intersection");
		record = fopen(ver2,"w");
		handle_query(inv_index,filename,0);
		fclose(record);
		free(ver2);

		char * ver3 = concat(resultfile, "#intersection_with_compression");
		record = fopen(ver3,"w");
		handle_query(inv_index,filename,2);
		fclose(record);
		free(ver3);

		char * ver4 = concat(resultfile, "#k_intersection_with_compression");
		record = fopen(ver4,"w");
		handle_query(inv_index,filename,3);
		fclose(record);
		free(ver4);

		char * ver5 = concat(resultfile, "#membership_query");
		record = fopen(ver5,"w");
		handle_query(inv_index,filename,6);
		fclose(record);
		free(ver5);

		free(filename);
		free(resultfile);
	// printf("check 2\n");

	}
	free_inv_index(inv_index);
}

int main(LONG_INT argc, char** argv) {
	srand(time(0));
	init_bloomParameters(256,3,1,7,0.01);
	printf("starting version 4 experiments\n");
	seiveInitial();
	char * filelist = "../../../../synthetic_dataset/file_list";
	char * exp_dir  = "../../../../synthetic_dataset/QUERIES_and_RESPONSES/";
	int i = 0;
	for( i = 0 ; i < 4 ; i++)
	{
		char str[50];
		sprintf(str, "Q%d/", i);
		char str2[50];
		sprintf(str2, "Q%d_response/", i);

		char * query_dir = concat(exp_dir,str);
		char * response_dir = concat(exp_dir,str2);

		printf("%s\n%s\n",query_dir,response_dir );
		experiments(filelist, query_dir, response_dir);
		free(query_dir);
		free(response_dir);
	}
	printf("version 4 experiments over!!\n");
}


// int main(LONG_INT argc, char** argv) {
// 	srand(time(0));
// 	init_bloomParameters(256,3,1,7,0.01);
// 	printf("%d,%d,%d,%d,%f\n\n",bloomParam->SIZE_BLOOM,bloomParam->K, bloomParam->COUNTER_CHUNK,bloomParam->NUM_PARTITION,bloomParam->FALSE_PSTV_PROB );
// 	printf("starting version 4 experiments\n");

// 	seiveInitial();
// 	invIndex inv_index;

// 	record = fopen("../../../../doc_dataset/records/record_1","w");

// 	// read_data("../../../../doc_dataset/chunk1",&inv_index);
// 	read_data("../../../../doc_dataset/sample_complete",&inv_index);
// 	init_updates(&inv_index,8);

// 	// update("../../../../doc_dataset/chunk2",&inv_index);
// 	// update("../../../../doc_dataset/chunk3",&inv_index);
// 	// update("../../../../doc_dataset/chunk4",&inv_index);
// 	// update("../../../../doc_dataset/chunk5",&inv_index);


// 	handle_query(&inv_index,"../../../../doc_dataset/query_random",1);
// 	// handle_query(&inv_index,"../../../../doc_dataset/query_maxuid",1);
// 	// handle_query(&inv_index,"../../../../doc_dataset/query_maxuid_inter",1);

// 	fclose(record);


// 	// record = fopen("../../../../doc_dataset/records/record_2","w");
// 	// fprintf(record, "time,%f,memory,%lld\n",1,1);

// 	// handle_query(&inv_index,"../../../../doc_dataset/query_random",2);
// 	// handle_query(&inv_index,"../../../../doc_dataset/query_maxuid",2);
// 	// handle_query(&inv_index,"../../../../doc_dataset/query_maxuid_inter",2);

// 	// fclose(record);


// 	// record = fopen("../../../../doc_dataset/records/record_3","w");
// 	// fprintf(record, "time,%f,memory,%lld\n",1,1);

// 	// handle_query(&inv_index,"../../../../doc_dataset/query_random",3);
// 	// handle_query(&inv_index,"../../../../doc_dataset/query_maxuid",3);
// 	// handle_query(&inv_index,"../../../../doc_dataset/query_maxuid_inter",3);

// 	// fclose(record);


// 	// 	record = fopen("../../../../doc_dataset/records/record_4","w");
// 	// fprintf(record, "time,%f,memory,%lld\n",1,1);

// 	// handle_query(&inv_index,"../../../../doc_dataset/query_random",4);
// 	// handle_query(&inv_index,"../../../../doc_dataset/query_maxuid",4);
// 	// handle_query(&inv_index,"../../../../doc_dataset/query_maxuid_inter",4);

// 	// fclose(record);


// 	record = fopen("../../../../doc_dataset/records/record_0","w");
// 	fprintf(record, "time,%f,memory,%lld\n",1,1);

// 	handle_query(&inv_index,"../../../../doc_dataset/query_random",0);
// 	// handle_query(&inv_index,"../../../../doc_dataset/query_maxuid",0);
// 	// handle_query(&inv_index,"../../../../doc_dataset/query_maxuid_inter",0);

// 	fclose(record);

// 	record = fopen("../../../../doc_dataset/records/record_6","w");
// 	fprintf(record, "time,%f,memory,%lld\n",1,1);

// 	handle_query(&inv_index,"../../../../doc_dataset/query_random",6);
// 	// handle_query(&inv_index,"../../../../doc_dataset/query_maxuid",0);
// 	// handle_query(&inv_index,"../../../../doc_dataset/query_maxuid_inter",0);

// 	fclose(record);

// 	printf("version 4 experiments over!!\n");
// }



