#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include "../../../count_bloom.h"
#include <omp.h>
#include "dynamic_array/array.h"

typedef uint32_t LONG_INT;

typedef guint GLONG;

int query_max_len;

typedef struct indUpdate
{
	comp_array_long** elems;
}indUpdate;


typedef struct invIndex
{
	GHashTable* vocab_hash;    // hashtag to index hash table 
	LONG vocab_size;
	LONG vocab_current;
	// LONG * elems_size;
	comp_array_long** elems;
	indUpdate *updates;
	int update_len_current;
	int update_len_max;

}invIndex;




void read_data(const char * data_file, invIndex* inv_index);

// void query_intersect( char ** query_terms, LONG query_len, invIndex* inv_index, LONG *size_res, const char* output_file);

void query_basic(char **query_terms, LONG query_len, invIndex* inv_index, LONG *size_res, const char* output_file);

void test_suite(invIndex *inv_index, const char *query_file);

