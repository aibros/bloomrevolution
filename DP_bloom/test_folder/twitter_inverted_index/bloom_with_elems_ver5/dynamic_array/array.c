#include "array.h"

void arrayInt_init_extended(my_array_extended* array)
{
	array->elements 	= (unsigned int*)malloc(8*sizeof(unsigned int));
	array->curr_size 	= 0;
	array->max_size 	= 8;
}

void arrayInt_addElement_extended(my_array_extended* array,unsigned int element)
{
	if (array == NULL)
	{
		array = (my_array_extended*)malloc(sizeof(my_array_extended));
		arrayInt_init_extended(array);
	}
	if (array->curr_size < array->max_size)
	{
		array->elements[array->curr_size] = element;
		array->curr_size++;
	}
	else // Need to double array size
	{	
		unsigned int* new_list = (unsigned int*)malloc(2*(array->max_size)*sizeof(unsigned int));
		unsigned int i;
		for (i = 0; i < array->max_size; ++i)
		{
			new_list[i] = array->elements[i];
		}
		new_list[array->curr_size] = element;
		array->curr_size++;

		array->max_size *= 2;
		free(array->elements);
		array->elements  = new_list;
	}
}

void arrayInt_free_extended(my_array_extended* array)
{
	free(array->elements);
}

int arrayInt_find_extended(my_array_extended* array,unsigned int element)
{
	unsigned int i;
	for (i = 0; i < array->curr_size; ++i)
	{
		if (array->elements[i] == element)
			return 1;
	}
	return 0;
}

void arrayLong_init_extended(my_array_long_extended* array)
{
	array->elements 	= (LONG*)malloc(32*sizeof(LONG));
	array->curr_size 	= 0;
	array->max_size 	= 32;
}


void arrayLong_extend_extended(my_array_long_extended *array, int size)
{
	free(array->elements);
	array->elements 	= (LONG*)malloc(size*sizeof(LONG));
	array->curr_size = 0;
	array->max_size 	= size;
}

void arrayLong_addElement_extended(my_array_long_extended* array,LONG element)
{
	if (array == NULL)
	{
		array = (my_array_long_extended*)malloc(sizeof(my_array_long_extended));
		arrayLong_init_extended(array);
		printf("Array was NULL\n");
	}
	if (array->curr_size < array->max_size)
	{
		// printf("here 2\n");
		array->elements[array->curr_size] = element;
		array->curr_size++;
	}
	else // Need to double array size
	{	

		LONG* new_list = (LONG*)malloc(2*(array->max_size)*sizeof(LONG));
		unsigned int i;
		for (i = 0; i < array->max_size; ++i)
		{
			new_list[i] = array->elements[i];
		}
		new_list[array->curr_size] = element;
		array->curr_size++;

		array->max_size *= 2;
		free(array->elements);
		array->elements  = new_list;
	}
}

void arrayLong_free_extended(my_array_long_extended* array)
{
	free(array->elements);
}

int arrayLong_find_extended(my_array_long_extended* array,LONG element)
{
	unsigned int i;
	for (i = 0; i < array->curr_size; ++i)
	{
		if (array->elements[i] == element)
			return 1;
	}
	return 0;
}

void print_elems_extended(my_array_extended* array)
{
	int i = 0;
	for(i = 0 ; i < array->curr_size ; i++)
	{
		printf("%d ",array->elements[i] );
	}
	printf("\n");
}

void print_elems_long(my_array_long_extended* array)
{
	int i = 0;
	for(i = 0 ; i < array->curr_size ; i++)
	{
		printf("%lld\n",array->elements[i] );
	}
	printf("\n");
}

int bit = 32;









int cmpfunc (const void * a, const void * b)
{
	LONG x = *(LONG *)a;
	LONG y = *(LONG *)b;
	if(x == y)
		return 0;
	else
	{
		if(x>y)
			return 1;
		else
			return -1;
	}

}


int compress_elems_long(my_array_long_extended* array, comp_array_long* c_array)
{
	//init here only
	LONG size = array->curr_size;

	c_array->comp_elements = (unsigned  char *)malloc(sizeof(LONG)*(size+32) ); // not a problem
	c_array->size = size;

	qsort(array->elements, size, sizeof(LONG), cmpfunc);

	int buffer_size = vbyte_encode_delta(array->elements, size, c_array->comp_elements,0); // encoding

	// printf("%d\n",buffer_size );
	return buffer_size;
}

int decompress_elems_long(comp_array_long *c_array, my_array_long_extended *array)
{
	//init here only, don't initialize array early on
	LONG size = c_array->size;

	array->elements 	= (LONG*)malloc( 2*(size)*sizeof(LONG));
	array->curr_size 	= size;
	array->max_size 	= 2*size;
	
	int buffer_size = masked_vbyte_decode_delta(c_array->comp_elements, array->elements, size ,0); // encoding

	// printf("%d\n", buffer_size);

	return buffer_size;
}


size_t intersect_scalar(uint32_t *A, uint32_t *B, size_t s_a, size_t s_b, uint32_t *C) {
    size_t i_a = 0, i_b = 0;
    size_t counter = 0;
    while(i_a < s_a && i_b < s_b) {
        if(A[i_a] < B[i_b]) {
            i_a++;
        } else if(B[i_b] < A[i_a]) {
            i_b++;
        } else {
            C[counter++] = A[i_a];
            i_a++; i_b++;
        }
    }
    return counter;
}

/*
size_t intersect_vector(uint32_t *A, uint32_t *B, size_t s_a, size_t s_b, uint32_t *C) {
	size_t count = 0;
	size_t i_a = 0, i_b = 0;

	// trim lengths to be a multiple of 4
	size_t st_a = (s_a / 4) * 4;
	size_t st_b = (s_b / 4) * 4;

	while(i_a < st_a && i_b < st_b) {
		//[ load segments of four 32-bit elements
		__m128i v_a = _mm_load_si128((__m128i*)&A[i_a]);
		__m128i v_b = _mm_load_si128((__m128i*)&B[i_b]);
		//]

		//[ move pointers
		uint32_t a_max = _mm_extract_epi32(v_a, 3);
		uint32_t b_max = _mm_extract_epi32(v_b, 3);
		i_a += (a_max <= b_max) * 4;
		i_b += (a_max >= b_max) * 4;
		//]

		//[ compute mask of common elements
		uint32_t cyclic_shift = _MM_SHUFFLE(0,3,2,1);
		__m128i cmp_mask1 = _mm_cmpeq_epi32(v_a, v_b);    // pairwise comparison
		v_b = _mm_shuffle_epi32(v_b, cyclic_shift);       // shuffling
		__m128i cmp_mask2 = _mm_cmpeq_epi32(v_a, v_b);    // again...
		v_b = _mm_shuffle_epi32(v_b, cyclic_shift);
		__m128i cmp_mask3 = _mm_cmpeq_epi32(v_a, v_b);    // and again...
		v_b = _mm_shuffle_epi32(v_b, cyclic_shift);
		__m128i cmp_mask4 = _mm_cmpeq_epi32(v_a, v_b);    // and again.
		__m128i cmp_mask = _mm_or_si128(
				_mm_or_si128(cmp_mask1, cmp_mask2),
				_mm_or_si128(cmp_mask3, cmp_mask4)
		); // OR-ing of comparison masks
		// convert the 128-bit mask to the 4-bit mask
		uint32_t mask = _mm_movemask_ps((__m128)cmp_mask);
		//]

		//[ copy out common elements
		__m128i p = _mm_shuffle_epi8(v_a, shuffle_mask[mask]);
		_mm_storeu_si128((__m128i*)&C[count], p);
		count += _mm_popcnt_u32(mask); // a number of elements is a weight of the mask
		//]
	}

	// intersect the tail using scalar intersection
	while(i_a < s_a && i_b < s_b) {
        if(A[i_a] < B[i_b]) {
            i_a++;
        } else if(B[i_b] < A[i_a]) {
            i_b++;
        } else {
            C[count++] = A[i_a];
            i_a++; i_b++;
            // printf("--%d\n",A[i_a] );
        }
    }

	return count;
}

int getBit(int value, int position) {
    return ( ( value & (1 << position) ) >> position);
}

// a simple implementation, we don't care about performance here
void prepare_shuffling_dictionary() {
	int i;
    for(i = 0; i < 16; i++) {
        int counter = 0;
        char permutation[16];
        memset(permutation, 0xFF, sizeof(permutation));
        char b;
        for(b = 0; b < 4; b++) {
            if(getBit(i, b)) {
                permutation[counter++] = 4*b;
                permutation[counter++] = 4*b + 1;
                permutation[counter++] = 4*b + 2;
                permutation[counter++] = 4*b + 3;
            }
        }
        __m128i mask = _mm_loadu_si128((const __m128i*)permutation);
        shuffle_mask[i] = mask;
    }
}
 */

int intersect_array_long(my_array_long_extended *A, my_array_long_extended *B, my_array_long_extended* C)
{

	int min;
	if(A->curr_size < B->curr_size)
		min = A->curr_size;
	else
		min = B->curr_size;

	if(C->max_size < min)
	{
		// printf("C not correctly allocated\n");
		arrayLong_extend_extended(C, min);
	}

	C->curr_size = 0;
	//assume C is allocated
	int size = intersect_scalar(A->elements, B->elements ,A->curr_size, B->curr_size, C->elements);
	// int size = intersect_vector(A->elements, B->elements ,A->curr_size, B->curr_size, C->elements);
	C->curr_size = size;
	return size;
}


void test_intersect()
{
	// prepare_shuffling_dictionary();
	my_array_long_extended *A = (my_array_long_extended *)malloc(sizeof(my_array_long_extended));
	my_array_long_extended *B = (my_array_long_extended *)malloc(sizeof(my_array_long_extended));
	arrayLong_init_extended(A);
	arrayLong_init_extended(B);

	int size = 0;
	int i = 0;
	while(i++ < 100)
	{
		if(i%2 == 0)
			arrayLong_addElement_extended(A,i);
		else
			arrayLong_addElement_extended(B,i);

		if(i%6 == 0)
		{
			arrayLong_addElement_extended(B,i);
			size++;
		}
	}
	// print_elems_long(A);
	// printf("---------------\n");
	// print_elems_long(B);

	uint32_t *C = (uint32_t *)malloc( (size+2)*sizeof(uint32_t));
	// int size2 = intersect_vector(A->elements, B->elements ,A->curr_size, B->curr_size, C);
	int size2 = intersect_scalar(A->elements, B->elements ,A->curr_size, B->curr_size, C);

	printf("%d  %d\n",size2,size );
	i = 0;
	while(i < size2)
	{
		printf("%d\n",C[i] );
		i++;
	}
}

// int main()
// {
// 	test_intersect();
// }


// ///////not suitable
// int compress_elems_long_little(my_array_long_extended* array, comp_array_long* c_array)
// {
// 	//init here only
// 	LONG buffer_size =  byte_count(array->curr_size, bit);
// 	printf("buffer_size - %d\n", buffer_size);
// 	c_array->comp_elements = (uint8_t *)malloc(buffer_size*sizeof(uint8_t));
// 	c_array->size = array->curr_size;
// 	turbopack32(array->elements, array->curr_size, bit, c_array->comp_elements);

// 	return buffer_size;
// }
// ///////not suitable
// int decompress_elems_long_little(comp_array_long *c_array, my_array_long_extended *array)
// {
// 	//init here only, don't initialize array early on
// 	LONG num_elems = c_array->size;

// 	array->elements 	= (LONG*)malloc( (num_elems+bit)*sizeof(LONG));
// 	array->curr_size 	= num_elems;
// 	array->max_size 	= num_elems+bit;

// 	turbounpack32(c_array->comp_elements, num_elems, bit, array->elements);

// 	return 0;
// }
// ///////not suitable
// int compress_elems_long_p4(my_array_long_extended* array, comp_array_long* c_array)
// {
// 	//init here only
// 	LONG size = array->curr_size;
// 	LONG size_rounded = SIZE_ROUNDUP(size,32);

// 	//
// 	LONG *temp = (LONG *)malloc(sizeof(temp[0])*size_rounded);
// 	LONG i = 0;
// 	for(i = 0 ; i < size ; i++)
// 		temp[i] = array->elements[i];
// 	printf("rounded -- %lu\n",size_rounded );

// 	//

// 	LONG b = bit64(array->elements,size);
// 	printf("size needed %d\n",((b+7)/8)*size_rounded + 1  );

// 	c_array->comp_elements = (unsigned  char *)malloc( ((b+7)/8)*size_rounded + 1 ); // not a problem
// 	c_array->size = size;
// 	// unsigned char *op = bitpack64( array->elements, size, c_array->comp_elements, b );
// 	unsigned char *op = p4enc64( temp, size, c_array->comp_elements);

// 	int buffer_size = (int)(op - c_array->comp_elements);
// 	printf("%d\n",buffer_size );
// 	return buffer_size;
// }
// ///////not suitable
// int decompress_elems_long_p4(comp_array_long *c_array, my_array_long_extended *array)
// {
// 	//init here only, don't initialize array early on
// 	LONG size = c_array->size;
// 	LONG size_rounded = SIZE_ROUNDUP(size,32);

// 	array->elements 	= (LONG*)malloc( (size_rounded)*sizeof(LONG));
// 	array->curr_size 	= size;
// 	array->max_size 	= size_rounded;

// 	// uint8_t *op2 =  bitunpack64(c_array->comp_elements, size, array->elements,b);
// 	unsigned char *op2 =  p4dec64(c_array->comp_elements, size, array->elements);

// 	int buffer_size = (int)(op2-c_array->comp_elements);
// 	printf("%d\n", buffer_size);

// 	return buffer_size;
// }
// ///////

// typedef uint64_t u64;







// int main()
// {
// 	unsigned cnt = 577;  //no issue if cnt <= 576 
// 	unsigned a = SIZE_ROUNDUP(cnt,32);

// 	u64 *array = (u64 *)malloc(sizeof(array[0])*a);

// 	int i = 0;
//     for(i=0;i<cnt;i++)
//     {
//     	if(i%2==0)		
//     	{			
//     		// array[i] =  rand();     //or any integer with size 32 bit
//     		array[i] = (u64)-1; //no issue on using this one
//     	}
//     	else
//     		array[i] = (u64)-1; 
//     }

// 	qsort(array, cnt, sizeof(u64), cmpfunc);



// 	int b;
// 	printf("a=%d cnt=%d bit64=%d\n",a, cnt, b = bit64(array,cnt) ); 

// 	unsigned char* outb = (unsigned char *)malloc( ((b+7)/8)*a); 

// 	unsigned char* op = bitpack64(array, cnt, outb, b);
// 	printf("bitpack64=%d\n",(int)(op-outb) );   


// 	unsigned char* out = (unsigned char *)malloc( ((b+7)/8)*a+1);

// 	op = p4enc64(array, cnt, out);
// 	printf("p4enc64=%d\n",(int)(op-out) );

// 	u64 *capacity = (u64 *)malloc(sizeof(capacity[0])*(a));
// 	unsigned char *op2 =  p4dec64(out, cnt, capacity);
// 	printf("%d\n",(int)(op2-out) );
// }


