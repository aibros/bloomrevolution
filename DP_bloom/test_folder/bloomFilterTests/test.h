#include "../../count_bloom.h"
#include "../../bloom_tree.h"
#include "sys/time.h"

void read_and_insert(char * filename, b_node* filter);
void estimate_false_positive(char* filename,b_node* filter, FILE* logger);
void test_memory_usage_unitSize(char** filenameList,int numFiles,char* logFileName);
void test_intersection_overlap(int NumElemsA, int NumElemsB, char* logFileName);
void test_union_overlap(int NumElemsA, int NumElemsB, char* logFileName);
void run_unit_size_analysis(char* fileListName, int M, UINT K, float FALSE_PSTV_PROB, int numUnitSize, int* unitSizeVals);
void run_setOps_vary_percent_overlap();
void run_setOps_fixed_overlap(int* overlapList,int numOverlapVals, int numSets, int K, int M, int unitSize, float FALSE_PSTV_PROB );

static inline float get_time_diff(struct timeval t1, struct timeval t2);

extern char* bloomTestingPath;
extern char* logsPath;
extern char* datasetPath;