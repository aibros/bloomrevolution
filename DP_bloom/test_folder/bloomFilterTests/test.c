#include "test.h"

char* bloomTestingPath;
char* logsPath;
char* datasetPath;

// Reads data from file and inserts them in the bloom filter
void read_and_insert(char * filename, b_node* filter)
{
	FILE * reader;
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,filename);
	reader = fopen(fullFilename,"r");
	UINT i,num_elems = 0,namespace = 1;
	fscanf(reader,"%d",&num_elems);
	fscanf(reader,"%d",&namespace);

	int maxLevel = num_elems/bloomParam->FILL_THRESHOLD;
	if (maxLevel > bloomParam->NUM_PARTITION)
		maxLevel = bloomParam->NUM_PARTITION;
	// grow_full_tree_(filter,0); // Can be interlligent about it
	grow_full_tree_upto_level(filter,0,maxLevel); // Being intelligent

	for(i = 0 ; i < num_elems ; i++)
	{
		int elem = 0;
		fscanf(reader,"%d",&elem);
		insert_at_level(elem,filter,0,maxLevel);
	}
	finish_insertion(filter);
	fclose(reader);
}

// currFilter stores elements from initial set and batches processed so far
void read_and_insert_batchWise(char * filename, b_node* filter, b_node* currFilter)
{
	FILE * reader;
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,filename);
	reader = fopen(fullFilename,"r");
	UINT i,num_elems = 0,namespace = 1;
	fscanf(reader,"%d",&num_elems);
	fscanf(reader,"%d",&namespace);

	int currDepth = findTreeDepth(currFilter);
	int maxLevel = num_elems/bloomParam->FILL_THRESHOLD;
	if (maxLevel > bloomParam->NUM_PARTITION)
		maxLevel = bloomParam->NUM_PARTITION;

	if (maxLevel < currDepth)
		maxLevel = currDepth;

	grow_full_tree_upto_level(filter,0,maxLevel); // Have to grow to full tree 

	for(i = 0 ; i < num_elems ; i++)
	{
		int elem = 0;
		fscanf(reader,"%d",&elem);
		insert_at_level(elem,filter,0,maxLevel);
	}
	finish_batch_insertion(currFilter, filter);
	fclose(reader);
}


// Takes a bloom filter and estimates number of false positives in it.
void estimate_false_positive(char* filename,b_node* filter, FILE* logger)
{
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,filename);
	FILE * reader = fopen(fullFilename,"r");
	int num_elems,namespace;
	fscanf(reader,"%d",&num_elems);
	fscanf(reader,"%d",&namespace);
	fclose(reader);
	int i;
	int numPresent = 0;
	for (i = 1; i <= namespace; ++i)
	{
		if(is_in(i,filter))
		{
			numPresent++;
		}
	}
	fprintf(logger, "%.7f,",(numPresent - num_elems)/(float)namespace);
}

void test_memory_usage_unitSize(char** filenameList,int numFiles,char* logFileName)
{
	FILE* logger ;
	logger = fopen(logFileName,"w");
	fprintf(logger, "%s","FileName,NumElems,UnitSize,NumUnitBFs,EffMemUsage,FP_Ratio,TimeTaken,MembershipQueryTime,NULL");

	int i;
	float timeTaken = 0.0;
	struct timeval start,end;
	for (i = 0; i < numFiles; ++i)
	{
		fprintf(logger, "\n");
		gettimeofday(&start,NULL);
		b_node * filter = (b_node*)malloc(sizeof(b_node));
		init_without_bloom(filter);
		// grow_full_tree(filter,0); // Called inside read_and_insert
		read_and_insert(filenameList[i],filter);
		gettimeofday(&end,NULL);
		float creationTime = get_time_diff(start,end);


		char fullFilename[1000];
		sprintf(fullFilename,"%s%s",datasetPath,filenameList[i]);
		FILE * reader = fopen(fullFilename,"r");
		int num_elems,namespace;
		fscanf(reader,"%d",&num_elems);
		fscanf(reader,"%d",&namespace);
		fclose(reader);

		fprintf(logger, "%s,",filenameList[i]);
		fprintf(logger, "%d,",num_elems);
		fprintf(logger, "%d,",bloomParam->SIZE_BLOOM);
		int tempCount = count_leaves(filter);
		fprintf(logger, "%d,",tempCount);
		fprintf(logger, "%d,",tempCount*(bloomParam->SIZE_BLOOM));
		gettimeofday(&start,NULL);
		estimate_false_positive(filenameList[i],filter,logger);
		gettimeofday(&end,NULL);

		float memQueryTime = get_time_diff(start,end);
		fprintf(logger, "%f,",creationTime);
		fprintf(logger, "%f,",memQueryTime);
		fprintf(logger, "NULL");

		free_bloom(filter);
	}
	fclose(logger);
}

// Runs test for fixed size of A and B, but varying size of overlap from 0.1 to 0.9
// For each (sizeA,sizeB) pair, there are 100 samples
// This function generates log file for graph where we plot FPRatio vs overlap between A nd B
void test_intersection_overlap(int NumElemsA, int NumElemsB, char* logFileName)
{
	FILE* logger;
	logger = fopen(logFileName,"w");
	fprintf(logger,  "%s\n","FileA,FileB,ResultFile,Overlap,Operation,Namespace,NumHashFunc,UnitSize,FalsePstvProb,FillThreshold,NumElemsA,NumElemsB,NumElemsResult,NumUnitBFs,EffMemUsage,FP_Ratio,NULL");

	float maxOverlap = 1.0;
	int numSamples  = 10;
	float overlap;
	int sampleIter;
	for (overlap = 0.1; overlap < maxOverlap; overlap += 0.1)
	{
		for (sampleIter = 0; sampleIter < numSamples; ++sampleIter)
		{
			char fileA[1000];
			char fileB[1000];
			char intersect_filename[1000];
			sprintf(fileA	,"rand_%d_%d_A_%.1f_%d", NumElemsA, NumElemsB, overlap, sampleIter);
			sprintf(fileB	,"rand_%d_%d_B_%.1f_%d", NumElemsA, NumElemsB, overlap, sampleIter);
			sprintf(intersect_filename	,"rand_%d_%d_inter_%.1f_%d",NumElemsA, NumElemsB, overlap, sampleIter);
			
			b_node* filter_1 			= (b_node*)malloc(sizeof(b_node));
			b_node* filter_2 			= (b_node*)malloc(sizeof(b_node));
			b_node* filter_intersect 	= (b_node*)malloc(sizeof(b_node));
			init_without_bloom(filter_1);
			init_without_bloom(filter_2);
			init_without_bloom(filter_intersect);
			// grow_full_tree(filter_1,0); // Called inside read_and_insert
			// grow_full_tree(filter_2,0); // Called inside read_and_insert
		
			FILE * reader;
			int namespace;
			int num_elems_intersect;

			char *fullFilename = (char*)malloc(1000*sizeof(char));
			sprintf(fullFilename,"%s%s",datasetPath,intersect_filename);
			reader = fopen(fullFilename,"r");
			fscanf(reader,"%d",&num_elems_intersect);
			fscanf(reader,"%d",&namespace);
			fclose(reader);
			free(fullFilename);

			fprintf(logger, "%s,", fileA);
			fprintf(logger, "%s,", fileB);
			fprintf(logger, "%s,", intersect_filename);
			fprintf(logger, "%.1f,",overlap);
			fprintf(logger, "Intersection,");

			fprintf(logger, "%d,",namespace);
			fprintf(logger, "%d,",bloomParam->K);
			fprintf(logger, "%d,",bloomParam->SIZE_BLOOM);
			fprintf(logger, "%.5f,",bloomParam->FALSE_PSTV_PROB);
			fprintf(logger, "%.3f,",bloomParam->FILL_THRESHOLD);

			fprintf(logger, "%d,",NumElemsA);
			fprintf(logger, "%d,",NumElemsB);
			fprintf(logger, "%d,",num_elems_intersect);
			
			read_and_insert(fileA,filter_1);
			read_and_insert(fileB,filter_2);
			intersect_sans_expansion(filter_1,filter_2,filter_intersect);
			// Details for set (A intersect B)
			int tempLeaves  = count_leaves(filter_intersect);
			fprintf(logger, "%d,",tempLeaves);
			fprintf(logger, "%d,",bloomParam->SIZE_BLOOM*tempLeaves);
			estimate_false_positive(intersect_filename,filter_intersect,logger);

			fprintf(logger, "NULL\n");
			free_bloom(filter_1);
			free_bloom(filter_2);
			free_bloom(filter_intersect);
			free(filter_1);
			free(filter_2);
			free(filter_intersect);
		}
	}
	fclose(logger);
}

// Runs test where size of overlap is fixed and size of set A and B varies
// overlap: Size of overlap
// numSets: Number of sets generated to intersect/union for this particular overlap size
void test_intersection_fixed_overlap(int overlap, int numSets, char* logFileName)
{
	FILE* logger;
	logger = fopen(logFileName,"w");
	fprintf(logger,  "%s\n","FileA,FileB,ResultFile,Overlap,Operation,Namespace,NumHashFunc,UnitSize,FalsePstvProb,FillThreshold,NumElemsA,NumElemsB,NumElemsResult,SymmetricDiffSize,NumUnitBFs,EffMemUsage,FP_Ratio,NULL");
	
	for (int i = 1; i <= numSets; ++i)
	{
		char fileA[1000];
		char fileB[1000];
		char intersect_filename[1000];
		sprintf(fileA	,"rand_oLap_%d_A_%d", overlap, i);
		sprintf(fileB	,"rand_oLap_%d_B_%d", overlap, i);
		sprintf(intersect_filename	,"rand_oLap_%d_inter_%d", overlap, i);
		
		b_node* filter_1 			= (b_node*)malloc(sizeof(b_node));
		b_node* filter_2 			= (b_node*)malloc(sizeof(b_node));
		b_node* filter_intersect 	= (b_node*)malloc(sizeof(b_node));
		init_without_bloom(filter_1);
		init_without_bloom(filter_2);
		init_without_bloom(filter_intersect);
		// grow_full_tree(filter_1,0); // Called inside read_and_insert
		// grow_full_tree(filter_2,0); // Called inside read_and_insert
	
		FILE * reader;
		int namespace;
		int num_elems_intersect, num_elems_A,num_elems_B;

		char *fullFilename = (char*)malloc(1000*sizeof(char));
		sprintf(fullFilename,"%s%s",datasetPath,intersect_filename);
		reader = fopen(fullFilename,"r");
		fscanf(reader,"%d",&num_elems_intersect);
		fscanf(reader,"%d",&namespace);
		fclose(reader);
		free(fullFilename);

		fullFilename = (char*)malloc(1000*sizeof(char));
		sprintf(fullFilename,"%s%s",datasetPath,fileA);
		reader = fopen(fullFilename,"r");
		fscanf(reader,"%d",&num_elems_A);
		fclose(reader);
		free(fullFilename);

		fullFilename = (char*)malloc(1000*sizeof(char));
		sprintf(fullFilename,"%s%s",datasetPath,fileB);
		reader = fopen(fullFilename,"r");
		fscanf(reader,"%d",&num_elems_B);
		fclose(reader);
		free(fullFilename);

		fprintf(logger, "%s,", fileA);
		fprintf(logger, "%s,", fileB);
		fprintf(logger, "%s,", intersect_filename);
		fprintf(logger, "%d,",overlap);
		fprintf(logger, "Intersection,");

		fprintf(logger, "%d,",namespace);
		fprintf(logger, "%d,",bloomParam->K);
		fprintf(logger, "%d,",bloomParam->SIZE_BLOOM);
		fprintf(logger, "%f,",bloomParam->FALSE_PSTV_PROB);
		fprintf(logger, "%f,",bloomParam->FILL_THRESHOLD);

		fprintf(logger, "%d,",num_elems_A);
		fprintf(logger, "%d,",num_elems_B);
		fprintf(logger, "%d,",num_elems_intersect);
		fprintf(logger, "%d,",num_elems_A + num_elems_B - 2*overlap);
		if(num_elems_intersect != overlap)
		{
			printf("num_elems_intersect(%d) is not equal to overlap(%d)\n",num_elems_intersect,overlap);
		}

		read_and_insert(fileA,filter_1);
		read_and_insert(fileB,filter_2);
		intersect_sans_expansion(filter_1,filter_2,filter_intersect);
		finish_insertion(filter_intersect);

		// Details for set (A intersect B)
		int tempLeaves  = count_leaves(filter_intersect);
		fprintf(logger, "%d,",tempLeaves);
		fprintf(logger, "%d,",bloomParam->SIZE_BLOOM*tempLeaves);
		estimate_false_positive(intersect_filename,filter_intersect,logger);

		fprintf(logger, "NULL\n");
		free_bloom(filter_1);
		free_bloom(filter_2);
		free_bloom(filter_intersect);
	}
	fclose(logger);
}

// Runs test where size of overlap is fixed and size of set A and B varies
// overlap: Size of overlap
// numSets: Number of sets generated to intersect/union for this particular overlap size
void test_union_fixed_overlap(int overlap, int numSets, char* logFileName)
{
	FILE* logger;
	logger = fopen(logFileName,"w");
	fprintf(logger,  "%s\n","FileA,FileB,ResultFile,Overlap,Operation,Namespace,NumHashFunc,UnitSize,FalsePstvProb,FillThreshold,NumElemsA,NumElemsB,NumElemsResult,SymmetricDiffSize,NumUnitBFs,EffMemUsage,FP_Ratio,NULL");

	for (int i = 1; i <= numSets; ++i)
	{
		char fileA[1000];
		char fileB[1000];
		char union_filename[1000];
		sprintf(fileA	,"rand_oLap_%d_A_%d", overlap, i);
		sprintf(fileB	,"rand_oLap_%d_B_%d", overlap, i);
		sprintf(union_filename	,"rand_oLap_%d_union_%d", overlap, i);
		
		b_node* filter_1 			= (b_node*)malloc(sizeof(b_node));
		b_node* filter_2 			= (b_node*)malloc(sizeof(b_node));
		b_node* filter_union 	= (b_node*)malloc(sizeof(b_node));
		init_without_bloom(filter_1);
		init_without_bloom(filter_2);
		init_without_bloom(filter_union);
		// grow_full_tree(filter_1,0); // Called inside read_and_insert
		// grow_full_tree(filter_2,0); // Called inside read_and_insert
	 
		FILE * reader;
		int namespace;
		int num_elems_union,num_elems_A,num_elems_B;

		char *fullFilename = (char*)malloc(1000*sizeof(char));
		sprintf(fullFilename,"%s%s",datasetPath,union_filename);
		reader = fopen(fullFilename,"r");
		fscanf(reader,"%d",&num_elems_union);
		fscanf(reader,"%d",&namespace);
		fclose(reader);
		free(fullFilename);

		fullFilename = (char*)malloc(1000*sizeof(char));
		sprintf(fullFilename,"%s%s",datasetPath,fileA);
		reader = fopen(fullFilename,"r");
		fscanf(reader,"%d",&num_elems_A);
		fclose(reader);
		free(fullFilename);

		fullFilename = (char*)malloc(1000*sizeof(char));
		sprintf(fullFilename,"%s%s",datasetPath,fileB);
		reader = fopen(fullFilename,"r");
		fscanf(reader,"%d",&num_elems_B);
		fclose(reader);
		free(fullFilename);

		fprintf(logger, "%s,", fileA);
		fprintf(logger, "%s,", fileB);
		fprintf(logger, "%s,", union_filename);
		fprintf(logger, "%d,",overlap);
		fprintf(logger, "Union,");

		fprintf(logger, "%d,",namespace);
		fprintf(logger, "%d,",bloomParam->K);
		fprintf(logger, "%d,",bloomParam->SIZE_BLOOM);
		fprintf(logger, "%f,",bloomParam->FALSE_PSTV_PROB);
		fprintf(logger, "%f,",bloomParam->FILL_THRESHOLD);

		fprintf(logger, "%d,",num_elems_A);
		fprintf(logger, "%d,",num_elems_B);
		fprintf(logger, "%d,",num_elems_union);
		fprintf(logger, "%d,",num_elems_A + num_elems_B - 2*overlap);
		if(num_elems_union != num_elems_A + num_elems_B - overlap)
		{
			printf("num_elems_union not correct::%d\n",num_elems_union);
			printf("overlap::%d\n",overlap);
			printf("Size A::%d\n",num_elems_A );
			printf("Size B::%d\n",num_elems_B );
			exit(0);
		}
		read_and_insert(fileA,filter_1);
		read_and_insert(fileB,filter_2);
		union_bloom(filter_1,filter_2,filter_union,bloom_tree,NULL);
		
		// Details for set (A union B)
		int tempLeaves  = count_leaves(filter_union);
		fprintf(logger, "%d,",tempLeaves);
		fprintf(logger, "%d,",bloomParam->SIZE_BLOOM*tempLeaves);
		estimate_false_positive(union_filename,filter_union,logger);

		fprintf(logger, "NULL\n");
		free_bloom(filter_1);
		free_bloom(filter_2);
		free_bloom(filter_union);
	}
	fclose(logger);
}

// Runs test for fixed size of A and B, but varying size of overlap from 0.1 to 0.9
// For each (sizeA,sizeB) pair, there are 100 samples
// This function generates log file for graph where we plot FPRatio vs overlap between A nd B
void test_union_overlap(int NumElemsA, int NumElemsB, char* logFileName)
{
	FILE* logger;
	logger = fopen(logFileName,"w");
	fprintf(logger,  "%s\n","FileA,FileB,ResultFile,Overlap,Operation,Namespace,NumHashFunc,UnitSize,FalsePstvProb,FillThreshold,NumElemsA,NumElemsB,NumElemsResult,NumUnitBFs,EffMemUsage,FP_Ratio,NULL");

	float maxOverlap 	= 1.0;
	int numSamples  	= 10;
	float overlap;
	int sampleIter;
	for (overlap = 0.1; overlap < maxOverlap; overlap += 0.1)
	{
		for (sampleIter = 0; sampleIter < numSamples; ++sampleIter)
		{
			char fileA[1000];
			char fileB[1000];
			char union_filename[1000];
			sprintf(fileA	,"rand_%d_%d_A_%.1f_%d", NumElemsA, NumElemsB, overlap, sampleIter);
			sprintf(fileB	,"rand_%d_%d_B_%.1f_%d", NumElemsA, NumElemsB, overlap, sampleIter);
			sprintf(union_filename	,"rand_%d_%d_union_%.1f_%d",NumElemsA, NumElemsB, overlap, sampleIter);
			b_node* filter_1 		= (b_node*)malloc(sizeof(b_node));
			b_node* filter_2 		= (b_node*)malloc(sizeof(b_node));
			b_node* filter_union 	= (b_node*)malloc(sizeof(b_node));
			init_without_bloom(filter_1);
			init_without_bloom(filter_2);
			init_without_bloom(filter_union);
			// grow_full_tree(filter_1,0); // Called inside read_and_insert
			// grow_full_tree(filter_2,0); // Called inside read_and_insert
		
			FILE * reader;
			int namespace;
			int num_elems_union;

			char *fullFilename = (char*)malloc(1000*sizeof(char));
			sprintf(fullFilename,"%s%s",datasetPath,union_filename);
			reader = fopen(fullFilename,"r");
			fscanf(reader,"%d",&num_elems_union);
			fscanf(reader,"%d",&namespace);
			fclose(reader);
			free(fullFilename);

			fprintf(logger, "%s,", fileA);
			fprintf(logger, "%s,", fileB);
			fprintf(logger, "%s,", union_filename);
			fprintf(logger, "%.1f,",overlap);
			fprintf(logger, "Union,");

			fprintf(logger, "%d,",namespace);
			fprintf(logger, "%d,",bloomParam->K);
			fprintf(logger, "%d,",bloomParam->SIZE_BLOOM);
			fprintf(logger, "%.5f,",bloomParam->FALSE_PSTV_PROB);
			fprintf(logger, "%.3f,",bloomParam->FILL_THRESHOLD);

			fprintf(logger, "%d,",NumElemsA);
			fprintf(logger, "%d,",NumElemsB);
			fprintf(logger, "%d,",num_elems_union);
			
			read_and_insert(fileA,filter_1);
			read_and_insert(fileB,filter_2);
			union_bloom(filter_1,filter_2,filter_union,bloom_tree,NULL);

			// Details for set (A intersect B)
			int tempLeaves  = count_leaves(filter_union);
			fprintf(logger, "%d,",tempLeaves);
			fprintf(logger, "%d,",bloomParam->SIZE_BLOOM*tempLeaves);
			estimate_false_positive(union_filename,filter_union,logger);

			fprintf(logger, "NULL\n");
			free_bloom(filter_1);
			free_bloom(filter_2);
			free_bloom(filter_union);
			free(filter_1);
			free(filter_2);
			free(filter_union);
		}
	}
	fclose(logger);
}

// M: Namespace size
// K: Number of hash functions
// FALSE_PSTV_PROB : False positive probability
// fileListName: 	This is name of file present inside dataset folder which contains name of files on which the
// experiments have to be run( if fileList is not present in datasetfolder, but is some other folder inside it
// the fileListName should have the path relative to dataset folder as well )
void run_unit_size_analysis(char* fileListName, int M, UINT K, float FALSE_PSTV_PROB, int numUnitSize, int* unitSizeVals)
{
	// Fixed parameters
	// False positive probability( fixed n_threshold for bloom filter and depth of bloomTree), 
	// Namespace size & number of hash functions

	// UINT K 				= 3;
	// UINT M 				= 100000;
	// int unitSizeVals[2] 	= {1024,2048};//,2048,4000,10000,20000,40000};
	// float FALSE_PSTV_PROB = 0.001;
	UINT COUNTER_SIZE 	= 1;
	int numFiles = 0,i;

	char** filenameList, *logFileName;
	char fullFilename[1000];
	FILE* reader;	

	char directoryName[1000];
	sprintf(directoryName,"%sSetCreationUnitSizeAnalysis_DynPart",logsPath);
	mkdir(directoryName,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

	filenameList 	= (char**)malloc(1000*sizeof(char*));
	sprintf(fullFilename,"%s%s",datasetPath,fileListName);
	reader = fopen(fullFilename,"r");
	
	while(!feof(reader))
	{
		filenameList[numFiles] = (char*)malloc(200*sizeof(char));
		fscanf(reader,"%s\n",filenameList[numFiles]);
		numFiles += 1;
	}
	fclose(reader);

	for (i = 0; i < numUnitSize; ++i)
	{
		printf("Running test for unit size %d\n",unitSizeVals[i] );
		UINT FILL_THRESHOLD  	= -1*((int)(unitSizeVals[i])/((float)K*COUNTER_SIZE))*log( 1 - pow(FALSE_PSTV_PROB,1.0/K));
		UINT NUM_PARTITION   	= (UINT)(log2(ceil(M*1.0/FILL_THRESHOLD)));
		logFileName 			= (char*)malloc(1000*sizeof(char));
		printf("NAMESPACE     ::%u\n",M );
		printf("FILL_THRESHOLD::%u\n",FILL_THRESHOLD );
		printf("NUM_PARTITION ::%u\n",NUM_PARTITION );
		sprintf(logFileName,"%s/%d.csv",directoryName, unitSizeVals[i]);
		init_bloomParameters(unitSizeVals[i],K,COUNTER_SIZE,NUM_PARTITION,FALSE_PSTV_PROB);
		
		// BloomTree not needed in set creation
		// bloom_tree = setupTreeRange(1,M);
		test_memory_usage_unitSize(filenameList,numFiles,logFileName);
		free(logFileName);
	}
}

// Run test where we vary percentage overalap between A & B with their sizes fixed
void run_setOps_vary_percent_overlap()
{
	int setSizes[6] = {10,100,200,500,1000,2000};
	int numSetSizes = 6;
	UINT K 				= 3;
	UINT COUNTER_SIZE 	= 1;
	UINT M 				= 100000;
	int unitSize 		= 1024;
	float FALSE_PSTV_PROB = 0.01;
	
	char directoryNameInterect[1000], directoryNameUnion[1000];
	sprintf(directoryNameInterect,"%sintersection_dynPartition_overlap",logsPath);	
	sprintf(directoryNameUnion,"%sunion_dynPartition_overlap",logsPath);
	mkdir(directoryNameInterect,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	mkdir(directoryNameUnion,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

	int i,j;	
	for (i = 0; i < numSetSizes; ++i)
	{
		for (j = i; j < numSetSizes; ++j)
		{
			UINT FILL_THRESHOLD  = -1*((int)(unitSize)/((float)K*COUNTER_SIZE))*log( 1 - pow(FALSE_PSTV_PROB,1.0/K));
			UINT NUM_PARTITION   = (UINT)(log2(ceil(M*1.0/FILL_THRESHOLD)));

			init_bloomParameters(unitSize,K,COUNTER_SIZE,NUM_PARTITION,FALSE_PSTV_PROB);
			bloom_tree = setupTreeRange(1,M);

			printf("Running for %d,%d\n",i,j );
			char*	logFileName 	= (char*)malloc(1000*sizeof(char));
			sprintf(logFileName,"../logs/intersection_dynPartition_overlap/%d_%d.csv",setSizes[i],setSizes[j]);
			test_intersection_overlap(setSizes[i],setSizes[j],logFileName);
			free(logFileName);

			logFileName 	= (char*)malloc(1000*sizeof(char));
			sprintf(logFileName,"../logs/union_dynPartition_overlap/%d_%d.csv",setSizes[i],setSizes[j]);
			test_union_overlap(setSizes[i],setSizes[j],logFileName);
			free(logFileName);
		}
	}
}

// Run test where we have fixed size of overlap between A & B but vary size of symmetric difference
// overlapList		: List of overlap sizes for which to run test, 
// numOverlapVals	: Size of overlapList
// numSets 			: number of sets generated for performing union/intersection for a particular value of overlap
// K 				: Number of hash functions
// M 				: Namespace size
// unitSize 		: Size of unit bloom filter for DPBF
// FALSE_PSTV_PROB 	: False positive probability
void run_setOps_fixed_overlap(int* overlapList,int numOverlapVals, int numSets, int K, int M, int unitSize, float FALSE_PSTV_PROB )
{
	UINT COUNTER_SIZE 	= 1;
	
	char directoryNameInterect[1000], directoryNameUnion[1000];
	sprintf(directoryNameInterect,"%sintersection_dynPartition_fixedOverlap",logsPath);	
	sprintf(directoryNameUnion,"%sunion_dynPartition_fixedOverlap",logsPath);
	mkdir(directoryNameInterect,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	mkdir(directoryNameUnion,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	
	UINT FILL_THRESHOLD  = -1*((int)(unitSize)/((float)K*COUNTER_SIZE))*log( 1 - pow(FALSE_PSTV_PROB,1.0/K));
	UINT NUM_PARTITION   = (UINT)(log2(ceil(M*1.0/FILL_THRESHOLD)));
	init_bloomParameters(unitSize,K,COUNTER_SIZE,NUM_PARTITION,FALSE_PSTV_PROB);
	bloom_tree = setupTreeRange(1,M);

	for (int i = 0; i < numOverlapVals; ++i)
	{
		printf("Running for overlap %d\n",overlapList[i]);
		char*	logFileName 	= (char*)malloc(1000*sizeof(char));
		sprintf(logFileName,"%s/%d.csv",directoryNameInterect,overlapList[i]);
		test_intersection_fixed_overlap(overlapList[i],numSets,logFileName);
		free(logFileName);

		logFileName 	= (char*)malloc(1000*sizeof(char));
		sprintf(logFileName,"%s/%d.csv",directoryNameUnion,overlapList[i]);
		test_union_fixed_overlap(overlapList[i],numSets,logFileName);
		free(logFileName);
	}
}

static inline float get_time_diff(struct timeval t1, struct timeval t2)
{
	return t2.tv_sec - t1.tv_sec + 1e-6 * (t2.tv_usec - t1.tv_usec);
}

void check_tree(b_node* filter)
{
	if(filter->left == NULL)
	{
		if(filter->bf == NULL)
		{
			printf("No bloom filter at leaf\n");
		}
	}
	else
	{
		check_tree(filter->left);
		check_tree(filter->right);
		if (filter->bf != NULL)
		{
			printf("Bloom filter present at some internal node\n");
		}
	}
}

void test_batch_insert()
{
	int COUNTER_SIZE 	=  1;
	int M 				= 1000000;
	UINT K				= 3;
	int unitSize 		= 1024;
	float FALSE_PSTV_PROB = 0.01;

	FILE* logger = fopen("tempLog.txt","w");
	UINT FILL_THRESHOLD  	= -1*((int)(unitSize)/((float)K*COUNTER_SIZE))*log( 1 - pow(FALSE_PSTV_PROB,1.0/K));
	UINT NUM_PARTITION   	= (UINT)(log2(ceil(M*1.0/FILL_THRESHOLD)));
	printf("NAMESPACE     ::%u\n",M );
	printf("FILL_THRESHOLD::%u\n",FILL_THRESHOLD );
	printf("NUM_PARTITION ::%u\n",NUM_PARTITION );
	init_bloomParameters(unitSize, K, COUNTER_SIZE, NUM_PARTITION, FALSE_PSTV_PROB);
	bloom_tree = setupTreeRange(1,M);

	b_node * filter = (b_node*)malloc(sizeof(b_node));
	init_without_bloom(filter);
	read_and_insert("tempFile.txt",filter);
	fprintf(logger, "%d\t",count_leaves(filter) );
	estimate_false_positive("tempFile.txt",filter,logger);
	fprintf(logger, "\n");
	
	b_node * filter_1 = (b_node*)malloc(sizeof(b_node));
	init_without_bloom(filter_1);
	read_and_insert("tempFile_1.txt",filter_1);
	fprintf(logger, "%d\t",count_leaves(filter_1) );
	estimate_false_positive("tempFile_1.txt",filter_1,logger);
	fprintf(logger, "\n");

	b_node * filter_2 = (b_node*)malloc(sizeof(b_node));
	init_without_bloom(filter_2);
	read_and_insert("tempFile_2.txt",filter_2);
	fprintf(logger, "%d\t",count_leaves(filter_2) );
	estimate_false_positive("tempFile_2.txt",filter_2,logger);
	fprintf(logger, "\n");

	b_node * filter_3 = (b_node*)malloc(sizeof(b_node));
	init_without_bloom(filter_3);
	read_and_insert("tempFile_3.txt",filter_3);
	fprintf(logger, "%d\t",count_leaves(filter_3) );
	estimate_false_positive("tempFile_3.txt",filter_3,logger);
	fprintf(logger, "\n");

	b_node * filter_union = (b_node*)malloc(sizeof(b_node));
	init_without_bloom(filter_union);

	union_bloom(filter_1,filter_2,filter_union,bloom_tree,NULL);
	fprintf(logger, "Batch 1::%d\t",count_leaves(filter_union) );
	estimate_false_positive("tempFile_1_2.txt",filter_union,logger);
	fprintf(logger, "\n");

	union_bloom(filter_union,filter_3,filter_union,bloom_tree,NULL);
	fprintf(logger, "Batch 2::%d\t",count_leaves(filter_union) );
	estimate_false_positive("tempFile.txt",filter_union,logger);
	fprintf(logger, "\n");

	fprintf(logger, "\n");
	check_tree(filter_1);
	check_tree(filter_2);
	check_tree(filter_3);
	check_tree(filter_union);
}

void test_batch_insert_2()
{
	int COUNTER_SIZE 	=  1;
	int M 				= 1000000;
	UINT K				= 3;
	int unitSize 		= 1024;
	float FALSE_PSTV_PROB = 0.01;

	FILE* logger = fopen("tempLog_2.txt","w");
	UINT FILL_THRESHOLD  	= -1*((int)(unitSize)/((float)K*COUNTER_SIZE))*log( 1 - pow(FALSE_PSTV_PROB,1.0/K));
	UINT NUM_PARTITION   	= (UINT)(log2(ceil(M*1.0/FILL_THRESHOLD)));
	printf("NAMESPACE     ::%u\n",M );
	printf("FILL_THRESHOLD::%u\n",FILL_THRESHOLD );
	printf("NUM_PARTITION ::%u\n",NUM_PARTITION );
	init_bloomParameters(unitSize, K, COUNTER_SIZE, NUM_PARTITION, FALSE_PSTV_PROB);
	bloom_tree = setupTreeRange(1,M);

	b_node * filter = (b_node*)malloc(sizeof(b_node));
	init_without_bloom(filter);
	read_and_insert("tempFile.txt",filter);
	fprintf(logger, "Filter  ::%d\t",count_leaves(filter) );
	estimate_false_positive("tempFile.txt",filter,logger);
	fprintf(logger, "\n");
	
	b_node * filter_1 = (b_node*)malloc(sizeof(b_node));
	init_without_bloom(filter_1);
	read_and_insert("tempFile_1.txt",filter_1);
	fprintf(logger, "Filter 1::%d\t",count_leaves(filter_1) );
	estimate_false_positive("tempFile_1.txt",filter_1,logger);
	fprintf(logger, "\n");

	b_node * filter_2 = (b_node*)malloc(sizeof(b_node));
	init_without_bloom(filter_2);
	read_and_insert_batchWise("tempFile_2.txt",filter_2,filter_1);
	fprintf(logger, "Filter 2::%d\t",count_leaves(filter_2) );
	estimate_false_positive("tempFile_2.txt",filter_2,logger);
	fprintf(logger, "\n");

	b_node * filter_3 = (b_node*)malloc(sizeof(b_node));
	init_without_bloom(filter_3);
	read_and_insert_batchWise("tempFile_3.txt",filter_3,filter_1);
	fprintf(logger, "Filter 3::%d\t",count_leaves(filter_3) );
	estimate_false_positive("tempFile_3.txt",filter_3,logger);
	fprintf(logger, "\n");

	b_node * filter_union = (b_node*)malloc(sizeof(b_node));
	init_without_bloom(filter_union);

	union_bloom(filter_1,filter_2,filter_union,bloom_tree,NULL);
	fprintf(logger, "Batch 1::%d\t",count_leaves(filter_union) );
	estimate_false_positive("tempFile_1_2.txt",filter_union,logger);
	fprintf(logger, "\n");

	union_bloom(filter_union,filter_3,filter_union,bloom_tree,NULL);
	fprintf(logger, "Batch 2::%d\t",count_leaves(filter_union) );
	estimate_false_positive("tempFile.txt",filter_union,logger);
	fprintf(logger, "\n");

	fprintf(logger, "\n");
	check_tree(filter_1);
	check_tree(filter_2);
	check_tree(filter_3);
	check_tree(filter_union);
}

int main(int argc, char** argv)
{
	bloomTestingPath 	= (char*)malloc(1000*sizeof(char));
	datasetPath 		= (char*)malloc(1000*sizeof(char));
	logsPath 			= (char*)malloc(1000*sizeof(char));
	strcpy(bloomTestingPath,"../../../bloomTesting/");
	strcpy(datasetPath,"../../../bloomTesting/datasets/");
	strcpy(logsPath,"../../../bloomTesting/logs/");
	// strcpy(bloomTestingPath,"./");
	// strcpy(datasetPath,"./");
	// strcpy(logsPath,"./");
	time_t t;
	srand((unsigned) time(&t));

	// test_batch_insert_2();
	// exit(0);
	if (argc >=2)
	{
		int testId = atoi(argv[1]);

		if ((testId == 1) && (argc >= 8))
		{
			char* fileListName 		= argv[2];
			int M 					= atoi(argv[3]);
			UINT K 					= atoi(argv[4]);
			float FALSE_PSTV_PROB 	= atof(argv[5]);
			int numUnitSize 		= atoi(argv[6]);
			int* unitSizeVals 		= (int*)malloc(numUnitSize*sizeof(int));
			if (argc < numUnitSize + 7)
			{
				printf("Not enough unit size values specified\n");
				exit(0);
			}

			for (int i = 0; i < numUnitSize; ++i)
			{
				unitSizeVals[i] = atoi(argv[7+i]);
			}
			run_unit_size_analysis(fileListName, M, K, FALSE_PSTV_PROB, numUnitSize, unitSizeVals);
		}
		else if ((testId == 2) && (argc >= 9))
		{
			int M 					= atoi(argv[2]);
			int K 					= atoi(argv[3]);
			float FALSE_PSTV_PROB 	= atof(argv[4]);
			int unitSize 			= atoi(argv[5]);
			int numSets 			= atoi(argv[6]);
			int numOverlapVals 		= atoi(argv[7]);
			int* overlapList 		= (int*)malloc(numOverlapVals*sizeof(int));
			if( argc < numOverlapVals + 8)
			{
				printf("Not enough values for ovarlap specified\n");
				exit(0);
			}
			if ((unitSize % (8*sizeof(int))) != 0)
			{
				printf("Unitsize value should be a multiple of %ld\n",8*sizeof(int));
				exit(0);
			}
			for (int i = 0; i < numOverlapVals; ++i)
			{
				overlapList[i] = atoi(argv[8+i]);
			}
			run_setOps_fixed_overlap(overlapList,numOverlapVals, numSets, K, M, unitSize, FALSE_PSTV_PROB);
		}
		else
		{
			printf("Error:: Not enough arguments after testId\n");
			printf("Usage:: ./test <testId> <other parameters>\n");
			printf("testId = 1 for set creation tests and run test while varying size of unit Bloom filter\n");
			printf("testId = 2 for set intersection/union tests with fixed overlap\n");
			printf("For testId = 1 Usage:: ./test 1 <fileListName> <M> <K> <False Positive probability> <number of unitSizes(n)>  <unit size value_1> ... <unit size value_n>\n");
			printf("For testId = 2 Usage:: ./test 2 <M> <K> <False Positive probability> <unitSize> <number of sets> <number of overlap values(n)>  <overalap value_1> ... <overalap value_n>\n");
		}
	}	
	else
	{
		printf("Error:: Not enough arguments\n");
		printf("Usage:: ./test <testId> <other parameters>\n");
		printf("testId = 1 for set creation tests\n");
		printf("testId = 2 for set intersection/union tests with fixed overlap\n");
		printf("For testId = 1 Usage:: ./test 1 <fileListName> <M> <K> <False Positive probability> <number of unitSizes(n)>  <unit size value_1> ... <unit size value_n>\n");
		printf("For testId = 2 Usage:: ./test 2 <M> <K> <False Positive probability> <unitSize> <number of sets> <number of overlap values(n)>  <overlap value_1> ... <overlap value_n>\n");
	}
}