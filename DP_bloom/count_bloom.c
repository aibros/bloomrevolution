#include "count_bloom.h"
#include "dynamic_array/array.h"
#include "bloom_tree.h"
/*
tree based dynamic bloom filter --
* New components:
* Hash fn -- K level , level as argument
* partition fn -- grows a bloom filter into two based on which level it is
* structure of the dynamic_partition bloom filter
* Binary Tree of bloom filters with a pointer based structure
* Each leaf consists of a bloom filter
* rest is easy to handle
*/



int seed[10];
int aHash[10]={3730, 9830, 3830, 2110, 4740, 9440, 5540, 2710, 690, 7470};
int bHash[10]={269, 229, 929, 949, 624, 390, 64, 67, 945, 253};
int m[10];

int mPrime[10];

bloomParameters* bloomParam;
struct bloomTree* bloom_tree;
char* datasetPath;

void init_bloomParameters(UINT SIZE_BLOOM ,UINT K ,UINT COUNTER_CHUNK,UINT NUM_PARTITION,float FALSE_PSTV_PROB)
{
	bloomParam = (bloomParameters*)malloc(sizeof(bloomParameters));
	bloomParam->NUM_BITS     	= 8*sizeof(UINT);
	bloomParam->SIZE_BLOOM   	= SIZE_BLOOM;
	bloomParam->K 			  	= K;
	bloomParam->COUNTER_CHUNK  	= COUNTER_CHUNK;		//should be exponents of 2
	bloomParam->BIT_MAP 		= pow(2,COUNTER_CHUNK) -1 ;	//2^COUNTER_CHUNK - 1;

	bloomParam->FALSE_PSTV_PROB = FALSE_PSTV_PROB;
	bloomParam->FILL_THRESHOLD  = -1*((int)SIZE_BLOOM/((float)K*COUNTER_CHUNK))*log( 1 - pow(FALSE_PSTV_PROB,1.0/K));
	bloomParam->NUM_PARTITION	= NUM_PARTITION;
	bloomParam->TEST_RANGE 		= 500;

	seiveInitial();
}

void seiveInitial()
{
	int num_bits = bloomParam->SIZE_BLOOM/bloomParam->COUNTER_CHUNK;
	int *x = (int*)malloc(sizeof(int)*num_bits),i,j;

	
	for (i=0;i<num_bits;i++) 
		x[i] = i+1;
	for (i=1;i<num_bits;i++)
	{
		j = x[i];
		if (j==-1) 
			continue;

		int q = 1;
		while ((i + q*j) < num_bits)
		{
			x[i+q*j] = -1;
			q++;
		}
	}
	i = num_bits;
	for (j=0;j<10;j++)
	{
		for (i=i-1;i>0;i--)
		{
			if (x[i]!=-1)
			{
				m[j] = x[i];
				break;
			}
		}
	}
}

/*
* returns the value of the hash function based on the level
* provided as input to the function
*/ 
int partition_hash_prime( UINT val, UINT level )
{
	switch(level)
	{
		case 0:
			return (val%5)%2;				//hash functions to be modified later
		case 1:
			return (val%7)%2;
		case 2:
			return (val%13)%2;
		case 3:
			return (val%17)%2;
		case 4:
			return (val%19)%2;
		case 5:
			return (val%23)%2;
		case 6:
			return (val%29)%2;
		case 7:
			return (val%31)%2;
		case 8:
			return (val%37)%2;
		case 9:
			return (val%43)%2;
		case 10:
			return (val%53)%2;
		case 11:
			return (val%59)%2;
		case 12:
			return (val%61)%2;
		case 13:
			return (val%67)%2;
		case 14:
			return (val%71)%2;
		case 15:
			return (val%73)%2;
		case 16:
			return (val%79)%2;
		case 17:
			return (val%83)%2;
		case 18:
			return (val%89)%2;
		case 19:
			return (val%97)%2;
		case 20:
			return (val%101)%2;
	}

	printf("Value of level is larger than what we can handle\n");
	exit(0);
	return -1;
}

inline int partition_hash( UINT val, UINT level )
{
	switch(level)
	{
		case 0:
			return val & 1;
		case 1:
			return (val & (1<<1))>>1;
		case 2:
			return (val & (1<<2))>>2;
		case 3:
			return (val & (1<<3))>>3;
		case 4:
			return (val & (1<<4))>>4;
		case 5:
			return (val & (1<<5))>>5;
		case 6:
			return (val & (1<<6))>>6;
		case 7:
			return (val & (1<<7))>>7;
		case 8:
			return (val & (1<<8))>>8;
		case 9:
			return (val & (1<<9))>>9;
		case 10:
			return (val & (1<<10))>>10;
		case 11:
			return (val & (1<<11))>>11;
		case 12:
			return (val & (1<<12))>>12;
		case 13:
			return (val & (1<<13))>>13;
		case 14:
			return (val & (1<<14))>>14;
		case 15:
			return (val & (1<<15))>>15;
		case 16:
			return (val & (1<<16))>>16;
		case 17:
			return (val & (1<<17))>>17;
		case 18:
			return (val & (1<<18))>>18;
		case 19:
			return (val & (1<<19))>>19;
		case 20:
			return (val & (1<<20))>>20;
		case 21:
			return (val & (1<<21))>>21;
		case 22:
			return (val & (1<<22))>>22;
		case 23:
			return (val & (1<<23))>>23;
		case 24:
			return (val & (1<<24))>>24;
		case 25:
			return (val & (1<<25))>>25;
		case 26:
			return (val & (1<<26))>>26;
		case 27:
			return (val & (1<<27))>>27;
		case 28:
			return (val & (1<<28))>>28;
		case 29:
			return (val & (1<<29))>>29;
		case 30:
			return (val & (1<<30))>>30;
		case 31:
			return (val & (1<<31))>>31;
	}

	// Switch case based implementation is marginally faster
	// return (val & (1<<level))>>level;

	printf("Value of level is larger than what we can handle\n");
	exit(0);
	return -1;
}

void init_node(bloom *bl)
{
 	int size = (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);
	bl->flag_array = (UINT *)malloc(sizeof(UINT)*size);
	memset(bl->flag_array,0,sizeof(UINT)*size);
	bl->num_elems = 0;
}

/*
* initializes a single b_node to act as root of the tree
* call only for leaves as otherwise the bloom filters would get unnecessarily initialized
*/
void init(b_node *bloomf)
{
	bloomf->bf = (bloom *)malloc(sizeof(bloom));
	init_node(bloomf->bf);								//call only for leaves
	bloomf->left = NULL;
	bloomf->right = NULL;
}

void init_without_bloom(b_node * a)
{
	a->bf = NULL;
	a->left = NULL;
	a->right = NULL;
}


/*
* splits a bloom filter based on the level on which it is present
* the level decides the hash function to use to split
*/
void partition_node(b_node *node,struct bloomTree *bTree,struct bloomNode *Bnode)
{
	// printf("make sure the corresponding node in Tree is provided as input otherwise incorrect, use last input to trace the path to the node in tree\n");

	if( (node->left!=NULL) || (node->right!=NULL) )
	{
		printf("only leaves can be partitioned\n");
		return;
	}

	node->left  =  (b_node *)malloc(sizeof(b_node *));
	node->right =  (b_node *)malloc(sizeof(b_node *));
	init(node->left);
	init(node->right);
	if(Bnode==NULL) // This is the first partition taking place 
	{
		intersect_bloom_node(node->bf, &bTree->left->filter, node->left->bf);
		intersect_bloom_node(node->bf, &bTree->right->filter, node->right->bf);
	}
	else
	{
		intersect_bloom_node(node->bf, &Bnode->lchild->filter, node->left->bf);
		intersect_bloom_node(node->bf, &Bnode->rchild->filter, node->right->bf);
	}

	free_bloom_node(node->bf);				//free the current bloom filter
	node->bf = NULL;
}

// level starts with zero and hence ends at NUM_PARTITION
void grow_full_tree(b_node* node,int level)
{
	if (level == bloomParam->NUM_PARTITION )
	{
		node->bf = (bloom *)malloc(sizeof(bloom));
		init_node(node->bf);
	}
	else
	{
		node->left = (b_node*)malloc(sizeof(b_node));
		node->right = (b_node*)malloc(sizeof(b_node));
		init_without_bloom(node->left);
		init_without_bloom(node->right);
		grow_full_tree(node->left,level+1);
		grow_full_tree(node->right,level+1);
	}
}

void grow_full_tree_upto_level(b_node* node,int currLevel,int maxLevel)
{
	if (currLevel == maxLevel )
	{
		node->bf = (bloom *)malloc(sizeof(bloom));
		init_node(node->bf);
	}
	else if (currLevel < maxLevel)
	{
		node->left  = (b_node*)malloc(sizeof(b_node));
		node->right = (b_node*)malloc(sizeof(b_node));
		init_without_bloom(node->left);
		init_without_bloom(node->right);
		grow_full_tree_upto_level(node->left,currLevel+1,maxLevel);
		grow_full_tree_upto_level(node->right,currLevel+1,maxLevel);
	}
	else
	{
		printf("currLevel is more than maxLevel, we can never terminate is such case\n");
		exit(0);
	}
}

UINT count_leaves(b_node *node)
{
	if((node->left==NULL) && (node->left==NULL))
		return 1;
	else if (node->left==NULL)
	{
		printf("Caution:: Binary tree is not a full binary tree\n");
		return count_leaves(node->right);
	}
	else if (node->right==NULL)
	{
		printf("Caution:: Binary tree is not a full binary tree\n");
		return count_leaves(node->left);
	}
	else
	{
		return count_leaves(node->left)+count_leaves(node->right);
	}
}

void insert_node(UINT val, bloom* bl)
{
	UINT i;
	UINT chunk_per_ind = bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK;
	if(is_in_node(val,bl))
	{
		return;
	}		
	bl->num_elems++;
	for (i=0;i<bloomParam->K;i++)
	{
		UINT a = hash(val,i);
		UINT loc = a/chunk_per_ind;
		UINT off = a%chunk_per_ind;

		UINT new = 1<<(off*bloomParam->COUNTER_CHUNK);
		UINT bitmap = (bloomParam->BIT_MAP<<(off*bloomParam->COUNTER_CHUNK));
		UINT chunk = bitmap & bl->flag_array[loc];
		if(chunk==bitmap)
		{
			// printf("overflow of the bucket for counter -- making no change\n");
			continue;
		}
		bl->flag_array[loc] = bl->flag_array[loc] + new;
	}
}

// level = i implies we are using ith partition functions. So level = 0 means we are not using any partition function at all
// 1st partition function is the one when we pass partition_hash level 0.
// assumes that b_node is a fully grown tree--> need to change code if this needs to be relaxed
// Assumption b_node* node is not NULL and is initialised (with or without bloom filter)
void insert_new(UINT val,b_node* node,int level)
{
	if(level  == bloomParam->NUM_PARTITION) // We should be inserting at this level
	{
		if (node->bf == NULL)
		{
			node->bf = (bloom*)malloc(sizeof(bloom));
			init_node(node->bf);
		}
		insert_node(val,node->bf);
	}
	else  // level + 1 < bloomParam->NUM_PARTITION 
	{	
		int retVal = partition_hash(val,level);
		if (retVal == 0) // Go left
		{
			if(node->left == NULL)
			{
				node->left = (b_node*)malloc(sizeof(b_node));
				init_without_bloom(node->left);
				printf("This should not happen,node->left uninitialised \n");
				exit(0);
			}
			insert_new(val,node->left,level+1);
		}
		else if (retVal == 1) // Go right
		{
			if(node->right == NULL)
			{
				node->right = (b_node*)malloc(sizeof(b_node));
				init_without_bloom(node->right);
				printf("This should not happen,node->right uninitialised \n");
				exit(0);
			}
			insert_new(val,node->right,level+1);
		}
		else
		{
			printf("We have exhausted all partition functions, what to do??\n\n\n\n\n");
		}
	}
}

// Inserts element at the given maxLevel, but make sure that the bloom filter is grown up to that level at least
void insert_at_level(UINT val,b_node* node,int currLevel, int maxLevel)
{
	if(currLevel  == maxLevel) // We should be inserting at this currLevel
	{
		if (node->bf == NULL)
		{
			node->bf = (bloom*)malloc(sizeof(bloom));
			init_node(node->bf);
		}
		insert_node(val,node->bf);
	}
	else if (currLevel < maxLevel) // currLevel + 1 < bloomParam->NUM_PARTITION 
	{	
		int retVal = partition_hash(val,currLevel);
		if (retVal == 0) // Go left
		{
			if(node->left == NULL)
			{
				node->left = (b_node*)malloc(sizeof(b_node));
				init_without_bloom(node->left);
				printf("This should not happen,node->left uninitialised \n");
				exit(0);
			}
			insert_at_level(val,node->left,currLevel+1,maxLevel);
		}
		else if (retVal == 1) // Go right
		{
			if(node->right == NULL)
			{
				node->right = (b_node*)malloc(sizeof(b_node));
				init_without_bloom(node->right);
				printf("This should not happen,node->right uninitialised \n");
				exit(0);
			}
			insert_at_level(val,node->right,currLevel+1,maxLevel);
		}
		else
		{
			printf("We have exhausted all partition functions, what to do??\n\n\n\n\n");
		}
	}
	else
	{
		printf("currLevel is more than maxLevel\n");
		exit(0);
	}
}

void finish_insertion(b_node* node)
{
	if ((node->left == NULL) && (node->right == NULL)) // we are at a leaf 
	{
		// pass
	}
	else
	{
		finish_insertion(node->left);
		finish_insertion(node->right);
		// Check if we can shrink subtree under this node by combining left and right child
		// Before doing so we need to check that both of them have a valid bloomfilter
		// A node having a valid bloom filter is indicative of the fact that subtree under it has shrunken into that node
		if ((node->left->bf != NULL) && (node->right->bf != NULL)) 
		{
			int tempSum = node->left->bf->num_elems + node->right->bf->num_elems;
			if (tempSum < bloomParam->FILL_THRESHOLD + 1) // Shrink left and right child to this node
			{
				// A better way of doint it is to reuse either left or right bloom filters and then change pointer accordingly
				// But doing it this way for now
				///////////////////////////////////////////////////////////////////
				// node->bf =(bloom*)malloc(sizeof(bloom));
				// init_node(node->bf);
				// union_bloom_node(node->bf,node->left->bf,node->bf);
				// union_bloom_node(node->bf,node->right->bf,node->bf);
				// node->bf->num_elems = tempSum;
				// free_bloom(node->left);
				// free_bloom(node->right);
				// node->left 	= NULL;
				// node->right = NULL;
				///////////////////////////////////////////////////////////////////

				union_bloom_node(node->left->bf,node->right->bf,node->left->bf); // Combine left and right BFs and store in left BF
				node->bf = node->left->bf;
				node->bf->num_elems = tempSum;

				free_bloom_node(node->right->bf);
				free(node->right->bf);
				free(node->right);
				free(node->left);
				node->left 	= NULL;
				node->right = NULL;
	
			}
		}
	}
}

int findTreeDepth(b_node* node)
{
	if ((node->left == NULL) && (node->right == NULL))
	{
		return 0;	
	}
	else
	{
		int depth = 0, tempDepth;
		if (node->left != NULL)	
		{
			tempDepth = findTreeDepth(node->left);
			if (tempDepth > depth)
				depth = tempDepth;
		}
		if (node->right != NULL)	
		{
			tempDepth = findTreeDepth(node->right);	
			if (tempDepth > depth)
				depth = tempDepth;
		}
		return depth + 1;
	}
}
/*
*	traverses the tree and inserts at the right node without any expansion
*   if threshold reached then partitioning is no more correct, but in such case
*   the simple membership variant may be used where allocate leaf when threshold reached
*/
void insert(UINT val, b_node * bloomf)
{
	int not_found 		= 1;
	b_node * curr_node  = bloomf;
	UINT curr_level 	= 0;
	// struct bloomNode* currTreeNode = NULL;

	while(not_found)
	{
		UINT bin = partition_hash(val,curr_level);

		if(bin==0)
		{
			if(curr_node->left == NULL)
			{
				insert_node(val, curr_node->bf);
				break;
			}
			curr_node = curr_node->left;
			// if(curr_level==0)
			// 	currTreeNode = bloom_tree->left;
			// else
			// 	currTreeNode = currTreeNode->lchild;
		}
		else if(bin==1)
		{
			if(curr_node->right == NULL)
			{
				insert_node(val, curr_node->bf);
				break;
			}

			curr_node = curr_node->right;
			// if(curr_level==0)
			// 	currTreeNode = bloom_tree->right;
			// else
			// 	currTreeNode = currTreeNode->rchild;
		}
		else
		{
			printf("partition functions exhausted !! review the code !!\n\n\n\n");
			exit(0);
		}

		curr_level++;
	}
}


void delete_node(UINT val, bloom* bl)
{
	UINT i;
	UINT chunk_per_ind = bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK;
	if(!is_in_node(val,bl))
	{
		return;
	}
	if( bl->num_elems > 0 )
		bl->num_elems--;

	for ( i = 0 ; i < bloomParam->K ; i++ )
	{
		UINT a = hash(val,i);
		UINT loc = a/chunk_per_ind;
		UINT off = a%chunk_per_ind;

		UINT new = 1<<(off*bloomParam->COUNTER_CHUNK);
		UINT bitmap = (bloomParam->BIT_MAP<<(off*bloomParam->COUNTER_CHUNK));
		UINT chunk = bitmap & bl->flag_array[loc];

		if( chunk > 0 )
		{
			bl->flag_array[loc] = bl->flag_array[loc] - new;
		}
	}
}

/*
*	traverses the tree and deletes at the right spot
*/
void delete(UINT val, b_node * bloomf)
{
	int not_found 		= 1;
	b_node * curr_node  = bloomf;
	UINT curr_level 	= 0;

	while(not_found)
	{
		int bin = partition_hash(val,curr_level);
		if( bin == 0 )
		{
			if(curr_node->left == NULL)
			{
				if(is_in_node(val,curr_node->bf))
				{
					delete_node(val, curr_node->bf);
				}
				else
					return;		
				break;
			}

			curr_node = curr_node->left;
		}
		else if(bin == 1)
		{
			if(curr_node->right == NULL)
			{
				if(is_in_node(val,curr_node->bf))
				{
					delete_node(val, curr_node->bf);
				}
				else
					return;		
				break;
			}
			curr_node = curr_node->right;
		}
		else
		{
			printf("partition functions exhausted !! review the code !!\n\n\n\n");
			exit(0);
		}

		curr_level++;
	
	}
}


UINT is_in_node(UINT val, bloom* bl)
{
	UINT chunk_per_ind = bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK;
	for (int i = 0 ; i < bloomParam->K ; i++)
	{
		UINT a = hash(val,i);
		UINT loc = a/chunk_per_ind;
		UINT off = a%chunk_per_ind;

		UINT bitmap = (bloomParam->BIT_MAP<<(off*bloomParam->COUNTER_CHUNK));
		UINT chunk = bitmap & bl->flag_array[loc];
		if (!chunk) return 0;
	}
	return 1;
}

/*
*	traverses the tree and checks at the right spot
*/
UINT is_in_unoptimized(UINT val, b_node * bloomf)
{
	int not_found 		= 1;
	b_node * curr_node  = bloomf;
	UINT curr_level 	= 0;
	while(not_found)
	{
		int bin  = partition_hash(val,curr_level);
		if(bin == 0)
		{
			if(curr_node->left == NULL)
			{
				if(is_in_node(val,curr_node->bf))
				{
					return 1;
				}
				else
				{
					return 0;							//element not present in the overall bloom filter
				}
			}
			curr_node = curr_node->left;
		}	
		else if(bin == 1)
		{
			if(curr_node->right == NULL)
			{
				if(is_in_node(val,curr_node->bf))
				{
					return 1;
				}
				else
				{
					return 0;							//element not present in the overall bloom filter
				}
			}
			curr_node = curr_node->right;
		}
		else
		{
			printf("We have exhausted all partition functions\n");
			exit(0);
		}
		curr_level++;
	}
}


UINT is_in(UINT val, b_node * bloomf)
{
	b_node * curr_node  = bloomf;
	UINT curr_level 	= 0;
	while(1)
	{
		if (curr_node->bf != NULL)
		{
			if(is_in_node(val,curr_node->bf))
			{
				return 1;
			}
			else
			{
				return 0;							//element not present in the overall bloom filter
			}
		}
		else
		{
			if(partition_hash(val,curr_level) == 0)
			{
				curr_node = curr_node->left;
			}	
			else
			{
				curr_node = curr_node->right;
			}
		}
		curr_level++;
	}
}

UINT hash(int a, int i)
{
	unsigned long z = aHash[i];
	z = z*a + bHash[i];
	UINT retVal = (z % m[i]);
	return retVal;
}

void free_bloom_node(bloom *bl)
{
	free(bl->flag_array);
	bl->num_elems = 0;
}

/*
* traverse the tree in a dfs fashion and free all memory
*/

void free_bloom(b_node * bloomf)
{

	if(bloomf == NULL)
		return;
	if((bloomf->left==NULL) && (bloomf->right==NULL))
	{
		// printf("Freeing leaf\n");
		if(bloomf->bf != NULL)
		{
			free_bloom_node(bloomf->bf);
			free(bloomf->bf);
		}
		free(bloomf);
		bloomf = NULL;
		return;
	}
	else
	{
		if(bloomf->left!=NULL)
		{
			free_bloom(bloomf->left);
		}
		if(bloomf->right!=NULL)
		{
			free_bloom(bloomf->right);
		}
	}
}

UINT count_elems_node(bloom *a)
{
	UINT count = num_ones(a)/bloomParam->K;
	return count;
}

/*
* traverse the tree in a dfs fashion and count all the elements
*/
UINT count_elems(b_node* bloomf)
{
	if(bloomf->left==NULL && bloomf->right==NULL)
	{
		UINT count = bloomf->bf->num_elems;
		return count;
	}
	else
	{
		UINT count = 0;
		if(bloomf->left != NULL)
			count += count_elems(bloomf->left);

		if(bloomf->right != NULL)
			count += count_elems(bloomf->right);
		return count;
	}
}

UINT num_ones(bloom *a)
{
	UINT i = 0, n = 0;
	for (i = 0 ; i < bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS ; i++)
		n += count_ones(a->flag_array[i]);
	return n;
}

UINT numOnes_intersection_node(bloom *a, bloom *b)
{
	UINT i , j, num = 0, numCounts = 0;

	for (i=0;i<(bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			 UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			 UINT curr = 0;
			if(new<0)
			{
				curr = MIN( (a->flag_array[i]&new)>>bloomParam->COUNTER_CHUNK , (b->flag_array[i]&new)>>bloomParam->COUNTER_CHUNK );
				curr = curr<<bloomParam->COUNTER_CHUNK;
			}
			else
				curr = MIN( (a->flag_array[i]&new) , (b->flag_array[i]&new) );
			temp+=curr;
		}
		num += count_ones(temp);										//can be made efficient
	}
	return num;
}

double elemsIntersection_node(bloom *a, bloom *b, int ta, int tb)
{
	double t1 = (double)ta;
	double t2 = (double)tb;
	double th = (double)numOnes_intersection_node(a,b);
	return th/bloomParam->K;
}

UINT count_ones(UINT x)
{
	UINT i = 0;
	UINT count = 0;
	for(i = 0 ; i < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; i++)
	{
		UINT new = bloomParam->BIT_MAP<<(i*bloomParam->COUNTER_CHUNK);
		UINT intersect = x&new;
		count += intersect>>(i*bloomParam->COUNTER_CHUNK);
	}

	return count;
}


UINT * reconstruct_dict_attack(bloom* bl, UINT start, UINT end )
{
	UINT i;
	UINT num_elems,elem = 0;
	for(i = start; i <= end; i++)
	{
		if(is_in_node(i,bl))
			num_elems++;
	}

	UINT * val_array = (UINT *)malloc(sizeof(UINT)*num_elems);
	
	memset(val_array, 0, sizeof(UINT)*num_elems);
	for(i = start; i <= end; i++)
	{
		if(is_in_node(i,bl))
		{
			val_array[elem] = i;
			elem++;
		}
	}
	return val_array;
}

//slightly inefficient as twice time taken for now but for complete reconstruction use this 
void reconstruct_complete(b_node* a,my_array_long * return_array)	
{
	if(a->left==NULL)
	{
		my_array_long* reconstruct_array;
		UINT size_array,i = 0;
		reconstructSet( bloom_tree ,a->bf, &reconstruct_array, &size_array);
		for(i = 0 ; i < size_array ; i++)
		{
			arrayLong_addElement(return_array, reconstruct_array->elements[i]);
		}
		arrayLong_free(reconstruct_array);
	}
	else
	{
		reconstruct_complete(a->left,return_array);
		reconstruct_complete(a->right,return_array);
	}
}

void print_bloom_filter_node(bloom *a)
{
	int i,j;
	for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			printf("%0*d ",bloomParam->COUNTER_CHUNK,(a->flag_array[i]&new)>>(j*bloomParam->COUNTER_CHUNK));
		}
	}
	printf("\n");
}

void print_dyn_part_bloom_filter(b_node *b)
{
	if(b->left == NULL && b->right == NULL)
	{
		print_bloom_filter_node(b->bf);
	}
	else
	{
		if(b->left != NULL)
			print_dyn_part_bloom_filter(b->left);
		if(b->right != NULL)
			print_dyn_part_bloom_filter(b->right);
	}
}


/************************************/
void intersect_bloom_node(bloom *a, bloom *b, bloom *c)
{
	if(bloomParam->COUNTER_CHUNK > 1)
	{
		printf("Please set the bin size to one!!\n");
		intersect_bloom_count_node(a,b,c);
	}

	int i;
	for(i = 0 ; i < bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS ; i++)
	{
		UINT temp = a->flag_array[i] & b->flag_array[i];
		c->flag_array[i] = temp;
	}
}

void union_bloom_node(bloom *a, bloom *b, bloom *c)
{
	if(bloomParam->COUNTER_CHUNK > 1)
	{
		printf("Please set the bin size to one!!\n");
		union_bloom_count_node(a,b,c);

	}

	int i;
	for(i = 0 ; i < bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS ; i++)
	{
		// printf("%d-",i );
		UINT temp = a->flag_array[i] | b->flag_array[i];
		c->flag_array[i] = temp;
	}
	// printf("\n");
	c->num_elems = count_elems_node(c);
}
/************************************/



void intersect_bloom_count_node(bloom *a, bloom *b, bloom *c)
{
	UINT i,j,num;
	for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			temp += MIN(a->flag_array[i]&new,b->flag_array[i]&new);
		}
		c->flag_array[i] = temp;
	}
}

void subtract_bloom_count_node(bloom *a, bloom *b, bloom *c)
{
	UINT i,j,num=0;
	for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			if ( (a->flag_array[i]&new) >= (b->flag_array[i]&new) )
				temp += (a->flag_array[i]&new)-(b->flag_array[i]&new);
		}
		c->flag_array[i] = temp;
	}
}

void add_bloom_count_node(bloom *a, bloom *b, bloom *c)
{
	UINT i,j,num=0;
	for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			UINT curr = (a->flag_array[i]&new)+(b->flag_array[i]&new);
			if(curr > new && j!=(bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK-1))
			{
				curr = new;
			}
			else if(j==(bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK-1))			//as this chunk stores the sign bit so it is dealt separately as in this case it can overflow
			{
				curr = (a->flag_array[i]&new)>>2+(b->flag_array[i]&new)>>2;
				if(curr > (new>>2))
					curr = new;
				else
					curr = curr<<2;
			}
			temp+=curr;
		}
		c->flag_array[i] = temp;
	}
}

void union_bloom_count_node(bloom *a, bloom *b, bloom *c)
{
	UINT i,j,num=0;
	for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			temp += MAX(a->flag_array[i]&new,b->flag_array[i]&new);
		}
		c->flag_array[i] = temp;
	}
}

void combine_leafs(b_node *a, bloom *temp)
{
	if(a==NULL)
	{
		printf("The bloom node for compression is not initialized\n");
		return;
	}
	if(a->bf != NULL)
	{
		union_bloom_node(a->bf,temp,temp);
	}
	else
	{
		combine_leafs(a->left, temp);
		combine_leafs(a->right, temp);
	}
}


///////////////////////////////

void intersect_k(b_node **a, int len_a, b_node *c)
{
	//check a->left
	int i = 0;
	int check = 0;
	for( i = 0 ; i < len_a ; i++)
	{

		if(a[i]->bf != NULL)
		{
			check = 1;
		}
		else if(a[i]->left == NULL || a[i]->right == NULL)
		{
			printf("problem\n");
			exit(0);
		}
	}

	if(check == 0)
	{
		if(c->left==NULL)	//if it is not declared here it won't be linked
		{
			c->left = (b_node *)malloc(sizeof(b_node));
			init_without_bloom(c->left);
		}
		if(c->right==NULL)
		{
			c->right = (b_node *)malloc(sizeof(b_node));
			init_without_bloom(c->right);
		}

		b_node **b = (b_node **)malloc(len_a*sizeof(b_node*));
		int j = 0;
		for( j = 0 ; j < len_a ; j++ )
		{
			b[j] = a[j]->left;
			a[j] = a[j]->right;
		}
		intersect_k(a, len_a, c->right);
		intersect_k(b, len_a, c->left);

	}
	else
	{
		if(c->bf==NULL)
			init(c);

		if( a[0]->bf == NULL )
		{
			bloom *temp = (bloom *)malloc(sizeof(bloom));
			init_node(temp);
			combine_leafs(a[0],temp);
			union_bloom_node(temp, c->bf, c->bf);
			free_bloom_node(temp);
		}
		else
			union_bloom_node(a[0]->bf, c->bf, c->bf);

		int j = 1;
		for( j = 1 ; j < len_a ; j++ )
		{
			// printf("*%d-",a[j] );
			if( a[j]->bf == NULL )
			{
				bloom *temp = (bloom *)malloc(sizeof(bloom));
				init_node(temp);
				combine_leafs(a[j],temp);
				intersect_bloom_node(temp, c->bf, c->bf);
				free_bloom_node(temp);
			}
			else
			{
				intersect_bloom_node(a[j]->bf, c->bf, c->bf);
			}
		}
		// printf("----\n");
		// exit(0);
	}
}

void intersect_k_sans_compression(b_node **a, int len_a, b_node *c)
{
	//check a->left

	int i = 0;
	int check = 1;
	for( i = 0 ; i < len_a ; i++)
	{

		if(a[i]->bf == NULL)
		{
			check = 0;
			if(a[i]->left == NULL || a[i]->right==NULL)
				printf("this is an issue\n");
		}
	}

	if(check == 0)
	{
		if(c->left==NULL)	//if it is not declared here it won't be linked
		{
			c->left = (b_node *)malloc(sizeof(b_node));
			init_without_bloom(c->left);
		}
		if(c->right==NULL)
		{
			c->right = (b_node *)malloc(sizeof(b_node));
			init_without_bloom(c->right);
		}



		b_node **b = (b_node **)malloc(len_a*sizeof(b_node*));
		int j = 0;
		for( j = 0 ; j < len_a ; j++ )
		{
			if(a[j]->bf == NULL)
			{
				b[j] = a[j]->left;
				a[j] = a[j]->right;
			}
			else
			{
				b[j] = a[j];
			}
		}
		intersect_k_sans_compression(a, len_a, c->right);
		intersect_k_sans_compression(b, len_a, c->left);
		// exit(0);

	}
	else
	{
		if(c->bf==NULL)
			init(c);

		union_bloom_node(a[0]->bf, c->bf, c->bf);

		int j = 1;
		for( j = 1 ; j < len_a ; j++ )
		{
			intersect_bloom_node(a[j]->bf, c->bf, c->bf);
		}
	}
}


///////////////////////////////

void intersection_with_compression(b_node *a, b_node *b, b_node *c)
{
	if(c==NULL)
	{
		c = (b_node *)malloc(sizeof(b_node));
		init_without_bloom(c);
	}
	//going recursive
	if(a->left==NULL && b->left==NULL)			//both are leaves and at same level
	{
		if(c->left!=NULL)					//as c may not necessarily be empty , can be possibly a or b even
			free_bloom(c->left);
		if(c->right!=NULL)
			free_bloom(c->right);

		if(c->bf==NULL)
			init(c);
		intersect_bloom_node(a->bf, b->bf, c->bf);
	}
	else if(a->left != NULL && b->left == NULL)
	{
		bloom *temp = (bloom *)malloc(sizeof(bloom));
		init_node(temp);
		combine_leafs(a,temp);

		if(c->left!=NULL)					//as c may not necessarily be empty , can be possibly a or b even
			free_bloom(c->left);
		if(c->right!=NULL)
			free_bloom(c->right);

		if(c->bf==NULL)
			init(c);
		intersect_bloom_node(b->bf, temp, c->bf);
		// printf("--- %d %d %d\n",c->bf->num_elems, b->bf->num_elems, temp->num_elems );
	}
	else if(a->left == NULL && b->left != NULL)
	{
		bloom *temp = (bloom *)malloc(sizeof(bloom));
		init_node(temp);
		combine_leafs(b,temp);

		if(c->left!=NULL)					//as c may not necessarily be empty , can be possibly a or b even
			free_bloom(c->left);
		if(c->right!=NULL)
			free_bloom(c->right);

		if(c->bf==NULL)
			init(c);
		intersect_bloom_node(a->bf, temp, c->bf);
		// printf("--- %d %d %d\n",c->bf->num_elems, a->bf->num_elems, temp->num_elems );
	}
	else
	{	
		if(c->left==NULL)	//if it is not declared here it won't be linked
		{
			c->left = (b_node *)malloc(sizeof(b_node));
			init_without_bloom(c->left);
		}
		if(c->right==NULL)
		{
			c->right = (b_node *)malloc(sizeof(b_node));
			init_without_bloom(c->right);
		}
		intersection_with_compression(a->left, b->left, c->left);
		intersection_with_compression(a->right, b->right, c->right);
	}
}

// this is the intersection algorithm picked up as the final approach
void intersect_sans_expansion(b_node *a, b_node *b, b_node* c)
{
	if(c==NULL)
		init_without_bloom(c);
	//going recursive
	if(a->left==NULL && b->left==NULL)			//both are leaves and at same level
	{
		if(c->left!=NULL)					//as c may not necessarily be empty , can be possibly a or b even
			free_bloom(c->left);
		if(c->right!=NULL)
			free_bloom(c->right);

		if(c->bf==NULL)
			init(c);
		intersect_bloom_node(a->bf, b->bf, c->bf);
	}
	else
	{
		if(c->left==NULL)
		{
			c->left = (b_node *)malloc(sizeof(b_node));
			init_without_bloom(c->left);
		}
		if(c->right==NULL)
		{
			c->right = (b_node *)malloc(sizeof(b_node));
			init_without_bloom(c->right);
		}

		if(a->left==NULL && b->left!=NULL)
		{
			intersect_sans_expansion(a ,b->left, c->left);
			intersect_sans_expansion(a ,b->right, c->right);
		}
		else if(a->left!=NULL && b->left==NULL)	//b ends and a continues so a needs to be compressed
		{
			intersect_sans_expansion(a->left, b, c->left);
			intersect_sans_expansion(a->right, b, c->right);
		}
		else //both are not leaves so initializing both children of c and 
		{
			intersect_sans_expansion(a->left, b->left, c->left);
			intersect_sans_expansion(a->right, b->right, c->right);
		}
	}
}

// We want to do union of "a" & "b"and store it in "c"
// "a" is the shorter subtree of "a" and "b"
// "b" is the larger subtree of "a" and "b"
void expand_tree(b_node *a, b_node *b, b_node *c,struct bloomTree *bTree,struct bloomNode *bNode)	//takes b and replicates it in c separately b would definitely have some depth
{	
	if(b->bf!=NULL)// b has hit a leaf, time to take union now!!
	{
		if(b != c)
		{
			c->bf = (bloom *)malloc(sizeof(bloom));
			init_node(c->bf);
			intersect_bloom_node(a->bf, &bNode->filter, c->bf);
			union_bloom_node(c->bf,b->bf,c->bf);	
		}
		else// if b and c are same, then we can use c->bf to store temporary intersection of a->bf and bloomNode->filter
		{	
			bloom* tempBF = (bloom *)malloc(sizeof(bloom));
			init_node(tempBF);
			intersect_bloom_node(a->bf, &bNode->filter, tempBF);
			union_bloom_node(tempBF,b->bf,c->bf);
		}
	}
	else
	{
		if(b != c)
		{
			c->left = (b_node *)malloc(sizeof(b_node));
			c->right = (b_node *)malloc(sizeof(b_node));
			init_without_bloom(c->left);
			init_without_bloom(c->right);	
		}
		if(bNode==NULL)	//case when a at root , a and b both at root will not happen
		{
			expand_tree(a,b->left,c->left,bTree,bTree->left);
			expand_tree(a,b->right,c->right,bTree,bTree->right);
		}
		else
		{
			expand_tree(a,b->left,c->left,bTree,bNode->lchild);
			expand_tree(a,b->right,c->right,bTree,bNode->rchild);
		}
	}
}

void union_bloom(b_node *a, b_node *b, b_node *c,struct bloomTree *bTree,struct bloomNode *bNode)
{

	if(a->left==NULL && b->left==NULL)			//both are leaves and at same level
	{
		if(c->left!=NULL)					//as c may not necessarily be empty , can be possibly a or b even
		{
			free_bloom(c->left);
			free_bloom(c->right);
		}
		if(c->bf==NULL)
			init(c);
		union_bloom_node(a->bf, b->bf, c->bf);
	}
	else if(a->left==NULL && b->left!=NULL)
	{
		expand_tree(a,b,c,bTree,bNode);
		if( a == c) // If they are same, it would lead "a"(same as "c") to be grown to lower levels with a BF present at some intermediate node
		{
			free_bloom_node(a->bf);
			free(a->bf);
			a->bf = NULL;
		}
	}
	else if(a->left!=NULL && b->left==NULL)	//b ends and a continues so a needs to be compressed
	{
		expand_tree(b,a,c,bTree,bNode);
		if( b == c) // If they are same, it would lead "b"(same as "c") to be grown to lower levels with a BF present at some intermediate node
		{
			free_bloom_node(b->bf);
			free(b->bf);
			b->bf = NULL;
		}
	}
	else //both are not leaves so initializing both children of c and 
	{
		if(c->left==NULL)
		{
			c->left = (b_node *)malloc(sizeof(b_node));
			init_without_bloom(c->left);
		}
		if(c->right==NULL)
		{
			c->right = (b_node *)malloc(sizeof(b_node));
			init_without_bloom(c->right);
		}
		if(bNode != NULL)
		{
			union_bloom(a->left, b->left, c->left, bTree, bNode->lchild);					//recursion continues
			union_bloom(a->right, b->right, c->right, bTree, bNode->rchild);
		}
		else // starting from root
		{
			union_bloom(a->left, b->left, c->left, bTree, bTree->left);					//recursion continues
			union_bloom(a->right, b->right, c->right, bTree, bTree->right);
		}
	}
}


void finish_batch_insertion(b_node* refNode, b_node* node)
{
	if ((node->left == NULL) && (node->right == NULL)) // we are at a leaf 
	{
		// pass
	}
	else
	{
		if (refNode->left != NULL)
		{
			finish_batch_insertion(refNode->left,node->left);
		}
		else // refNode is a leaf
		{
			finish_batch_insertion(refNode,node->left);	
		}

		if (refNode->right != NULL)
		{
			finish_batch_insertion(refNode->right,node->right);
		}
		else// refNode is a leaf
		{
			finish_batch_insertion(refNode,node->right);	
		}
		// Check if we can shrink subtree under this node by combining left and right child
		// Before doing so we need to check that both of them have a valid bloomfilter
		// A node having a valid bloom filter is indicative of the fact that subtree under it has shrunken into that node
		if ((node->left->bf != NULL) && (node->right->bf != NULL)) 
		{

			if ( (refNode->left == NULL) && (refNode->right == NULL) && (refNode->bf != NULL) ) // We may combine left and right child of node, only is refNode is a leaf
			{
				int tempSum = node->left->bf->num_elems + node->right->bf->num_elems;
				if (tempSum < bloomParam->FILL_THRESHOLD + 1) // Shrink left and right child to this node
				{
					union_bloom_node(node->left->bf,node->right->bf,node->left->bf); // Combine left and right BFs and store in left BF
					node->bf = node->left->bf;
					node->bf->num_elems = tempSum;

					free_bloom_node(node->right->bf);
					free(node->right->bf);
					free_bloom(node->right);
					free_bloom(node->left);
					node->left 	= NULL;
					node->right = NULL;
		
				}	
			}
		}
	}
}


void bloom_test_suit1()
{
	b_node *a = (b_node*)malloc(sizeof(b_node));
	init(a);
	UINT i;
	printf("bloom graph insert/member/delete test initiated\n");
	// partition_node(a);
	for(i = 0 ; i < bloomParam->TEST_RANGE ; i++ )
	{
		UINT z = i;//rand()%bloomParam->TEST_RANGE;
		insert(z,a);
		if(!is_in(z,a))
		{
			printf("bloom graph not functioning properly - inserted val :: %d not found\n",z);
			exit(0);
		}
		// delete(z,a);
	}
	// partition_node(a,bloom_tree,NULL);

	printf("count of elems :: %u  %u \n",count_elems(a),bloomParam->TEST_RANGE );
	free_bloom(a);
	printf("bloom graph insert/member/delete test successful\n");
}

void bloom_test_suit2()
{
	b_node *a = (b_node*)malloc(sizeof(b_node));
	b_node *b = (b_node*)malloc(sizeof(b_node));

	init(a);	
	init(b);

	UINT i;
	printf("bloom graph union/intersect test initiated bloomParam->SIZE_BLOOM :: %u\n",bloomParam->SIZE_BLOOM);
	UINT count_a = 0,count_b = 0;
	for( i = 0 ; i < bloomParam->TEST_RANGE ; i++ )
	{
		if(i%2==0)
		{
			insert(i,a);
			if(!is_in(i,a))
				printf("element not found a %u\n",i);
			count_a++;
		}
		else
		{
			insert(i,b);
			if(!is_in(i,b))
				printf("element not found b  %u\n",i);
			count_b++;
		}
	}
	// partition_node(a,bloom_tree,NULL);
	// partition_node(a,bloom_tree,NULL);

	printf("insertion stage over\n");
	printf("size a :: %u actual:: %u\n",count_elems(a),count_a );	//the number of elements may be less as some counter may completely fill up
	printf("size b :: %u actual:: %u\n",count_elems(b),count_b );	//

	b_node * c = (b_node *)malloc(sizeof(b_node));
	init_without_bloom(c);
	
	b_node * d = (b_node *)malloc(sizeof(b_node));
	init_without_bloom(d);	

	intersect_sans_expansion(a,b,c);
	union_bloom(a,b,d,bloom_tree,NULL);



	printf("union/intersection operations executed -- size union size::%u intersect::%u \n",count_elems(d),count_elems(c));
	// printf("intersection count via-- current elemsIntersection::%f \n",elemsIntersection(a,b,num_ones(a),num_ones(b)));
	// exit(0);

	ULONG con_union = 0;
	ULONG con_inter = 0;
	for( i = 0 ; i < bloomParam->TEST_RANGE ; i++ )
	{

		if(!is_in(i,d))
		{
			con_union++;
			// exit(0);
		}
		if(is_in(i,c)==1)
		{
			con_inter++;
		}
	}
	printf("errors_union :: %lu errors_intersect :: %lu \n",con_union,con_inter );

	free_bloom(a);
	free_bloom(b);
	free_bloom(c);
	printf("bloom graph union/intersect test successful\n");
}

void level_partition_node(b_node *a, b_node *curr, UINT level,struct bloomTree *bTree,struct bloomNode *Bnode)
{
	if(a->bf == NULL)
	{
		printf("level : %d\n",level );
	}
	if(level == 0)
	{
		if(Bnode==NULL)
		{
			printf("please enter a level greater than 0 or node not present at root. Otherwise no splitting\n");
		}
		else
		{
			init(curr);
			intersect_bloom_node(a->bf, &Bnode->filter, curr->bf);
		}
	}
	else
	{
		if(Bnode==NULL)
		{
			curr->left = (b_node *)malloc(sizeof(b_node));
			curr->right = (b_node *)malloc(sizeof(b_node));
			level_partition_node(a,curr->left,level-1,bTree,bTree->left);
			level_partition_node(a,curr->right,level-1,bTree,bTree->right);
		}
		else
		{
			curr->left = (b_node *)malloc(sizeof(b_node));
			curr->right = (b_node *)malloc(sizeof(b_node));
			level_partition_node(a,curr->left,level-1,bTree,Bnode->lchild);
			level_partition_node(a,curr->right,level-1,bTree,Bnode->rchild);
		}
	}
}

void bloom_test_suit3()
{
	//test parameters
	UINT M = 1000;
	UINT elemCount = 100;
	UINT size_bloom = 512;
	UINT k = 3;
	UINT counter_chunk = 1;
	UINT num_partition = 10;
	float fp_prob  = 0.001;

	//initialization
 	init_bloomParameters(size_bloom,k,counter_chunk,num_partition,fp_prob);
 	seiveInitial();
	bloom_tree = setupTreeRange(1,M);

	//test cases
	//1: splitting vs non splitting comparison 
	UINT num_splits = 5;
	b_node a;
	b_node b;
	b_node c;
	b_node d;
	init(&a);
	init(&b);
	init(&c);
	init(&d);
	grow_full_tree(&d,0);

	level_partition_node(&c,&c,num_splits,bloom_tree,NULL);
	free_bloom_node(c.bf);

	int ind = 1;
	for(;ind <= elemCount ; ind++)
	{
		insert_node(ind,a.bf);
		insert_node(ind,b.bf);
		insert(ind,&c);
		insert_new(ind,&d,0);
	}
	finish_insertion(&d);

	printf("start from here -- \n\n");
	level_partition_node(&b,&b,num_splits,bloom_tree,NULL);
	free_bloom_node(b.bf);
	b.bf = NULL;

	int count_a = 0;
	int count_b = 0;
	int count_c = 0;
	int count_d = 0;
	for(ind = 1; ind <= M; ind++)
	{
		// printf("Searching is a :: \t");
		if(is_in(ind,&a))
		{
			count_a++;
			if(is_in(ind,&b))
			{
				count_b++;
			}
			else
			{
				printf("Element %d is present in A but not in B, so we have some benefit of partitioning\n",ind );
				exit(0);
			}
		}
		else
		{
			if(is_in(ind,&b))
			{
				printf("Element not present in A but present in B\n");
				exit(0);
			}
		}
		// printf("Searching is b :: \t");
		if(is_in(ind,&c))
			count_c++;
		if(is_in(ind,&d))
			count_d++;
		// if(is_in(ind,&a) && !is_in(ind,&b))
		// {
		// 	printf("Present is original but not is partitioned::%d\n",ind);
		// }
	}

	printf("empirical false positives probability for a : %f b : %f  c: %f d: %f\n", (float)count_a/(float)M,(float)count_b/(float)M, (float)count_c/(float)M,(float)count_d/(float)M  );
	printf("num leaves in a : %d b : %d c : %d d : %d \n",count_leaves(&a),count_leaves(&b),count_leaves(&c),count_leaves(&d) );
	free_bloom(&a);
	free_bloom(&b);
	free_bloom(&c);
	free_bloom(&d);

	//intersection check

	b_node e;
	b_node f;
	b_node g;
	init(&a);
	init(&b);
	init(&c);
	init(&d);

	init(&e);
	init(&f);
	init(&g);
	grow_full_tree(&d,0);
	level_partition_node(&c,&c,num_splits,bloom_tree,NULL);
	free_bloom_node(c.bf);

	for(ind = 0;ind <= elemCount ; ind++)
	{
		if(ind%2==1)
		{
			insert_node(ind,a.bf);
			if(ind%17 == 1)
			{
				insert_node(ind,b.bf);
				insert(ind,&c);
				insert_new(ind,&d,0);
				printf("%d \n",ind);
			}

		}
		else
		{
			insert_node(ind,b.bf);
			insert(ind,&c);
			insert_new(ind,&d,0);
		}
		
	}
	level_partition_node(&b,&b,num_splits,bloom_tree,NULL);
	free_bloom_node(b.bf);
	b.bf = NULL;
	finish_insertion(&d);

	intersect_sans_expansion(&a,&c,&e);

	intersect_sans_expansion(&a,&b,&f);

	intersect_sans_expansion(&a,&d,&g);

	int count_e = 0;
	int count_f = 0;
	int count_g = 0;
	// int count_h = 0;
	for(ind = 1; ind <= M; ind++)
	{
		if(is_in(ind,&e))
		{
			count_e++;
		}
		if(is_in(ind,&f))
		{
			count_f++;
		}
		if(is_in(ind,&g))
		{
			count_g++;
		}
		// if(is_in(ind,&h))
		// {
		// 	count_h++;
		// }
	}
	printf("num leaves in e : %d f : %d g : %d \n",count_leaves(&e),count_leaves(&f),count_leaves(&g) );
	printf("%d %d %d \n",count_e,count_f,count_g);
	printf("empirical false positives probability for e : %f f : %f  g : %f\n", (float)count_e/(float)M,(float)count_f/(float)M,(float)count_g/(float)M);

	//seeing union
	b_node x;
	b_node y;
	b_node z;
	init(&x);
	init(&y);
	init(&z);
	
	int count_x = 0;
	int count_y = 0;
	int count_z = 0;
	
	union_bloom(&a,&b,&x,bloom_tree,NULL);
	union_bloom(&a,&c,&y,bloom_tree,NULL);
	union_bloom(&a,&d,&z,bloom_tree,NULL);

	for(ind = 1; ind <= M; ind++)
	{
		if(is_in(ind,&x))
		{
			count_x++;
		}
		if(is_in(ind,&y))
		{
			count_y++;
		}
		if(is_in(ind,&z))
		{
			count_z++;
		}
	}


	printf("\nunion analysis\n\n");
	printf("num leaves in x : %d y : %d z : %d \n",count_leaves(&x),count_leaves(&y),count_leaves(&z) );
	printf("%d %d %d \n",count_x,count_y,count_z);
	printf("empirical false positives probability for x : %f y : %f  z : %f\n", (float)count_x/(float)M,(float)count_y/(float)M,(float)count_z/(float)M);



	free_bloom(&a);
	free_bloom(&b);
	free_bloom(&c);
	free_bloom(&d);
	free_bloom(&e);
	free_bloom(&f);
	free_bloom(&g);
	free_bloom(&x);
	free_bloom(&y);
	free_bloom(&z);
	// free_bloom(&h);
}
