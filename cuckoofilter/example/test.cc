#include "cuckoofilter.h"

#include <assert.h>
#include <math.h>

#include <iostream>
#include <vector>

using cuckoofilter::CuckooFilter;

int main(int argc, char **argv) 
{
  int total_items = 1000000;

  // Create a cuckoo filter where each item is of type size_t and
  // use 12 bits for each item:
  //    CuckooFilter<size_t, 12> filter(total_items);
  // To enable semi-sorting, define the storage of cuckoo filter to be
  // PackedTable, accepting keys of size_t type and making 13 bits
  // for each key:
  //   CuckooFilter<size_t, 13, cuckoofilter::PackedTable> filter(total_items);
  CuckooFilter<int, 12> filter(total_items);

  // Insert items to this cuckoo filter
  int num_inserted = 0;
  for (int i = 0; i < total_items; i++, num_inserted++) 
  {
    // if (filter.Add(i) != cuckoofilter::Ok) 
    // {
    //   break;
    // }
  }

  // Check if previously inserted items are in the filter, expected
  // true for all items
  for (int i = 0; i < num_inserted; i++) 
  {
    // assert(filter.Contain(i) == cuckoofilter::Ok);
  }

  // Check non-existing items, a few false positives expected
  int total_queries = 0;
  int false_queries = 0;
  for (int i = total_items; i < 2 * total_items; i++) 
  {
    if (filter.Contain(i) == cuckoofilter::Ok) 
    {
      false_queries++;
    }
    total_queries++;
  }

  // Output the measured false positive rate
  std::cout << "false positive rate is "<< 100.0 * false_queries / total_queries << "%\n";
  printf("Size of cuckoofilter::%d\n",filter.SizeInBytes());
  printf("Bits per item::%d\n",(filter.SizeInBytes()*8)/total_items );
  printf("Size of data stored ::%d\n",total_items*4);
  printf("Percent Compression ::%f\n",filter.SizeInBytes()*1.0/(total_items*4));
  return 0;
}
