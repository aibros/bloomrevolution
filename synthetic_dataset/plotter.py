import matplotlib.pyplot as plt
import matplotlib
from operator import add
import os
from pprint import pprint
import numpy as np


types = ['k_intersection_with_compression', 'standard_inv_index', 'simple_intersection', 'simple_k_intersection', 'membership_query', 'intersection_with_compression']

def plot_graph( fixed_vars , Y_label, X_label , instance_labels, X, Y,  save_name):
	# print instance_labels
	print save_name
	fig = plt.figure(figsize=(16, 9), dpi=80);
	ax = fig.add_subplot(111)
	# print(X)
	for ind in range(len(Y)):	
		# print(Y[ind])
		plt.plot(np.array(X),np.array(Y[ind]),marker='o',ls='-');
		plt.xticks(X)
		# for xy in zip(X, Y[ind]):                                       
		# 	ax.annotate('(%s, %s)' % xy, xy=xy, textcoords='data')

	# plt.xscale('log')
	plt.legend(instance_labels, loc='upper left')
	plt.suptitle(Y_label+" vs "+X_label+" : "+fixed_vars);
	plt.xlabel(X_label);
	plt.ylabel(Y_label);
	# exit(0);
	fig.savefig(save_name+".png")


def get_response_files(folder_names, query_id ):
	time_dict = {}
	response_dict = {}

	for folder in folder_names:
		folder_name = folder+"_response"
		for filename in os.listdir(folder_name):
			if "q"+str(query_id)+"#" in filename:
				file = open(folder_name+"/"+filename,"r");
				method = filename.split("#")[1]
				data = file.read().strip().split("\n");
				temp_time = [];
				temp_resp = [];
				for ind in range(len(data)):
						t_label,t,res_label,res,c,terms = data[ind].split(",");
						temp_time += [float(t)]
						temp_resp += [float(res)]
				if method in time_dict:
					time_dict[method] = map(add, temp_time, time_dict[method]);
					response_dict[method] = map(add, temp_resp, response_dict[method]);
				else:
					time_dict[method] = temp_time;
					response_dict[method] = temp_resp;
				file.close();
	count = len(folder_names);
	#averaging
	for key in time_dict.keys():
		time_dict[key] = map( lambda x: x/count, time_dict[key]);
		response_dict[key] = map( lambda x: x/count, response_dict[key]);

	return (time_dict,response_dict)


def get_y_vals(response_dict, start, end):
	Y = []
	for ind in range(len(types)):
		k = types[ind]
		# print k
		temp_y = response_dict[k][start:end]
		Y += [temp_y]
	return Y;


#read a folder, first read a meta file, then go read the responses and draw a single graph
def plot(folder_names, num_queries, GRAPHS):
	for q in range(1,num_queries+1):
		meta_file = folder_names[0] + "/m"+str(q);
		meta = open(meta_file,"r")
		temp = meta.readline().split(",")
		time_dict,response_dict = get_response_files(folder_names, q);
		x_var = temp[0];
		num_graph = int(temp[1]);
		counter = 0;
		for k in xrange(num_graph):
			fixed_vars = meta.readline().split(",")
			num_xvar = int(fixed_vars[-1])
			fixed_vars = fixed_vars[:-1]
			X = []
			for v in xrange(num_xvar):
				X += [float(meta.readline().strip())]
			Y_time = get_y_vals(time_dict,counter,counter+num_xvar);
			Y_resp = get_y_vals(response_dict,counter,counter+num_xvar);
			counter += num_xvar;
			print q , x_var
			plot_graph( " ".join(fixed_vars) ,"time", x_var, types, X, Y_time , GRAPHS+"/g"+str(q)+"_time_"+"_".join(fixed_vars) );
			# plot_graph( " ".join(fixed_vars) ,"response size", x_var, types, X, Y_resp , GRAPHS+"/g"+str(q)+"_response_"+"_".join(fixed_vars) );
		meta.close()


PATH = "QUERIES_and_RESPONSES/"
QUERIES = []

for x in range(4):
	QUERIES += [PATH+"Q"+str(x)];

plot(QUERIES, 6, "GRAPHS");