import random
import copy
import os

NAMESPACE = 2000000000;
SETSIZES = [100,200,500,1000,10000]

PARAMS = ["query_len","overlap","small_set"]

def create_sets(overlap, term_lens):
	result = [];
	overlap_abs = int(overlap*min(term_lens));
	full_namespace = xrange(1,NAMESPACE)
	total_length = sum(term_lens)-(len(term_lens)-1)*overlap_abs
	all_samples = random.sample(full_namespace, total_length)
	counter = overlap_abs;
	common_part = all_samples[:overlap_abs]

	for ind in range(len(term_lens)):
		set_size = term_lens[ind]-len(common_part)
		elems = all_samples[counter:counter+set_size]
		counter += set_size
		temp = copy.deepcopy(common_part)
		elems = elems + temp
		result += [elems]
	return result

# time vs overlap : terms - 5 ,15, 60
# 	- all of same size 500, 
# 	- all sizes equally divided




def write_query(sets, file):
	global DB,COUNT
	file.write(str(len(sets)))
	for i in range(len(sets)):
		DB["#"+str(COUNT)] = sets[i]
		file.write(" #"+str(COUNT))
		COUNT+=1;
	file.write("\n")


def get_terms(num_terms, elems):
	terms = elems*(num_terms/len(elems))
	for i in range(num_terms%len(elems)):
		terms += [elems[i]]
	return terms;


def write_DB(filename):
	total_len = 0;
	data = ""
	for key in DB.keys():
		total_len += len(DB[key]);
		for vals in DB[key]:
			data += key+" "+str(vals)+"\n";
	data = str(total_len)+"\n"+data;
	file = open(filename,"w")
	file.write(data);
	file.close();


# how to setup the graph making
# make a file_list for overlap and put all query names + one metadata file arranged in graph wise manner

def create_database(foldername):		#create one folder for each instance then average them all
	if(not(os.path.isdir(foldername))):
		os.mkdir(foldername);
	if(not(os.path.isdir(foldername+"_response"))):
		os.mkdir(foldername+"_response");

	#case 1 = overlap
	file1 = open(foldername+"/q1","w")
	meta1 = open(foldername+"/m1","w")
	file1.write("21\n")
	meta1.write(PARAMS[1]+",3\n")					#x axis parameter, number of graphs to be drawn
	for t in [5,15,60]:
		meta1.write(PARAMS[0]+","+str(t)+",All sets of size 500,"+"7"+"\n")		#for second last loop describing the parameters for the graph
		term_lens1 = [500]*t
		for overlap in [0.0,0.1,0.2,0.3,0.5,0.7,0.9]:
			set1 = create_sets(overlap, term_lens1);
			write_query(set1,file1)
			meta1.write(str(overlap)+"\n")
	file1.close()
	meta1.close()


	#case 1 = overlap
	file1 = open(foldername+"/q2","w")
	meta1 = open(foldername+"/m2","w")
	file1.write("21\n")
	meta1.write(PARAMS[1]+",3\n")					#x axis parameter, number of graphs to be drawn
	for t in [5,15,60]:
		meta1.write(PARAMS[0]+","+str(t)+",Sets of varying size equally distributed in :"+" ".join(map(str,SETSIZES))+",7"+"\n")		#for second last loop describing the parameters for the graph
		term_lens1 = SETSIZES*int(t/len(SETSIZES))
		for overlap in [0.0,0.1,0.2,0.3,0.5,0.7,0.9]:
			set1 = create_sets(overlap, term_lens1);
			write_query(set1,file1)
			meta1.write(str(overlap)+"\n")
	file1.close()
	meta1.close()

	#case 2 = terms
	file1 = open(foldername+"/q3","w")
	meta1 = open(foldername+"/m3","w")
	file1.write("28\n")
	meta1.write(PARAMS[0]+",2\n")					#x axis parameter, number of graphs to be drawn
	for overlap in [0.0,0.1]:
		meta1.write(PARAMS[1]+","+str(overlap)+",All sets of size 500,"+"14"+"\n")		#for second last loop describing the parameters for the graph
		for t in [5,10,15,20,25,30,35,40,60,100,160,240,340,400]:
			term_lens1 = [500]*t
			set1 = create_sets(overlap, term_lens1);
			write_query(set1,file1)
			meta1.write(str(t)+"\n")
	file1.close()
	meta1.close()

	#case 2 = terms
	file1 = open(foldername+"/q4","w")
	meta1 = open(foldername+"/m4","w")
	file1.write("28\n")
	meta1.write(PARAMS[0]+",2\n")					#x axis parameter, number of graphs to be drawn
	for overlap in [0.0,0.1]:
		meta1.write(PARAMS[1]+","+str(overlap)+",Sets of varying size equally distributed in :"+" ".join(map(str,SETSIZES))+",14"+"\n")		#for second last loop describing the parameters for the graph
		for t in [5,10,15,20,25,30,35,40,60,100,160,240,340,400]:
			term_lens1 = SETSIZES*int(t/len(SETSIZES))
			set1 = create_sets(overlap, term_lens1);
			write_query(set1,file1)
			meta1.write(str(t)+"\n")
	file1.close()
	meta1.close()


	#case 3 = smallest set size
	file1 = open(foldername+"/q5","w")
	meta1 = open(foldername+"/m5","w")
	file1.write("16\n")
	meta1.write(PARAMS[2]+",4\n")					#x axis parameter, number of graphs to be drawn
	for overlap in [0.0,0.1]:		
		for t in [5,20]:
			meta1.write(PARAMS[0]+","+str(t)+","+PARAMS[1]+","+str(overlap)+",All sets of same size as the smallest set,"+str(len(SETSIZES)-1)+"\n")		#for second last loop describing the parameters for the graph
			for small_set in SETSIZES[:len(SETSIZES)-1]:
				term_lens1 = [small_set]*t
				set1 = create_sets(overlap, term_lens1);
				write_query(set1,file1)
				meta1.write(str(small_set)+"\n")
	file1.close()
	meta1.close()


	#case 3 = smallest set size
	file1 = open(foldername+"/q6","w")
	meta1 = open(foldername+"/m6","w")
	file1.write("16\n")
	meta1.write(PARAMS[2]+",4\n")					#x axis parameter, number of graphs to be drawn
	for overlap in [0.0,0.1]:		
		for t in [5,20]:
			meta1.write(PARAMS[0]+","+str(t)+PARAMS[1]+","+str(overlap)+",All set sizes equally distributed among sizes starting from the smallest set in"+" ".join(map(str,SETSIZES))+","+str(len(SETSIZES)-1)+"\n")		#for second last loop describing the parameters for the graph
			for small_set_ind in range(len(SETSIZES)-1):
				small_set = SETSIZES[small_set_ind];
				term_lens1 = get_terms(t, SETSIZES[small_set_ind:])#[small_set]*t
				set1 = create_sets(overlap, term_lens1);
				write_query(set1,file1)
				meta1.write(str(small_set)+"\n")
	file1.close()
	meta1.close()



DB = {};
COUNT = 0;
FOLDER = "QUERIES_and_RESPONSES/Q"

def setup(instances):
	file = open("file_list","w")
	file.write("6\n")
	file.write("q1\n")
	file.write("q2\n")
	file.write("q3\n")
	file.write("q4\n")
	file.write("q5\n")
	file.write("q6\n")
	file.close()
	for i in xrange(instances):
		global DB,COUNT
		DB = {};
		COUNT = 0;
		create_database(FOLDER+str(i));
		write_DB(FOLDER+str(i)+"/database")


setup(4);



	# #case 3 = smallest set size
	# file1 = open(foldername+"/q5","w")
	# meta1 = open(foldername+"/m5","w")
	# file1.write("16\n")
	# meta1.write(PARAMS[2]+",4\n")					#x axis parameter, number of graphs to be drawn
	# for overlap in [0.5,0.7]:		
	# 	for t in [100,200]:
	# 		meta1.write(PARAMS[0]+","+str(t)+","+PARAMS[1]+","+str(overlap)+",All sets of same size as the smallest set,"+str(len(SETSIZES)-1)+"\n")		#for second last loop describing the parameters for the graph
	# 		for small_set in SETSIZES[:len(SETSIZES)-1]:
	# 			term_lens1 = [small_set]*t
	# 			set1 = create_sets(overlap, term_lens1);
	# 			write_query(set1,file1)
	# 			meta1.write(str(small_set)+"\n")
	# file1.close()
	# meta1.close()


	# #case 3 = smallest set size
	# file1 = open(foldername+"/q6","w")
	# meta1 = open(foldername+"/m6","w")
	# file1.write("16\n")
	# meta1.write(PARAMS[2]+",4\n")					#x axis parameter, number of graphs to be drawn
	# for overlap in [0.5,0.7]:		
	# 	for t in [100,200]:
	# 		meta1.write(PARAMS[0]+","+str(t)+PARAMS[1]+","+str(overlap)+",All set sizes equally distributed among sizes starting from the smallest set in"+" ".join(map(str,SETSIZES))+","+str(len(SETSIZES)-1)+"\n")		#for second last loop describing the parameters for the graph
	# 		for small_set_ind in range(len(SETSIZES)-1):
	# 			small_set = SETSIZES[small_set_ind];
	# 			term_lens1 = get_terms(t, SETSIZES[small_set_ind:])#[small_set]*t
	# 			set1 = create_sets(overlap, term_lens1);
	# 			write_query(set1,file1)
	# 			meta1.write(str(small_set)+"\n")
	# file1.close()
	# meta1.close()
