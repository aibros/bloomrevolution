#include "usage.h"


int main()
{
	datasetPath = (char*)malloc(1000*sizeof(char));
	strcpy(datasetPath,"../datasets/");

	time_t t;
	srand((unsigned) time(&t));
	
	init_bloomParameters(1024,3,2);
	init_dyn_parameters(1,0.001); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
	seiveInitial(); // Important to initialize it here to reset m[i] values to suit the changed values of SIZE_BLOOM

	dyn_bloom_test_suit1();
	dyn_bloom_test_suit2();
	// run_unit_size_analysis();
	// run_setOps_overlap();

	// run_memory_tests();
	// run_set_operation_tests();
}