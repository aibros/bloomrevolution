import glob
from sets import Set
import nltk
from nltk.tokenize import RegexpTokenizer

def read_folder(folder_name):
	print("Assuming all the files in the folder: "+folder_name+" are part of dataset for inverse indexing!!")
	tokenizer = RegexpTokenizer(r'\w+');
	file_list = glob.glob(folder_name+"/*.*");
	docDB = [];
	vocab = Set([]);
	for file_name in file_list:
		file = open(file_name);
		data = file.read();
		file.close();
		tokens = tokenizer.tokenize(data);
		docDB += [tokens];
		vocab = vocab | Set(tokens);
	return (docDB,list(vocab))

def save_data(docDB,vocab,vocab_file="vocabulary.txt",db_file="docDB.txt"):
	file = open(vocab_file,"w");
	file.write(str(len(vocab))+"\n"+"\n".join(vocab));
	file.close();
	file = open(db_file,"w");
	file.write(str(len(docDB))+"\n");
	for doc in docDB:
		doc_string = str(len(doc))+" "+" ".join(doc)+"\n"
		file.write(doc_string);
	file.close();


(docDB,vocab) = read_folder("doc_dataset");
save_data(docDB,vocab);
