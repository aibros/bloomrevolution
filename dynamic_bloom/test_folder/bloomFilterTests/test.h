#include "../../dynamic_bloom.h"
#include "sys/time.h"

void read_and_insert(char * filename, dyn_bloom* filter);
void estimate_false_positive(char* filename,dyn_bloom* filter, FILE* logger);
void estimate_false_positive_union(char* filename,dyn_bloom* filter, FILE* logger);
void estimate_false_positive_intersect(char* filename,dyn_bloom* filter, FILE* logger);

void test_intersection_varying_overlap(int NumElemsA, int NumElemsB, char* logFileName);
void test_union_varying_overlap(int NumElemsA, int NumElemsB, char* logFileName);

void test_intersection_fixed_overlap(int overlap, int numSets, char* logFileName);
void test_union_fixed_overlap(int overlap, int numSets, char* logFileName);

// This function creates DBF which has SAME EFFECTIVE MEMORY USAGE as DynPartition Bloom Filter
void test_FP_sameMemUsage(char** filenameList,int numFiles,char* logFileName);

// This function created DBF which has SAME FALSE POSITIVE PROBABILITY as DynPartition Bloom Filter
void test_memUsage_sameFP(char** filenameList,int numFiles,char* logFileName);

void run_FP_test_sameMemUsage(char* fileListName, int M, UINT K, float FALSE_PSTV_PROB, int numUnitSize, int* unitSizeVals);
void run_memUsage_test_sameFP(char* fileListName, int M, UINT K, float FALSE_PSTV_PROB, int numUnitSize, int* unitSizeVals);

void run_setOps_varying_overlap();
void run_setOps_fixed_overlap(int* overlapList,int numOverlapVals, int numSets, int K, int M, int unitSize, float FALSE_PSTV_PROB );

static inline float get_time_diff(struct timeval t1, struct timeval t2);

extern char* bloomTestingPath;
extern char* logsPath;
extern char* datasetPath;