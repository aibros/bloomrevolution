#include "test.h"

char* bloomTestingPath;
char* logsPath;
char* datasetPath;

// Reads data from file and inserts them in the bloom filter
void read_and_insert(char * filename, dyn_bloom* filter)
{
	FILE * reader;
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,filename);
	// printf("Reading FileName::%s~~~~~~~~~~\n",fullFilename);
	reader = fopen(fullFilename,"r");
	UINT i,num_elems = 0,namespace = 1;
	fscanf(reader,"%d",&num_elems);
	fscanf(reader,"%d",&namespace);
	for(i = 0 ; i < num_elems ; i++)
	{
		int elem = 0;
		fscanf(reader,"%d",&elem);
		dyn_insert(elem,filter);
	}
	fclose(reader);
}

// Takes a bloom filter and estimates number of false positives in it.
void estimate_false_positive(char* filename,dyn_bloom* filter, FILE* logger)
{
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,filename);
	FILE * reader = fopen(fullFilename,"r");
	int num_elems,namespace;
	fscanf(reader,"%d",&num_elems);
	fscanf(reader,"%d",&namespace);
	fclose(reader);
	int i;
	int numPresent = 0;
	for (i = 1; i <= namespace; ++i)
	{
		if(dyn_is_in(i,filter))
		{
			numPresent++;
		}
	}
	fprintf(logger, "%.7f,",(numPresent - num_elems)/(float)namespace);
}

void estimate_false_positive_union(char* filename,dyn_bloom* filter, FILE* logger)
{
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,filename);
	FILE * reader = fopen(fullFilename,"r");
	int num_elems,namespace;
	fscanf(reader,"%d",&num_elems);
	fscanf(reader,"%d",&namespace);
	int i;
	int numPresent = 0;
	for (i = 1; i <= namespace; ++i)
	{
		if(dyn_is_in(i,filter))
		{
			numPresent++;
		}
	}
	int false_negative=0;
	int elem;
	for (i = 0; i < num_elems; ++i)
	{
		fscanf(reader,"%d",&elem);
		if(!dyn_is_in(elem,filter))
			false_negative++;
	}
	fclose(reader);
	fprintf(logger, "%.7f,",(numPresent - num_elems + false_negative)/(float)namespace);
}

void estimate_false_positive_intersect(char* filename,dyn_bloom* filter, FILE* logger)
{
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,filename);
	FILE * reader = fopen(fullFilename,"r");
	int num_elems,namespace;
	fscanf(reader,"%d",&num_elems);
	fscanf(reader,"%d",&namespace);
	int i;
	int numPresent = 0;
	for (i = 1; i <= namespace; ++i)
	{
		if(dyn_is_in(i,filter))
		{
			numPresent++;
		}
	}

	int false_negative=0;
	int elem;
	for (i = 0; i < num_elems; ++i)
	{
		fscanf(reader,"%d",&elem);
		if(!dyn_is_in(elem,filter))
			false_negative++;
	}
	fclose(reader);
	fprintf(logger, "%.7f,",(numPresent - num_elems + false_negative)/(float)namespace);
}

// Runs test for fixed size of A and B, but varying size of overlap from 0.1 to 0.9
// For each (sizeA,sizeB) pair, there are 100 samples
// This function generates log file for graph where we plot FPRatio vs overlap between A nd B
void test_intersection_varying_overlap(int NumElemsA, int NumElemsB, char* logFileName)
{
	FILE* logger;
	logger = fopen(logFileName,"w");
	fprintf(logger,  "%s\n","FileA,FileB,Overlap,Operation,Namespace,NumHashFunc,UnitSize,FalsePstvProb,FillThreshold,NumElemsA,NumElemsB,NumElemsResult,NumUnitBFs,EffMemUsage,FP_Ratio,NULL");

	float maxOverlap = 1.0, overlap;
	int numSamples  = 10, sampleIter;
	for (overlap = 0.1; overlap < maxOverlap; overlap += 0.1)
	{
		for (sampleIter = 0; sampleIter < numSamples; ++sampleIter)
		{
			char fileA[1000];
			char fileB[1000];
			char intersect_filename[1000];
			sprintf(fileA	,"rand_%d_%d_A_%.1f_%d", NumElemsA, NumElemsB, overlap, sampleIter);
			sprintf(fileB	,"rand_%d_%d_B_%.1f_%d", NumElemsA, NumElemsB, overlap, sampleIter);
			sprintf(intersect_filename	,"rand_%d_%d_inter_%.1f_%d",NumElemsA, NumElemsB, overlap, sampleIter);

			dyn_bloom* filter_1 		= (dyn_bloom*)malloc(sizeof(dyn_bloom));
			dyn_bloom* filter_2 		= (dyn_bloom*)malloc(sizeof(dyn_bloom));
			dyn_bloom* filter_intersect = (dyn_bloom*)malloc(sizeof(dyn_bloom));
			dyn_init(filter_1);
			dyn_init(filter_2);
			dyn_init(filter_intersect);
		
			FILE * reader;
			int namespace;
			int num_elems_intersect;

			char *fullFilename = (char*)malloc(1000*sizeof(char));
			sprintf(fullFilename,"%s%s",datasetPath,intersect_filename);
			reader = fopen(fullFilename,"r");

			fscanf(reader,"%d",&num_elems_intersect);
			fscanf(reader,"%d",&namespace);
			fclose(reader);
			free(fullFilename);

			fprintf(logger, "%s,", fileA);
			fprintf(logger, "%s,", fileB);
			fprintf(logger, "%.1f,",overlap);
			fprintf(logger, "Intersection,");

			fprintf(logger, "%d,",namespace);
			fprintf(logger, "%d,",bloomParam->K);
			fprintf(logger, "%d,",bloomParam->SIZE_BLOOM);
			fprintf(logger, "%.5f,",bloomParam->FALSE_PSTV_PROB);
			fprintf(logger, "%.3f,",bloomParam->FILL_THRESHOLD);

			fprintf(logger, "%d,",NumElemsA);
			fprintf(logger, "%d,",NumElemsB);
			fprintf(logger, "%d,",num_elems_intersect);
			
			read_and_insert(fileA,filter_1);
			read_and_insert(fileB,filter_2);
			dyn_intersect_bloom(filter_1,filter_2,filter_intersect);

			// Details for set (A intersect B)
			fprintf(logger, "%d,",filter_intersect->currNum + 1);
			fprintf(logger, "%d,",bloomParam->SIZE_BLOOM*(filter_intersect->currNum + 1));
			estimate_false_positive_intersect(intersect_filename,filter_intersect,logger);

			fprintf(logger, "NULL\n");
			dyn_free_bloom(filter_1);
			dyn_free_bloom(filter_2);
			dyn_free_bloom(filter_intersect);
			free(filter_1);
			free(filter_2);
			free(filter_intersect);
		}
	}
	fclose(logger);
}

// Runs test for fixed size of A and B, but varying size of overlap from 0.1 to 0.9
// For each (sizeA,sizeB) pair, there are 100 samples
// This function generates log file for graph where we plot FPRatio vs overlap between A nd B
void test_union_varying_overlap(int NumElemsA, int NumElemsB, char* logFileName)
{
	FILE* logger;
	logger = fopen(logFileName,"w");
	fprintf(logger,  "%s\n","FileA,FileB,Overlap,Operation,Namespace,NumHashFunc,UnitSize,FalsePstvProb,FillThreshold,NumElemsA,NumElemsB,NumElemsResult,NumUnitBFs,EffMemUsage,FP_Ratio,NULL");

	float maxOverlap 	= 1.0, overlap;
	int numSamples  	= 10, sampleIter;
	for (overlap = 0.1; overlap < maxOverlap; overlap += 0.1)
	{
		for (sampleIter = 0; sampleIter < numSamples; ++sampleIter)
		{
			char fileA[1000];
			char fileB[1000];
			char union_filename[1000];
			sprintf(fileA	,"rand_%d_%d_A_%.1f_%d", NumElemsA, NumElemsB, overlap, sampleIter);
			sprintf(fileB	,"rand_%d_%d_B_%.1f_%d", NumElemsA, NumElemsB, overlap, sampleIter);
			sprintf(union_filename	,"rand_%d_%d_union_%.1f_%d",NumElemsA, NumElemsB, overlap, sampleIter);
			dyn_bloom* filter_1 	= (dyn_bloom*)malloc(sizeof(dyn_bloom));
			dyn_bloom* filter_2 	= (dyn_bloom*)malloc(sizeof(dyn_bloom));
			dyn_bloom* filter_union = (dyn_bloom*)malloc(sizeof(dyn_bloom));
			dyn_init(filter_1);
			dyn_init(filter_2);
			dyn_init(filter_union);
		
			FILE * reader;
			int namespace;
			int num_elems_union;

			char *fullFilename = (char*)malloc(1000*sizeof(char));
			sprintf(fullFilename,"%s%s",datasetPath,union_filename);
			reader = fopen(fullFilename,"r");
			if (reader == NULL)
			{
				printf("Empty file handle");
				exit(0);
			}
			fscanf(reader,"%d",&num_elems_union);
			fscanf(reader,"%d",&namespace);
			fclose(reader);
			free(fullFilename);

			fprintf(logger, "%s,", fileA);
			fprintf(logger, "%s,", fileB);
			fprintf(logger, "%.1f,",overlap);
			fprintf(logger, "Union,");

			fprintf(logger, "%d,",namespace);
			fprintf(logger, "%d,",bloomParam->K);
			fprintf(logger, "%d,",bloomParam->SIZE_BLOOM);
			fprintf(logger, "%.5f,",bloomParam->FALSE_PSTV_PROB);
			fprintf(logger, "%.3f,",bloomParam->FILL_THRESHOLD);

			fprintf(logger, "%d,",NumElemsA);
			fprintf(logger, "%d,",NumElemsB);
			fprintf(logger, "%d,",num_elems_union);
			
			read_and_insert(fileA,filter_1);
			read_and_insert(fileB,filter_2);
			dyn_union_bloom(filter_1,filter_2,filter_union);

			// Details for set (A intersect B)
			fprintf(logger, "%d,",filter_union->currNum + 1);
			fprintf(logger, "%d,",bloomParam->SIZE_BLOOM*(filter_union->currNum + 1));
			estimate_false_positive_union(union_filename,filter_union,logger);

			fprintf(logger, "NULL\n");
			dyn_free_bloom(filter_1);
			dyn_free_bloom(filter_2);
			dyn_free_bloom(filter_union);
			free(filter_1);
			free(filter_2);
			free(filter_union);
		}
	}
	fclose(logger);
}

// Runs test where size of overlap is fixed and size of set A and B varies
// overlap: Size of overlap
// numSets: Number of sets generated to intersect/union for this particular overlap size
void test_intersection_fixed_overlap(int overlap, int numSets, char* logFileName)
{
	FILE* logger;
	logger = fopen(logFileName,"w");
	fprintf(logger,  "%s\n","FileA,FileB,ResultFile,Overlap,Operation,Namespace,NumHashFunc,UnitSize,FalsePstvProb,FillThreshold,NumElemsA,NumElemsB,NumElemsResult,SymmetricDiffSize,NumUnitBFs,EffMemUsage,FP_Ratio,NULL");
	
	for (int i = 1; i <= numSets; ++i)
	{
		char fileA[1000];
		char fileB[1000];
		char intersect_filename[1000];
		sprintf(fileA	,"rand_oLap_%d_A_%d", overlap, i);
		sprintf(fileB	,"rand_oLap_%d_B_%d", overlap, i);
		sprintf(intersect_filename	,"rand_oLap_%d_inter_%d", overlap, i);
		
		dyn_bloom* filter_1 			= (dyn_bloom*)malloc(sizeof(dyn_bloom));
		dyn_bloom* filter_2 			= (dyn_bloom*)malloc(sizeof(dyn_bloom));
		dyn_bloom* filter_intersect 	= (dyn_bloom*)malloc(sizeof(dyn_bloom));
		dyn_init(filter_1);
		dyn_init(filter_2);
		dyn_init(filter_intersect);
	
		FILE * reader;
		int namespace;
		int num_elems_intersect, num_elems_A,num_elems_B;

		char *fullFilename = (char*)malloc(1000*sizeof(char));
		sprintf(fullFilename,"%s%s",datasetPath,intersect_filename);
		reader = fopen(fullFilename,"r");
		fscanf(reader,"%d",&num_elems_intersect);
		fscanf(reader,"%d",&namespace);
		fclose(reader);
		free(fullFilename);

		fullFilename = (char*)malloc(1000*sizeof(char));
		sprintf(fullFilename,"%s%s",datasetPath,fileA);
		reader = fopen(fullFilename,"r");
		fscanf(reader,"%d",&num_elems_A);
		fclose(reader);
		free(fullFilename);

		fullFilename = (char*)malloc(1000*sizeof(char));
		sprintf(fullFilename,"%s%s",datasetPath,fileB);
		reader = fopen(fullFilename,"r");
		fscanf(reader,"%d",&num_elems_B);
		fclose(reader);
		free(fullFilename);

		fprintf(logger, "%s,", fileA);
		fprintf(logger, "%s,", fileB);
		fprintf(logger, "%s,", intersect_filename);
		fprintf(logger, "%d,",overlap);
		fprintf(logger, "Intersection,");

		fprintf(logger, "%d,",namespace);
		fprintf(logger, "%d,",bloomParam->K);
		fprintf(logger, "%d,",bloomParam->SIZE_BLOOM);
		fprintf(logger, "%f,",bloomParam->FALSE_PSTV_PROB);
		fprintf(logger, "%f,",bloomParam->FILL_THRESHOLD);

		fprintf(logger, "%d,",num_elems_A);
		fprintf(logger, "%d,",num_elems_B);
		fprintf(logger, "%d,",num_elems_intersect);
		fprintf(logger, "%d,",num_elems_A + num_elems_B - 2*overlap);
		if(num_elems_intersect != overlap)
		{
			printf("num_elems_intersect(%d) is not equal to overlap(%d)\n",num_elems_intersect,overlap);
		}

		read_and_insert(fileA,filter_1);
		read_and_insert(fileB,filter_2);
		dyn_intersect_bloom(filter_1,filter_2,filter_intersect);

		// Details for set (A intersect B)
		fprintf(logger, "%d,",filter_intersect->currNum + 1);
		fprintf(logger, "%d,",bloomParam->SIZE_BLOOM*(filter_intersect->currNum + 1));
		estimate_false_positive_intersect(intersect_filename,filter_intersect,logger);

		fprintf(logger, "NULL\n");
		dyn_free_bloom(filter_1);
		dyn_free_bloom(filter_2);
		dyn_free_bloom(filter_intersect);
		free(filter_1);
		free(filter_2);
		free(filter_intersect);
	}

	fclose(logger);
}

// Runs test where size of overlap is fixed and size of set A and B varies
// overlap: Size of overlap
// numSets: Number of sets generated to intersect/union for this particular overlap size
void test_union_fixed_overlap(int overlap, int numSets, char* logFileName)
{
	FILE* logger;
	logger = fopen(logFileName,"w");
	fprintf(logger,  "%s\n","FileA,FileB,ResultFile,Overlap,Operation,Namespace,NumHashFunc,UnitSize,FalsePstvProb,FillThreshold,NumElemsA,NumElemsB,NumElemsResult,SymmetricDiffSize,NumUnitBFs,EffMemUsage,FP_Ratio,NULL");

	for (int i = 1; i <= numSets; ++i)
	{
		char fileA[1000];
		char fileB[1000];
		char union_filename[1000];
		sprintf(fileA	,"rand_oLap_%d_A_%d", overlap, i);
		sprintf(fileB	,"rand_oLap_%d_B_%d", overlap, i);
		sprintf(union_filename	,"rand_oLap_%d_union_%d", overlap, i);
		
		dyn_bloom* filter_1 		= (dyn_bloom*)malloc(sizeof(dyn_bloom));
		dyn_bloom* filter_2 		= (dyn_bloom*)malloc(sizeof(dyn_bloom));
		dyn_bloom* filter_union 	= (dyn_bloom*)malloc(sizeof(dyn_bloom));
		dyn_init(filter_1);
		dyn_init(filter_2);
		dyn_init(filter_union);
		
		FILE * reader;
		int namespace;
		int num_elems_union,num_elems_A,num_elems_B;

		char *fullFilename = (char*)malloc(1000*sizeof(char));
		sprintf(fullFilename,"%s%s",datasetPath,union_filename);
		reader = fopen(fullFilename,"r");
		fscanf(reader,"%d",&num_elems_union);
		fscanf(reader,"%d",&namespace);
		fclose(reader);
		free(fullFilename);

		fullFilename = (char*)malloc(1000*sizeof(char));
		sprintf(fullFilename,"%s%s",datasetPath,fileA);
		reader = fopen(fullFilename,"r");
		fscanf(reader,"%d",&num_elems_A);
		fclose(reader);
		free(fullFilename);

		fullFilename = (char*)malloc(1000*sizeof(char));
		sprintf(fullFilename,"%s%s",datasetPath,fileB);
		reader = fopen(fullFilename,"r");
		fscanf(reader,"%d",&num_elems_B);
		fclose(reader);
		free(fullFilename);

		fprintf(logger, "%s,", fileA);
		fprintf(logger, "%s,", fileB);
		fprintf(logger, "%s,", union_filename);
		fprintf(logger, "%d,",overlap);
		fprintf(logger, "Union,");

		fprintf(logger, "%d,",namespace);
		fprintf(logger, "%d,",bloomParam->K);
		fprintf(logger, "%d,",bloomParam->SIZE_BLOOM);
		fprintf(logger, "%f,",bloomParam->FALSE_PSTV_PROB);
		fprintf(logger, "%f,",bloomParam->FILL_THRESHOLD);

		fprintf(logger, "%d,",num_elems_A);
		fprintf(logger, "%d,",num_elems_B);
		fprintf(logger, "%d,",num_elems_union);
		fprintf(logger, "%d,",num_elems_A + num_elems_B - 2*overlap);
		if(num_elems_union != num_elems_A + num_elems_B - overlap)
		{
			printf("num_elems_union not correct::%d\n",num_elems_union);
			printf("overlap::%d\n",overlap);
			printf("Size A::%d\n",num_elems_A );
			printf("Size B::%d\n",num_elems_B );
			exit(0);
		}
		read_and_insert(fileA,filter_1);
		read_and_insert(fileB,filter_2);
		dyn_union_bloom(filter_1,filter_2,filter_union);
		
		// Details for set (A union B)
		fprintf(logger, "%d,",filter_union->currNum + 1);
		fprintf(logger, "%d,",bloomParam->SIZE_BLOOM*(filter_union->currNum + 1));
		estimate_false_positive_union(union_filename,filter_union,logger);

		fprintf(logger, "NULL\n");
		dyn_free_bloom(filter_1);
		dyn_free_bloom(filter_2);
		dyn_free_bloom(filter_union);
		free(filter_1);
		free(filter_2);
		free(filter_union);
	}
	fclose(logger);
}


// This function creates DBF which has SAME EFFECTIVE MEMORY USAGE as DynPartition Bloom Filter
void test_FP_sameMemUsage(char** filenameList,int numFiles,char* logFileName) // set creation tests
{
	FILE* logger ;
	logger = fopen(logFileName,"w");
	fprintf(logger, "%s","FileName,NumElems,UnitSize,NumUnitBFs,EffMemUsage,FP_Ratio,TimeTaken,MembershipQueryTime,NULL");

	int i;
	float timeTaken = 0.0;
	struct timeval start,end;
	for (i = 0; i < numFiles; ++i)
	{
		fprintf(logger, "\n");
		gettimeofday(&start,NULL);
		dyn_bloom * filter = (dyn_bloom*)malloc(sizeof(dyn_bloom));
		dyn_init(filter);
		read_and_insert(filenameList[i],filter);
		gettimeofday(&end,NULL);
		float creationTime = get_time_diff(start,end);

		char fullFilename[1000];
		sprintf(fullFilename,"%s%s",datasetPath,filenameList[i]);
		FILE * reader = fopen(fullFilename,"r");
		int num_elems,namespace;
		fscanf(reader,"%d",&num_elems);
		fscanf(reader,"%d",&namespace);
		fclose(reader);

		fprintf(logger, "%s,",filenameList[i]);
		fprintf(logger, "%d,",num_elems);
		fprintf(logger, "%d,",bloomParam->SIZE_BLOOM);
		fprintf(logger, "%d,",(filter->currNum + 1));
		fprintf(logger, "%d,",(filter->currNum + 1)*(bloomParam->SIZE_BLOOM));
		gettimeofday(&start,NULL);
		estimate_false_positive(filenameList[i],filter,logger);
		gettimeofday(&end,NULL);
		float memQueryTime = get_time_diff(start,end);

		fprintf(logger, "%f,",creationTime);
		fprintf(logger, "%f,",memQueryTime);
		fprintf(logger, "NULL");
		dyn_free_bloom(filter);
		free(filter);
	}
	fclose(logger);
}

// This function created DBF which has SAME FALSE POSITIVE PROBABILITY as DynPartition Bloom Filter
void test_memUsage_sameFP(char** filenameList,int numFiles,char* logFileName) // set creation tests
{
	FILE* logger ;
	logger = fopen(logFileName,"w");
	fprintf(logger, "%s","FileName,NumElems,UnitSize,NumUnitBFs,EffMemUsage,FP_Ratio,TimeTaken,MembershipQueryTime,NULL");

	int i;
	float timeTaken = 0.0;
	struct timeval start,end;
	for (i = 0; i < numFiles; ++i)
	{
		fprintf(logger, "\n");
		
		char fullFilename[1000];
		sprintf(fullFilename,"%s%s",datasetPath,filenameList[i]);
		FILE * reader = fopen(fullFilename,"r");
		int num_elems,namespace;
		fscanf(reader,"%d",&num_elems);
		fscanf(reader,"%d",&namespace);
		fclose(reader);

		// printf("Initial FPP(for splitting)\t::%f\n",bloomParam->FALSE_PSTV_PROB );
		// printf("Initial FILL_THRESHOLD\t::%f\n",bloomParam->FILL_THRESHOLD );
		float K 	= bloomParam->K;
		float n_t 	= bloomParam->FILL_THRESHOLD;
		float m 	= bloomParam->SIZE_BLOOM;
		float p 	=  pow( 1 - exp(-1*K*n_t/m), K);
		float fpp 	= 1 - pow( 1 - p, num_elems*1.0/n_t);
		// printf("Initial Effective FPP\t::%f\n",fpp);

		// Second condition is needed because there is no point in storing fewer elements in one unit bloom filter than 
		// the number of integers (n_int), where n_int integers take same space as this unit bloom filter
		// while( (fpp > bloomParam->FALSE_PSTV_PROB) && (bloomParam->FILL_THRESHOLD > bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS) )
		while( (fpp > bloomParam->FALSE_PSTV_PROB) && (bloomParam->FILL_THRESHOLD >= 2 ) )
		{
			K 	= bloomParam->K;
			n_t = bloomParam->FILL_THRESHOLD - 1;
			m 	= bloomParam->SIZE_BLOOM;
			p 	=  pow( 1 - exp(-1*K*n_t/m), K);
			fpp = 1 - pow( 1 - p, num_elems*1.0/n_t);

			bloomParam->FILL_THRESHOLD  = (UINT)n_t;
			// printf("Effective FPP ::%f\n",fpp );
			// printf("FILL_THRESHOLD::%f\n",bloomParam->FILL_THRESHOLD );
		}
		// printf("Final Effective FPP \t::%f\n",fpp );
		// printf("Final FILL_THRESHOLD\t::%f\n",bloomParam->FILL_THRESHOLD );
		init_dyn_parameters(bloomParam->SIZE_BLOOM,bloomParam->K,bloomParam->COUNTER_CHUNK,p);

		gettimeofday(&start,NULL);
		dyn_bloom * filter = (dyn_bloom*)malloc(sizeof(dyn_bloom));
		dyn_init(filter);
		read_and_insert(filenameList[i],filter);
		gettimeofday(&end,NULL);
		float creationTime = get_time_diff(start,end);

		fprintf(logger, "%s,",filenameList[i]);
		fprintf(logger, "%d,",num_elems);
		fprintf(logger, "%d,",bloomParam->SIZE_BLOOM);
		fprintf(logger, "%d,",(filter->currNum + 1));
		fprintf(logger, "%d,",(filter->currNum + 1)*(bloomParam->SIZE_BLOOM));
		
		gettimeofday(&start,NULL);
		estimate_false_positive(filenameList[i],filter,logger);
		// fprintf(logger, "%f,",0.0); // No need of empirical false positive probability as such. This takes painfully long time
		gettimeofday(&end,NULL);
		float memQueryTime = get_time_diff(start,end);

		fprintf(logger, "%f,",creationTime);
		fprintf(logger, "%f,",memQueryTime);
		fprintf(logger, "NULL");
		dyn_free_bloom(filter);
		free(filter);
	}
	fclose(logger);
}

// M: Namespace size
// K: Number of hash functions
// FALSE_PSTV_PROB : False positive probability
// fileListName: 	This is name of file present inside dataset folder which contains name of files on which the
// experiments have to be run( if fileList is not present in datasetfolder, but is some other folder inside it
// the fileListName should have the path relative to dataset folder as well )
// This function creates DBF which has SAME EFFECTIVE MEMORY USAGE as DynPartition Bloom Filter
void run_FP_test_sameMemUsage(char* fileListName, int M, UINT K, float FALSE_PSTV_PROB, int numUnitSize, int* unitSizeVals)
{
	// Fixed parameters
	// False positive probability( fixed n_threshold for bloom filter and depth of bloomTree), 
	// Namespace size & number of hash functions

	UINT COUNTER_SIZE 		= 1;
	int i, numFiles = 0;

	char** filenameList, *logFileName;
	char fullFilename[1000], directoryName[1000];
	FILE* reader;

	sprintf(directoryName,"%sSetCreationUnitSizeAnalysis_DynBloom",logsPath);
	mkdir(directoryName,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	
	filenameList 	= (char**)malloc(1000*sizeof(char*));
	sprintf(fullFilename,"%s%s",datasetPath,fileListName);
	reader = fopen(fullFilename,"r");
	
	while(!feof(reader))
	{
		filenameList[numFiles] = (char*)malloc(200*sizeof(char));
		fscanf(reader,"%s\n",filenameList[numFiles]);
		numFiles += 1;
	}
	fclose(reader);
	
	for (i = 0; i < numUnitSize; ++i)
	{
		UINT FILL_THRESHOLD  	= -1*((int)(unitSizeVals[i])/((float)K*COUNTER_SIZE))*log( 1 - pow(FALSE_PSTV_PROB,1.0/K));
		printf("FILL_THRESHOLD::%d\n",FILL_THRESHOLD);
		logFileName 			= (char*)malloc(1000*sizeof(char));
		sprintf(logFileName,"%s/%d.csv",directoryName,unitSizeVals[i]);
		
		init_dyn_parameters(unitSizeVals[i],K,COUNTER_SIZE,FALSE_PSTV_PROB); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
		float tempFill = bloomParam->FILL_THRESHOLD;

		// 1.5 comes from empirical observation and not from any calculation
		tempFill /= 1.5; //This is to make the memory usage in the dynamic bloom filter comparable to Dynamic Partition Bloom filter
		bloomParam->FILL_THRESHOLD = tempFill;

		test_FP_sameMemUsage(filenameList,numFiles,logFileName);
		free(logFileName);
	}
}

// M: Namespace size
// K: Number of hash functions
// FALSE_PSTV_PROB : False positive probability
// fileListName: 	This is name of file present inside dataset folder which contains name of files on which the
// experiments have to be run( if fileList is not present in datasetfolder, but is some other folder inside it
// the fileListName should have the path relative to dataset folder as well )
// This function created DBF which has SAME FALSE POSITIVE PROBABILITY as DynPartition Bloom Filter
void run_memUsage_test_sameFP(char* fileListName, int M, UINT K, float FALSE_PSTV_PROB, int numUnitSize, int* unitSizeVals)
{
	// Fixed parameters
	// False positive probability( fixed n_threshold for bloom filter and depth of bloomTree), 
	// Namespace size & number of hash functions

	UINT COUNTER_SIZE 		= 1;
	int i, numFiles = 0;

	char** filenameList, *logFileName;
	char fullFilename[1000], directoryName[1000];
	FILE* reader;

	sprintf(directoryName,"%sSetCreationUnitSizeAnalysis_DynBloom",logsPath);
	mkdir(directoryName,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	
	filenameList 	= (char**)malloc(1000*sizeof(char*));
	sprintf(fullFilename,"%s%s",datasetPath,fileListName);
	reader = fopen(fullFilename,"r");
	
	while(!feof(reader))
	{
		filenameList[numFiles] = (char*)malloc(200*sizeof(char));
		fscanf(reader,"%s\n",filenameList[numFiles]);
		numFiles += 1;
	}
	fclose(reader);
	
	for (i = 0; i < numUnitSize; ++i)
	{
		UINT FILL_THRESHOLD  	= -1*((int)(unitSizeVals[i])/((float)K*COUNTER_SIZE))*log( 1 - pow(FALSE_PSTV_PROB,1.0/K));
		printf("FILL_THRESHOLD::%d\n",FILL_THRESHOLD);
		logFileName 			= (char*)malloc(1000*sizeof(char));
		sprintf(logFileName,"%s/%d.csv",directoryName,unitSizeVals[i]);
		
		init_dyn_parameters(unitSizeVals[i],K,COUNTER_SIZE,FALSE_PSTV_PROB); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
		test_memUsage_sameFP(filenameList,numFiles,logFileName);
		free(logFileName);
	}
}

void run_setOps_varying_overlap()
{
	int setSizes[6] = {10,100,200,500,1000,2000};
	int numSetSizes = 6;
	UINT K 				= 3;
	UINT COUNTER_SIZE 	= 1;
	UINT M 				= 100000;
	int unitSize 		= 1024;
	float FALSE_PSTV_PROB = 0.001;
	int i,j;
	char directoryNameInterect[1000], directoryNameUnion[1000];

	sprintf(directoryNameInterect,"%sintersection_dynBloom_overlap",logsPath);	
	sprintf(directoryNameUnion,"%sunion_dynBloom_overlap",logsPath);
	mkdir(directoryNameInterect,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	mkdir(directoryNameUnion,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

	for (i = 0; i < numSetSizes; ++i)
	{
		for (j = i; j < numSetSizes; ++j)
		{
			// printf("Running for i,j = %d,%d\n",i,j);
			init_dyn_parameters(unitSize,K,COUNTER_SIZE,FALSE_PSTV_PROB); // Sets SIZE_BLOOM, K, and COUNTER_SIZE

			char logFileName[1000];
			sprintf(logFileName,"%s/%d_%d.csv",directoryNameInterect,setSizes[i],setSizes[j]);
			test_intersection_varying_overlap(setSizes[i],setSizes[j],logFileName);

			char logFileName_2[1000];
			sprintf(logFileName_2,"%s/%d_%d.csv",directoryNameUnion,setSizes[i],setSizes[j]);
			test_union_varying_overlap(setSizes[i],setSizes[j],logFileName_2);
		}
	}
}

// Run test where we have fixed size of overlap between A & B but vary size of symmetric difference
// overlapList		: List of overlap sizes for which to run test, 
// numOverlapVals	: Size of overlapList
// numSets 			: number of sets generated for performing union/intersection for a particular value of overlap
// K 				: Number of hash functions
// M 				: Namespace size
// unitSize 		: Size of unit bloom filter for DPBF
// FALSE_PSTV_PROB 	: False positive probability
void run_setOps_fixed_overlap(int* overlapList,int numOverlapVals, int numSets, int K, int M, int unitSize, float FALSE_PSTV_PROB )
{
	UINT COUNTER_SIZE 	= 1;
	
	char directoryNameInterect[1000], directoryNameUnion[1000];
	sprintf(directoryNameInterect,"%sintersection_dynBloom_fixedOverlap",logsPath);	
	sprintf(directoryNameUnion,"%sunion_dynBloom_fixedOverlap",logsPath);
	mkdir(directoryNameInterect,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	mkdir(directoryNameUnion,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	
	UINT FILL_THRESHOLD  = -1*((int)(unitSize)/((float)K*COUNTER_SIZE))*log( 1 - pow(FALSE_PSTV_PROB,1.0/K));
	// float tempFill = (float)FILL_THRESHOLD;
	// tempFill = tempFill/1.5; // This is done to ensure that both DBF and DPBF have same memory usage. 1.5 comes from emprical observation
	// FILL_THRESHOLD = (UINT)tempFill;
	init_dyn_parameters(unitSize,K,COUNTER_SIZE,FALSE_PSTV_PROB); // Sets SIZE_BLOOM, K, and COUNTER_SIZE and FalsePostvProb

	for (int i = 0; i < numOverlapVals; ++i)
	{
		printf("Running for overlap %d\n",overlapList[i]);
		char*	logFileName 	= (char*)malloc(1000*sizeof(char));
		sprintf(logFileName,"%s/%d.csv",directoryNameInterect,overlapList[i]);
		test_intersection_fixed_overlap(overlapList[i],numSets,logFileName);
		free(logFileName);

		logFileName 	= (char*)malloc(1000*sizeof(char));
		sprintf(logFileName,"%s/%d.csv",directoryNameUnion,overlapList[i]);
		test_union_fixed_overlap(overlapList[i],numSets,logFileName);
		free(logFileName);
	}
}

static inline float get_time_diff(struct timeval t1, struct timeval t2)
{
	return t2.tv_sec - t1.tv_sec + 1e-6 * (t2.tv_usec - t1.tv_usec);
}

int main(int argc, char** argv)
{
	bloomTestingPath 	= (char*)malloc(1000*sizeof(char));
	datasetPath 		= (char*)malloc(1000*sizeof(char));
	logsPath 			= (char*)malloc(1000*sizeof(char));
	strcpy(bloomTestingPath,"../../../bloomTesting/");
	strcpy(datasetPath,"../../../bloomTesting/datasets/");
	strcpy(logsPath,"../../../bloomTesting/logs/");
	time_t t;
	srand((unsigned) time(&t));	

	if (argc >=2)
	{
		int testId = atoi(argv[1]);

		if ( ((testId == 3) || (testId == 4)) && (argc >= 8))
		{
			char* fileListName 		= argv[2];
			int M 					= atoi(argv[3]);
			UINT K 					= atoi(argv[4]);
			float FALSE_PSTV_PROB 	= atof(argv[5]);
			int numUnitSize 		= atoi(argv[6]);
			int* unitSizeVals 		= (int*)malloc(numUnitSize*sizeof(int));
			if (argc < numUnitSize + 7)
			{
				printf("Not enough unit size values specified\n");
				exit(0);
			}

			for (int i = 0; i < numUnitSize; ++i)
			{
				unitSizeVals[i] = atoi(argv[7+i]);
			}
			if (testId == 3)
				run_FP_test_sameMemUsage(fileListName, M, K, FALSE_PSTV_PROB, numUnitSize, unitSizeVals);
			else if (testId == 4)
				run_memUsage_test_sameFP(fileListName, M, K, FALSE_PSTV_PROB, numUnitSize, unitSizeVals);
		}
		else if ((testId == 2) && (argc >= 9))
		{
			int M 					= atoi(argv[2]);
			int K 					= atoi(argv[3]);
			float FALSE_PSTV_PROB 	= atof(argv[4]);
			int unitSize 			= atoi(argv[5]);
			int numSets 			= atoi(argv[6]);
			int numOverlapVals 		= atoi(argv[7]);
			int* overlapList 		= (int*)malloc(numOverlapVals*sizeof(int));
			if( argc < numOverlapVals + 8)
			{
				printf("Not enough values for overlap specified\n");
				exit(0);
			}
			if ((unitSize % (8*sizeof(int))) != 0)
			{
				printf("Unitsize value should be a multiple of %ld\n",8*sizeof(int));
				exit(0);
			}
			for (int i = 0; i < numOverlapVals; ++i)
			{
				overlapList[i] = atoi(argv[8+i]);
			}
			run_setOps_fixed_overlap(overlapList,numOverlapVals, numSets, K, M, unitSize, FALSE_PSTV_PROB);
		}
		else
		{
			printf("Error:: Not enough arguments after testId\n");
			printf("Usage:: ./test <testId> <other parameters>\n");
			printf("testId = 2 for set intersection/union tests with fixed overlap\n");
			printf("testId = 3 for set creation tests (with varying size of unit Bloom filter)(with same Memory as DPBF)\n");
			printf("testId = 4 for set creation tests (with varying size of unit Bloom filter)(with bounded FP Rate)\n");
			printf("For testId = 2 Usage:: ./test 2 <M> <K> <False Positive probability> <unitSize> <number of sets> <number of overlap values(n)>  <overalap value_1> ... <overalap value_n>\n");
			printf("For testId = 3 & 4 Usage:: ./test 1 <fileListName> <M> <K> <False Positive probability> <number of unitSizes(n)>  <unit size value_1> ... <unit size value_n>\n");
		}
	}	
	else
	{
		printf("Error:: Not enough arguments\n");
		printf("Usage:: ./test <testId> <other parameters>\n");
		printf("testId = 2 for set intersection/union tests with fixed overlap\n");
		printf("testId = 3 for set creation tests (with varying size of unit Bloom filter)(with same Memory as DPBF)\n");
		printf("testId = 4 for set creation tests (with varying size of unit Bloom filter)(with bounded FP Rate)\n");
		printf("For testId = 2 Usage:: ./test 2 <M> <K> <False Positive probability> <unitSize> <number of sets> <number of overlap values(n)>  <overalap value_1> ... <overalap value_n>\n");
		printf("For testId = 3 & 4 Usage:: ./test 1 <fileListName> <M> <K> <False Positive probability> <number of unitSizes(n)>  <unit size value_1> ... <unit size value_n>\n");
	}
	
}
