#ifndef DYNAMIC_BLOOM_H
#define DYNAMIC_BLOOM_H

#include "count_bloom.h"
#include <math.h>

extern bloomParameters* bloomParam;
extern char* datasetPath;
typedef struct dyn_bloom
{
	bloom** bf_array;
	int maxSize; 	// Stores the maximum number of bloomFilters that bf_array
	int currNum;	// Stores index of currently active bloomFilters
	int filled_chunks;
}dyn_bloom;


extern char* datasetPath;
/*
*
*/
void init_dyn_parameters(UINT SIZE_BLOOM, UINT K,UINT COUNTER_CHUNK, float ARG_FALSE_PSTV_PROB);

/*
* INPUT : pointer to dynamic bloomfilter
* WORK	: allocates space and for one bloom filter and also initialised maxSize and currNum
* OUTPUT: none
* NOTE 	: using same as in bloomsampling but memory allocation in this function only.
*/
void dyn_init(dyn_bloom *dbf);

/*
* INPUT : val {value to be inserted} , pointer to dynamic bloom filter
* WORK	: inserts the value in the active bloom filter after calculating suitable hash functions .
* insertion happens by incrementing the value in COUNTER_CHUNK bits present in the array. 
* OUTPUT: none
* NOTE 	: using as it is from bloomsampling codebase.
*/
void dyn_insert(UINT val, dyn_bloom* dbf);

/*
* INPUT : val {value to be confirmed} , pointer to bloom filter
* WORK	: checks the bloom filter for the provided value
* OUTPUT: 1 if success and 0 on failure.
* NOTE 	: using as it is from bloomsampling codebase.
*/
UINT dyn_is_in(UINT val, dyn_bloom* dbf);

/*
* INPUT : pointer to bloom filter
* WORK	: frees the allocated space for the bloom filter
* OUTPUT: none
* NOTE 	: as allocation in init so wrapping up the freeing function.
*/
void dyn_free_bloom(dyn_bloom* dbf);

/*
*
*/
UINT * reconstruct_dyn_bloom(dyn_bloom* bl, UINT start, UINT end );

/*
* INPUT : two counting bloom filters
* WORK	: assigns a new bloom filter which stores the intersection of two given bloom 
* filters
* OUTPUT: the bloom filter c is modified and also it takes care if c is same as a or b
* NOTE 	: -
*/
void dyn_intersect_bloom(dyn_bloom *a, dyn_bloom *b, dyn_bloom *c);

/*
* Returns 1 on success and 0 otherwise
*/
int dyn_delete(UINT val, dyn_bloom* dbf);

/*
* INPUT : two counting bloom filters
* WORK	: assigns a new bloom filter which stores the difference of two given bloom 
* filters in order elements in a- elements in b
* OUTPUT: the bloom filter c is modified and also it takes care if c is same as a or b
* NOTE 	: -
*/
void dyn_subtract_bloom(dyn_bloom *a, dyn_bloom *b, dyn_bloom *c);


/*
* INPUT : two counting bloom filters
* WORK	: assigns a new bloom filter which stores the exact sum of two given bloom 
* filters in order elements in a- elements in b
* OUTPUT: the bloom filter c is modified and also it takes care if c is same as a or b
* NOTE 	: -
*/
void dyn_add_bloom(dyn_bloom *a, dyn_bloom *b, dyn_bloom *c);

/*
* INPUT : two counting bloom filters
* WORK	: assigns a new bloom filter which stores the union of two given bloom 
* filters
* OUTPUT: the bloom filter c is modified and also it takes care if c is same as a or b
* NOTE 	: -
*/
void dyn_union_bloom(dyn_bloom *a, dyn_bloom *b, dyn_bloom *c);

/*
* NOTE  : testing fn which creates a bloom filter and inserts checks membership and deletes 
* 5000 numbers which are generated randomly
*/
void dyn_bloom_test_suit1();

/*
* NOTE 	: testing fn which creates two bloom filters with odd and even no.s and takes their 
* intersection and union which are later checked for membership. 
*/
void dyn_bloom_test_suit2();


//##############extra elements required for bloom sampling########################//

/*
* INPUT : counting bloom filter
* WORK	: counts the total no. of 1s present in the bloom filter
* OUTPUT: long count for the no. of ones
* NOTE 	: makes call to the count_ones fn for each element of the array
*/
UINT dyn_num_ones(dyn_bloom *a);

/*
* INPUT : counting bloom filter
* WORK	: counts the total no. of positive chunks present in the bloom filter
* OUTPUT: long count for the no. of positive chunks
* NOTE  : seeing chunks count suitable for applying approximate formula
*/
// long num_pos_chunks(bloom *a);

/*
* INPUT : counting bloom filter
* WORK	: counts the total no. of elements in the bloom filter
* OUTPUT: long count for the no. of ones
* NOTE 	: makes call to the num_ones fn and assumes no bucket overflow happens and just 
* divides by k.
*/
UINT dyn_count_elems(dyn_bloom *a);

/*
* INPUT : two counting bloom filters
* WORK	: counts the no. of ones in the intersection of the given two bloom filters
* OUTPUT: count of ones common to both
* NOTE 	: -
*/
UINT dyn_numOnes_intersection(dyn_bloom *a, dyn_bloom *b);


/*
* INPUT : two counting bloom filters
* WORK	: counts the no. of positive chunks in the intersection of the given two bloom filters
* OUTPUT: count of positive chunks common to both
* NOTE 	: suitable for approximation formula based on simple bloom filter
*/
// int numChunks_intersection(bloom *a, bloom *b);



void dyn_grow(dyn_bloom *dbf);


/*
* INPUT : two counting bloom filters
* WORK	: gives the no. of common elements by using the formula for bloom filters
* OUTPUT: double for the no. of common elements
* NOTE 	: may have some fallacy as using for the counting bloom filter in place of normal
*/
double dyn_elemsIntersection(dyn_bloom *a, dyn_bloom *b, int ta, int tb);

/*
* INPUT : two counting bloom filters
* WORK	: gives the no. of common elements by using the formula for bloom filters
* OUTPUT: double for the no. of common elements
* NOTE 	: the fallacy mentioned above has been corrected
*/
// double elemsIntersection_corrected(bloom *a, bloom *b, int ta, int tb);

/*
* INPUT  : x - integer located at a location in bloom filter which stores hashed values in 
* counter present in chunks of COUNTER_CHUNK
* OUTPUT : count of the number of elements hashed in this location across all chunks
* NOTE 	 : slightly expensive as multiple left shift and right shift operations
*/
UINT dyn_count_ones(UINT x);


void dyn_bloom_test_suit1();
void dyn_bloom_test_suit2();

void print_bloom_filter(dyn_bloom * da);

#endif