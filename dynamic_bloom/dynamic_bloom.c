#include "dynamic_bloom.h"

/*
* IMPORTANT : maxSize > currNum always for dynamic bloomFilter and this array of bloomFilter is not dynamic
*/

/*
* INPUT : pointer to dynamic bloomfilter
* WORK	: allocates space and for one bloom filter and also initialised maxSize and currNum
* OUTPUT: none
* NOTE 	: using same as in bloomsampling but memory allocation in this function only.
*/

char* datasetPath;

void init_dyn_parameters(UINT SIZE_BLOOM, UINT K,UINT COUNTER_CHUNK, float ARG_FALSE_PSTV_PROB)
{
	init_bloomParameters(SIZE_BLOOM, K, COUNTER_CHUNK);
	bloomParam->FALSE_PSTV_PROB = ARG_FALSE_PSTV_PROB;
	bloomParam->FILL_THRESHOLD  = -1*((int)bloomParam->SIZE_BLOOM/(float)(bloomParam->K*bloomParam->COUNTER_CHUNK))*log( 1 - pow(ARG_FALSE_PSTV_PROB,1.0/bloomParam->K));
	bloomParam->MAX_DYN_SIZE =  1;
}

void dyn_init(dyn_bloom *dbf)
{
	dbf->maxSize 	= bloomParam->MAX_DYN_SIZE;
	dbf->currNum 	= 0;
	dbf->bf_array 	= (bloom**)malloc(dbf->maxSize*sizeof(bloom*));
	dbf->bf_array[dbf->currNum] = (bloom*)malloc(sizeof(bloom));
	dbf->filled_chunks = 0;
	init(dbf->bf_array[dbf->currNum]);
}

void dyn_grow(dyn_bloom *dbf)
{
	if(dbf->currNum==dbf->maxSize)
	{
		int i;
		bloom **temp = (bloom**)malloc(dbf->maxSize*2*sizeof(bloom*));
		for( i = 0 ; i <= dbf->currNum ; i++)
			temp[i] = dbf->bf_array[i];
		free(dbf->bf_array);
		dbf->bf_array = temp;
		dbf->maxSize *= 2;
	}
	dbf->currNum++;
	dbf->bf_array[dbf->currNum] = (bloom*)malloc(sizeof(bloom));
	dbf->filled_chunks = 0;
	init(dbf->bf_array[dbf->currNum]);
}

/*
* INPUT : val {value to be inserted} , pointer to dynamic bloom filter
* WORK	: inserts the value in the active bloom filter after calculating suitable hash functions .
* insertion happens by incrementing the value in COUNTER_CHUNK bits present in the array. 
* OUTPUT: none
* NOTE 	: using as it is from bloomsampling codebase.
*/
void dyn_insert(UINT val, dyn_bloom* dbf)
{
	insert(val,dbf->bf_array[dbf->currNum]);
	dbf->filled_chunks += 1;
	//  Check for false positive probability bound so that currNum can be incremented
	if (dbf->filled_chunks >= bloomParam->FILL_THRESHOLD)
	{
		dyn_grow(dbf);
	}
}

/*
* INPUT : val {value to be confirmed} , pointer to bloom filter
* WORK	: checks the bloom filter for the provided value
* OUTPUT: 1 if success and 0 on failure.
* NOTE 	: using as it is from bloomsampling codebase.
*/
UINT dyn_is_in(UINT val, dyn_bloom* dbf)
{
	int i;
	for (i = 0; i <= dbf->currNum; ++i)
	{
		if (is_in(val,dbf->bf_array[i]))
			return 1;
	}
	return 0;
}

/*
* INPUT : pointer to bloom filter
* WORK	: frees the allocated space for the bloom filter
* OUTPUT: none
* NOTE 	: as allocation in init so wrapping up the freeing function.
*/
void dyn_free_bloom(dyn_bloom* dbf)
{
	int i;
	for (i = 0; i <= dbf->currNum; ++i)
	{
		free_bloom(dbf->bf_array[i]);
	}
	free(dbf->bf_array);
}

void dyn_adjust_bloom(dyn_bloom * dbf, int size)
{
	int i;
	if( (dbf->currNum+1)<size)
	{
		for( i = dbf->currNum+1 ; i < size ; i++ )
		{
			dyn_grow(dbf);
		}
	}
	else if( (dbf->currNum+1)>size)
	{
		for( i = size  ; i <= dbf->currNum ; i++ )
		{
			free_bloom(dbf->bf_array[i]);
			free(dbf->bf_array[i]);
			dbf->currNum--;
		}
	}
}

UINT * reconstruct_dyn_bloom(dyn_bloom* bl, UINT start, UINT end )
{
	UINT i;
	UINT num_elems,elem = 0;
	for(i = start; i <= end; i++)
	{
		if(dyn_is_in(i,bl))
			num_elems++;
	}

	UINT * val_array = (UINT *)malloc(sizeof(UINT)*num_elems);
	
	memset(val_array, 0, sizeof(UINT)*num_elems);
	for(i = start; i <= end; i++)
	{
		if(dyn_is_in(i,bl))
		{
			val_array[elem] = i;
			elem++;
		}
	}
	return val_array;
}

/*
* INPUT : two counting bloom filters
* WORK	: assigns a new bloom filter which stores the intersection of two given bloom 
* filters
* OUTPUT: the bloom filter c is modified and also it takes care if c is same as a or b
* NOTE 	: Basic idea , each bf of smaller dyn_bloom takes intersection with each of the 
* bloom filters and union of all those is stored into the resultant bloom filter
* issue -- possible effect on the count elems **maybe for 2 bits can fall back to the 
* simple probabilistic formula to get size out of a given bloom filter
*/
void dyn_intersect_bloom(dyn_bloom *a, dyn_bloom *b, dyn_bloom *c)
{
	dyn_bloom * small;
	dyn_bloom * large;
	if( a == c || b == c )
	{
		printf("dyn_intersect_bloom()::inplace intersection not allowed\n");
		exit(0);
	}
	if( a->currNum >= b->currNum )
	{
		small = b;
		large = a;
	}
	else
	{
		small = a;
		large = b;
	}

	if(c->currNum!=small->currNum)
	{
		dyn_adjust_bloom(c,small->currNum+1);
	}

	int i,j,k;
	for (i = 0; i <= small->currNum; ++i)
	{
		for (j = 0; j <= large->currNum; ++j)
		{
				UINT m,n,num=0;
				for(m = 0 ; m < (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS) ; m++)
				{ 
					UINT temp = 0;
					for( n = 0 ; n < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; n++)
					{
						UINT new = bloomParam->BIT_MAP<<(n*bloomParam->COUNTER_CHUNK);
						temp += MAX( c->bf_array[i]->flag_array[m]&new , MIN(small->bf_array[i]->flag_array[m]&new , large->bf_array[j]->flag_array[m]&new));
					}
					c->bf_array[i]->flag_array[m] = temp;					//taking += here which is required as against to = in case of normal intersection
				}
		}
	}
}

/*
* Returns 1 on success and 0 on failure
*/
int dyn_delete(UINT val, dyn_bloom* dbf)
{
	int i , delCtr = 0, index = 0;
	for ( i = 0; i <= dbf->currNum; ++i)
	{
		if (is_in(val,dbf->bf_array[i]))
		{
			delCtr 	+= 1;
			index = i;
		}
	}

	if (delCtr == 0)
	{
		printf("dyn_delete()::Element not present in the dynamic bloom filter\n");
	}
	else if (delCtr == 1)
	{
		delete(val,dbf->bf_array[index]);
	}
	else if (delCtr >= 2)
	{
		printf("dyn_delete()::Element deleted from multiple bloomFilters\n");
	}
}


/*
* INPUT : two counting bloom filters
* WORK	: assigns a new bloom filter which stores the difference of two given bloom 
* filters in order elements in a- elements in b
* OUTPUT: the bloom filter c is modified and also it takes care if c is same as a or b
* NOTE 	: chunkwise modification is done by taking the chunk in small and subtracting 
* from it intersection with all the chunks of those of large bf. 
*/
void dyn_subtract_bloom(dyn_bloom *a, dyn_bloom *b, dyn_bloom *c)
{
	if( b == c )
	{
		printf("dyn_subtract_bloom()::inplace subtraction not allowed\n");
		exit(0);
	}

	if(c->currNum!=a->currNum)
	{
		dyn_adjust_bloom(c,a->currNum+1);
	}

	int i,j,k;
	for (i = 0; i <= a->currNum; ++i)
	{

			UINT m,n,num=0;
			for(m = 0 ; m < (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS) ; m++)
			{ 
				UINT temp = 0;

				for( n = 0 ; n < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; n++)
				{
					UINT new = bloomParam->BIT_MAP<<(n*bloomParam->COUNTER_CHUNK);
					UINT curr = a->bf_array[i]->flag_array[m]&new ;

					for (j = 0; j <= b->currNum; ++j)
					{
						//no need to update small as we do as only when less than 0 then only it might affect
						curr -= MIN(a->bf_array[i]->flag_array[m]&new , b->bf_array[j]->flag_array[m]&new);
						if(curr < 0)
						{
							curr = 0;
							break;
						}
					}
					if(curr>new)
						curr = new;

					temp+=curr;
				}
				c->bf_array[i]->flag_array[m] = temp;					//taking += here which is required as against to = in case of normal intersection
			}

	}
}

/*
* INPUT : two counting bloom filters
* WORK	: assigns a new bloom filter which stores the exact sum of two given bloom 
* filters in order elements in a- elements in b
* OUTPUT: the bloom filter c is modified and also it takes care if c is same as a or b
* NOTE 	: simply add ith small to (n-i)th big and leave rest unchanged invariant of 
* having filled up fraction is no more maintained in this case.
*/
void dyn_add_bloom(dyn_bloom *a, dyn_bloom *b, dyn_bloom *c)
{
	dyn_bloom * small;
	dyn_bloom * large;
	if( b == c )
	{
		printf("dyn_add_bloom()::inplace add not allowed\n");
		exit(0);
	}
	if( a->currNum >= b->currNum )
	{
		small = b;
		large = a;
	}
	else
	{
		small = a;
		large = b;
	}

	if(c->currNum!=large->currNum)
	{
		dyn_adjust_bloom(c,large->currNum+1);
	}

	int i,j,k;
	for (i = 0; i <= large->currNum; ++i)
	{
		int curr_large = large->currNum - i;
		if(i <= small->currNum)
		{
				UINT m,n;
				for(m = 0 ; m < (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS) ; m++)
				{ 
					UINT temp = 0;

					for( n = 0 ; n < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; n++)
					{
						UINT new = bloomParam->BIT_MAP<<(n*bloomParam->COUNTER_CHUNK);
						UINT curr = (small->bf_array[i]->flag_array[m])&new + (large->bf_array[curr_large]->flag_array[m])&new;

						if(curr>new)
						{
							curr = new;
						}

						temp += curr;
					}
					c->bf_array[curr_large]->flag_array[m] = temp;
				}
		}
		else
		{
				UINT m = 0;
				for(m = 0 ; m < (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS) ; m++)
				{
					 c->bf_array[curr_large]->flag_array[m] = large->bf_array[curr_large]->flag_array[m];
				}
		}
	}
}


/*
* INPUT : two counting bloom filters
* WORK	: assigns a new bloom filter which stores the union of two given bloom 
* filters
* OUTPUT: the bloom filter c is modified and also it takes care if c is same as a or b
* NOTE 	: here first in each chunk of the larger which is iterated in reverse in order
* as compared to small array  both the large and corresponding small if there added and 
* then min of the chunks with all the small array bfs is subtracted. And finally assigned
* to temp which finally sets c.
*/
void dyn_union_bloom(dyn_bloom *a, dyn_bloom *b, dyn_bloom *c)
{
	dyn_bloom * small;
	dyn_bloom * large;
	if( b == c )
	{
		printf("dyn_union_bloom()::inplace union not allowed\n");
		exit(0);
	}
	if (a->currNum > b->currNum)
	{
		large = a;
		small = b;	
	}
	else
	{
		large = b;
		small = a;
	}

	if(c->currNum!=large->currNum)
	{
		dyn_adjust_bloom(c,large->currNum+1);
	}

	int i,j,k;
	for (i = 0; i <= large->currNum; ++i)
	{
		int curr_large = i;
		if(i <= small->currNum)
		{
			UINT m,n,num=0;
			for(m = 0 ; m < (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS) ; m++)
			{ 
				UINT temp = 0;
				for( n = 0 ; n < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; n++)
				{
					UINT new 		= bloomParam->BIT_MAP<<(n*bloomParam->COUNTER_CHUNK);
					UINT curr 		= large->bf_array[curr_large]->flag_array[m]&new;
					UINT b_union 	= 0;
					UINT sub 		= 0;
					for (j = 0; j <= small->currNum; ++j)
					{
						b_union = MAX((small->bf_array[j]->flag_array[m]&new) ,b_union );
					}
					
					if ( b_union <= curr)
					{
						curr = curr - b_union;
					}
					else
					{
						curr = 0;
					}
					curr += (small->bf_array[i]->flag_array[m]&new);
					if(curr > new)
						curr = new;

					temp += curr;
				}
				c->bf_array[curr_large]->flag_array[m] = temp;					//taking += here which is required as against to = in case of normal intersection
			}
		}
		else
		{	
				UINT m = 0;
				for(m = 0 ; m < (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS) ; m++)
				{
					 c->bf_array[curr_large]->flag_array[m] = large->bf_array[curr_large]->flag_array[m];
				}
		}
	}
}


//##############extra elements required for bloom sampling########################//

/*
* INPUT : counting bloom filter
* WORK	: counts the total no. of 1s present in the bloom filter
* OUTPUT: long count for the no. of ones
* NOTE 	: makes call to the count_ones fn for each element of the array
*/
UINT dyn_num_ones(dyn_bloom *dbf)
{
	UINT count = 0;
	int i;
	for (i = 0; i <= dbf->currNum; ++i)
	{
		count += num_ones(dbf->bf_array[i]);
	}
	return count;
}


/*
* INPUT : counting bloom filter
* WORK	: counts the total no. of elements in the bloom filter
* OUTPUT: long count for the no. of ones
* NOTE 	: makes call to the num_ones fn and assumes no bucket overflow happens and just 
* divides by k.
*/
UINT dyn_count_elems(dyn_bloom *dbf)
{
	ULONG count = dyn_num_ones(dbf)/bloomParam->K;
	return count;
}


/*
* INPUT : two counting bloom filters
* WORK	: counts the no. of ones in the intersection of the given two bloom filters
* OUTPUT: count of ones common to both
* NOTE 	: -
*/
UINT dyn_numOnes_intersection(dyn_bloom *a, dyn_bloom *b)
{
	int i,j;
	UINT count = 0;
	for (i = 0; i <= a->currNum; ++i)
	{
		for (j = 0; j <= b->currNum; ++j)
		{
			count +=  numOnes_intersection(a->bf_array[i],b->bf_array[j]);
		}
	}
	return count;
}

/*
* INPUT : two counting bloom filters
* WORK	: gives the no. of common elements by using the formula for bloom filters
* OUTPUT: double for the no. of common elements
* NOTE 	: Just counting number of elements present by summing up counters in pairwise intersection
* 			of bloomFilters in 2 DBFs
*/
double dyn_elemsIntersection(dyn_bloom *a, dyn_bloom *b, int ta, int tb)
{
	double th = (double)dyn_numOnes_intersection(a,b);
	return th/bloomParam->K;
}

void dyn_bloom_test_suit1()
{
	dyn_bloom *a = (dyn_bloom*)malloc(sizeof(dyn_bloom));
	dyn_init(a);
	dyn_grow(a);
	UINT i;
	printf("bloom graph insert/member/delete test initiated\n");
	for(i = 0 ; i < bloomParam->TEST_RANGE ; i++ )
	{
		if(i%100==0)
			dyn_grow(a);
		UINT z = rand()%bloomParam->TEST_RANGE;
		dyn_insert(z,a);
		if(!dyn_is_in(z,a))
		{
			printf("bloom graph not functioning properly - inserted val :: %d not found\n",z);
			exit(0);
		}
		dyn_delete(z,a);
	}

	printf("count of elems :: %u  %u \n",dyn_count_elems(a),bloomParam->TEST_RANGE );
	dyn_free_bloom(a);
	free(a);
	printf("bloom graph insert/member/delete test successful\n");
}

void dyn_bloom_test_suit2()
{
	dyn_bloom *a = (dyn_bloom*)malloc(sizeof(dyn_bloom));
	dyn_bloom *b = (dyn_bloom*)malloc(sizeof(dyn_bloom));

	dyn_init(a);	

	dyn_init(b);

	UINT i;
	printf("bloom graph union/intersect test initiated with range %u\n",bloomParam->TEST_RANGE);
	UINT count_a = 0,count_b = 0;
	for( i = 0 ; i < bloomParam->TEST_RANGE ; i++ )
	{
		if(i%100==0 && i>0)
		{
			dyn_grow(a);
			dyn_grow(b);
			printf("curr a %u size of last chunk %u \n",a->currNum,count_elems(a->bf_array[b->currNum-1]) );
			printf("curr b %u size of last chunk %u \n",b->currNum,count_elems(a->bf_array[b->currNum-1]) );
		}
		if(i%2==0)
		{
			dyn_insert(i,a);
			if(!dyn_is_in(i,a))
				printf("element not found a %u\n",i);
			count_a++;
		}
		else
		{
			dyn_insert(i,b);
			if(!dyn_is_in(i,b))
				printf("element not found b  %u\n",i);
			count_b++;
		}
	}
	printf("insertion stage over\n");
	printf("size a :: %u actual:: %u\n",dyn_count_elems(a),count_a );	//the number of elements may be less as some counter may completely fill up
	printf("size b :: %u actual:: %u\n",dyn_count_elems(b),count_b );	//

	dyn_bloom * c = (dyn_bloom *)malloc(sizeof(dyn_bloom));
	dyn_init(c);
	dyn_intersect_bloom(a,b,c);
	printf("intersection done  growth :: %d\n",c->currNum+1);
	dyn_bloom * d = (dyn_bloom *)malloc(sizeof(dyn_bloom));
	dyn_init(d);
	dyn_union_bloom(b,a,d);
	printf("union done growth :: %d\n",d->currNum+1);

	// dyn_bloom * e = (dyn_bloom *)malloc(sizeof(dyn_bloom));
	// dyn_init(e);
	// dyn_add_bloom(a,b,e);
	// printf("add done growth :: %d\n",e->currNum);



	printf("union/intersection operations executed -- size union size::%u intersect::%u \n",dyn_count_elems(d),dyn_count_elems(c));
	printf("intersection count via-- current elemsIntersection::%f \n",dyn_elemsIntersection(a,b,dyn_num_ones(a),dyn_num_ones(b)));

	ULONG con_union = 0;
	ULONG con_inter = 0;
	for( i = 0 ; i < bloomParam->TEST_RANGE ; i++ )
	{

		if(!dyn_is_in(i,d))
		{
			// printf("\tunion not working correctly %d\n",i);
			con_union++;
		}
		if(dyn_is_in(i,c)==1)
		{
			// printf("\tintersect conflict found - %d\n",i);
			con_inter++;
		}
	}
	printf("errors_union :: %llu errors_intersect :: %llu \n",con_union,con_inter );
	dyn_free_bloom(a);
	dyn_free_bloom(b);
	dyn_free_bloom(c);
	// dyn_free_bloom(e);
	free(a);
	free(b);
	free(c);
	// free(e);
	printf("bloom graph union/intersect test successful\n");
}

void print_bloom_filter(dyn_bloom * da)
{
	int i,j,k;
	for (k = 0; k <= da->currNum; ++k)
	{
		bloom* a = da->bf_array[k];	
		for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
		{
			for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
			{
				UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
				printf("%0*d ",2,(a->flag_array[i]&new)>>(j*bloomParam->COUNTER_CHUNK));
			}
		}
		printf("\n");
	}
}



