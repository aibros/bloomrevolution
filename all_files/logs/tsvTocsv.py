#!/usr/bin/env python
import sys,csv
fileName 	= raw_input("Enter Filename(without .tsv) to convert it into .csv file::\t");
csvFilename = fileName + '.csv'
writer 		= open(csvFilename,'w');
f = open(fileName + '.tsv')
csvReader = csv.reader(f,delimiter='\t')
ctr = 0
for row in csvReader:
	ctr+= 1;
	if ctr > 1:
		writer.write('\n')
	
	temp = ",".join(row)
	writer.write(temp);

