#ifndef _BLOOM_H
#define _BLOOM_H

#include <limits.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>

#include "dynamic_array/array.h"

#define MAX(a,b) ((a) > (b) ? a : b)
#define MIN(a,b) ((a) < (b) ? a : b)

typedef unsigned int UINT;
typedef unsigned long int ULONG;


typedef struct bloomParameters
{
	UINT NUM_BITS;
	UINT SIZE_BLOOM;
	UINT K;
	UINT COUNTER_CHUNK;		
	UINT BIT_MAP;
	UINT NUM_PARTITION;			//number of partition functions
	float FALSE_PSTV_PROB;
	float FILL_THRESHOLD;   // This is the max number of elements that we can have so as to maintain the required false +ve rate
	/*
	* testing suite variables
	*/
	UINT TEST_RANGE;
}bloomParameters;



/*
* using integer array to keep the flags with the notion that each place keeps 32 bits so
* will have 32 flags present . Extend it to counting by setting fixed no. of bits for the 
* counting purpose.
*/
typedef struct bloom{
	UINT *flag_array;	
	UINT num_elems;	
}bloom;

/*********************************************************************************************/

/*
* new bloom filter : structured as tree with partitioned ranges 
* represets a node present in the tree structure
* IF bf not NULL then leaf and bf initialized
* pointers to left and right children if not leaf
*/
typedef struct b_node b_node;

struct b_node{
	bloom *bf;
	b_node *left;
	b_node *right;		
};



/*********************************************************************************************/

extern bloomParameters* bloomParam;
extern char* datasetPath;

/*
* INPUT : none
* WORK	: sets up m array required in calculating hash values
* OUTPUT: none
* NOTE 	: using the hash function along with necessary setup provided as in bloomsampling 
* codebase inplace of directly using murmur.
*/

void init_bloomParameters(UINT SIZE_BLOOM ,UINT K ,UINT COUNTER_CHUNK,UINT NUM_PARTITION,float FALSE_PSTV_PROB);


void seiveInitial();


int partition_hash( UINT val, UINT level );

// void partition_node(b_node *node, struct bloomTree *bTree, bloomNode *Bnode);

// void DFS_tree(bloomNode* treeNode,b_node* node);


/*
* INPUT : pointer to bloom filter
* WORK	: allocates space and initialises the bloom filter with all zero values
* OUTPUT: none
* NOTE 	: using same as in bloomsampling but memory allocation in this function only.
*/
void init_node(bloom *bl);
void init(b_node *bl);


UINT hash(int a, int i);

/*
* INPUT : val {value to be inserted} , pointer to bloom filter
* WORK	: inserts the value in the bloom filter after calculating suitable hash functions . 
* insertion happens by incrementing the value in COUNTER_CHUNK bits present in the array. 
* OUTPUT: none
* NOTE 	: using as it is from bloomsampling codebase.
*/
void insert_node(UINT val, bloom* bl);
void insert(UINT val, b_node* bl);
void insert_new(UINT val,b_node* node,int level);
void finish_insertion(b_node* node);
void grow_full_tree(b_node* node,int level);
/*
* INPUT : val {value to be confirmed} , pointer to bloom filter
* WORK	: checks the bloom filter for the provided value
* OUTPUT: 1 if success and 0 on failure.
* NOTE 	: using as it is from bloomsampling codebase.
*/
UINT is_in_node(UINT val, bloom* bl);
UINT is_in(UINT val, b_node* bl);

/*
*
*/
void read_and_insert(char * filename, b_node* bl);

/*
* INPUT : pointer to bloom filter
* WORK	: frees the allocated space for the bloom filter
* OUTPUT: none
* NOTE 	: as allocation in init so wrapping up the freeing function.
*/
void free_bloom_node(bloom* bl);
void free_bloom(b_node* bl);


/*
*
*/
UINT * reconstruct_bloom(bloom* bl, UINT start, UINT end );

void reconstruct_complete(b_node* a,my_array_long * return_array);	//slightly inefficient as twice time taken for now

/*
* INPUT : two counting bloom filters
* WORK	: assigns a new bloom filter which stores the intersection of two given bloom 
* filters
* OUTPUT: the bloom filter c is modified and also it takes care if c is same as a or b
* NOTE 	: -
*/
void intersect_bloom_node(bloom *a, bloom *b, bloom *c);
// void intersect_bloom(b_node *a, b_node *b, b_node* c,struct bloomTree *bTree,struct bloomNode *bNode);

/*
*
*/
void delete_node(UINT val, bloom* bl);
void delete(UINT val, b_node* bl);


/*
* INPUT : two counting bloom filters
* WORK	: assigns a new bloom filter which stores the difference of two given bloom 
* filters in order elements in a- elements in b
* OUTPUT: the bloom filter c is modified and also it takes care if c is same as a or b
* NOTE 	: -
*/
void subtract_bloom_node(bloom *a, bloom *b, bloom *c);
// int subtract_chunk(UINT *a, UINT *b, UINT *c);


/*
* INPUT : two counting bloom filters
* WORK	: assigns a new bloom filter which stores the exact sum of two given bloom 
* filters in order elements in a- elements in b
* OUTPUT: the bloom filter c is modified and also it takes care if c is same as a or b
* NOTE 	: -
*/
void add_bloom_node(bloom *a, bloom *b, bloom *c);
// int add_chunk(UINT *a, UINT *b, UINT *c);

/*
* INPUT : two counting bloom filters
* WORK	: assigns a new bloom filter which stores the union of two given bloom 
* filters
* OUTPUT: the bloom filter c is modified and also it takes care if c is same as a or b
* NOTE 	: -
*/
void union_bloom_node(bloom *a, bloom *b, bloom *c);
// void union_bloom(b_node *a, b_node *b, b_node *c, bloomTree *bTree, bloomNode *bNode);
// int union_chunk(UINT *a, UINT *b, UINT *c);

/*
* NOTE  : testing fn which creates a bloom filter and inserts checks membership and deletes 
* 5000 numbers which are generated randomly
*/
void bloom_test_suit1();

/*
* NOTE 	: testing fn which creates two bloom filters with odd and even no.s and takes their 
* intersection and union which are later checked for membership. 
*/
void bloom_test_suit2();


//##############extra elements required for bloom sampling########################//

/*
* INPUT : counting bloom filter
* WORK	: counts the total no. of 1s present in the bloom filter
* OUTPUT: long count for the no. of ones
* NOTE 	: makes call to the count_ones fn for each element of the array
*/
UINT num_ones(bloom *a);
// UINT num_ones_chunk(UINT *a);

/*
* INPUT : counting bloom filter
* WORK	: counts the total no. of positive chunks present in the bloom filter
* OUTPUT: long count for the no. of positive chunks
* NOTE  : seeing chunks count suitable for applying approximate formula
*/
// long num_pos_chunks(bloom *a);

/*
* INPUT : counting bloom filter
* WORK	: counts the total no. of elements in the bloom filter
* OUTPUT: long count for the no. of ones
* NOTE 	: makes call to the num_ones fn and assumes no bucket overflow happens and just 
* divides by k.
*/
UINT count_elems_node(bloom *a);
UINT count_elems(b_node *a);

void print_bloom_filter_node(bloom *a);
void print_dyn_part_bloom_filter(b_node *b);

/*
* INPUT : two counting bloom filters
* WORK	: counts the no. of ones in the intersection of the given two bloom filters
* OUTPUT: count of ones common to both
* NOTE 	: -
*/
UINT numOnes_intersection_node(bloom *a, bloom *b);
// UINT numOnes_intersection_chunk(UINT *a, UINT *b);


/*
* INPUT : two counting bloom filters
* WORK	: counts the no. of positive chunks in the intersection of the given two bloom filters
* OUTPUT: count of positive chunks common to both
* NOTE 	: suitable for approximation formula based on simple bloom filter
*/
// int numChunks_intersection(bloom *a, bloom *b);

/*
* INPUT : two counting bloom filters
* WORK	: gives the no. of common elements by using the formula for bloom filters
* OUTPUT: double for the no. of common elements
* NOTE 	: may have some fallacy as using for the counting bloom filter in place of normal
*/
double elemsIntersection_node(bloom *a, bloom *b, int ta, int tb);

/*
* INPUT : two counting bloom filters
* WORK	: gives the no. of common elements by using the formula for bloom filters
* OUTPUT: double for the no. of common elements
* NOTE 	: the fallacy mentioned above has been corrected
*/
// double elemsIntersection_corrected(bloom *a, bloom *b, int ta, int tb);

/*
* INPUT  : x - integer located at a location in bloom filter which stores hashed values in 
* counter present in chunks of COUNTER_CHUNK
* OUTPUT : count of the number of elements hashed in this location across all chunks
* NOTE 	 : slightly expensive as multiple left shift and right shift operations
*/
UINT count_ones(UINT x);

UINT count_leaves(b_node *node);

void read_and_insert(char * filename, b_node* filter);

void test_memory_usage(char** filenameList,int numFiles);

void estimate_false_positive(char* filename,b_node* filter, FILE* logger);

void test_union_intersection(char** filenameList_1,char** filenameList_2,int numFiles);

void run_memory_tests();

void run_set_operation_tests();

#endif