#ifndef ARRAY_H
#define ARRAY_H

#include <stdio.h>
#include <stdlib.h>

typedef struct my_array
{
	unsigned int* elements;
	unsigned int curr_size;
	unsigned int max_size;
}my_array;

typedef struct my_array_long
{
	unsigned long* elements;
	unsigned int curr_size;
	unsigned int max_size;
}my_array_long;

void arrayInt_init(my_array* array);
void arrayInt_addElement(my_array* array,unsigned int element);
void arrayInt_free(my_array* array);
int arrayInt_find(my_array* array,unsigned int element);
void print_elems(my_array* array);

void arrayLong_init(my_array_long* array);
void arrayLong_addElement(my_array_long* array,unsigned long element);
void arrayLong_free(my_array_long* array);
int arrayLong_find(my_array_long* array,unsigned long element);
#endif