#include "count_bloom.h"
#include "dynamic_array/array.h"
#include "bloom_tree.h"
/*
tree based dynamic bloom filter --
* New components:
* Hash fn -- K level , level as argument
* partition fn -- grows a bloom filter into two based on which level it is
* structure of the dynamic_partition bloom filter
* Binary Tree of bloom filters with a pointer based structure
* Each leaf consists of a bloom filter
* rest is easy to handle
*/



int seed[10];
int aHash[10]={3730, 9830, 3830, 2110, 4740, 9440, 5540, 2710, 690, 7470};
int bHash[10]={269, 229, 929, 949, 624, 390, 64, 67, 945, 253};
int m[10];

int mPrime[10];

bloomParameters* bloomParam;
struct bloomTree* bloom_tree;
char* datasetPath;

void init_bloomParameters(UINT SIZE_BLOOM ,UINT K ,UINT COUNTER_CHUNK,UINT NUM_PARTITION,float FALSE_PSTV_PROB)
{
	bloomParam = (bloomParameters*)malloc(sizeof(bloomParameters));
	bloomParam->NUM_BITS     	= 8*sizeof(UINT);
	bloomParam->SIZE_BLOOM   	= SIZE_BLOOM;
	bloomParam->K 			  	= K;
	bloomParam->COUNTER_CHUNK  	= COUNTER_CHUNK;		//should be exponents of 2
	bloomParam->BIT_MAP 		= pow(2,COUNTER_CHUNK) -1 ;	//2^COUNTER_CHUNK - 1;

	bloomParam->FALSE_PSTV_PROB = FALSE_PSTV_PROB;
	bloomParam->FILL_THRESHOLD  = -1*((int)SIZE_BLOOM/((float)K*COUNTER_CHUNK))*log( 1 - pow(FALSE_PSTV_PROB,1.0/K));
	printf("FILL_THRESHOLD::%f\n",bloomParam->FILL_THRESHOLD);
	printf("FALSE_PSTV_PROB::%f\n",bloomParam->FALSE_PSTV_PROB);
	bloomParam->NUM_PARTITION	= NUM_PARTITION;
	bloomParam->TEST_RANGE 		= 500;
}

void seiveInitial()
{
	int num_bits = bloomParam->SIZE_BLOOM/bloomParam->COUNTER_CHUNK;
	int *x = (int*)malloc(sizeof(int)*num_bits),i,j;

	// for (i = 0; i < 10; ++i)
	// {
	// 	aHash[i] = rand() % num_bits;
	// 	bHash[i] = rand() % num_bits;
	// }
	
	for (i=0;i<num_bits;i++) 
		x[i] = i+1;
	for (i=1;i<num_bits;i++)
	{
		j = x[i];
		if (j==-1) 
			continue;

		int q = 1;
		while ((i + q*j) < num_bits)
		{
			x[i+q*j] = -1;
			q++;
		}
	}
	i = num_bits;
	for (j=0;j<10;j++)
	{
		for (i=i-1;i>0;i--)
		{
			if (x[i]!=-1)
			{
				m[j] = x[i];
				break;
			}
		}
	}
}

/*
* returns the value of the hash function based on the level
* provided as input to the function
*/ 
int partition_hash( UINT val, UINT level )
{
	switch(level)
	{
		case 0:
			return val%2;				//hash functions to be modified later
		case 1:
			return (val%7)%2;
		case 2:
			return (val%13)%2;
		case 3:
			return (val%17)%2;
		case 4:
			return (val%19)%2;
		case 5:
			return (val%23)%2;
		case 6:
			return (val%29)%2;
		case 7:
			return (val%31)%2;
		case 8:
			return (val%37)%2;
		case 9:
			return (val%43)%2;
		case 10:
			return (val%53)%2;
		case 11:
			return (val%59)%2;
		case 12:
			return (val%61)%2;
		case 13:
			return (val%67)%2;
		case 14:
			return (val%71)%2;
		case 15:
			return (val%73)%2;
		case 16:
			return (val%79)%2;
	}

	printf("Value of level is larger than what we can handle\n");
	exit(0);
	return -1;
}

void init_node(bloom *bl)
{
	int size = (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);
	bl->flag_array = (UINT *)malloc(sizeof(UINT)*size);
	memset(bl->flag_array,0,sizeof(UINT)*size);
	bl->num_elems = 0;
}

/*
* initializes a single b_node to act as root of the tree
* call only for leaves as otherwise the bloom filters would get unnecessarily initialized
*/
void init(b_node *bloomf)
{
	bloomf->bf = (bloom *)malloc(sizeof(bloom *));
	init_node(bloomf->bf);								//call only for leaves
	bloomf->left = NULL;
	bloomf->right = NULL;
}

void init_without_bloom(b_node * a)
{
	a->bf = NULL;
	a->left = NULL;
	a->right = NULL;
}


void DFS_tree(struct bloomNode* treeNode,b_node* node,b_node* result)
{
	if (treeNode->lchild != NULL)
	{
		DFS_tree(treeNode->lchild,node,result);
		DFS_tree(treeNode->rchild,node,result);
	}
	else
	{
		bloom* temp = (bloom*)malloc(sizeof(bloom));
		init_node(temp);
		intersect_bloom_node(node->bf,&(treeNode->filter),temp);
		union_bloom_node(result->bf,temp,result->bf);
		free_bloom_node(temp);
	}
}

/*
* splits a bloom filter based on the level on which it is present
* the level decides the hash function to use to split
*/
void partition_node(b_node *node,struct bloomTree *bTree,struct bloomNode *Bnode)
{
	// printf("make sure the corresponding node in Tree is provided as input otherwise incorrect, use last input to trace the path to the node in tree\n");

	if(node->left!=NULL)
	{
		printf("only leaves can be partitioned\n");
		return;
	}

	node->left  =  (b_node *)malloc(sizeof(b_node *));
	node->right =  (b_node *)malloc(sizeof(b_node *));
	init(node->left);
	init(node->right);
	if(Bnode==NULL) // This is the first partition taking place 
	{
		// intersect_bloom_node(node->bf, &bTree->left->filter, node->left->bf);
		// intersect_bloom_node(node->bf, &bTree->right->filter, node->right->bf);
		DFS_tree(bTree->left,node,node->left);
		DFS_tree(bTree->right,node,node->right);
		// printf("In partition_node:: count_elems(parent)		%d\n",count_elems_node(node->bf));
		// printf("In partition_node:: count_elems(left)		%d\n",count_elems_node(node->left->bf));
		// printf("In partition_node:: count_elems(left)_tree	%d\n",count_elems_node(&bTree->left->filter));
		// printf("In partition_node:: count_elems(right)		%d\n",count_elems_node(node->right->bf));
		// printf("In partition_node:: count_elems(right)_tree	%d\n",count_elems_node(&bTree->right->filter));

		// printf("Left node\n");
		// print_bloom_filter_node(node->bf);
		// print_bloom_filter_node(&bTree->left->filter);	
		// printf("Result\n");
		// print_bloom_filter_node(node->left->bf);

		// printf("Right node\n");
		// print_bloom_filter_node(node->bf);
		// print_bloom_filter_node(&bTree->right->filter);	
		// printf("Result\n");
		// print_bloom_filter_node(node->right->bf);

		// printf("--------------left and right nodes-----------------\n");
		// print_bloom_filter_node(&bTree->left->filter);	
		// print_bloom_filter_node(&bTree->right->filter);	
	}
	else
	{
		// intersect_bloom_node(node->bf, &Bnode->lchild->filter, node->left->bf);
		// intersect_bloom_node(node->bf, &Bnode->rchild->filter, node->right->bf);
		DFS_tree(Bnode->lchild,node,node->left);
		DFS_tree(Bnode->rchild,node,node->right);
		
		// printf("In partition_node:: count_elems(parent)		%d\n",count_elems_node(node->bf));
		// printf("In partition_node:: count_elems(left)		%d\n",count_elems_node(node->left->bf));
		// printf("In partition_node:: count_elems(left)_tree	%d\n",count_elems_node(&(Bnode->lchild->filter)));
		// printf("In partition_node:: count_elems(right)		%d\n",count_elems_node(node->right->bf));
		// printf("In partition_node:: count_elems(right)_tree	%d\n",count_elems_node(&(Bnode->rchild->filter)));

		// printf("Left node\n");
		// print_bloom_filter_node(node->bf);
		// print_bloom_filter_node(&(Bnode->lchild->filter));	
		// printf("Result\n");
		// print_bloom_filter_node(node->left->bf);

		// printf("Right node\n");
		// print_bloom_filter_node(node->bf);
		// print_bloom_filter_node(&(Bnode->rchild->filter));	
		// printf("Result\n");
		// print_bloom_filter_node(node->right->bf);

		// printf("--------------left and right nodes-----------------\n");
		// print_bloom_filter_node(&(Bnode->lchild->filter));	
		// print_bloom_filter_node(&(Bnode->rchild->filter));	
	}

	free_bloom_node(node->bf);				//free the current bloom filter
	node->bf = NULL;
}

void insert_node(UINT val, bloom* bl)
{
	UINT i;
	UINT chunk_per_ind = bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK;
	bl->num_elems++;
	for (i=0;i<bloomParam->K;i++)
	{
		UINT a = hash(val,i);
		UINT loc = a/chunk_per_ind;
		UINT off = a%chunk_per_ind;

		UINT new = 1<<(off*bloomParam->COUNTER_CHUNK);
		UINT bitmap = (bloomParam->BIT_MAP<<(off*bloomParam->COUNTER_CHUNK));
		UINT chunk = bitmap & bl->flag_array[loc];
		if(chunk==bitmap)
		{
			// printf("overflow of the bucket for counter -- making no change\n");
			continue;
		}
		bl->flag_array[loc] = bl->flag_array[loc] + new;
	}
}

// level = i implies we are using ith partition functions. So level = 0 means we are not using any partition function at all
// 1st partition function is the one when we pass partition_hash level 0.
// assumes that b_node is a fully grown tree--> need to change code if this needs to be relaxed
// Assumption b_node* node is not NULL and is initialised (with or without bloom filter)
void insert_new(UINT val,b_node* node,int level)
{
	if(level  == bloomParam->NUM_PARTITION) // We should be inserting at this level
	{
		if (node->bf == NULL)
		{
			node->bf = (bloom*)malloc(sizeof(bloom));
			init_node(node->bf);
		}
		insert_node(val,node->bf);
	}
	else  // level + 1 < bloomParam->NUM_PARTITION 
	{	
		int retVal = partition_hash(val,level);
		if (retVal == 0) // Go left
		{
			if(node->left == NULL)
			{
				node->left = (b_node*)malloc(sizeof(b_node));
				init_without_bloom(node->left);
				printf("This should not happen,node->left uninitialised \n");
				exit(0);
			}
			insert_new(val,node->left,level+1);
		}
		else if (retVal == 1) // Go right
		{
			if(node->right == NULL)
			{
				node->right = (b_node*)malloc(sizeof(b_node));
				init_without_bloom(node->right);
				printf("This should not happen,node->right uninitialised \n");
				exit(0);
			}
			insert_new(val,node->right,level+1);
		}
		else
		{
			printf("We have exhausted all partition functions, what to do??\n\n\n\n\n");
		}
	}
}

void finish_insertion(b_node* node)
{
	if ((node->left == NULL) && (node->right == NULL)) // we are at a leaf 
	{
		// pass
	}
	else
	{
		finish_insertion(node->left);
		finish_insertion(node->right);
		// Check if we can shrink subtree under this node by combining left and right child
		// Before doing so we need to check that both of them have a valid bloomfilter
		// A node having a valid bloom filter is indicative of the fact that subtree under it has shrunken into that node
		if ((node->left->bf != NULL) && (node->right->bf != NULL)) 
		{
			int tempSum = node->left->bf->num_elems + node->right->bf->num_elems;
			if (tempSum < bloomParam->FILL_THRESHOLD) // Shrink left and right child to this node
			{
				// A better way of doint it is to reuse either left or right bloom filters and then change pointer accordingly
				// But doing it this way for now
				node->bf =(bloom*)malloc(sizeof(bloom));
				init_node(node->bf);
				union_bloom_node(node->bf,node->left->bf,node->bf);
				union_bloom_node(node->bf,node->right->bf,node->bf);
				node->bf->num_elems = tempSum;
				free_bloom(node->left);
				free_bloom(node->right);
				node->left 	= NULL;
				node->right = NULL;
			}
		}
	}
}

/*
*	traverses the tree and inserts at the right node
*/
void insert(UINT val, b_node * bloomf)
{
	int not_found 		= 1;
	b_node * curr_node  = bloomf;
	UINT curr_level 	= 0;
	struct bloomNode* currTreeNode = NULL;			
	while(not_found)
	{
		if(curr_node->left==NULL)				//identified as leaf
		{
			insert_node(val, curr_node->bf);
			float fill = curr_node->bf->num_elems*bloomParam->K*bloomParam->COUNTER_CHUNK/(float)bloomParam->SIZE_BLOOM;
			// printf("fill fraction :: %f\n",fill );
			// if( (fill > bloomParam->FILL_THRESHOLD) && (curr_level < bloomParam->NUM_PARTITION - 1))
			if ((curr_node->bf->num_elems > bloomParam->FILL_THRESHOLD) && (curr_level < bloomParam->NUM_PARTITION - 1))
			{
				// printf("Level::%d < NUM_PARTITION :: %d\n",curr_level,bloomParam->NUM_PARTITION );
				// printf("num_elems::%d > FILL_THRESHOLD :: %f\n",curr_node->bf->num_elems, bloomParam->FILL_THRESHOLD );
				partition_node(curr_node,bloom_tree,currTreeNode);
			}
			not_found = 0;
		}
		else
		{
			UINT bin = partition_hash(val,curr_level);
			if(bin==0)
			{
				curr_node = curr_node->left;
				if(curr_level==0)
					currTreeNode = bloom_tree->left;
				else
					currTreeNode = currTreeNode->lchild;
			}
			else
			{
				curr_node = curr_node->right;
				if(curr_level==0)
					currTreeNode = bloom_tree->right;
				else
					currTreeNode = currTreeNode->rchild;
			}


			curr_level++;
		}
	}
}

void delete_node(UINT val, bloom* bl)
{
	UINT i;
	UINT chunk_per_ind = bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK;
	if(!is_in_node(val,bl))
	{
		return;
	}
	if(bl->num_elems>0)
		bl->num_elems--;
	for (i=0;i<bloomParam->K;i++)
	{
		UINT a = hash(val,i);
		UINT loc = a/chunk_per_ind;
		UINT off = a%chunk_per_ind;

		UINT new = 1<<(off*bloomParam->COUNTER_CHUNK);
		UINT bitmap = (bloomParam->BIT_MAP<<(off*bloomParam->COUNTER_CHUNK));
		UINT chunk = bitmap & bl->flag_array[loc];
		if(chunk>0)
		{
			bl->flag_array[loc] = bl->flag_array[loc] - new;
		}
	}
}

/*
*	traverses the tree and deletes at the right spot
*/
void delete(UINT val, b_node * bloomf)
{
	int not_found 		= 1;
	b_node * curr_node  = bloomf;
	UINT curr_level 	= 0;

	while(not_found)
	{
		if(curr_node->left==NULL)				//identified as leaf
		{
			if(is_in_node(val,curr_node->bf))
			{
				delete_node(val, curr_node->bf);
			}
			else
				return;							//element not present in the overall bloom filter
			not_found = 0;
		}
		else
		{
			if(partition_hash(val,curr_level)==0)
				curr_node = curr_node->left;
			else
				curr_node = curr_node->right;
			curr_level++;
		}
	}
}


UINT is_in_node(UINT val, bloom* bl)
{
	UINT i;
	UINT chunk_per_ind = bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK;
	for (i=0;i<bloomParam->K;i++){

		UINT a = hash(val,i);
		UINT loc = a/chunk_per_ind;
		UINT off = a%chunk_per_ind;

		UINT bitmap = (bloomParam->BIT_MAP<<(off*bloomParam->COUNTER_CHUNK));
		UINT chunk = bitmap & bl->flag_array[loc];
		if (!chunk) return 0;
	}
	return 1;
}

/*
*	traverses the tree and checks at the right spot
*/
UINT is_in(UINT val, b_node * bloomf)
{
	int not_found 		= 1;
	b_node * curr_node  = bloomf;
	UINT curr_level 	= 0;

	while(not_found)
	{
		if(curr_node->left==NULL)				//identified as leaf
		{
			if(is_in_node(val,curr_node->bf))
			{
				return 1;
			}
			else
				return 0;							//element not present in the overall bloom filter
			not_found = 0;
		}
		else
		{
			if(partition_hash(val,curr_level)==0)
				curr_node = curr_node->left;
			else
				curr_node = curr_node->right;
			curr_level++;
		}
	}
}

UINT hash(int a, int i)
{
	unsigned long z = aHash[i];
	z = z*a + bHash[i];
	UINT retVal = (z % m[i]);
	return retVal;
}

void free_bloom_node(bloom *bl)
{
	free(bl->flag_array);
	bl->num_elems = 0;
}

/*
* traverse the tree in a dfs fashion and free all memory
*/
void free_bloom(b_node * bloomf)
{
	if(bloomf == NULL)
		return;
	if(bloomf->left==NULL)
	{
		free_bloom_node(bloomf->bf);
		return;
	}
	else
	{
		free_bloom(bloomf->left);
		free_bloom(bloomf->right);
		free(bloomf);
	}
}

UINT * reconstruct_dict_attack(bloom* bl, UINT start, UINT end )
{
	UINT i;
	UINT num_elems,elem = 0;
	for(i = start; i <= end; i++)
	{
		if(is_in_node(i,bl))
			num_elems++;
	}

	UINT * val_array = (UINT *)malloc(sizeof(UINT)*num_elems);
	
	memset(val_array, 0, sizeof(UINT)*num_elems);
	for(i = start; i <= end; i++)
	{
		if(is_in_node(i,bl))
		{
			val_array[elem] = i;
			elem++;
		}
	}
	return val_array;
}

//slightly inefficient as twice time taken for now but for complete reconstruction use this 
void reconstruct_complete(b_node* a,my_array_long * return_array)	
{
	if(a->left==NULL)
	{
		my_array_long* reconstruct_array;
		UINT size_array,i = 0;
		reconstructSet( bloom_tree ,a->bf, &reconstruct_array, &size_array);
		for(i = 0 ; i < size_array ; i++)
		{
			arrayLong_addElement(return_array, reconstruct_array->elements[i]);
		}
		arrayLong_free(reconstruct_array);
	}
	else
	{
		reconstruct_complete(a->left,return_array);
		reconstruct_complete(a->right,return_array);
	}
}

void print_bloom_filter_node(bloom *a)
{
	int i,j;
	for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			printf("%0*d ",2,(a->flag_array[i]&new)>>(j*bloomParam->COUNTER_CHUNK));
		}
	}
	printf("\n");
}

void intersect_bloom_node(bloom *a, bloom *b, bloom *c)
{

	UINT i,j,num,check=0;
	for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			temp += MIN(a->flag_array[i]&new,b->flag_array[i]&new);
		}
		if(temp>0)
			check = 1;
		c->flag_array[i] = temp;
	}
	c->num_elems = count_elems_node(c);
}


void union_bloom_node(bloom *a, bloom *b, bloom *c)
{
	UINT i,j,num=0;
	for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			temp += MAX(a->flag_array[i]&new,b->flag_array[i]&new);
		}
		c->flag_array[i] = temp;
	}
	c->num_elems = count_elems_node(c);
	// return 1;
}


//compresses allof a into c with just one node
void expand_intersect_tree(b_node *a, b_node *b, b_node *c,struct bloomTree *bTree,struct bloomNode *bNode)	//takes b and replicates it in c separately b would definitely have some depth
{
	if(b->left==NULL)
	{
		c->bf = (bloom *)malloc(sizeof(bloom));
		int size = (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);
		init_node(c->bf);
		
		intersect_bloom_node(a->bf, &bNode->filter, c->bf);
		intersect_bloom_node(c->bf,b->bf,c->bf);
	}
	else
	{
		c->left = (b_node *)malloc(sizeof(b_node));
		c->right = (b_node *)malloc(sizeof(b_node));
		init_without_bloom(c->left);
		init_without_bloom(c->right);
		if(bNode==NULL)	//case when a at root , a and b both at root will not happen
		{
			expand_intersect_tree(a,b->left,c->left,bTree,bTree->left);
			expand_intersect_tree(a,b->right,c->right,bTree,bTree->right);
		}
		else
		{
			expand_intersect_tree(a,b->left,c->left,bTree,bNode->lchild);
			expand_intersect_tree(a,b->right,c->right,bTree,bNode->rchild);
		}
	}
}

void intersect_bloom(b_node *a, b_node *b, b_node* c,struct bloomTree *bTree,struct bloomNode *bNode)
{
	if(c==NULL)
		init_without_bloom(c);
	//going recursive
	if(a->left==NULL && b->left==NULL)			//both are leaves and at same level
	{
		if(c->left!=NULL)					//as c may not necessarily be empty , can be possibly a or b even
		{
			free_bloom(c->left);
			free_bloom(c->right);
		}
		if(c->bf==NULL)
			init(c);
		intersect_bloom_node(a->bf, b->bf, c->bf);
	}
	else if(a->left==NULL && b->left!=NULL)
	{
		// printf("calling hoohahh\n");
		expand_intersect_tree(a ,b, c, bTree, bNode);
	}
	else if(a->left!=NULL && b->left==NULL)	//b ends and a continues so a needs to be compressed
	{
		// printf("calling hoohahh\n");
		expand_intersect_tree(b ,a , c, bTree ,bNode);
	}
	else //both are not leaves so initializing both children of c and 
	{
		if(c->left==NULL)
		{
			c->left = (b_node *)malloc(sizeof(b_node));
			init_without_bloom(c->left);
		}
		if(c->right==NULL)
		{
			c->right = (b_node *)malloc(sizeof(b_node));
			init_without_bloom(c->right);
		}
			if(bNode != NULL)
			{
				intersect_bloom(a->left, b->left, c->left, bTree, bNode->lchild);					//recursion continues
				intersect_bloom(a->right, b->right, c->right, bTree, bNode->rchild);
			}
			else // starting from root
			{
				intersect_bloom(a->left, b->left, c->left, bTree, bTree->left);					//recursion continues
				intersect_bloom(a->right, b->right, c->right, bTree, bTree->right);
			}
	}
}


void expand_tree(b_node *a, b_node *b, b_node *c,struct bloomTree *bTree,struct bloomNode *bNode)	//takes b and replicates it in c separately b would definitely have some depth
{
	if(b->left==NULL)
	{
		c->bf = (bloom *)malloc(sizeof(bloom));
		int size = (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);
		init_node(c->bf);
		
		intersect_bloom_node(a->bf, &bNode->filter, c->bf);
		union_bloom_node(c->bf,b->bf,c->bf);
	}
	else
	{
		c->left = (b_node *)malloc(sizeof(b_node));
		c->right = (b_node *)malloc(sizeof(b_node));
		init_without_bloom(c->left);
		init_without_bloom(c->right);
		if(bNode==NULL)	//case when a at root , a and b both at root will not happen
		{
			expand_tree(a,b->left,c->left,bTree,bTree->left);
			expand_tree(a,b->right,c->right,bTree,bTree->right);
		}
		else
		{
			expand_tree(a,b->left,c->left,bTree,bNode->lchild);
			expand_tree(a,b->right,c->right,bTree,bNode->rchild);
		}
	}
}

void union_bloom(b_node *a, b_node *b, b_node *c,struct bloomTree *bTree,struct bloomNode *bNode)
{

		if(a->left==NULL && b->left==NULL)			//both are leaves and at same level
		{
			if(c->left!=NULL)					//as c may not necessarily be empty , can be possibly a or b even
			{
				free_bloom(c->left);
				free_bloom(c->right);
			}
			if(c->bf==NULL)
				init(c);
			union_bloom_node(a->bf, b->bf, c->bf);
		}
		else if(a->left==NULL && b->left!=NULL)
		{

			expand_tree(a,b,c,bTree,bNode);
		}
		else if(a->left!=NULL && b->left==NULL)	//b ends and a continues so a needs to be compressed
		{
			expand_tree(b,a,c,bTree,bNode);
		}
		else //both are not leaves so initializing both children of c and 
		{
			if(c->left==NULL)
			{
				c->left = (b_node *)malloc(sizeof(b_node));
				init_without_bloom(c->left);
			}
			if(c->right==NULL)
			{
				c->right = (b_node *)malloc(sizeof(b_node));
				init_without_bloom(c->right);
			}
			if(bNode != NULL)
			{
				union_bloom(a->left, b->left, c->left, bTree, bNode->lchild);					//recursion continues
				union_bloom(a->right, b->right, c->right, bTree, bNode->rchild);
			}
			else // starting from root
			{
				union_bloom(a->left, b->left, c->left, bTree, bTree->left);					//recursion continues
				union_bloom(a->right, b->right, c->right, bTree, bTree->right);
			}
		}
}

void bloom_test_suit1()
{
	b_node *a = (b_node*)malloc(sizeof(b_node));
	init(a);
	UINT i;
	printf("bloom graph insert/member/delete test initiated\n");
	// partition_node(a);
	for(i = 0 ; i < bloomParam->TEST_RANGE ; i++ )
	{
		UINT z = i;//rand()%bloomParam->TEST_RANGE;
		insert(z,a);
		if(!is_in(z,a))
		{
			printf("bloom graph not functioning properly - inserted val :: %d not found\n",z);
			exit(0);
		}
		// delete(z,a);
	}
	// partition_node(a,bloom_tree,NULL);

	printf("count of elems :: %u  %u \n",count_elems(a),bloomParam->TEST_RANGE );
	free_bloom(a);
	printf("bloom graph insert/member/delete test successful\n");
}

void bloom_test_suit2()
{
	b_node *a = (b_node*)malloc(sizeof(b_node));
	b_node *b = (b_node*)malloc(sizeof(b_node));

	init(a);	
	init(b);

	UINT i;
	printf("bloom graph union/intersect test initiated bloomParam->SIZE_BLOOM :: %u\n",bloomParam->SIZE_BLOOM);
	UINT count_a = 0,count_b = 0;
	for( i = 0 ; i < bloomParam->TEST_RANGE ; i++ )
	{
		if(i%2==0)
		{
			insert(i,a);
			if(!is_in(i,a))
				printf("element not found a %u\n",i);
			count_a++;
		}
		else
		{
			insert(i,b);
			if(!is_in(i,b))
				printf("element not found b  %u\n",i);
			count_b++;
		}
	}
	// partition_node(a,bloom_tree,NULL);
	// partition_node(a,bloom_tree,NULL);

	printf("insertion stage over\n");
	printf("size a :: %u actual:: %u\n",count_elems(a),count_a );	//the number of elements may be less as some counter may completely fill up
	printf("size b :: %u actual:: %u\n",count_elems(b),count_b );	//

	b_node * c = (b_node *)malloc(sizeof(b_node));
	init_without_bloom(c);
	
	b_node * d = (b_node *)malloc(sizeof(b_node));
	init_without_bloom(d);	

	intersect_bloom(a,b,c,bloom_tree,NULL);
	union_bloom(a,b,d,bloom_tree,NULL);



	printf("union/intersection operations executed -- size union size::%u intersect::%u \n",count_elems(d),count_elems(c));
	// printf("intersection count via-- current elemsIntersection::%f \n",elemsIntersection(a,b,num_ones(a),num_ones(b)));
	// exit(0);

	ULONG con_union = 0;
	ULONG con_inter = 0;
	for( i = 0 ; i < bloomParam->TEST_RANGE ; i++ )
	{

		if(!is_in(i,d))
		{
			con_union++;
			// exit(0);
		}
		if(is_in(i,c)==1)
		{
			con_inter++;
		}
	}
	printf("errors_union :: %lu errors_intersect :: %lu \n",con_union,con_inter );

	free_bloom(a);
	free_bloom(b);
	free_bloom(c);
	printf("bloom graph union/intersect test successful\n");
}

UINT count_elems_node(bloom *a)
{
	UINT count = num_ones(a)/bloomParam->K;
	return count;
}

/*
* traverse the tree in a dfs fashion and count all the elements
*/
UINT count_elems(b_node* bloomf)
{
	if(bloomf->left==NULL)
	{
		UINT count = bloomf->bf->num_elems;
		return count;
	}
	else
	{
		UINT count = 0;
		count += count_elems(bloomf->left);
		count += count_elems(bloomf->right);
		return count;
	}
}

UINT num_ones(bloom *a)
{
	UINT i = 0, n = 0;
	for (i = 0 ; i < bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS ; i++)
		n += count_ones(a->flag_array[i]);
	return n;
}

UINT numOnes_intersection_node(bloom *a, bloom *b)
{
	UINT i , j, num = 0, numCounts = 0;

	for (i=0;i<(bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			 UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			 UINT curr = 0;
			if(new<0)
			{
				curr = MIN( (a->flag_array[i]&new)>>bloomParam->COUNTER_CHUNK , (b->flag_array[i]&new)>>bloomParam->COUNTER_CHUNK );
				curr = curr<<bloomParam->COUNTER_CHUNK;
			}
			else
				curr = MIN( (a->flag_array[i]&new) , (b->flag_array[i]&new) );
			temp+=curr;
		}
		num += count_ones(temp);										//can be made efficient
	}
	return num;
}

double elemsIntersection_node(bloom *a, bloom *b, int ta, int tb)
{
	double t1 = (double)ta;
	double t2 = (double)tb;
	double th = (double)numOnes_intersection_node(a,b);
	return th/bloomParam->K;
}

UINT count_ones(UINT x)
{
	UINT i = 0;
	UINT count = 0;
	for(i = 0 ; i < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; i++)
	{
		UINT new = bloomParam->BIT_MAP<<(i*bloomParam->COUNTER_CHUNK);
		UINT intersect = x&new;
		count += intersect>>(i*bloomParam->COUNTER_CHUNK);
	}

	return count;
}

// level starts with zero and hence ends at NUM_PARTITION
void grow_full_tree(b_node* node,int level)
{
	if (level == bloomParam->NUM_PARTITION )
	{
		node->bf = (bloom *)malloc(sizeof(bloom *));
		init_node(node->bf);
	}
	else
	{
		node->left = (b_node*)malloc(sizeof(b_node));
		node->right = (b_node*)malloc(sizeof(b_node));
		init_without_bloom(node->left);
		init_without_bloom(node->right);
		grow_full_tree(node->left,level+1);
		grow_full_tree(node->right,level+1);
	}
}

// Reads data from file and inserts them in the bloom filter
void read_and_insert(char * filename, b_node* filter)
{
	FILE * reader;
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,filename);
	reader = fopen(fullFilename,"r");
	UINT i,num_elems = 0,namespace = 1;
	fscanf(reader,"%d",&num_elems);
	fscanf(reader,"%d",&namespace);
	for(i = 0 ; i < num_elems ; i++)
	{
		int elem = 0;
		fscanf(reader,"%d",&elem);
		// printf("Inserting element::%d\n",elem);
		// insert_new(elem,filter,0);
		insert(elem,filter);
	}
	// finish_insertion(filter);
	fclose(reader);
}

// Estimates memory usage by bloom filter for different set of points
void test_memory_usage(char** filenameList,int numFiles)
{
	FILE* logger ;
	if( access( "../logs/dyn_partition_1_individual.txt", F_OK ) == -1 ) 
	{
    	// file does not exist
    	logger = fopen("../logs/dyn_partition_1_individual.txt","w");
		fprintf(logger, "%s\n","File Name,No. of elements,Namespace,NumHashFunc,Size of BF,CtrSize,FalsePstvProb,Split_Threshold,NumPartitionFunc,Num of BFs,Memory Usage, Ratio of false positives to set size,NULL");

	}
	else
	{
    	logger = fopen("../logs/dyn_partition_1_individual.txt","a");
	}

	int i;
	for (i = 0; i < numFiles; ++i)
	{
		// printf("File::%s\n",filenameList[i] );
		b_node * filter = (b_node*)malloc(sizeof(b_node));
		init(filter);
		// grow_full_tree(filter,0);

		read_and_insert(filenameList[i],filter);
		char fullFilename[1000];
		sprintf(fullFilename,"%s%s",datasetPath,filenameList[i]);
		FILE * reader = fopen(fullFilename,"r");
		int num_elems,namespace;
		fscanf(reader,"%d",&num_elems);
		fscanf(reader,"%d",&namespace);
		fclose(reader);

		fprintf(logger, "%s,",filenameList[i]);
		fprintf(logger, "%d,",num_elems);
		fprintf(logger, "%d,",namespace);
		fprintf(logger, "%d,",bloomParam->K);
		fprintf(logger, "%d,",bloomParam->SIZE_BLOOM/8);
		fprintf(logger, "%d,",bloomParam->COUNTER_CHUNK);
		fprintf(logger, "%.5f,",bloomParam->FALSE_PSTV_PROB);
		fprintf(logger, "%.3f,",bloomParam->FILL_THRESHOLD);
		fprintf(logger, "%d,",bloomParam->NUM_PARTITION);
		int tempCount = count_leaves(filter);
		// printf("Filename::%s, count_elems()::%d, NumLeaves::%d\n",filenameList[i],count_elems(filter),tempCount );
		fprintf(logger, "%d,",tempCount);
		fprintf(logger, "%d,",tempCount*(bloomParam->SIZE_BLOOM)/8);

		estimate_false_positive(filenameList[i],filter,logger);
		fprintf(logger, "NULL\n");
	}
	fclose(logger);
}

// Takes a bloom filter and estimates number of false positives in it.
void estimate_false_positive(char* filename,b_node* filter, FILE* logger)
{
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,filename);
	FILE * reader = fopen(fullFilename,"r");
	int num_elems,namespace;
	fscanf(reader,"%d",&num_elems);
	fscanf(reader,"%d",&namespace);
	fclose(reader);
	int i;
	int numPresent = 0;
	for (i = 1; i <= namespace; ++i)
	{
		if(is_in(i,filter))
		{
			numPresent++;
		}
	}
	// fprintf(logger, "%d,",numPresent - num_elems);
	fprintf(logger, "%.5f,",(numPresent - num_elems)/(float)num_elems);
}

void test_union_intersection(char** filenameList_1,char** filenameList_2,int numFiles)
{
	FILE* logger;
	
	if( access( "../logs/dyn_partition_1_set_ops.txt", F_OK ) == -1 ) 
	{
		logger = fopen("../logs/dyn_partition_1_set_ops.txt","w");
		// fprintf(logger,"%s\n","File A,File B,Namespace,NumHashFunc,Size of unit BF,CtrSize,False_Pstv_Prob,Fill_Threshold,Num PartitionFunc,No. of Elems (A),Num Filters used,Memory Usage, Ratio of false positives to set size,No. of Elems (B),Num Filters used,Memory Usage, Ratio of false positives to set size,No. of Elems (A union B),Num Filters used,Memory Usage, Ratio of false positives to set size,No. of Elems (A intersect B),Num Filters used,Memory Usage, Ratio of false positives to set size,NULL");
		fprintf(logger,"%s\n","File A,File B,Namespace,hash,size,CtrSize,False_Pstv_Prob,fillThresh,numPartition,numA,numBf,memA,fpA,numB,numBf,memB,fpB,No. of Elems (A union B),numBfUnion,memUnion, fpUnion,numIntersect,numBfIntersect ,memIntersect, fpIntersect,NULL");
	
	}
	else
	{
		logger = fopen("../logs/dyn_partition_1_set_ops.txt","a");
	}

	// fprintf(logger,   "%s\n","File A,File B,Union File,Intersection File,Namespace,NumHashFunc,Size of BF,CtrSize,
	// No. of Elems (A),No. of false positives, Ratio of false positives to set size,
	// No. of Elems (B),No. of false positives, Ratio of false positives to set size,
	// No. of Elems (A union B),No. of false positives, Ratio of false positives to set size,
	// No. of Elems (A intersect B),No. of false positives, Ratio of false positives to set size,NULL");

	int i;
	for (i = 0; i < numFiles; ++i)
	{
		char union_filename[1000];
		char intersect_filename[1000];
		sprintf(union_filename		,"%s_%s_union",		filenameList_1[i],filenameList_2[i]);
		sprintf(intersect_filename	,"%s_%s_intersect",	filenameList_1[i],filenameList_2[i]);

		b_node* filter_1 		= (b_node*)malloc(sizeof(b_node));
		b_node* filter_2 		= (b_node*)malloc(sizeof(b_node));
		b_node* filter_union 	= (b_node*)malloc(sizeof(b_node));
		b_node* filter_intersect = (b_node*)malloc(sizeof(b_node));
		init(filter_1);
		init(filter_2);
		init(filter_union);
		init(filter_intersect);
		// grow_full_tree(filter_1,0);
		// grow_full_tree(filter_2,0);
	
		FILE * reader;
		int namespace,tempCount;
		int num_elems_1,num_elems_2, num_elems_union, num_elems_intersect;

		char* fullFilename = (char*)malloc(1000*sizeof(char));
		sprintf(fullFilename,"%s%s",datasetPath,filenameList_1[i]);
		reader = fopen(fullFilename,"r");
		fscanf(reader,"%d",&num_elems_1);
		fscanf(reader,"%d",&namespace);
		fclose(reader);
		free(fullFilename);

		fullFilename = (char*)malloc(1000*sizeof(char));
		sprintf(fullFilename,"%s%s",datasetPath,filenameList_2[i]);
		reader = fopen(fullFilename,"r");
		fscanf(reader,"%d",&num_elems_2);
		fclose(reader);
		free(fullFilename);

		fullFilename = (char*)malloc(1000*sizeof(char));
		sprintf(fullFilename,"%s%s",datasetPath,union_filename);
		reader = fopen(fullFilename,"r");
		fscanf(reader,"%d",&num_elems_union);
		fclose(reader);
		free(fullFilename);

		fullFilename = (char*)malloc(1000*sizeof(char));
		sprintf(fullFilename,"%s%s",datasetPath,intersect_filename);
		reader = fopen(fullFilename,"r");
		fscanf(reader,"%d",&num_elems_intersect);
		fclose(reader);
		free(fullFilename);

		fprintf(logger, "%s,", filenameList_1[i]);
		fprintf(logger, "%s,", filenameList_2[i]);
		// fprintf(logger, "%s_%s_union,", filenameList_1[i],filenameList_2[i]);
		// fprintf(logger, "%s_%s_intersect,", filenameList_1[i],filenameList_2[i]);

		fprintf(logger, "%d,",namespace);
		fprintf(logger, "%d,",bloomParam->K);
		fprintf(logger, "%d,",bloomParam->SIZE_BLOOM/8);
		fprintf(logger, "%d,",bloomParam->COUNTER_CHUNK);
		fprintf(logger, "%.5f,",bloomParam->FALSE_PSTV_PROB);
		fprintf(logger, "%.3f,",bloomParam->FILL_THRESHOLD);
		fprintf(logger, "%d,",bloomParam->NUM_PARTITION);
		
		read_and_insert(filenameList_1[i],filter_1);
		read_and_insert(filenameList_2[i],filter_2);
		union_bloom(filter_1,filter_2,filter_union,bloom_tree,NULL);
		intersect_bloom(filter_1,filter_2,filter_intersect,bloom_tree,NULL);

		// Details for set A
		fprintf(logger, "%d,",num_elems_1 );
		tempCount = count_leaves(filter_1);
		fprintf(logger, "%d,",tempCount);
		fprintf(logger, "%d,",(int)(tempCount*(bloomParam->SIZE_BLOOM)/8));
		estimate_false_positive(filenameList_1[i],filter_1,logger);

		// Details for set B
		fprintf(logger, "%d,",num_elems_2 );
		tempCount = count_leaves(filter_2);
		fprintf(logger, "%d,",tempCount);
		fprintf(logger, "%d,",(int)(tempCount*(bloomParam->SIZE_BLOOM)/8));
		estimate_false_positive(filenameList_2[i],filter_2,logger);
	
		// Details for set (A union B)
		fprintf(logger, "%d,",num_elems_union );
		tempCount = count_leaves(filter_union);
		fprintf(logger, "%d,",tempCount);
		fprintf(logger, "%d,",(int)(tempCount*(bloomParam->SIZE_BLOOM)/8));
		estimate_false_positive(union_filename, filter_union,logger);

		// Details for set (A intersect B)
		fprintf(logger, "%d,",num_elems_intersect );
		tempCount = count_leaves(filter_intersect);
		fprintf(logger, "%d,",tempCount);
		fprintf(logger, "%d,",(int)(tempCount*(bloomParam->SIZE_BLOOM)/8));
		estimate_false_positive(intersect_filename,filter_intersect,logger);

		fprintf(logger, "NULL\n");
	}
	
	fclose(logger);
}

UINT count_leaves(b_node *node)
{
	if(node->left==NULL)
		return 1;
	else
	{
		return count_leaves(node->left)+count_leaves(node->right);
	}
}

void run_memory_tests()
{
	// correct overwrite issues
	// int numFiles = 2;
	int i,j,k;
	int numFiles = 5;
	char** filenameList = (char**)malloc(numFiles*sizeof(char*));
	for (i = 0; i < numFiles; ++i)
	{
		filenameList[i] = (char*)malloc(sizeof(char)*100);
	}
	strcpy(filenameList[0],"rand::10");
	strcpy(filenameList[1],"rand::100");
	strcpy(filenameList[2],"rand::500");
	strcpy(filenameList[3],"rand::1000");
	strcpy(filenameList[4],"rand::2000");

	
	int num_K_Vals 				= 2;
	int num_FalsePstvProbVals 	= 2;
	int num_filterSizeVals 		= 2;

	int K_Vals[2] 				= {3,5};
	float FalsePstvProbVals[2]	= {0.0001,0.01};
	int filterSizeVals[2] 		= {1024,2048};//,2048,4000,10000,20000,40000};

	for (i = 0; i < num_filterSizeVals; ++i)
	{
		for (j = 0; j < num_K_Vals; ++j)
		{
			for (k = 0; k < num_FalsePstvProbVals; ++k)
			{
				printf("Running test for K = %d, FalsePstvProbVals = %.3f, FilterSize = %d\n",K_Vals[j], FalsePstvProbVals[k], filterSizeVals[i]);
				init_bloomParameters(filterSizeVals[i],K_Vals[j],2,10,FalsePstvProbVals[k]); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
				seiveInitial(); // Important to initialize it here to reset m[i] values to suit the changed values of SIZE_BLOOM
				bloom_tree = setupTreeRange(1,10000);
				test_memory_usage(filenameList,numFiles);				
			}
		}
	}
	// init_bloomParameters(10240,3,2); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
	// seiveInitial(); // Important to initialize it here to reset m[i] values to suit the changed values of SIZE_BLOOM

	// test_memory_usage(filenameList,2);
}

void getFilenames(char** filenameList_1,char** filenameList_2,int * numFiles)
{
	filenameList_1 = (char**)malloc(1000*sizeof(char*));
	filenameList_2 = (char**)malloc(1000*sizeof(char*));
	FILE* reader ;
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,"filenameList");
	reader = fopen(fullFilename,"r");
	
	int ctr  = 0;
	while(!feof(reader))
	{
		filenameList_1[ctr] = (char*)malloc(200*sizeof(char));
		filenameList_2[ctr] = (char*)malloc(200*sizeof(char));
		fscanf(reader,"%s %s",filenameList_1[ctr],filenameList_2[ctr]);
		ctr += 1;
	}
	*numFiles = ctr;
	printf("Filenamelist size::%d\n",ctr );
}

void run_set_operation_tests()
{
	int numFiles = 10;
	char** filenameList_1, **filenameList_2;
	filenameList_1 = (char**)malloc(1000*sizeof(char*));
	filenameList_2 = (char**)malloc(1000*sizeof(char*));
	FILE* reader ;
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,"filenameList");
	reader = fopen(fullFilename,"r");
	
	int ctr  = 0;
	while(!feof(reader))
	{
		filenameList_1[ctr] = (char*)malloc(200*sizeof(char));
		filenameList_2[ctr] = (char*)malloc(200*sizeof(char));
		fscanf(reader,"%s %s",filenameList_1[ctr],filenameList_2[ctr]);
		ctr += 1;
	}
	numFiles = ctr;

	int num_K_Vals 				= 1;
	int num_FalsePstvProbVals 	= 1;
	int num_filterSizeVals 		= 4;

	int K_Vals[1] 				= {5};
	float FalsePstvProbVals[1]	= {0.0001};
	int filterSizeVals[4] 		= {512,512*2,512*4,512*8};//,2048,4000,10000,20000,40000};

	int i,j,k;
	for (i = 0; i < num_filterSizeVals; ++i)
	{
		for (j = 0; j < num_K_Vals; ++j)
		{
			for (k = 0; k < num_FalsePstvProbVals; ++k)
			{
				printf("Running test for K = %d, FalsePstvProbVals = %.3f, FilterSize = %d\n",K_Vals[j], FalsePstvProbVals[k], filterSizeVals[i]);
				init_bloomParameters(filterSizeVals[i],K_Vals[j],1,10,FalsePstvProbVals[k]); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
				seiveInitial(); // Important to initialize it here to reset m[i] values to suit the changed values of SIZE_BLOOM
				bloom_tree = setupTreeRange(1,10000);
				test_union_intersection(filenameList_1,filenameList_2,numFiles);
			}
		}
	}

	// init_bloomParameters(10240,3,2,10,0.5); // Sets SIZE_BLOOM, K, and COUNTER_SIZE, NUM_PARTITION_FUNC
	// seiveInitial(); // Important to initialize it here to reset m[i] values to suit the changed values of SIZE_BLOOM

	// test_union_intersection(filenameList,filenameList,2);
}

int main()
{
	datasetPath = (char*)malloc(1000*sizeof(char));
	strcpy(datasetPath,"../datasets/");
	time_t t;
	srand((unsigned) time(&t));	
	// run_memory_tests();
	run_set_operation_tests();
	return 1;
}



// void compress_subtree(b_node *a, b_node *c)
// {
// 	if(a->left!=NULL)		//not a leaf
// 	{
// 		compress_subtree(a->left,c);
// 		compress_subtree(a->right,c);
// 	}
// 	else
// 	{
// 		union_bloom_node(a->bf,c->bf,c->bf);
// 	}
// }

// void intersect_bloom(b_node *a, b_node *b, b_node* c)
// {
// 	if(c==NULL)
// 		init_without_bloom(c);
// 	//going recursive
// 	if(a->left==NULL && b->left==NULL)			//both are leaves and at same level
// 	{
// 		if(c->left!=NULL)					//as c may not necessarily be empty , can be possibly a or b even
// 		{
// 			free_bloom(c->left);
// 			free_bloom(c->right);
// 		}
// 		if(c->bf==NULL)
// 			init(c);
// 		intersect_bloom_node(a->bf, b->bf, c->bf);
// 	}
// 	else if(a->left==NULL && b->left!=NULL)
// 	{
// 		b_node * temp = (b_node *)malloc(sizeof(b_node));
// 		init(temp);
// 		compress_subtree(b,temp);
// 		if(c->left!=NULL )					//as c may not necessarily be empty , can be possibly a or b even
// 		{
// 			free_bloom(c->left);
// 			free_bloom(c->right);
// 		}
// 		if(c->bf==NULL)
// 			init(c);
// 		intersect_bloom_node(a->bf ,temp->bf, c->bf);
// 	}
// 	else if(a->left!=NULL && b->left==NULL)	//b ends and a continues so a needs to be compressed
// 	{
// 		b_node * temp = (b_node *)malloc(sizeof(b_node));
// 		init(temp);
// 		compress_subtree(a,temp);
// 		if(c->left!=NULL)					//as c may not necessarily be empty , can be possibly a or b even
// 		{
// 			free_bloom(c->left);
// 			free_bloom(c->right);
// 		}
// 		if(c->bf==NULL)
// 			init(c);
// 		intersect_bloom_node(temp->bf ,b->bf, c->bf);
// 	}
// 	else //both are not leaves so initializing both children of c and 
// 	{
// 		if(c->left==NULL)
// 		{
// 			c->left = (b_node *)malloc(sizeof(b_node));
// 			init_without_bloom(c->left);
// 		}
// 		if(c->right==NULL)
// 		{
// 			c->right = (b_node *)malloc(sizeof(b_node));
// 			init_without_bloom(c->right);
// 		}
// 		intersect_bloom(a->left, b->left, c->left);					//recursion continues
// 		intersect_bloom(a->right, b->right, c->right);
// 	}
// }