#!/usr/bin/python

import random

def generateRandomSet(filename,numVal,startVal,endVal):

	# First number in the file is size of set generated
	# Second number is size of namespace(actually endPoint of namespace) from which values are generated

	tempDict = {}
	while len(tempDict.keys()) < numVal:
		randVal = random.randint(startVal,endVal);
		tempDict[randVal] = 1;

	tempList = tempDict.keys();
	writer = open(filename,'w');

	writer.write(str(numVal) + '\n');
	writer.write(str(endVal));
	for ele in tempList:
		writer.write('\n'+str(ele));

	writer.close();
	return tempDict;

def createUnion(file1,file2,file_union):
	reader = open(file1,"r");

	tempDict = {}
	ctr = 0;
	for line in reader:
		ctr += 1;
		if ctr == 2:
			namespace_1 = int(line.strip())
		if ctr <=2:  # Skipping first two numbers, one is numElems, other is namespace size
			continue;

		tempDict[int(line.strip())] = 1;

	reader.close();
	reader = open(file2,"r");

	ctr = 0;
	for line in reader:
		ctr += 1;
		if ctr == 2:
			namespace_2 = int(line.strip())
		if ctr <=2:  # Skipping first two numbers, one is numElems, other is namespace size
			continue;

		tempDict[int(line.strip())] = 1;


	if namespace_1 != namespace_2:
		print "Namespaces don't match",namespace_1,namespace_2
		return None

	unionList =  tempDict.keys();
	numElemsInUnion = len(unionList);

	# writer = open(file1+'_'+file2+'_union','w');
	writer = open(file_union,'w');
	writer.write(str(numElemsInUnion)+'\n');
	writer.write(str(namespace_1));

	for item in unionList:
		writer.write('\n'+str(item));

	writer.close();

def createIntersection(file1,file2,file_intersect):

	reader = open(file1,"r");

	tempDict_1 = {}
	ctr = 0;
	for line in reader:
		ctr += 1;
		if ctr == 2:
			namespace_1 = int(line.strip())
		if ctr <=2:  # Skipping first two numbers, one is numElems, other is namespace size
			continue;

		tempDict_1[int(line.strip())] = 1;

	reader.close();
	reader = open(file2,"r");

	tempDict_2 = {}
	ctr = 0;
	for line in reader:
		ctr += 1;
		if ctr == 2:
			namespace_2 = int(line.strip())
		if ctr <=2:  # Skipping first two numbers, one is numElems, other is namespace size
			continue;

		elem = int(line.strip());
		if tempDict_1.has_key(elem):
			tempDict_2[elem] = 1;

	if namespace_1 != namespace_2:
		print "Namespaces don't match",namespace_1,namespace_2
		return None


	intersectionList = tempDict_2.keys()

	writer = open(file_intersect,'w');
	writer.write(str(len(intersectionList))+'\n');
	writer.write(str(namespace_1));

	for item in intersectionList:
		writer.write('\n'+str(item));

# Size of origFilename should be less than equal to num_val
def generateOverlappingSets(origFilename,newFilename,numVal,overlapFrac,startVal,endVal):

	# newFilename logic = origFilename + :: + newFileSize + overlapFraction
	reader 		= open(origFilename,'r');
	writer 		= open(newFilename,'w')
	ctr  = 0;
	origDict = {}
	for line in reader:
		ctr += 1;
		if ctr == 1:
			origSize 	= int(line.strip());
		if ctr <= 2:
			continue;

		origDict[int(line.strip())] = 1;

	origValList = origDict.keys();
	origValList = origValList[0:int(origSize*overlapFrac)]  # Keep only the required fraction of original elements

	newDict = {};
	numNewVal = numVal - int(origSize*overlapFrac);
	while len(newDict.keys()) < numNewVal:
		randVal = random.randint(startVal,endVal);
		if not origDict.has_key(randVal):
			newDict[randVal] = 1;

	newValList = newDict.keys();
	combinedList = origValList + newValList;  # Can shuffle the list here if needed to

	writer.write(str(len(combinedList))+'\n');
	writer.write(str(endVal));
	for val in combinedList:
		writer.write('\n'+str(val));

def generateSets():

	setSizes 	= [10,100,200,500,1000,2000];
	method ="rand"  # for random generation method for sets
	startVal 	= 1;
	endVal		= 100000;
	numSamples 	= 20;
	ctr = 0;
	for size_1 in setSizes:
		ctr += 1;
		for sampleIter in xrange(0,numSamples):		
			origFilename = method + '::'+str(size_1) + '_'+ str(sampleIter);
			generateRandomSet(origFilename,size_1,startVal,endVal);
			print origFilename
	
# Generate sets for testing union and intersection functions by varying set overlap
def generateSets_2():
	setSizes 	= [10,100,200,500,1000,2000];
	# setSizes 	= [10];
	overlapFractions = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9];
	method ="rand"  # for random generation method for sets
	startVal 	= 1;
	endVal		= 100000;
	numSamples 	= 10;
	ctrA 		= 0;
	fileDiction = {}
	for size_A in setSizes:
		ctrA += 1;
		for size_B in setSizes[ctrA-1:]:
			for overlap in overlapFractions:
				for sample_iter  in range(0,numSamples):
					file_A 			= "_".join([method,str(size_A),str(size_B),'A',str(overlap),str(sample_iter)])
					file_B 			= "_".join([method,str(size_A),str(size_B),'B',str(overlap),str(sample_iter)])
					file_intersect 	= "_".join([method,str(size_A),str(size_B),'inter',str(overlap),str(sample_iter)])
					file_union 		= "_".join([method,str(size_A),str(size_B),'union',str(overlap),str(sample_iter)])
					
					# print file_A
					# print file_B
					# print file_intersect
					# print file_union
					
					
					generateRandomSet(file_A,size_A,startVal,endVal); # Generate set A
					generateOverlappingSets(file_A,file_B,size_B,overlap,startVal,endVal); # Generate set B
					createIntersection(file_A,file_B,file_intersect); # Generate intersection of A and B
					createUnion(file_A,file_B,file_union);  # Generate union of A and b


def test():
	generateRandomSet('randomNum_1',1,1000,500);
	generateRandomSet('randomNum_2',1,1000,500);
	createUnion('randomNum_1','randomNum_2');
	createIntersection('randomNum_1','randomNum_2');



if __name__ == '__main__':

	# generateSets()
	generateSets_2();