#include <stdio.h>
#include <stdlib.h>
#include "adj_list.h"
#include <assert.h>

//format for the graph for reading
//first line "n v" -- number of edges and vertices
//followed by edges in "v1 v2" manner
//assuming node numbering starts at 0

struct graph_node* graph;
int max_id;

int search_max(char *filename)
{
	FILE *fp = fopen(filename,"r");
	int n,v,max;
	max = 0;
	fscanf(fp,"%d %d\n",&n,&v);
	while(n--)
	{
		int node1,node2;
		fscanf(fp,"%d %d\n",&node1,&node2);
		if(node1>max)
			max = node1;
		if(node2>max)
			max = node2;
	}
	return max+1;
}

graph_node* init_dir(char *filename)
{
	FILE *fp = fopen(filename,"r");
	int n,v,i,j;
	fscanf(fp,"%d %d\n",&n,&v);
	max_id = v;
	graph_node* graph = (struct graph_node *)malloc(v*sizeof(struct graph_node));
	for( i = 0 ; i < v ; i++)
	{
		graph[i].id 	= i;
		graph[i].size 	= 0;
		graph[i].ngbrs 	= (my_array*)malloc(sizeof(my_array));
		arrayInt_init(graph[i].ngbrs);
	}

	//assumption graph is undirected and only once the edge is present
	while(n--)
	{
		int node1,node2;
		fscanf(fp,"%d %d",&node1,&node2);

		if(node1==node2)
		{
			arrayInt_addElement(graph[node1].ngbrs,node2);
			graph[node1].size++;
		}
		else
		{
			arrayInt_addElement(graph[node1].ngbrs,node2);
			arrayInt_addElement(graph[node2].ngbrs,node1);
			graph[node1].size++;
			graph[node2].size++;
		}
	}
	fclose(fp);
	return graph;
}
