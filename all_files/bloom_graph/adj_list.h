#ifndef ADJ_LIST
#define ADJ_LIST

#include <stdio.h>
#include <stdlib.h>
#include "array.h"

struct graph_node{
	int id;
	int size;
	my_array *ngbrs;
};

typedef struct graph_node graph_node;

extern int max_id;

int search_max(char *filename);
graph_node* init_dir(char *filename);
#endif