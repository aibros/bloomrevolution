#include "dynamic_bloom_graph.h"

float BOUND_FILL_BLOOM  = 0.5;
int BLOOM_EDGE_CAPACITY = 8;



bloom_graph* createBloomGraphFromFile(char* filename)
{

	// Initialise graph data structure
	FILE *fp = fopen(filename,"r");
	UINT num_nodes,num_edges;
	fscanf(fp,"%u %u\n",&num_edges,&num_nodes);

	bloom_graph* bGraph 	= (bloom_graph*)malloc(sizeof(bloom_graph));	
	bGraph->node_list 	= (bloom_node*)malloc(num_nodes*sizeof(bloom_node));
	bGraph->graph_size 	= num_nodes;
	bGraph->graph_volume = num_edges;
	int i;
	for (i = 1; i < num_nodes; ++i)
	{
		bGraph->node_list[i].num_ngbrs 	= 0;
		bGraph->node_list[i].ngbrs = (b_node*)malloc(sizeof(b_node));
		init(bGraph->node_list[i].ngbrs);
	}
	printf("createBloomGraph()::Initialised bGraph data structure and now going to create graph\n");
	
	for (i = 1; i < num_nodes; ++i)
	{
		grow_full_tree(bGraph->node_list[i].ngbrs,0);
	}
	// Read graph from file and create a graph
	UINT node1,node2;
	int maxNodeId = 0;
	while(!feof(fp))
	{		
		fscanf(fp,"%u %u\n",&node1,&node2);
		bGraph->node_list[node1].num_ngbrs 	+= 1;
		bGraph->node_list[node2].num_ngbrs 	+= 1;
		insert_new(node2,bGraph->node_list[node1].ngbrs,0);
		insert_new(node1,bGraph->node_list[node2].ngbrs,0);
		if(node1 > maxNodeId)
		{
			maxNodeId =  node1;
		}
		if(node2 > maxNodeId)
		{
			maxNodeId =  node2;
		}
	}
	printf("maxNodeId::%d\n",maxNodeId);
	int totalNumLeaves = 0;
	for (i = 1; i < num_nodes; ++i)
	{
		finish_insertion(bGraph->node_list[node1].ngbrs);
		int numLeaves  = count_leaves(bGraph->node_list[i].ngbrs);
		totalNumLeaves += numLeaves;
	}
	printf("createBloomGraph()::Successfully created graph\n");
	printf("createBloomGraph()::Size of bloom_graph(numLeaves)::%d\n",totalNumLeaves );
	return bGraph;
}

bloom_graph* createBloomGraph(graph_node* graph,char* filename)
{

	// Initialise graph data structure
	FILE *fp = fopen(filename,"r");
	UINT num_nodes,num_edges;
	fscanf(fp,"%u %u\n",&num_edges,&num_nodes);

	bloom_graph* bGraph 	= (bloom_graph*)malloc(sizeof(bloom_graph));	
	bGraph->node_list 		= (bloom_node*)malloc(num_nodes*sizeof(bloom_node));
	bGraph->graph_size 		= num_nodes;
	bGraph->graph_volume 	= num_edges;
	fclose(fp);
	int i;
	int totalNumLeaves = 0;
	int totalNumNgbrs = 0;
	// for (i = 1; i < num_nodes; ++i)
	for (i = 1; i < 20; ++i)
	{
		bGraph->node_list[i].num_ngbrs 	= 0;
		bGraph->node_list[i].ngbrs = (b_node*)malloc(sizeof(b_node));
		init(bGraph->node_list[i].ngbrs);
		grow_full_tree(bGraph->node_list[i].ngbrs,0);
		int j;
		for (j = 0; j < graph[i].size; ++j)
		{
			int node = graph[i].ngbrs->elements[j];
			insert_new(node,bGraph->node_list[i].ngbrs,0);
		}
		finish_insertion(bGraph->node_list[i].ngbrs);
		int numLeaves  = count_leaves(bGraph->node_list[i].ngbrs);
		totalNumLeaves += numLeaves;
		totalNumNgbrs  += graph[i].size; 
		printf("Node::%d\tNgbrs::%d\tTotalNgbrs::%d\tnumLeaves::%d\tTotalLeaves::%d\n",i,graph[i].size,totalNumNgbrs,numLeaves,totalNumLeaves );

		arrayInt_free(graph[i].ngbrs);
		free(graph[i].ngbrs);

	}
	free(graph);
	printf("createBloomGraph()::Initialised bGraph data structure and now going to create graph\n");
	printf("createBloomGraph()::Successfully created graph\n");
	printf("createBloomGraph()::Size of bloom_graph(numLeaves)::%d\n",totalNumLeaves );
	return bGraph;
}

int main(int argc, char const *argv[])
{
	UINT SIZE_BLOOM 	= 1024;
	UINT K 				= 3;
	UINT COUNTER_SIZE 	= 1;
	// int M 				= 3072441;
	int M 				= 3072500;
	float FALSE_PSTV_PROB = 0.01;

	UINT FILL_THRESHOLD  = -1*((int)(SIZE_BLOOM)/((float)K*COUNTER_SIZE))*log( 1 - pow(FALSE_PSTV_PROB,1.0/K));
	UINT NUM_PARTITION   = (UINT)(log2(ceil(M*1.0/FILL_THRESHOLD)));

	printf("SIZE_BLOOM::%u\tK::%u\tCOUNTER_SIZE::%u\n",SIZE_BLOOM,K,COUNTER_SIZE);
	
	printf("FALSE_PSTV_PROB::%f\n",FALSE_PSTV_PROB );
	printf("NUM_PARTITION::%u\n",NUM_PARTITION );
	init_bloomParameters(SIZE_BLOOM,K,COUNTER_SIZE,NUM_PARTITION,FALSE_PSTV_PROB);
	seiveInitial();

	// graph_node* graph = init_dir("out.orkut-links");
	// printf("Gotten adjacency list based graph\n");
	// bloom_graph* bGraph  = createBloomGraph(graph,"out.orkut-links");
	// while(1)
	// {
	// }
	int ctr = 0;
	while(ctr < 600000000)
		ctr++;
	printf("Starting\n");
	b_node * a = (b_node *)malloc(sizeof(b_node));
	// init_without_bloom(a);
	grow_full_tree(a,0);
	int i;
	for(i = 0; i < 200 ; i++ )
		insert_new(i,a,0);
	finish_insertion(a);
	return 0;
}