#ifndef _BLOOM_TREE_H
#define _BLOOM_TREE_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <pthread.h>
#include <string.h>
#include <omp.h>

#include "count_bloom.h"
#include "array.h"


#define NUMTHREADS 48
#define QSIZE 100000

extern UINT THRESH;
extern ULONG leafID;
extern UINT levelThreshold;
extern UINT nMembership;  // Not used in our code, used in original bloom sample


// UINT overlapThreshold;


// ULONG totalRange = 0;
// omp_lock_t m_totalRange;

// UINT k_ones_query;	//Number of 1s in the query bloom filter

// FILE *foutput;				
// omp_lock_t m_output;

// my_array_long **output;
// ULONG numLeaves;
// UINT *size_output;


// ULONG nVertices;

// bloom query;

struct task{
	struct bloomNode *root;
	struct task *next;
};

// struct bloomNode *taskQueue[QSIZE];
// pthread_mutex_t m_taskQueue;
// pthread_cond_t c_overflow = PTHREAD_COND_INITIALIZER;


// int rear = -1;	//rear == front indicates empty queue
// int front = -1;



struct bloomNode{
	bloom filter;
	ULONG start;		/*starting vertex for this node*/
	ULONG end;		/*ending vertex for this node*/
	ULONG min_start; /*minimum valued vertex inserted in the node*/
	ULONG max_end;   /*maximum valued vertex inserted in the node*/
	struct bloomNode *lchild;
	struct bloomNode *rchild;
	ULONG leafID;
    double prevEstimate;
	ULONG nOnes;
	ULONG level;
};


struct bloomTree{		/*The root level*/
	struct bloomNode* left;
	struct bloomNode* right;
};

extern bloomParameters* bloomParam;
extern struct bloomTree* bloom_tree;


//----------------------------------------------------------------------------------------//

struct bloomNode *extractTask();

void addTask(struct bloomNode *t);

void searchLeaf(struct bloomNode *t, double elemsIntersection);

void descendLevels(struct bloomNode *root, UINT numLevels, UINT currLevel, double estimate);

void *executeTask(void *x);

//----------------------------------------------------------------------------------------//

struct bloomTree *getBloomTree();

struct bloomNode *createBloomTree(UINT level);

struct bloomTree* setupTree(char* data_file_name);

struct bloomTree* setupTreeRange(ULONG start, ULONG end);

void insert_bloom_node(struct bloomNode* node, ULONG elem, UINT level);

void insert_bloom_tree( struct bloomTree* tree, ULONG elem);

void update_nones(struct bloomNode *tree);

ULONG intersectNode(struct bloomNode *r,bloom *k);

//----------------------------------------------------------------------------------------//

void reconstructSet(struct bloomTree* a, bloom* arg_query , my_array_long** return_array, UINT* return_array_size);

void init_reconstruct(UINT arg_levelThreshold, UINT  arg_overlapThreshold);

//----------------------------------------------------------------------------------------//

void printNode(struct bloomNode* n);
void printTree(struct bloomTree* a);
void print_bloom_filter(bloom *a);


#endif