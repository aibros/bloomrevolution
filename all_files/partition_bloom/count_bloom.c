#include "count_bloom.h"

/*
two level bloom filter
MAX_RANGE = larger range over which small chunks built
bloomParam->SIZE_BLOOM = bloom filter for the small chunk
HASH_ONE = the hash function for first designating the chunks
then each function is modified preferably locality sensitive hashing
*/

int aHash[10]={3730, 9830, 3830, 2110, 4740, 9440, 5540, 2710, 690, 7470};
int bHash[10]={269, 229, 929, 949, 624, 390, 64, 67, 945, 253};
int m[10];

int mPrime[10];
int counter = 0;
int num_bloom = 0;

bloomParameters* bloomParam;
char* datasetPath;

void init_bloomParameters(UINT SIZE_BLOOM ,UINT K ,UINT COUNTER_CHUNK, UINT NUM_FILTERS)
{
	bloomParam = (bloomParameters*)malloc(sizeof(bloomParameters));
	bloomParam->NUM_BITS     	= 8*sizeof(UINT);
	bloomParam->SIZE_BLOOM   	= SIZE_BLOOM;
	bloomParam->K 			  	= K;
	bloomParam->COUNTER_CHUNK  	= COUNTER_CHUNK;		//should be exponents of 2	
	bloomParam->BIT_MAP 		= pow(2,COUNTER_CHUNK) -1 ;	//2^COUNTER_CHUNK - 1;

	bloomParam->NUM_FILTERS 	= NUM_FILTERS;
	bloomParam->EFFECTIVE_RANGE = NUM_FILTERS*SIZE_BLOOM;

	bloomParam->TEST_RANGE 		= 500;
}

void seiveInitial()
{
	int num_bits = bloomParam->SIZE_BLOOM/bloomParam->COUNTER_CHUNK;
	int *x = (int*)malloc(sizeof(int)*num_bits),i,j;
	
	// for (i = 0; i < 10; ++i)
	// {
	// 	aHash[i] = rand() % num_bits;
	// 	bHash[i] = rand() % num_bits;
	// }

	for (i=0;i<num_bits;i++) 
		x[i] = i+1;
	for (i=1;i<num_bits;i++)
	{
		j = x[i];
		if (j==-1) 
			continue;

		int q = 1;
		while ((i + q*j) < num_bits)
		{
			x[i+q*j] = -1;
			q++;
		}
	}
	i = num_bits;
	for (j=0;j<10;j++)
	{
		for (i=i-1;i>0;i--)
		{
			if (x[i]!=-1)
			{
				m[j] = x[i];
				break;
			}
		}
	}
	seivePrimeInitial();
}

void seivePrimeInitial()
{
	int num_bits = bloomParam->EFFECTIVE_RANGE/4;
	int *x = (int*)malloc(sizeof(int)*num_bits),i,j;
	
	for (i=0;i<num_bits;i++) 
		x[i] = i+1;
	for (i=1;i<num_bits;i++)
	{
		j = x[i];
		if (j==-1) 
			continue;

		int q = 1;
		while ((i + q*j) < num_bits)
		{
			x[i+q*j] = -1;
			q++;
		}
	}
	i = num_bits;
	for (j=0;j<10;j++)
	{
		for (i=i-1;i>0;i--)
		{
			if (x[i]!=-1)
			{
				mPrime[j] = x[i];
				break;
			}
		}
	}
}

UINT* init_chunk()
{
	int size = (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);
	UINT *bl = (UINT *)malloc(sizeof(UINT)*size);
	memset(bl,0,sizeof(UINT)*size);
	counter++;
	return bl;
}

void init(bloom *bl)
{
	num_bloom++;
	bl->flag_array = (UINT **)malloc(sizeof(UINT *)*bloomParam->NUM_FILTERS);
}

void insert(UINT val, bloom* bl)
{
	UINT id = hash_prime(val);

	if(bl->flag_array[id]==NULL)
	{
		bl->flag_array[id] = init_chunk();
	}

	UINT i;
	UINT chunk_per_ind = bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK;
	for (i=0;i<bloomParam->K;i++)
	{
		UINT a = hash(val,i);
		UINT loc = a/chunk_per_ind;
		UINT off = a%chunk_per_ind;

		UINT new = 1<<(off*bloomParam->COUNTER_CHUNK);
		UINT bitmap = (bloomParam->BIT_MAP<<(off*bloomParam->COUNTER_CHUNK));
		UINT chunk = bitmap & bl->flag_array[id][loc];
		if(chunk==bitmap)
		{
			// printf("overflow of the bucket for counter -- making no change\n");
			continue;
		}
		bl->flag_array[id][loc] = bl->flag_array[id][loc] + new;
	}
}

void delete(UINT val, bloom* bl)
{
	UINT id = hash_prime(val);
	if(bl->flag_array[id]==NULL)
		return;

	UINT i;
	UINT chunk_per_ind = bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK;
	if(!is_in(val,bl))
	{
		return;
	}
	for (i=0;i<bloomParam->K;i++)
	{
		UINT a = hash(val,i);
		UINT loc = a/chunk_per_ind;
		UINT off = a%chunk_per_ind;

		UINT new = 1<<(off*bloomParam->COUNTER_CHUNK);
		UINT bitmap = (bloomParam->BIT_MAP<<(off*bloomParam->COUNTER_CHUNK));
		UINT chunk = bitmap & bl->flag_array[id][loc];
		if(chunk>0)
		{
			bl->flag_array[id][loc] = bl->flag_array[id][loc] - new;
		}
	}
}


UINT is_in(UINT val, bloom* bl)
{
	UINT id = hash_prime(val);
	if(bl->flag_array[id]==NULL)
		return 0;

	UINT i;
	UINT chunk_per_ind = bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK;
	for (i=0;i<bloomParam->K;i++){

		UINT a = hash(val,i);
		UINT loc = a/chunk_per_ind;
		UINT off = a%chunk_per_ind;

		UINT bitmap = (bloomParam->BIT_MAP<<(off*bloomParam->COUNTER_CHUNK));
		UINT chunk = bitmap & bl->flag_array[id][loc];
		if (!chunk) return 0;
	}
	return 1;
}

UINT hash(int a, int i)
{
	unsigned long z = aHash[i];
	z = z*a + bHash[i];
	UINT retVal = (z % m[i]);
	return retVal;
}

UINT hash_prime(int a)
{
	int i = 0;
	// unsigned long z = aHash[i];
	// z = z*a + bHash[i];
	UINT temp = bloomParam->SIZE_BLOOM/4;
	UINT retVal = (a % mPrime[i]);
	UINT bin = retVal / temp;
	return bin;
}

void free_bloom(bloom *bl)
{
	int i = 0;
	for( i = 0 ; i < bloomParam->NUM_FILTERS ; i++ )
	{
		// printf("checkin memory -- %d\n",i );
		if(bl->flag_array[i] != NULL)
		{
			// printf("freeing memory -- %d\n",i );

			free(bl->flag_array[i]);
		}
	}
	free(bl->flag_array);
}

UINT * reconstruct_bloom(bloom* bl, UINT start, UINT end )
{
	UINT i;
	UINT num_elems,elem = 0;
	for(i = start; i <= end; i++)
	{
		if(is_in(i,bl))
			num_elems++;
	}

	UINT * val_array = (UINT *)malloc(sizeof(UINT)*num_elems);
	
	memset(val_array, 0, sizeof(UINT)*num_elems);
	for(i = start; i <= end; i++)
	{
		if(is_in(i,bl))
		{
			val_array[elem] = i;
			elem++;
		}
	}
	return val_array;
}

void intersect_bloom(bloom *a, bloom *b, bloom *c)
{
	int i,ret = 0;
	for(i = 0; i < bloomParam->NUM_FILTERS ; i++)
	{
		if(a->flag_array[i]==NULL || b->flag_array[i]==NULL)
			continue;
		if(c->flag_array[i]==NULL)
		{
			c->flag_array[i] = init_chunk();
			// if(i==0 || i==1)
			// 	printf("c init %d %d\n",c->flag_array[i],i );
		}
		ret = intersect_chunk(a->flag_array[i],b->flag_array[i],c->flag_array[i]);
		if(ret==0)
		{
			free(c->flag_array[i]);
			c->flag_array[i] = NULL;
		}
	}
	// printf("c later %d %d\n",c->flag_array[0],0 );
	// printf("c later %d %d\n",c->flag_array[1],1 );
}

int intersect_chunk(UINT *a, UINT *b, UINT *c)
{

	UINT i,j,num,check=0;
	for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			temp += MIN(a[i]&new,b[i]&new);
		}
		if(temp>0)
			check = 1;
		c[i] = temp;
	}
	if(check==0)
		return 0;			//freeing memory if no intersection at all
	return 1;
}

void subtract_bloom(bloom *a, bloom *b, bloom *c)
{
	int i,j,ret = 0;
	for(i = 0; i < bloomParam->NUM_FILTERS ; i++)
	{
		if(a->flag_array[i]==NULL)
			continue;
		if(c->flag_array[i]==NULL)
			c->flag_array[i] = init_chunk();

		if(b->flag_array[i]==NULL)
		{
			for(j = 0;j < (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS); j++)
				c->flag_array[i][j] = a->flag_array[i][j]; 
		}

		ret = subtract_chunk(a->flag_array[i],b->flag_array[i],c->flag_array[i]);
		if(ret==0)
		{
			free(c->flag_array[i]);
			c->flag_array[i] = NULL;
		}
	}
}

int subtract_chunk(UINT *a, UINT *b, UINT *c)
{

	UINT i,j,check,num=0;
	for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			if ( (a[i]&new) >= (b[i]&new) )
				temp += (a[i]&new)-(b[i]&new);
		}
		if(temp!=0)
			check = 1;
		c[i] = temp;
	}
	if(check==0)
		return 0;
	return 1;
}

void add_bloom(bloom *a, bloom *b, bloom *c)
{
	int i,j = 0;
	for(i = 0; i < bloomParam->NUM_FILTERS ; i++)
	{
		if(a->flag_array[i]==NULL && b->flag_array[i]==NULL)
			continue;

		if(c->flag_array[i]==NULL)
		{
			c->flag_array[i] = init_chunk();
		}

		if(b->flag_array[i]==NULL)
		{
			for(j = 0;j < (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS); j++)
				c->flag_array[i][j] = a->flag_array[i][j]; 
		}

		if(a->flag_array[i]==NULL)
		{
			for(j = 0;j < (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS); j++)
				c->flag_array[i][j] = b->flag_array[i][j]; 
		}
		add_chunk(a->flag_array[i],b->flag_array[i],c->flag_array[i]);
	}
}

int add_chunk(UINT *a, UINT *b, UINT *c)
{
	UINT i,j,num=0;
	for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			UINT curr = (a[i]&new)+(b[i]&new);
			if(curr > new && j!=(bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK-1))
			{
				curr = new;
			}
			else if(j==(bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK-1))
			{
				curr = (a[i]&new)>>2+(b[i]&new)>>2;
				if(curr > (new>>2))
					curr = new;
				else
					curr = curr<<2;
			}
			temp+=curr;
		}
		c[i] = temp;
	}
	return 1;
}

void union_bloom(bloom *a, bloom *b, bloom *c)
{
	int i,j = 0;
	for(i = 0; i < bloomParam->NUM_FILTERS ; i++)
	{
		if(a->flag_array[i]==NULL && b->flag_array[i]==NULL)
			continue;

		if(c->flag_array[i]==NULL)
			c->flag_array[i] = init_chunk();

		if(b->flag_array[i]==NULL)
		{
			for(j = 0;j < (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS); j++)
				c->flag_array[i][j] = a->flag_array[i][j]; 
		}

		if(a->flag_array[i]==NULL)
		{
			for(j = 0;j < (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS); j++)
				c->flag_array[i][j] = b->flag_array[i][j]; 
		}
		union_chunk(a->flag_array[i],b->flag_array[i],c->flag_array[i]);
	}
}

int union_chunk(UINT *a, UINT *b, UINT *c)
{
	UINT i,j,num=0;
	for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			temp += MAX(a[i]&new,b[i]&new);
		}
		c[i] = temp;
	}
	return 1;
}

void bloom_test_suit1()
{
	bloom *a = (bloom*)malloc(sizeof(bloom));
	init(a);
	UINT i;
	printf("bloom graph insert/member/delete test initiated\n");
	for(i = 0 ; i < bloomParam->TEST_RANGE ; i++ )
	{
		UINT z = rand()%bloomParam->TEST_RANGE;
		insert(z,a);
		if(!is_in(z,a))
		{
			printf("bloom graph not functioning properly - inserted val :: %d not found\n",z);
			exit(0);
		}
		delete(z,a);
	}

	printf("count of elems :: %u  %u \n",count_elems(a),bloomParam->TEST_RANGE );
	free_bloom(a);
	free(a);
	printf("bloom graph insert/member/delete test successful\n");
}

void bloom_test_suit2()
{
	bloom *a = (bloom*)malloc(sizeof(bloom));
	bloom *b = (bloom*)malloc(sizeof(bloom));

	init(a);	
	init(b);

	UINT i;
	printf("bloom graph union/intersect test initiated bloomParam->SIZE_BLOOM :: %u\n",bloomParam->SIZE_BLOOM);
	UINT count_a = 0,count_b = 0;
	for( i = 0 ; i < bloomParam->TEST_RANGE ; i++ )
	{
		if(i%2==0)
		{
			insert(i,a);
			if(!is_in(i,a))
				printf("element not found a %u\n",i);
			// if(is_in(i,b))
			// 	printf("incorrect element found in b %u\n",i);
			count_a++;
		}
		else
		{
			insert(i,b);
			if(!is_in(i,b))
				printf("element not found b  %u\n",i);
			// if(is_in(i,a))
			// 	printf("incorrect element found in a  %u\n",i);
			count_b++;
		}
	}
	printf("insertion stage over\n");
	printf("size a :: %u actual:: %u\n",count_elems(a),count_a );	//the number of elements may be less as some counter may completely fill up
	printf("size b :: %u actual:: %u\n",count_elems(b),count_b );	//

	bloom * c = (bloom *)malloc(sizeof(bloom));
	init(c);
	intersect_bloom(a,b,c);
	
	bloom * d = (bloom *)malloc(sizeof(bloom));
	init(d);	
	union_bloom(a,b,d);

	bloom * e = (bloom *)malloc(sizeof(bloom));
	init(e);
	add_bloom(a,b,e);



	printf("union/intersection operations executed -- size union size::%u intersect::%u addition::%u\n",count_elems(d),count_elems(c),count_elems(e));
	printf("intersection count via-- current elemsIntersection::%f \n",elemsIntersection(a,b,num_ones(a),num_ones(b)));
	// exit(0);
	// if(is_in(bloomParam->TEST_RANGE*2,a)==1)
	// {
	// 	printf("insertion conflict found - %u\n",bloomParam->TEST_RANGE*2);
	// 	exit(0);
	// }
	ULONG con_union = 0;
	ULONG con_inter = 0;
	for( i = 0 ; i < bloomParam->TEST_RANGE ; i++ )
	{

		if(!is_in(i,d))
		{
			// printf("union not working correctly\n");
			con_union++;
			// exit(0);
		}
		if(is_in(i,c)==1)
		{
			// printf("intersect conflict found - %d\n",i);
			con_inter++;
		}
	}
	printf("errors_union :: %lu errors_intersect :: %lu \n",con_union,con_inter );

	free_bloom(a);
	free_bloom(b);
	free_bloom(c);
	free_bloom(e);
	free(a);
	free(b);
	free(c);
	free(e);
	printf("bloom graph union/intersect test successful\n");
}

UINT count_elems(bloom *a)
{
	UINT count = num_ones(a)/bloomParam->K;
	return count;
}

UINT num_ones(bloom *bl)
{
	UINT i,n = 0;
	for(i = 0 ; i < bloomParam->NUM_FILTERS ; i++)
	{
		if(bl->flag_array[i]!=NULL)
			n+=num_ones_chunk(bl->flag_array[i]);
	}
	return n;
}

UINT num_ones_chunk(UINT *a)
{
	UINT i = 0, n = 0;
	for (i = 0 ; i < bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS ; i++)
		n += count_ones(a[i]);
	return n;
}

UINT numOnes_intersection(bloom *a, bloom *b)
{
	UINT i,n=0;
	for(i = 0; i < bloomParam->NUM_FILTERS ; i++)
	{
		if(a->flag_array[i]==NULL || b->flag_array[i]==NULL)
			continue;
		n+=numOnes_intersection_chunk(a->flag_array[i],b->flag_array[i]);
	}
	return n;
}

UINT numOnes_intersection_chunk(UINT *a, UINT *b)
{
	UINT i , j, num = 0, numCounts = 0;

	for (i=0;i<(bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			 UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			 UINT curr = 0;
			if(new<0)
			{
				curr = MIN( (a[i]&new)>>bloomParam->COUNTER_CHUNK , (b[i]&new)>>bloomParam->COUNTER_CHUNK );
				curr = curr<<bloomParam->COUNTER_CHUNK;
			}
			else
				curr = MIN( (a[i]&new) , (b[i]&new) );
			temp+=curr;

		}
		num += count_ones(temp);										//can be made efficient
	}
	return num;
}

double elemsIntersection(bloom *a, bloom *b, int ta, int tb)
{
	double t1 = (double)ta;
	double t2 = (double)tb;
	double th = (double)numOnes_intersection(a,b);
	return th/bloomParam->K;
}

UINT count_ones(UINT x)
{
	UINT i = 0;
	UINT count = 0;
	for(i = 0 ; i < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; i++)
	{
		UINT new = bloomParam->BIT_MAP<<(i*bloomParam->COUNTER_CHUNK);
		UINT intersect = x&new;
		count += intersect>>(i*bloomParam->COUNTER_CHUNK);
	}

	return count;
}

// Reads data from file and inserts them in the bloom filter
void read_and_insert(char * filename, bloom* filter)
{
	FILE * reader;
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,filename);
	reader = fopen(fullFilename,"r");
	UINT i,num_elems = 0,namespace = 1;
	fscanf(reader,"%d",&num_elems);
	fscanf(reader,"%d",&namespace);
	for(i = 0 ; i < num_elems ; i++)
	{
		int elem = 0;
		fscanf(reader,"%d",&elem);
		// printf("Inserting element::%d\n",elem);
		insert(elem,filter);
	}
	fclose(reader);
}

// Estimates memory usage by bloom filter for different set of points
void test_memory_usage(char** filenameList,int numFiles)
{
	FILE* logger ;
	if( access( "../logs/static_partition_2_individual.txt", F_OK ) == -1 ) 
	{
    	// file does not exist
    	logger = fopen("../logs/static_partition_individual.txt","w");
		fprintf(logger, "%s\n","File Name,No. of elements,Namespace,NumHashFunc,Size of unit BF,CtrSize,Num_Partitions,Num of active BFs,Memory Usage,Ratio of false positives to set size,NULL");
	}
	else
	{
    	logger = fopen("../logs/static_partition_individual.txt","a");
	}

	int i;
	for (i = 0; i < numFiles; ++i)
	{
		printf("File::%s\n",filenameList[i] );
		bloom * filter = (bloom*)malloc(sizeof(bloom));
		init(filter);

		read_and_insert(filenameList[i],filter);
		char fullFilename[1000];
		sprintf(fullFilename,"%s%s",datasetPath,filenameList[i]);
		FILE * reader = fopen(fullFilename,"r");
		int num_elems,namespace;
		fscanf(reader,"%d",&num_elems);
		fscanf(reader,"%d",&namespace);
		fclose(reader);

		fprintf(logger, "%s,",filenameList[i]);
		fprintf(logger, "%d,",num_elems);
		fprintf(logger, "%d,",namespace);
		fprintf(logger, "%d,",bloomParam->K);
		fprintf(logger, "%d,",bloomParam->SIZE_BLOOM/8);
		fprintf(logger, "%d,",bloomParam->COUNTER_CHUNK);
		fprintf(logger, "%d,",bloomParam->NUM_FILTERS);
		int iter, tempCount=0;
		for (iter = 0; iter < bloomParam->NUM_FILTERS; ++iter)
		{
			if( filter->flag_array[iter] != NULL)
				tempCount++;	
		}
		fprintf(logger, "%d,",tempCount);
		fprintf(logger, "%d,",tempCount*(bloomParam->SIZE_BLOOM)/8);

		estimate_false_positive(filenameList[i],filter,logger);
		fprintf(logger, "NULL\n");
		// free_bloom(filter);
	}
	fclose(logger);
}

// Takes a bloom filter and estimates number of false positives in it.
void estimate_false_positive(char* filename,bloom* filter, FILE* logger)
{
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,filename);
	FILE * reader = fopen(fullFilename,"r");
	int num_elems,namespace;
	fscanf(reader,"%d",&num_elems);
	fscanf(reader,"%d",&namespace);
	fclose(reader);
	int i;
	int numPresent = 0;
	for (i = 1; i <= namespace; ++i)
	{
		if(is_in(i,filter))
		{
			numPresent++;
		}
	}
	// fprintf(logger, "%d,",numPresent - num_elems);
	fprintf(logger, "%.3f,",(numPresent - num_elems)/(float)num_elems);
}

void test_union_intersection(char** filenameList_1,char** filenameList_2,int numFiles)
{
	FILE* logger;
	
	if( access( "../logs/static_partition_set_ops.txt", F_OK ) == -1 ) 
	{
		logger = fopen("../logs/static_partition_set_ops.txt","w");
		fprintf(logger,  "%s\n","File A,File B,Namespace,NumHashFunc,Size of unit BF,CtrSize,Num Partitions,No. of Elems (A),Num Filters used,Memory Usage, Ratio of false positives to set size,No. of Elems (B),Num Filters used,Memory Usage, Ratio of false positives to set size,No. of Elems (A union B),Num Filters used,Memory Usage, Ratio of false positives to set size,No. of Elems (A intersect B),Num Filters used,Memory Usage, Ratio of false positives to set size,NULL");
	}
	else
	{
		logger = fopen("../logs/static_partition_set_ops.txt","a");
	}

	// fprintf(logger,   "%s\n","File A,File B,Union File,Intersection File,Namespace,NumHashFunc,Size of BF,CtrSize,
	// No. of Elems (A),No. of false positives, Ratio of false positives to set size,
	// No. of Elems (B),No. of false positives, Ratio of false positives to set size,
	// No. of Elems (A union B),No. of false positives, Ratio of false positives to set size,
	// No. of Elems (A intersect B),No. of false positives, Ratio of false positives to set size,NULL");

	int i;
	for (i = 0; i < numFiles; ++i)
	{
		char union_filename[1000];
		char intersect_filename[1000];
		sprintf(union_filename		,"%s_%s_union",		filenameList_1[i],filenameList_2[i]);
		sprintf(intersect_filename	,"%s_%s_intersect",	filenameList_1[i],filenameList_2[i]);

		bloom* filter_1 		= (bloom*)malloc(sizeof(bloom));
		bloom* filter_2 		= (bloom*)malloc(sizeof(bloom));
		bloom* filter_union 	= (bloom*)malloc(sizeof(bloom));
		bloom* filter_intersect = (bloom*)malloc(sizeof(bloom));
		init(filter_1);
		init(filter_2);
		init(filter_union);
		init(filter_intersect);
	
		FILE * reader;
		int namespace,tempCount,iter;
		int num_elems_1,num_elems_2, num_elems_union, num_elems_intersect;

		char* fullFilename = (char*)malloc(1000*sizeof(char));
		sprintf(fullFilename,"%s%s",datasetPath,filenameList_1[i]);
		reader = fopen(fullFilename,"r");
		fscanf(reader,"%d",&num_elems_1);
		fscanf(reader,"%d",&namespace);
		fclose(reader);
		free(fullFilename);

		fullFilename = (char*)malloc(1000*sizeof(char));
		sprintf(fullFilename,"%s%s",datasetPath,filenameList_2[i]);
		reader = fopen(fullFilename,"r");
		fscanf(reader,"%d",&num_elems_2);
		fclose(reader);
		free(fullFilename);

		fullFilename = (char*)malloc(1000*sizeof(char));
		sprintf(fullFilename,"%s%s",datasetPath,union_filename);
		reader = fopen(fullFilename,"r");
		fscanf(reader,"%d",&num_elems_union);
		fclose(reader);
		free(fullFilename);

		fullFilename = (char*)malloc(1000*sizeof(char));
		sprintf(fullFilename,"%s%s",datasetPath,intersect_filename);
		reader = fopen(fullFilename,"r");
		fscanf(reader,"%d",&num_elems_intersect);
		fclose(reader);
		free(fullFilename);

		fprintf(logger, "%s,", filenameList_1[i]);
		fprintf(logger, "%s,", filenameList_2[i]);
		// fprintf(logger, "%s_%s_union,", filenameList_1[i],filenameList_2[i]);
		// fprintf(logger, "%s_%s_intersect,", filenameList_1[i],filenameList_2[i]);

		fprintf(logger, "%d,",namespace);
		fprintf(logger, "%d,",bloomParam->K);
		fprintf(logger, "%d,",bloomParam->SIZE_BLOOM/8);
		fprintf(logger, "%d,",bloomParam->COUNTER_CHUNK);
		fprintf(logger, "%d,",bloomParam->NUM_FILTERS);
		
		read_and_insert(filenameList_1[i],filter_1);
		read_and_insert(filenameList_2[i],filter_2);
		union_bloom(filter_1,filter_2,filter_union);
		intersect_bloom(filter_1,filter_2,filter_intersect);

		// Details for set A
		fprintf(logger, "%d,",num_elems_1 );
		tempCount = 0;
		for (iter = 0; iter < bloomParam->NUM_FILTERS; ++iter)
		{
			if(filter_1->flag_array[iter] != NULL)
				tempCount++;
		}
		fprintf(logger, "%d,",tempCount);
		fprintf(logger, "%d,",(int)(tempCount*(bloomParam->SIZE_BLOOM)/8));
		estimate_false_positive(filenameList_1[i],filter_1,logger);

		// Details for set B
		fprintf(logger, "%d,",num_elems_2 );
		tempCount = 0;
		for (iter = 0; iter < bloomParam->NUM_FILTERS; ++iter)
		{
			if(filter_2->flag_array[iter] != NULL)
				tempCount++;
		}
		fprintf(logger, "%d,",tempCount);
		fprintf(logger, "%d,",(int)(tempCount*(bloomParam->SIZE_BLOOM)/8));
		estimate_false_positive(filenameList_2[i],filter_2,logger);
	
		// Details for set (A union B)
		fprintf(logger, "%d,",num_elems_union );
		tempCount = 0;
		for (iter = 0; iter < bloomParam->NUM_FILTERS; ++iter)
		{
			if(filter_union->flag_array[iter] != NULL)
				tempCount++;
		}
		fprintf(logger, "%d,",tempCount);
		fprintf(logger, "%d,",(int)(tempCount*(bloomParam->SIZE_BLOOM)/8));
		estimate_false_positive(union_filename, filter_union,logger);

		// Details for set (A intersect B)
		fprintf(logger, "%d,",num_elems_intersect );
		tempCount = 0;
		for (iter = 0; iter < bloomParam->NUM_FILTERS; ++iter)
		{
			if(filter_intersect->flag_array[iter] != NULL)
				tempCount++;
		}
		fprintf(logger, "%d,",tempCount);
		fprintf(logger, "%d,",(int)(tempCount*(bloomParam->SIZE_BLOOM)/8));
		estimate_false_positive(intersect_filename,filter_intersect,logger);

		fprintf(logger, "NULL\n");

		free_bloom(filter_1);
		free(filter_1);
		free_bloom(filter_2);
		free(filter_2);
		free_bloom(filter_union);
		free(filter_union);
		free_bloom(filter_intersect);
		free(filter_intersect);
	}
	
	fclose(logger);
}

void run_memory_tests()
{
	int i,j,k;
	int numFiles = 5;
	char** filenameList = (char**)malloc(numFiles*sizeof(char*));
	for (i = 0; i < numFiles; ++i)
	{
		filenameList[i] = (char*)malloc(sizeof(char)*100);
	}
	strcpy(filenameList[0],"rand::10");
	strcpy(filenameList[1],"rand::100");
	strcpy(filenameList[2],"rand::500");
	strcpy(filenameList[3],"rand::1000");
	strcpy(filenameList[4],"rand::2000");

	int num_K_Vals 				= 2;
	int num_num_Partitions 		= 3;
	int num_filterSizeVals 		= 2;

	int K_Vals[2] 				= {3,5};
	int num_Partitions[3]		= {2,4,8};
	int filterSizeVals[2] 		= {1024,2048};//,2048,4000,10000,20000,40000};

	for (i = 0; i < num_filterSizeVals; ++i)
	{
		for (j = 0; j < num_K_Vals; ++j)
		{
			for (k = 0; k < num_num_Partitions; ++k)
			{
				printf("Running test for K = %d, numPartitions = %d, FilterSize = %d\n",K_Vals[j], num_Partitions[k], filterSizeVals[i]);
				init_bloomParameters(filterSizeVals[i],K_Vals[j],2,num_Partitions[k]); // Sets SIZE_BLOOM, K, and COUNTER_SIZE, numPartitions
				seiveInitial(); // Important to initialize it here to reset m[i] values to suit the changed values of SIZE_BLOOM
				test_memory_usage(filenameList,numFiles);				
			}
		}
	}
}

void run_set_operation_tests()
{
	int numFiles = 10;
	char** filenameList_1, **filenameList_2;
	filenameList_1 = (char**)malloc(1000*sizeof(char*));
	filenameList_2 = (char**)malloc(1000*sizeof(char*));
	FILE* reader ;
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,"filenameList");
	reader = fopen(fullFilename,"r");
	
	int ctr  = 0;
	while(!feof(reader))
	{
		filenameList_1[ctr] = (char*)malloc(200*sizeof(char));
		filenameList_2[ctr] = (char*)malloc(200*sizeof(char));
		fscanf(reader,"%s %s",filenameList_1[ctr],filenameList_2[ctr]);
		ctr += 1;
	}
	numFiles = ctr;

	int num_K_Vals 				= 2;
	int num_num_Partitions 		= 3;
	int num_filterSizeVals 		= 2;

	int K_Vals[2] 				= {3,5};
	int num_Partitions[3]		= {2,4,8};
	int filterSizeVals[2] 		= {512,1024};//,2048,4000,10000,20000,40000};

	int i,j,k;
	for (i = 0; i < num_filterSizeVals; ++i)
	{
		for (j = 0; j < num_K_Vals; ++j)
		{
			for (k = 0; k < num_num_Partitions; ++k)
			{
				init_bloomParameters(filterSizeVals[i],K_Vals[j],2,num_Partitions[k]); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
				seiveInitial(); // Important to initialize it here to reset m[i] values to suit the changed values of SIZE_BLOOM
				test_union_intersection(filenameList_1,filenameList_2,numFiles);
			}
		}
	}
}

int main()
{
	time_t t;
	srand((unsigned) time(&t));
	datasetPath = (char*)malloc(1000*sizeof(char));
	strcpy(datasetPath,"../datasets/");

	run_memory_tests();
	run_set_operation_tests();
}
