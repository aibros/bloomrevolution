#ifndef _BLOOM_H
#define _BLOOM_H

#include <limits.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#define MAX(a,b) ((a) > (b) ? a : b)
#define MIN(a,b) ((a) < (b) ? a : b)

typedef unsigned int UINT;
typedef unsigned long long int ULONG;

typedef struct bloomParameters
{
	UINT NUM_BITS;
	UINT SIZE_BLOOM;
	UINT K;
	UINT COUNTER_CHUNK;		
	UINT BIT_MAP;

	//dynamic bloom
	UINT MAX_DYN_SIZE;
	float FALSE_PSTV_PROB;
	float FILL_THRESHOLD;   // This is the max number of elements that we can have so as to maintain the required false +ve rate
	/*
	* testing suite variables
	*/
	UINT TEST_RANGE;
}bloomParameters;

/*
* using integer array to keep the flags with the notion that each place keeps 32 bits so
* will have 32 flags present . Extend it to counting by setting fixed no. of bits for the 
* counting purpose.
*/
typedef struct bloom{
	UINT *flag_array;		
}bloom;

extern bloomParameters* bloomParam;


/*
*
*/
void init_bloomParameters(UINT SIZE_BLOOM,UINT K,UINT COUNTER_CHUNK);

/*
* INPUT : none
* WORK	: sets up m array required in calculating hash values
* OUTPUT: none
* NOTE 	: using the hash function along with necessary setup provided as in bloomsampling 
* codebase inplace of directly using murmur.
*/
void seiveInitial();

/*
* INPUT : pointer to bloom filter
* WORK	: allocates space and initialises the bloom filter with all zero values
* OUTPUT: none
* NOTE 	: using same as in bloomsampling but memory allocation in this function only.
*/
void init(bloom *bl);
// UINT* init_chunk();


// UINT hash_prime(int a);
UINT hash(int a, int i);
/*
* INPUT : val {value to be inserted} , pointer to bloom filter
* WORK	: inserts the value in the bloom filter after calculating suitable hash functions . 
* insertion happens by incrementing the value in COUNTER_CHUNK bits present in the array. 
* OUTPUT: none
* NOTE 	: using as it is from bloomsampling codebase.
*/
void insert(UINT val, bloom* bl);

/*
* INPUT : val {value to be confirmed} , pointer to bloom filter
* WORK	: checks the bloom filter for the provided value
* OUTPUT: 1 if success and 0 on failure.
* NOTE 	: using as it is from bloomsampling codebase.
*/
UINT is_in(UINT val, bloom* bl);

/*
* INPUT : pointer to bloom filter
* WORK	: frees the allocated space for the bloom filter
* OUTPUT: none
* NOTE 	: as allocation in init so wrapping up the freeing function.
*/
void free_bloom(bloom* bl);


/*
*
*/
UINT * reconstruct_bloom(bloom* bl, UINT start, UINT end );

/*
* INPUT : two counting bloom filters
* WORK	: assigns a new bloom filter which stores the intersection of two given bloom 
* filters
* OUTPUT: the bloom filter c is modified and also it takes care if c is same as a or b
* NOTE 	: -
*/
void intersect_bloom(bloom *a, bloom *b, bloom *c);

/*
*
*/
void delete(UINT val, bloom* bl);


/*
* INPUT : two counting bloom filters
* WORK	: assigns a new bloom filter which stores the difference of two given bloom 
* filters in order elements in a- elements in b
* OUTPUT: the bloom filter c is modified and also it takes care if c is same as a or b
* NOTE 	: -
*/
void subtract_bloom(bloom *a, bloom *b, bloom *c);

/*
* INPUT : two counting bloom filters
* WORK	: assigns a new bloom filter which stores the exact sum of two given bloom 
* filters in order elements in a- elements in b
* OUTPUT: the bloom filter c is modified and also it takes care if c is same as a or b
* NOTE 	: -
*/
void add_bloom(bloom *a, bloom *b, bloom *c);

/*
* INPUT : two counting bloom filters
* WORK	: assigns a new bloom filter which stores the union of two given bloom 
* filters
* OUTPUT: the bloom filter c is modified and also it takes care if c is same as a or b
* NOTE 	: -
*/
void union_bloom(bloom *a, bloom *b, bloom *c);

/*
* NOTE  : testing fn which creates a bloom filter and inserts checks membership and deletes 
* 5000 numbers which are generated randomly
*/
void bloom_test_suit1();

/*
* NOTE 	: testing fn which creates two bloom filters with odd and even no.s and takes their 
* intersection and union which are later checked for membership. 
*/
void bloom_test_suit2();


//##############extra elements required for bloom sampling########################//

/*
* INPUT : counting bloom filter
* WORK	: counts the total no. of 1s present in the bloom filter
* OUTPUT: long count for the no. of ones
* NOTE 	: makes call to the count_ones fn for each element of the array
*/
UINT num_ones(bloom *a);

/*
* INPUT : counting bloom filter
* WORK	: counts the total no. of positive chunks present in the bloom filter
* OUTPUT: long count for the no. of positive chunks
* NOTE  : seeing chunks count suitable for applying approximate formula
*/
// long num_pos_chunks(bloom *a);

/*
* INPUT : counting bloom filter
* WORK	: counts the total no. of elements in the bloom filter
* OUTPUT: long count for the no. of ones
* NOTE 	: makes call to the num_ones fn and assumes no bucket overflow happens and just 
* divides by k.
*/
UINT count_elems(bloom *a);

/*
* INPUT : two counting bloom filters
* WORK	: counts the no. of ones in the intersection of the given two bloom filters
* OUTPUT: count of ones common to both
* NOTE 	: -
*/
UINT numOnes_intersection(bloom *a, bloom *b);

/*
* INPUT : two counting bloom filters
* WORK	: counts the no. of positive chunks in the intersection of the given two bloom filters
* OUTPUT: count of positive chunks common to both
* NOTE 	: suitable for approximation formula based on simple bloom filter
*/
// int numChunks_intersection(bloom *a, bloom *b);

/*
* INPUT : two counting bloom filters
* WORK	: gives the no. of common elements by using the formula for bloom filters
* OUTPUT: double for the no. of common elements
* NOTE 	: may have some fallacy as using for the counting bloom filter in place of normal
*/
double elemsIntersection(bloom *a, bloom *b, int ta, int tb);

/*
* INPUT : two counting bloom filters
* WORK	: gives the no. of common elements by using the formula for bloom filters
* OUTPUT: double for the no. of common elements
* NOTE 	: the fallacy mentioned above has been corrected
*/
// double elemsIntersection_corrected(bloom *a, bloom *b, int ta, int tb);

/*
* INPUT  : x - integer located at a location in bloom filter which stores hashed values in 
* counter present in chunks of COUNTER_CHUNK
* OUTPUT : count of the number of elements hashed in this location across all chunks
* NOTE 	 : slightly expensive as multiple left shift and right shift operations
*/
UINT count_ones(UINT x);

#endif