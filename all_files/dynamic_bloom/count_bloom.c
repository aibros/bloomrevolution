#include "count_bloom.h"
#include <math.h>
/*
two level bloom filter
MAX_RANGE = larger range over which small chunks built
bloomParam->SIZE_BLOOM = bloom filter for the small chunk
HASH_ONE = the hash function for first designating the chunks
then each function is modified preferably locality sensitive hashing
*/


// a::501,b::221,m::509
// a::187,b::284,m::503
// a::1,b::499,m::499
// a::311,b::304,m::491
// a::33,b::105,m::487
// a::37,b::443,m::479
// a::360,b::80,m::467
// a::173,b::463,m::463
// a::22,b::394,m::461
// a::346,b::360,m::457

// int aHash[10] = {501,187,1,311,33,37,360,173,22,346};
// int bHash[10] = {221,284,499,304,105,443,80,463,394,360};
// int m[10] = {509,503,499,491,487,479,467,463,461,457};
int aHash[10]={3730, 9830, 3830, 2110, 4740, 9440, 5540, 2710, 690, 7470};
int bHash[10]={269, 229, 929, 949, 624, 390, 64, 67, 945, 253};
int m[10];

bloomParameters* bloomParam;

void init_bloomParameters(UINT SIZE_BLOOM ,UINT K ,UINT COUNTER_CHUNK )
{
	bloomParam = (bloomParameters*)malloc(sizeof(bloomParameters));
	bloomParam->NUM_BITS     	= 8*sizeof(UINT);
	bloomParam->SIZE_BLOOM   	= SIZE_BLOOM;
	bloomParam->K 			  	= K;
	bloomParam->COUNTER_CHUNK  	= COUNTER_CHUNK;		//should be exponents of 2	
	bloomParam->BIT_MAP 		= pow(2,COUNTER_CHUNK) -1 ;	//2^COUNTER_CHUNK - 1;
	bloomParam->TEST_RANGE 		= 500;
}

void seiveInitial()
{
	int num_bits = bloomParam->SIZE_BLOOM/bloomParam->COUNTER_CHUNK;
	int *x = (int*)malloc(sizeof(int)*num_bits),i,j;
	
	// for (i = 0; i < 10; ++i)
	// {
	// 	aHash[i] = rand() % num_bits;
	// 	bHash[i] = rand() % num_bits;
	// }

	for (i=0;i<num_bits;i++) 
		x[i] = i+1;
	for (i=1;i<num_bits;i++)
	{
		j = x[i];
		if (j==-1) 
			continue;

		int q = 1;
		while ((i + q*j) < num_bits)
		{
			x[i+q*j] = -1;
			q++;
		}
	}
	i = num_bits;
	for (j=0;j<10;j++)
	{
		for (i=i-1;i>0;i--)
		{
			if (x[i]!=-1)
			{
				m[j] = x[i];
				break;
			}
		}
	}
	// for (i = 0; i < 10; ++i)
	// {
	// 	printf("a::%d,b::%d,m::%d\n",aHash[i],bHash[i],m[i]);
	// }
}


void init(bloom *bl)
{
	int size = (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);
	bl->flag_array = (UINT *)malloc(sizeof(UINT)*size);
	memset(bl->flag_array,0,sizeof(UINT)*size);
}

void insert(UINT val, bloom* bl)
{

	UINT i;
	UINT chunk_per_ind = bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK;
	for (i=0;i<bloomParam->K;i++)
	{
		UINT a = hash(val,i);
		UINT loc = a/chunk_per_ind;
		UINT off = a%chunk_per_ind;

		UINT new = 1<<(off*bloomParam->COUNTER_CHUNK);
		UINT bitmap = (bloomParam->BIT_MAP<<(off*bloomParam->COUNTER_CHUNK));
		UINT chunk = bitmap & bl->flag_array[loc];
		if(chunk==bitmap)
		{
			// printf("overflow of the bucket for counter -- making no change\n");
			continue;
		}
		bl->flag_array[loc] = bl->flag_array[loc] + new;
	}
}

void delete(UINT val, bloom* bl)
{

	UINT i;
	UINT chunk_per_ind = bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK;
	if(!is_in(val,bl))
	{
		return;
	}
	for (i=0;i<bloomParam->K;i++)
	{
		UINT a = hash(val,i);
		UINT loc = a/chunk_per_ind;
		UINT off = a%chunk_per_ind;

		UINT new = 1<<(off*bloomParam->COUNTER_CHUNK);
		UINT bitmap = (bloomParam->BIT_MAP<<(off*bloomParam->COUNTER_CHUNK));
		UINT chunk = bitmap & bl->flag_array[loc];
		if(chunk>0)
		{
			bl->flag_array[loc] = bl->flag_array[loc] - new;
		}
	}
}


UINT is_in(UINT val, bloom* bl)
{
	UINT i;
	UINT chunk_per_ind = bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK;
	for (i=0;i<bloomParam->K;i++){

		UINT a = hash(val,i);
		UINT loc = a/chunk_per_ind;
		UINT off = a%chunk_per_ind;

		UINT bitmap = (bloomParam->BIT_MAP<<(off*bloomParam->COUNTER_CHUNK));
		UINT chunk = bitmap & bl->flag_array[loc];
		if (!chunk) return 0;
	}
	return 1;
}

UINT hash(int a, int i)
{
	unsigned long z = aHash[i];
	z = z*a + bHash[i];
	UINT retVal = (z % m[i]);
	return retVal;
}

void free_bloom(bloom *bl)
{
	free(bl->flag_array);
}


UINT * reconstruct_bloom(bloom* bl, UINT start, UINT end )
{
	UINT i;
	UINT num_elems,elem = 0;
	for(i = start; i <= end; i++)
	{
		if(is_in(i,bl))
			num_elems++;
	}

	UINT * val_array = (UINT *)malloc(sizeof(UINT)*num_elems);
	
	memset(val_array, 0, sizeof(UINT)*num_elems);
	for(i = start; i <= end; i++)
	{
		if(is_in(i,bl))
		{
			val_array[elem] = i;
			elem++;
		}
	}
	return val_array;
}


void intersect_bloom(bloom *a, bloom *b, bloom *c)
{

	UINT i,j,num,check=0;
	for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			temp += MIN(a->flag_array[i]&new,b->flag_array[i]&new);
		}
		if(temp>0)
			check = 1;
		c->flag_array[i] = temp;
	}
}

void subtract_bloom(bloom *a, bloom *b, bloom *c)
{
	UINT i,j,check,num=0;
	for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			if ( (a->flag_array[i]&new) >= (b->flag_array[i]&new) )
				temp += (a->flag_array[i]&new)-(b->flag_array[i]&new);
		}
		if(temp!=0)
			check = 1;
		c->flag_array[i] = temp;
	}
}

void add_bloom(bloom *a, bloom *b, bloom *c)
{
	UINT i,j,num=0;
	for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			UINT curr = (a->flag_array[i]&new)+(b->flag_array[i]&new);
			if(curr > new && j!=(bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK-1))
			{
				curr = new;
			}
			else if(j==(bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK-1))			//as this chunk stores the sign bit so it is dealt separately as in this case it can overflow
			{
				curr = (a->flag_array[i]&new)>>2+(b->flag_array[i]&new)>>2;
				if(curr > (new>>2))
					curr = new;
				else
					curr = curr<<2;
			}
			temp+=curr;
		}
		c->flag_array[i] = temp;
	}
}


void union_bloom(bloom *a, bloom *b, bloom *c)
{
	UINT i,j,num=0;
	for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			temp += MAX(a->flag_array[i]&new,b->flag_array[i]&new);
		}
		c->flag_array[i] = temp;
	}
}

void bloom_test_suit1()
{
	bloom *a = (bloom*)malloc(sizeof(bloom));
	init(a);
	UINT i;
	printf("bloom graph insert/member/delete test initiated\n");
	for(i = 0 ; i < bloomParam->TEST_RANGE ; i++ )
	{
		UINT z = rand()%bloomParam->TEST_RANGE;
		insert(z,a);
		if(!is_in(z,a))
		{
			printf("bloom graph not functioning properly - inserted val :: %d not found\n",z);
			exit(0);
		}
		delete(z,a);
	}

	printf("count of elems :: %u  %u \n",count_elems(a),bloomParam->TEST_RANGE );
	free_bloom(a);
	free(a);
	printf("bloom graph insert/member/delete test successful\n");
}

void bloom_test_suit2()
{
	bloom *a = (bloom*)malloc(sizeof(bloom));
	bloom *b = (bloom*)malloc(sizeof(bloom));

	init(a);	
	init(b);

	UINT i;
	printf("bloom graph union/intersect test initiated bloomParam->SIZE_BLOOM :: %u\n",bloomParam->SIZE_BLOOM);
	UINT count_a = 0,count_b = 0;
	for( i = 0 ; i < bloomParam->TEST_RANGE ; i++ )
	{
		if(i%2==0)
		{
			insert(i,a);
			if(!is_in(i,a))
				printf("element not found a %u\n",i);
			// if(is_in(i,b))
			// 	printf("incorrect element found in b %u\n",i);
			count_a++;
		}
		else
		{
			insert(i,b);
			if(!is_in(i,b))
				printf("element not found b  %u\n",i);
			// if(is_in(i,a))
			// 	printf("incorrect element found in a  %u\n",i);
			count_b++;
		}
	}
	printf("insertion stage over\n");
	printf("size a :: %u actual:: %u\n",count_elems(a),count_a );	//the number of elements may be less as some counter may completely fill up
	printf("size b :: %u actual:: %u\n",count_elems(b),count_b );	//

	bloom * c = (bloom *)malloc(sizeof(bloom));
	init(c);
	intersect_bloom(a,b,c);
	
	bloom * d = (bloom *)malloc(sizeof(bloom));
	init(d);	
	union_bloom(a,b,d);

	bloom * e = (bloom *)malloc(sizeof(bloom));
	init(e);
	add_bloom(a,b,e);



	printf("union/intersection operations executed -- size union size::%u intersect::%u addition::%u\n",count_elems(d),count_elems(c),count_elems(e));
	printf("intersection count via-- current elemsIntersection::%f \n",elemsIntersection(a,b,num_ones(a),num_ones(b)));
	// exit(0);
	// if(is_in(bloomParam->TEST_RANGE*2,a)==1)
	// {
	// 	printf("insertion conflict found - %u\n",bloomParam->TEST_RANGE*2);
	// 	exit(0);
	// }
	ULONG con_union = 0;
	ULONG con_inter = 0;
	for( i = 0 ; i < bloomParam->TEST_RANGE ; i++ )
	{

		if(!is_in(i,d))
		{
			// printf("union not working correctly\n");
			con_union++;
			// exit(0);
		}
		if(is_in(i,c)==1)
		{
			// printf("intersect conflict found - %d\n",i);
			con_inter++;
		}
	}
	printf("errors_union :: %llu errors_intersect :: %llu \n",con_union,con_inter );

	free_bloom(a);
	free_bloom(b);
	free_bloom(c);
	free_bloom(e);
	free(a);
	free(b);
	free(c);
	free(e);
	printf("bloom graph union/intersect test successful\n");
}

UINT count_elems(bloom *a)
{
	UINT count = num_ones(a)/bloomParam->K;
	return count;
}

UINT num_ones(bloom *a)
{
	UINT i = 0, n = 0;
	for (i = 0 ; i < bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS ; i++)
		n += count_ones(a->flag_array[i]);
	return n;
}

UINT numOnes_intersection(bloom *a, bloom *b)
{
	UINT i , j, num = 0, numCounts = 0;

	for (i=0;i<(bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			 UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			 UINT curr = 0;
			if(new<0)
			{
				curr = MIN( (a->flag_array[i]&new)>>bloomParam->COUNTER_CHUNK , (b->flag_array[i]&new)>>bloomParam->COUNTER_CHUNK );
				curr = curr<<bloomParam->COUNTER_CHUNK;
			}
			else
				curr = MIN( (a->flag_array[i]&new) , (b->flag_array[i]&new) );
			temp+=curr;
		}
		num += count_ones(temp);										//can be made efficient
	}
	return num;
}

double elemsIntersection(bloom *a, bloom *b, int ta, int tb)
{
	double t1 = (double)ta;
	double t2 = (double)tb;
	double th = (double)numOnes_intersection(a,b);
	return th/bloomParam->K;
}

UINT count_ones(UINT x)
{
	UINT i = 0;
	UINT count = 0;
	for(i = 0 ; i < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; i++)
	{
		UINT new = bloomParam->BIT_MAP<<(i*bloomParam->COUNTER_CHUNK);
		UINT intersect = x&new;
		count += intersect>>(i*bloomParam->COUNTER_CHUNK);
	}

	return count;
}

// int main()
// {
// 	init_bloomParameters();
// 	seiveInitial();
// 	bloom_test_suit1();
// 	bloom_test_suit2();	
// 	printf("new bloom filters used %d overall chunks initialized %d\n",num_bloom,counter );
// }
