#include "dynamic_bloom.h"

/*
* IMPORTANT : maxSize > currNum always for dynamic bloomFilter and this array of bloomFilter is not dynamic
*/

/*
* INPUT : pointer to dynamic bloomfilter
* WORK	: allocates space and for one bloom filter and also initialised maxSize and currNum
* OUTPUT: none
* NOTE 	: using same as in bloomsampling but memory allocation in this function only.
*/

char* datasetPath;

void init_dyn_parameters(UINT ARG_MAX_DYN_SIZE, float ARG_FALSE_PSTV_PROB)
{
	bloomParam->MAX_DYN_SIZE 	= ARG_MAX_DYN_SIZE;
	bloomParam->FALSE_PSTV_PROB 	= ARG_FALSE_PSTV_PROB;
	bloomParam->FILL_THRESHOLD  = -1*((int)bloomParam->SIZE_BLOOM/(float)(bloomParam->K*bloomParam->COUNTER_CHUNK))*log( 1 - pow(ARG_FALSE_PSTV_PROB,1.0/bloomParam->K));
	// printf("FALSE_PSTV_PROB::%f\n",bloomParam->FALSE_PSTV_PROB );
	// printf("FILL_THRESHOLD::%f\n",bloomParam->FILL_THRESHOLD );
}

void dyn_init(dyn_bloom *dbf)
{
	dbf->maxSize 	= bloomParam->MAX_DYN_SIZE;
	dbf->currNum 	= 0;
	dbf->bf_array 	= (bloom**)malloc(dbf->maxSize*sizeof(bloom*));
	dbf->bf_array[dbf->currNum] = (bloom*)malloc(sizeof(bloom));
	dbf->filled_chunks = 0;
	init(dbf->bf_array[dbf->currNum]);
}

void dyn_grow(dyn_bloom *dbf)
{
	if(dbf->currNum==dbf->maxSize)
	{
		int i;
		bloom **temp = (bloom**)malloc(dbf->maxSize*2*sizeof(bloom*));
		for( i = 0 ; i <= dbf->currNum ; i++)
			temp[i] = dbf->bf_array[i];
		free(dbf->bf_array);
		dbf->bf_array = temp;
		dbf->maxSize *= 2;
	}
	dbf->currNum++;
	dbf->bf_array[dbf->currNum] = (bloom*)malloc(sizeof(bloom));
	dbf->filled_chunks = 0;
	init(dbf->bf_array[dbf->currNum]);
}

/*
* INPUT : val {value to be inserted} , pointer to dynamic bloom filter
* WORK	: inserts the value in the active bloom filter after calculating suitable hash functions .
* insertion happens by incrementing the value in COUNTER_CHUNK bits present in the array. 
* OUTPUT: none
* NOTE 	: using as it is from bloomsampling codebase.
*/
void dyn_insert(UINT val, dyn_bloom* dbf)
{
	insert(val,dbf->bf_array[dbf->currNum]);
	dbf->filled_chunks += 1;

	//  Check for false positive probability bound so that currNum can be incremented
	// if (dbf->filled_chunks >= (bloomParam->FILL_THRESHOLD)*(bloomParam->SIZE_BLOOM)/bloomParam->COUNTER_CHUNK)
	if (dbf->filled_chunks >= bloomParam->FILL_THRESHOLD)
	{
		dyn_grow(dbf);
	}
}

/*
* INPUT : val {value to be confirmed} , pointer to bloom filter
* WORK	: checks the bloom filter for the provided value
* OUTPUT: 1 if success and 0 on failure.
* NOTE 	: using as it is from bloomsampling codebase.
*/
UINT dyn_is_in(UINT val, dyn_bloom* dbf)
{
	int i;
	for (i = 0; i <= dbf->currNum; ++i)
	{
		if (is_in(val,dbf->bf_array[i]))
			return 1;
	}
	return 0;
}

UINT is_in2(UINT val, bloom* bl)
{
	UINT i;
	UINT chunk_per_ind = bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK;
	for (i=0;i<bloomParam->K;i++){

		UINT a = hash(val,i);
		UINT loc = a/chunk_per_ind;
		UINT off = a%chunk_per_ind;

		UINT bitmap = (bloomParam->BIT_MAP<<(off*bloomParam->COUNTER_CHUNK));
		UINT chunk = bitmap & bl->flag_array[loc];
		printf("chunk for %u chunk off:: %u chunk index ::%u \n",val,off,loc );
		if (!chunk) return 0;
	}
	return 1;
}

UINT dyn_is_in2(UINT val, dyn_bloom* dbf)
{
	int i;
	for (i = 0; i <= dbf->currNum; ++i)
	{
		printf("Searching %u at location %u\n",val,i );
		if (is_in2(val,dbf->bf_array[i]))
		{
			printf("lost and found :: val :: %d location :: %d \n",val,i );
			return 1;
		}
	}
	return 0;
}

/*
* INPUT : pointer to bloom filter
* WORK	: frees the allocated space for the bloom filter
* OUTPUT: none
* NOTE 	: as allocation in init so wrapping up the freeing function.
*/
void dyn_free_bloom(dyn_bloom* dbf)
{
	int i;
	for (i = 0; i <= dbf->currNum; ++i)
	{
		free_bloom(dbf->bf_array[i]);
	}
	free(dbf->bf_array);
}

void dyn_adjust_bloom(dyn_bloom * dbf, int size)
{
	int i;
	if( (dbf->currNum+1)<size)
	{
		for( i = dbf->currNum+1 ; i < size ; i++ )
		{
			dyn_grow(dbf);
		}
	}
	else if( (dbf->currNum+1)>size)
	{
		for( i = size  ; i <= dbf->currNum ; i++ )
		{
			free_bloom(dbf->bf_array[i]);
			free(dbf->bf_array[i]);
			dbf->currNum--;
		}
	}
}

UINT * reconstruct_dyn_bloom(dyn_bloom* bl, UINT start, UINT end )
{
	UINT i;
	UINT num_elems,elem = 0;
	for(i = start; i <= end; i++)
	{
		if(dyn_is_in(i,bl))
			num_elems++;
	}

	UINT * val_array = (UINT *)malloc(sizeof(UINT)*num_elems);
	
	memset(val_array, 0, sizeof(UINT)*num_elems);
	for(i = start; i <= end; i++)
	{
		if(dyn_is_in(i,bl))
		{
			val_array[elem] = i;
			elem++;
		}
	}
	return val_array;
}

/*
* INPUT : two counting bloom filters
* WORK	: assigns a new bloom filter which stores the intersection of two given bloom 
* filters
* OUTPUT: the bloom filter c is modified and also it takes care if c is same as a or b
* NOTE 	: Basic idea , each bf of smaller dyn_bloom takes intersection with each of the 
* bloom filters and union of all those is stored into the resultant bloom filter
* issue -- possible effect on the count elems **maybe for 2 bits can fall back to the 
* simple probabilistic formula to get size out of a given bloom filter
*/
void dyn_intersect_bloom(dyn_bloom *a, dyn_bloom *b, dyn_bloom *c)
{
	dyn_bloom * small;
	dyn_bloom * large;
	if( a == c || b == c )
	{
		printf("dyn_intersect_bloom()::inplace intersection not allowed\n");
		exit(0);
	}
	if( a->currNum >= b->currNum )
	{
		small = b;
		large = a;
	}
	else
	{
		small = a;
		large = b;
	}

	if(c->currNum!=small->currNum)
	{
		dyn_adjust_bloom(c,small->currNum+1);
	}

	int i,j,k;
	for (i = 0; i <= small->currNum; ++i)
	{
		for (j = 0; j <= large->currNum; ++j)
		{
				UINT m,n,num=0;
				for(m = 0 ; m < (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS) ; m++)
				{ 
					UINT temp = 0;
					for( n = 0 ; n < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; n++)
					{
						UINT new = bloomParam->BIT_MAP<<(n*bloomParam->COUNTER_CHUNK);
						temp += MAX( c->bf_array[i]->flag_array[m]&new , MIN(small->bf_array[i]->flag_array[m]&new , large->bf_array[j]->flag_array[m]&new));
					}
					c->bf_array[i]->flag_array[m] = temp;					//taking += here which is required as against to = in case of normal intersection
				}
		}
	}
}

/*
* Returns 1 on success and 0 on failure
*/
int dyn_delete(UINT val, dyn_bloom* dbf)
{
	int i , delCtr = 0, index = 0;
	for ( i = 0; i <= dbf->currNum; ++i)
	{
		if (is_in(val,dbf->bf_array[i]))
		{
			delCtr 	+= 1;
			index = i;
		}
	}

	if (delCtr == 0)
	{
		printf("dyn_delete()::Element not present in the dynamic bloom filter\n");
	}
	else if (delCtr == 1)
	{
		delete(val,dbf->bf_array[index]);
	}
	else if (delCtr >= 2)
	{
		printf("dyn_delete()::Element deleted from multiple bloomFilters\n");
	}
}


/*
* INPUT : two counting bloom filters
* WORK	: assigns a new bloom filter which stores the difference of two given bloom 
* filters in order elements in a- elements in b
* OUTPUT: the bloom filter c is modified and also it takes care if c is same as a or b
* NOTE 	: chunkwise modification is done by taking the chunk in small and subtracting 
* from it intersection with all the chunks of those of large bf. 
*/
void dyn_subtract_bloom(dyn_bloom *a, dyn_bloom *b, dyn_bloom *c)
{
	if( b == c )
	{
		printf("dyn_subtract_bloom()::inplace subtraction not allowed\n");
		exit(0);
	}

	if(c->currNum!=a->currNum)
	{
		dyn_adjust_bloom(c,a->currNum+1);
	}

	int i,j,k;
	for (i = 0; i <= a->currNum; ++i)
	{

			UINT m,n,num=0;
			for(m = 0 ; m < (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS) ; m++)
			{ 
				UINT temp = 0;

				for( n = 0 ; n < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; n++)
				{
					UINT new = bloomParam->BIT_MAP<<(n*bloomParam->COUNTER_CHUNK);
					UINT curr = a->bf_array[i]->flag_array[m]&new ;

					for (j = 0; j <= b->currNum; ++j)
					{
						//no need to update small as we do as only when less than 0 then only it might affect
						curr -= MIN(a->bf_array[i]->flag_array[m]&new , b->bf_array[j]->flag_array[m]&new);
						if(curr < 0)
						{
							curr = 0;
							break;
						}
					}
					if(curr>new)
						curr = new;

					temp+=curr;
				}
				c->bf_array[i]->flag_array[m] = temp;					//taking += here which is required as against to = in case of normal intersection
			}

	}
}

/*
* INPUT : two counting bloom filters
* WORK	: assigns a new bloom filter which stores the exact sum of two given bloom 
* filters in order elements in a- elements in b
* OUTPUT: the bloom filter c is modified and also it takes care if c is same as a or b
* NOTE 	: simply add ith small to (n-i)th big and leave rest unchanged invariant of 
* having filled up fraction is no more maintained in this case.
*/
void dyn_add_bloom(dyn_bloom *a, dyn_bloom *b, dyn_bloom *c)
{
	dyn_bloom * small;
	dyn_bloom * large;
	if( b == c )
	{
		printf("dyn_add_bloom()::inplace add not allowed\n");
		exit(0);
	}
	// if( a->currSize >= b->currSize )
	// {
	// 	small = b;
	// 	large = a;
	// }
	// else
	// {
	// 	small = a;
	// 	large = b;
	// }
	large = a;
	small = b;

	if(c->currNum!=large->currNum)
	{
		dyn_adjust_bloom(c,large->currNum+1);
	}

	int i,j,k;
	for (i = 0; i <= large->currNum; ++i)
	{
		int curr_large = large->currNum - i;
		// printf("dyn_add_bloom()::%d::%d::%d\n\n\n\n",i,small->currNum,curr_large );
		if(i <= small->currNum)
		{
				UINT m,n;
				for(m = 0 ; m < (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS) ; m++)
				{ 
					UINT temp = 0;

					for( n = 0 ; n < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; n++)
					{
						UINT new = bloomParam->BIT_MAP<<(n*bloomParam->COUNTER_CHUNK);
						UINT curr = (small->bf_array[i]->flag_array[m])&new + (large->bf_array[curr_large]->flag_array[m])&new;

						if(curr>new)
						{
							curr = new;
						}

						temp += curr;
					}
					c->bf_array[curr_large]->flag_array[m] = temp;
				}
		}
		else
		{
				UINT m = 0;
				for(m = 0 ; m < (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS) ; m++)
				{
					 c->bf_array[curr_large]->flag_array[m] = large->bf_array[curr_large]->flag_array[m];
				}
		}
	}
}


/*
* INPUT : two counting bloom filters
* WORK	: assigns a new bloom filter which stores the union of two given bloom 
* filters
* OUTPUT: the bloom filter c is modified and also it takes care if c is same as a or b
* NOTE 	: here first in each chunk of the larger which is iterated in reverse in order
* as compared to small array  both the large and corresponding small if there added and 
* then min of the chunks with all the small array bfs is subtracted. And finally assigned
* to temp which finally sets c.
*/
void dyn_union_bloom(dyn_bloom *a, dyn_bloom *b, dyn_bloom *c)
{
	dyn_bloom * small;
	dyn_bloom * large;
	if( b == c )
	{
		printf("dyn_union_bloom()::inplace union not allowed\n");
		exit(0);
	}
	if (a->currNum > b->currNum)
	{
		large = a;
		small = b;	
	}
	else
	{
		large = b;
		small = a;
	}

	if(c->currNum!=large->currNum)
	{
		dyn_adjust_bloom(c,large->currNum+1);
	}

	int i,j,k;
	for (i = 0; i <= large->currNum; ++i)
	{
		int curr_large = i;
		if(i <= small->currNum)
		{
			UINT m,n,num=0;
			for(m = 0 ; m < (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS) ; m++)
			{ 
				UINT temp = 0;
				for( n = 0 ; n < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; n++)
				{
					UINT new 		= bloomParam->BIT_MAP<<(n*bloomParam->COUNTER_CHUNK);
					UINT curr 		= large->bf_array[curr_large]->flag_array[m]&new;
					UINT b_union 	= 0;
					UINT sub 		= 0;
					for (j = 0; j <= small->currNum; ++j)
					{
						b_union = MAX((small->bf_array[j]->flag_array[m]&new) ,b_union );
					}
					
					if ( b_union <= curr)
					{
						curr = curr - b_union;
					}
					else
					{
						curr = 0;
					}
					curr += (small->bf_array[i]->flag_array[m]&new);
					if(curr > new)
						curr = new;

					temp += curr;
				}
				c->bf_array[curr_large]->flag_array[m] = temp;					//taking += here which is required as against to = in case of normal intersection
			}
		}
		else
		{	
				UINT m = 0;
				for(m = 0 ; m < (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS) ; m++)
				{
					 c->bf_array[curr_large]->flag_array[m] = large->bf_array[curr_large]->flag_array[m];
				}
		}
	}
}


//##############extra elements required for bloom sampling########################//

/*
* INPUT : counting bloom filter
* WORK	: counts the total no. of 1s present in the bloom filter
* OUTPUT: long count for the no. of ones
* NOTE 	: makes call to the count_ones fn for each element of the array
*/
UINT dyn_num_ones(dyn_bloom *dbf)
{
	UINT count = 0;
	int i;
	for (i = 0; i <= dbf->currNum; ++i)
	{
		count += num_ones(dbf->bf_array[i]);
	}
	return count;
}


/*
* INPUT : counting bloom filter
* WORK	: counts the total no. of elements in the bloom filter
* OUTPUT: long count for the no. of ones
* NOTE 	: makes call to the num_ones fn and assumes no bucket overflow happens and just 
* divides by k.
*/
UINT dyn_count_elems(dyn_bloom *dbf)
{
	ULONG count = dyn_num_ones(dbf)/bloomParam->K;
	// printf("count :: %lu\n",count );
	return count;
}


/*
* INPUT : two counting bloom filters
* WORK	: counts the no. of ones in the intersection of the given two bloom filters
* OUTPUT: count of ones common to both
* NOTE 	: -
*/
UINT dyn_numOnes_intersection(dyn_bloom *a, dyn_bloom *b)
{
	int i,j;
	UINT count = 0;
	for (i = 0; i <= a->currNum; ++i)
	{
		for (j = 0; j <= b->currNum; ++j)
		{
			count +=  numOnes_intersection(a->bf_array[i],b->bf_array[j]);
		}
	}
	return count;
}

/*
* INPUT : two counting bloom filters
* WORK	: gives the no. of common elements by using the formula for bloom filters
* OUTPUT: double for the no. of common elements
* NOTE 	: Just counting number of elements present by summing up counters in pairwise intersection
* 			of bloomFilters in 2 DBFs
*/
double dyn_elemsIntersection(dyn_bloom *a, dyn_bloom *b, int ta, int tb)
{
	double th = (double)dyn_numOnes_intersection(a,b);
	return th/bloomParam->K;
}


void dyn_bloom_test_suit1()
{
	dyn_bloom *a = (dyn_bloom*)malloc(sizeof(dyn_bloom));
	dyn_init(a);
	dyn_grow(a);
	UINT i;
	printf("bloom graph insert/member/delete test initiated\n");
	for(i = 0 ; i < bloomParam->TEST_RANGE ; i++ )
	{
		if(i%100==0)
			dyn_grow(a);
		UINT z = rand()%bloomParam->TEST_RANGE;
		dyn_insert(z,a);
		if(!dyn_is_in(z,a))
		{
			printf("bloom graph not functioning properly - inserted val :: %d not found\n",z);
			exit(0);
		}
		dyn_delete(z,a);
	}

	printf("count of elems :: %u  %u \n",dyn_count_elems(a),bloomParam->TEST_RANGE );
	dyn_free_bloom(a);
	free(a);
	printf("bloom graph insert/member/delete test successful\n");
}


void dyn_bloom_test_suit2()
{
	dyn_bloom *a = (dyn_bloom*)malloc(sizeof(dyn_bloom));
	dyn_bloom *b = (dyn_bloom*)malloc(sizeof(dyn_bloom));

	dyn_init(a);	

	dyn_init(b);

	UINT i;
	printf("bloom graph union/intersect test initiated with range %u\n",bloomParam->TEST_RANGE);
	UINT count_a = 0,count_b = 0;
	for( i = 0 ; i < bloomParam->TEST_RANGE ; i++ )
	{
		if(i%100==0 && i>0)
		{
			dyn_grow(a);
			dyn_grow(b);
			printf("curr a %u size of last chunk %u \n",a->currNum,count_elems(a->bf_array[b->currNum-1]) );
			printf("curr b %u size of last chunk %u \n",b->currNum,count_elems(a->bf_array[b->currNum-1]) );
		}
		if(i%2==0)
		{
			dyn_insert(i,a);
			if(!dyn_is_in(i,a))
				printf("element not found a %u\n",i);
			count_a++;
		}
		else
		{
			dyn_insert(i,b);
			if(!dyn_is_in(i,b))
				printf("element not found b  %u\n",i);
			count_b++;
		}
	}
	printf("insertion stage over\n");
	printf("size a :: %u actual:: %u\n",dyn_count_elems(a),count_a );	//the number of elements may be less as some counter may completely fill up
	printf("size b :: %u actual:: %u\n",dyn_count_elems(b),count_b );	//

	dyn_bloom * c = (dyn_bloom *)malloc(sizeof(dyn_bloom));
	dyn_init(c);
	dyn_intersect_bloom(a,b,c);
	printf("intersection done  growth :: %d\n",c->currNum+1);
	dyn_bloom * d = (dyn_bloom *)malloc(sizeof(dyn_bloom));
	dyn_init(d);
	dyn_union_bloom(b,a,d);
	printf("union done growth :: %d\n",d->currNum+1);

	// dyn_bloom * e = (dyn_bloom *)malloc(sizeof(dyn_bloom));
	// dyn_init(e);
	// dyn_add_bloom(a,b,e);
	// printf("add done growth :: %d\n",e->currNum);



	printf("union/intersection operations executed -- size union size::%u intersect::%u \n",dyn_count_elems(d),dyn_count_elems(c));
	printf("intersection count via-- current elemsIntersection::%f \n",dyn_elemsIntersection(a,b,dyn_num_ones(a),dyn_num_ones(b)));

	ULONG con_union = 0;
	ULONG con_inter = 0;
	for( i = 0 ; i < bloomParam->TEST_RANGE ; i++ )
	{

		if(!dyn_is_in(i,d))
		{
			// printf("\tunion not working correctly %d\n",i);
			con_union++;
		}
		if(dyn_is_in(i,c)==1)
		{
			// printf("\tintersect conflict found - %d\n",i);
			con_inter++;
		}
	}
	printf("errors_union :: %llu errors_intersect :: %llu \n",con_union,con_inter );
	dyn_free_bloom(a);
	dyn_free_bloom(b);
	dyn_free_bloom(c);
	// dyn_free_bloom(e);
	free(a);
	free(b);
	free(c);
	// free(e);
	printf("bloom graph union/intersect test successful\n");
}

void print_bloom_filter(dyn_bloom * da)
{
	int i,j,k;
	for (k = 0; k <= da->currNum; ++k)
	{
		bloom* a = da->bf_array[k];	
		for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
		{
			for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
			{
				UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
				printf("%0*d ",2,(a->flag_array[i]&new)>>(j*bloomParam->COUNTER_CHUNK));
			}
		}
		printf("\n");
	}
}

// Reads data from file and inserts them in the bloom filter
void read_and_insert(char * filename, dyn_bloom* filter)
{
	FILE * reader;
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,filename);
	reader = fopen(fullFilename,"r");
	UINT i,num_elems = 0,namespace = 1;
	fscanf(reader,"%d",&num_elems);
	fscanf(reader,"%d",&namespace);
	for(i = 0 ; i < num_elems ; i++)
	{
		int elem = 0;
		fscanf(reader,"%d",&elem);
		dyn_insert(elem,filter);
	}
	fclose(reader);
}

// Estimates memory usage by bloom filter for different set of points
void test_memory_usage(char** filenameList,int numFiles)
{
	// bloomParam->SIZE_BLOOM 		= bloomSize;
	// bloomParam->K 				= numHashFunc;
	// bloomParam->COUNTER_CHUNK	= ctrSize;
	// bloomParam->FILL_THRESHOLD 	= threshold;

	FILE* logger ;
	
	if( access( "../logs/dyn_bloom_individual.txt", F_OK ) == -1 ) 
	{
 		logger = fopen("../logs/dyn_bloom_individual.txt","w");
		fprintf(logger,  "%s\n","File A,File B,Namespace,hash,size,CtrSize,False_Pstv_Prob,fillThresh,numA,numBf,memA,fpA,numB,numBf,memB,fpB,No. of Elems (A union B),numBfUnion,memUnion, fpUnion,numIntersect,numBfIntersect ,memIntersect, fpIntersect,NULL");
	}
	else
	{
 		logger = fopen("../logs/dyn_bloom_individual.txt","a");
	}


	int i;
	for (i = 0; i < numFiles; ++i)
	{
		dyn_bloom* filter = (dyn_bloom*)malloc(sizeof(dyn_bloom));
		dyn_init(filter);
		read_and_insert(filenameList[i],filter);
		char fullFilename[1000];
		sprintf(fullFilename,"%s%s",datasetPath,filenameList[i]);
		FILE * reader = fopen(fullFilename,"r");
		int num_elems,namespace;
		fscanf(reader,"%d",&num_elems);
		fscanf(reader,"%d",&namespace);
		fclose(reader);

		
		fprintf(logger, "%s,",filenameList[i]);
		fprintf(logger, "%d,",num_elems);
		fprintf(logger, "%d,",namespace);
		fprintf(logger, "%d,",bloomParam->K);
		fprintf(logger, "%d,",bloomParam->SIZE_BLOOM/8);
		fprintf(logger, "%d,",bloomParam->COUNTER_CHUNK);
		fprintf(logger, "%.5f,",bloomParam->FALSE_PSTV_PROB);
		fprintf(logger, "%.3f,",bloomParam->FILL_THRESHOLD);
		fprintf(logger, "%d,",filter->currNum+1);

		// Need to take more things into account for this expression like globals and arrays in dyn_bloom
		int memUsage = bloomParam->SIZE_BLOOM*(filter->currNum+1);
		fprintf(logger, "%d,",memUsage/8 );
		estimate_false_positive(filenameList[i],filter,logger);
		fprintf(logger, "NULL\n");

	}
	fclose(logger);

}

// Takes a bloom filter and estimates number of false positives in it.
void estimate_false_positive(char* filename,dyn_bloom* filter, FILE* logger)
{
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,filename);
	FILE * reader = fopen(fullFilename,"r");
	int num_elems,namespace;
	fscanf(reader,"%d",&num_elems);
	fscanf(reader,"%d",&namespace);
	fclose(reader);
	int i;
	int numPresent = 0;
	for (i = 1; i <= namespace; ++i)
	{
		if(dyn_is_in(i,filter))
		{
			numPresent++;
		}
	}
	// fprintf(logger, "%d,",numPresent - num_elems);
	fprintf(logger, "%.7f,",(numPresent - num_elems)/(float)namespace);
}

void estimate_false_positive_union(char* filename,dyn_bloom* filter, FILE* logger)
{
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,filename);
	FILE * reader = fopen(fullFilename,"r");
	int num_elems,namespace;
	fscanf(reader,"%d",&num_elems);
	fscanf(reader,"%d",&namespace);
	// fclose(reader);
	int i;
	int numPresent = 0;
	for (i = 1; i <= namespace; ++i)
	{
		if(dyn_is_in(i,filter))
		{
			numPresent++;
		}
	}
	int false_negative=0;
	int elem;
	for (i = 0; i < num_elems; ++i)
	{
		fscanf(reader,"%d",&elem);
		if(!dyn_is_in(elem,filter))
			false_negative++;
	}
	// fprintf(logger, "%d,",numPresent - num_elems);
	fprintf(logger, "%.7f,",(numPresent - num_elems + false_negative)/(float)namespace);
}

void estimate_false_positive_intersect(char* filename,dyn_bloom* filter, FILE* logger)
{
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,filename);
	FILE * reader = fopen(fullFilename,"r");
	int num_elems,namespace;
	fscanf(reader,"%d",&num_elems);
	fscanf(reader,"%d",&namespace);
	// fclose(reader);
	int i;
	int numPresent = 0;
	for (i = 1; i <= namespace; ++i)
	{
		if(dyn_is_in(i,filter))
		{
			numPresent++;
		}
	}

	int false_negative=0;
	int elem;
	for (i = 0; i < num_elems; ++i)
	{
		fscanf(reader,"%d",&elem);
		if(!dyn_is_in(elem,filter))
			false_negative++;
	}


	// fprintf(logger, "%d,",numPresent - num_elems);
	fprintf(logger, "%.7f,",(numPresent - num_elems + false_negative)/(float)namespace);
}

// Runs test for fixed size of A and B, but varying size of overlap from 0.1 to 0.9
// For each (sizeA,sizeB) pair, there are 100 samples
// This function generates log file for graph where we plot FPRatio vs overlap between A nd B
void test_intersection_overlap(int NumElemsA, int NumElemsB, char* logFileName)
{
	FILE* logger;
	// if( access( logFileName, F_OK ) == -1 ) 
	// {
	// 	logger = fopen(logFileName,"w");
	// 	fprintf(logger,  "%s\n","FileA,FileB,Namespace,NumHashFunc,UnitSize,FalsePstvProb,FillThreshold,NumElemsA,NumElemsB,NumElemsResult,NumUnitBFs,EffMemUsage,FP_Ratio,NULL");
	// }
	// else
	// {
	// 	logger = fopen(logFileName,"a");
	// }

	logger = fopen(logFileName,"w");
	fprintf(logger,  "%s\n","FileA,FileB,Overlap,Operation,Namespace,NumHashFunc,UnitSize,FalsePstvProb,FillThreshold,NumElemsA,NumElemsB,NumElemsResult,NumUnitBFs,EffMemUsage,FP_Ratio,NULL");

	float maxOverlap = 1.0;
	int numSamples  = 10;
	float overlap;
	int sampleIter;
	for (overlap = 0.1; overlap < maxOverlap; overlap += 0.1)
	{
		for (sampleIter = 0; sampleIter < numSamples; ++sampleIter)
		{
			char fileA[1000];
			char fileB[1000];
			char intersect_filename[1000];
			sprintf(fileA	,"rand_%d_%d_A_%.1f_%d", NumElemsA, NumElemsB, overlap, sampleIter);
			sprintf(fileB	,"rand_%d_%d_B_%.1f_%d", NumElemsA, NumElemsB, overlap, sampleIter);
			sprintf(intersect_filename	,"rand_%d_%d_inter_%.1f_%d",NumElemsA, NumElemsB, overlap, sampleIter);
			// printf("A::%s\n",fileA );
			// printf("B::%s\n",fileB );
			// printf("I::%s\n",intersect_filename );
			dyn_bloom* filter_1 		= (dyn_bloom*)malloc(sizeof(dyn_bloom));
			dyn_bloom* filter_2 		= (dyn_bloom*)malloc(sizeof(dyn_bloom));
			dyn_bloom* filter_intersect = (dyn_bloom*)malloc(sizeof(dyn_bloom));
			dyn_init(filter_1);
			dyn_init(filter_2);
			dyn_init(filter_intersect);
		
			FILE * reader;
			int namespace;
			int num_elems_intersect;

			char *fullFilename = (char*)malloc(1000*sizeof(char));
			sprintf(fullFilename,"%s%s",datasetPath,intersect_filename);
			reader = fopen(fullFilename,"r");
			// printf("Full filename::%s\n",fullFilename);
			fscanf(reader,"%d",&num_elems_intersect);
			fscanf(reader,"%d",&namespace);
			fclose(reader);
			free(fullFilename);

			fprintf(logger, "%s,", fileA);
			fprintf(logger, "%s,", fileB);
			fprintf(logger, "%.1f,",overlap);
			fprintf(logger, "Intersection,");

			fprintf(logger, "%d,",namespace);
			fprintf(logger, "%d,",bloomParam->K);
			fprintf(logger, "%d,",bloomParam->SIZE_BLOOM);
			fprintf(logger, "%.5f,",bloomParam->FALSE_PSTV_PROB);
			fprintf(logger, "%.3f,",bloomParam->FILL_THRESHOLD);

			fprintf(logger, "%d,",NumElemsA);
			fprintf(logger, "%d,",NumElemsB);
			fprintf(logger, "%d,",num_elems_intersect);
			
			read_and_insert(fileA,filter_1);
			read_and_insert(fileB,filter_2);
			dyn_intersect_bloom(filter_1,filter_2,filter_intersect);

			// Details for set (A intersect B)
			fprintf(logger, "%d,",filter_intersect->currNum + 1);
			fprintf(logger, "%d,",bloomParam->SIZE_BLOOM*(filter_intersect->currNum + 1));
			estimate_false_positive_intersect(intersect_filename,filter_intersect,logger);

			fprintf(logger, "NULL\n");
			dyn_free_bloom(filter_1);
			dyn_free_bloom(filter_2);
			dyn_free_bloom(filter_intersect);
			free(filter_1);
			free(filter_2);
			free(filter_intersect);
		}
	}
	fclose(logger);
}

// Runs test for fixed size of A and B, but varying size of overlap from 0.1 to 0.9
// For each (sizeA,sizeB) pair, there are 100 samples
// This function generates log file for graph where we plot FPRatio vs overlap between A nd B
void test_union_overlap(int NumElemsA, int NumElemsB, char* logFileName)
{
	FILE* logger;
	// if( access( logFileName, F_OK ) == -1 ) 
	// {
	// 	logger = fopen(logFileName,"w");
	// 	fprintf(logger,  "%s\n","FileA,FileB,Namespace,NumHashFunc,UnitSize,FalsePstvProb,FillThreshold,NumElemsA,NumElemsB,NumElemsResult,NumUnitBFs,EffMemUsage,FP_Ratio,NULL");
	// }
	// else
	// {
	// 	logger = fopen(logFileName,"a");
	// }

	logger = fopen(logFileName,"w");
	fprintf(logger,  "%s\n","FileA,FileB,Overlap,Operation,Namespace,NumHashFunc,UnitSize,FalsePstvProb,FillThreshold,NumElemsA,NumElemsB,NumElemsResult,NumUnitBFs,EffMemUsage,FP_Ratio,NULL");

	float maxOverlap 	= 1.0;
	int numSamples  	= 10;
	float overlap;
	int sampleIter;
	for (overlap = 0.1; overlap < maxOverlap; overlap += 0.1)
	{
		for (sampleIter = 0; sampleIter < numSamples; ++sampleIter)
		{
			char fileA[1000];
			char fileB[1000];
			char union_filename[1000];
			sprintf(fileA	,"rand_%d_%d_A_%.1f_%d", NumElemsA, NumElemsB, overlap, sampleIter);
			sprintf(fileB	,"rand_%d_%d_B_%.1f_%d", NumElemsA, NumElemsB, overlap, sampleIter);
			sprintf(union_filename	,"rand_%d_%d_union_%.1f_%d",NumElemsA, NumElemsB, overlap, sampleIter);
			dyn_bloom* filter_1 	= (dyn_bloom*)malloc(sizeof(dyn_bloom));
			dyn_bloom* filter_2 	= (dyn_bloom*)malloc(sizeof(dyn_bloom));
			dyn_bloom* filter_union = (dyn_bloom*)malloc(sizeof(dyn_bloom));
			dyn_init(filter_1);
			dyn_init(filter_2);
			dyn_init(filter_union);
		
			FILE * reader;
			int namespace;
			int num_elems_union;

			char *fullFilename = (char*)malloc(1000*sizeof(char));
			sprintf(fullFilename,"%s%s",datasetPath,union_filename);
			printf("union_filename::%s:::::\n",union_filename );
			printf("full_filename::%s:::::::\n",fullFilename );
			reader = fopen(fullFilename,"r");
			if (reader == NULL)
			{
				printf("1/0\n");
			}
			fscanf(reader,"%d",&num_elems_union);
			fscanf(reader,"%d",&namespace);
			fclose(reader);
			printf("Read union file\n");
			free(fullFilename);

			fprintf(logger, "%s,", fileA);
			fprintf(logger, "%s,", fileB);
			fprintf(logger, "%.1f,",overlap);
			fprintf(logger, "Union,");

			fprintf(logger, "%d,",namespace);
			fprintf(logger, "%d,",bloomParam->K);
			fprintf(logger, "%d,",bloomParam->SIZE_BLOOM);
			fprintf(logger, "%.5f,",bloomParam->FALSE_PSTV_PROB);
			fprintf(logger, "%.3f,",bloomParam->FILL_THRESHOLD);

			fprintf(logger, "%d,",NumElemsA);
			fprintf(logger, "%d,",NumElemsB);
			fprintf(logger, "%d,",num_elems_union);
			
			read_and_insert(fileA,filter_1);
			read_and_insert(fileB,filter_2);
			dyn_union_bloom(filter_1,filter_2,filter_union);

			// Details for set (A intersect B)
			fprintf(logger, "%d,",filter_union->currNum + 1);
			fprintf(logger, "%d,",bloomParam->SIZE_BLOOM*(filter_union->currNum + 1));
			estimate_false_positive_union(union_filename,filter_union,logger);

			fprintf(logger, "NULL\n");
			dyn_free_bloom(filter_1);
			dyn_free_bloom(filter_2);
			dyn_free_bloom(filter_union);
			free(filter_1);
			free(filter_2);
			free(filter_union);
		}
	}
	fclose(logger);
}

void run_memory_tests()
{	
	int i,j,k;
	int numFiles = 5;
	char** filenameList = (char**)malloc(numFiles*sizeof(char*));
	for (i = 0; i < numFiles; ++i)
	{
		filenameList[i] = (char*)malloc(sizeof(char)*100);
	}
	strcpy(filenameList[0],"rand::10");
	strcpy(filenameList[1],"rand::100");
	strcpy(filenameList[2],"rand::500");
	strcpy(filenameList[3],"rand::1000");
	strcpy(filenameList[4],"rand::2000");
	
	int num_K_Vals 				= 2;
	int num_FalsePstvProbVals 	= 2;
	int num_filterSizeVals 		= 2;

	int K_Vals[2] 				= {3,5};
	float FalsePstvProbVals[2]	= {0.0001,0.01};
	int filterSizeVals[2] 		= {1024,2048};//,2048,4000,10000,20000,40000};

	for (i = 0; i < num_filterSizeVals; ++i)
	{
		for (j = 0; j < num_K_Vals; ++j)
		{
			for (k = 0; k < num_FalsePstvProbVals; ++k)
			{
				printf("Running test for K = %d, FalsePstvProbVals = %.3f, FilterSize = %d\n",K_Vals[j], FalsePstvProbVals[k], filterSizeVals[i]);
				init_bloomParameters(filterSizeVals[i],K_Vals[j],2);
				init_dyn_parameters(1,FalsePstvProbVals[k]); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
				seiveInitial(); // Important to initialize it here to reset m[i] values to suit the changed values of SIZE_BLOOM

				test_memory_usage(filenameList,numFiles);				
			}
		}
	}
	
	// init_bloomParameters(10240,3,2); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
	// init_dyn_parameters(1,0.5);
	// seiveInitial(); // Important to initialize it here to reset m[i] values to suit the changed values of SIZE_BLOOM

	// test_memory_usage(filenameList,2);
}



void test_memory_usage_unitSize(char** filenameList,int numFiles,char* logFileName)
{
	FILE* logger ;
	// if( access( logFileName, F_OK ) == -1 ) 
	// {
	// 	// file does not exist
	// 	logger = fopen(logFileName,"w");
	// 	fprintf(logger, "%s","FileName,NumElems,UnitSize,NumUnitBFs,EffMemUsage,FP_Ratio,NULL");
	// }
	// else
	// {
	// 	logger = fopen(logFileName,"a");
	// }

	logger = fopen(logFileName,"w");
	fprintf(logger, "%s","FileName,NumElems,UnitSize,NumUnitBFs,EffMemUsage,FP_Ratio,NULL");

	int i;
	for (i = 0; i < numFiles; ++i)
	{
		fprintf(logger, "\n");
		dyn_bloom * filter = (dyn_bloom*)malloc(sizeof(dyn_bloom));
		dyn_init(filter);
		read_and_insert(filenameList[i],filter);

		char fullFilename[1000];
		sprintf(fullFilename,"%s%s",datasetPath,filenameList[i]);
		FILE * reader = fopen(fullFilename,"r");
		int num_elems,namespace;
		fscanf(reader,"%d",&num_elems);
		fscanf(reader,"%d",&namespace);
		fclose(reader);

		fprintf(logger, "%s,",filenameList[i]);
		fprintf(logger, "%d,",num_elems);
		fprintf(logger, "%d,",bloomParam->SIZE_BLOOM);
		fprintf(logger, "%d,",(filter->currNum + 1));
		fprintf(logger, "%d,",(filter->currNum + 1)*(bloomParam->SIZE_BLOOM));
		estimate_false_positive(filenameList[i],filter,logger);
		fprintf(logger, "NULL");
	}
	fclose(logger);
}

void run_unit_size_analysis()
{
	// Fixed parameters
	// False positive probability( fixed n_threshold for bloom filter and depth of bloomTree), 
	// Namespace size & number of hash functions

	mkdir("../logs/SetCreationUnitSizeAnalysis_DynBloom",S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	UINT K = 3;
	UINT COUNTER_SIZE = 1;
	float FALSE_PSTV_PROB = 0.001;
	UINT M = 100000;

	int unitSizeVals[2] 		= {1024,2048};//,2048,4000,10000,20000,40000};
	int numUnitSize 			= 2;
	int i;

	int numFiles = 0;
	char** filenameList, *logFileName;
	char fullFilename[1000];
	FILE* reader;
	filenameList 	= (char**)malloc(1000*sizeof(char*));
	
	sprintf(fullFilename,"%s%s",datasetPath,"filenameList");
	reader = fopen(fullFilename,"r");
	
	while(!feof(reader))
	{
		filenameList[numFiles] = (char*)malloc(200*sizeof(char));
		fscanf(reader,"%s\n",filenameList[numFiles]);
		numFiles += 1;
	}

	for (i = 0; i < numUnitSize; ++i)
	{
		UINT FILL_THRESHOLD  = -1*((int)(unitSizeVals[i])/((float)K*COUNTER_SIZE))*log( 1 - pow(FALSE_PSTV_PROB,1.0/K));
		UINT NUM_PARTITION   = (UINT)(log2(ceil(M*1.0/FILL_THRESHOLD)));
		logFileName 	= (char*)malloc(1000*sizeof(char));
		sprintf(logFileName,"../logs/SetCreationUnitSizeAnalysis_DynBloom/%d.csv",unitSizeVals[i]);
		// printf("logFileName::%s\n",logFileName);
		// printf("FILL_THRESHOLD::%d\n",FILL_THRESHOLD);
		// printf("NUM_PARTITION::%d\n",NUM_PARTITION);

		init_bloomParameters(unitSizeVals[i],K,COUNTER_SIZE);
		init_dyn_parameters(1,FALSE_PSTV_PROB); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
		seiveInitial(); // Important to initialize it here to reset m[i] values to suit the changed values of SIZE_BLOOM

		test_memory_usage_unitSize(filenameList,numFiles,logFileName);
		free(logFileName);
	}
}

void run_setOps_overlap()
{
	int setSizes[6] = {10,100,200,500,1000,2000};
	int numSetSizes = 6;
	mkdir("../logs/intersection_dynBloom_overlap",S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	mkdir("../logs/union_dynBloom_overlap",S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	UINT K 				= 3;
	UINT COUNTER_SIZE 	= 1;
	UINT M 				= 100000;
	int unitSize 		= 1024;
	float FALSE_PSTV_PROB = 0.001;
	int i,j;
	for (i = 0; i < numSetSizes; ++i)
	{
		for (j = i; j < numSetSizes; ++j)
		{
			printf("Running for i,j = %d,%d\n",i,j);
			init_bloomParameters(unitSize,K,COUNTER_SIZE);
			init_dyn_parameters(1,FALSE_PSTV_PROB); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
			seiveInitial(); // Important to initialize it here to reset m[i] values to suit the changed values of SIZE_BLOOM

			char logFileName[1000];
			sprintf(logFileName,"../logs/intersection_dynBloom_overlap/%d_%d.csv",setSizes[i],setSizes[j]);
			test_intersection_overlap(setSizes[i],setSizes[j],logFileName);

			char logFileName_2[1000];
			sprintf(logFileName_2,"../logs/union_dynBloom_overlap/%d_%d.csv",setSizes[i],setSizes[j]);
			test_union_overlap(setSizes[i],setSizes[j],logFileName_2);
		}
	}
}

int main()
{
	datasetPath = (char*)malloc(1000*sizeof(char));
	strcpy(datasetPath,"../datasets/");

	time_t t;
	srand((unsigned) time(&t));
	
	init_bloomParameters(1024,3,2);
	init_dyn_parameters(1,0.001); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
	seiveInitial(); // Important to initialize it here to reset m[i] values to suit the changed values of SIZE_BLOOM

	dyn_bloom_test_suit1();
	dyn_bloom_test_suit2();
	// run_unit_size_analysis();
	// run_setOps_overlap();

	// run_memory_tests();
	// run_set_operation_tests();
}