import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

data = pd.read_csv('dyn_bloom_set_ops.txt')
# print len(data['hash'])
directory = "dynamic_bloom"


# print data.keys()
# print len(data[data.keys()[0]])
# print data[" fpIntersect"]
def draw_graph(name,X,Y_set,title,legends):
	print X
	print Y_set
	fig = plt.figure(figsize=(12, 9), dpi=80);
	ax = fig.add_subplot(111)

	for i in range(len(Y_set)):
		plt.plot(X,Y_set[i],marker='o',ls='-');
		for xy in zip(X, Y_set[i]):                                       # <--
	  	  ax.annotate('(%s, %s)' % xy, xy=xy, textcoords='data') # <--
	plt.legend(legends, loc='upper right')
	plt.suptitle(title);
	plt.xlabel('Number of elements inserted');
	plt.ylabel('Ratio of false positives to set size');
	fig.savefig(directory+"/"+name+".png")

def draw_graph_mem(name,X,Y_set,title,legends):
	print X
	print Y_set
	fig = plt.figure(figsize=(12, 9), dpi=80);
	ax = fig.add_subplot(111)

	for i in range(len(Y_set)):
		plt.plot(X,Y_set[i],marker='o',ls='-');
		for xy in zip(X, Y_set[i]):                                       # <--
	  	  ax.annotate('(%s, %s)' % xy, xy=xy, textcoords='data') # <--
	plt.legend(legends, loc='upper right')
	plt.suptitle(title);
	plt.xlabel('Number of elements inserted');
	plt.ylabel('Memory(B)');
	fig.savefig(directory+"/"+name+".png")


# ['Single bloom filter', 'Union with 10% overlapp', 'Intersection with 10% overlapp', 'Union with 50% overlapp',"Intersection with 50% overlapp"]

def make_graphs(data):
	data_len = len(data['hash']);
	sample_len = 10;
	i = 0;
	while(i<data_len):
		X = [];
		Y = [[],[],[],[],[],[],[],[],[],[],[],[]]			#A_0.1 AUB_0.1 A-B_0.1 AUB_0.5 A-B_0.5
		for j in range(i,i+10):
			if(j%2==0):					#0.1 elem
				X += [data["numA"][j]];
				Y[0] += [data["fpA"][j]];
				Y[1] += [data["memA"][j]];
				Y[2] += [data[" fpUnion"][j]];
				Y[3] += [data[" fpIntersect"][j]];
				Y[4] += [data[" fpUnion"][j+1]];
				Y[5] += [data[" fpIntersect"][j+1]];

				Y[6] += [data["fpA"][j+10]];
				Y[7] += [data["memA"][j+10]];
				Y[8] += [data[" fpUnion"][j+10]];
				Y[9] += [data[" fpIntersect"][j+10]];
				Y[10] += [data[" fpUnion"][j+11]];
				Y[11] += [data[" fpIntersect"][j+11]];
		name = str(data["size"][i])+"_"+str(data["hash"][i])+"_"
		draw_graph_mem(name+"memory_bf",X,[Y[1],Y[7]],str(data["size"][i])+" Bytes unit dynamic bloom filter with K = "+str(data["hash"][i]),['Memory with f.p.p 0.0001','Memory with f.p.p 0.01'])
		draw_graph(name+"bf",X,[Y[0],Y[6]],str(data["size"][i])+" Bytes unit dynamic  bloom filter with K = "+str(data["hash"][i]),['Single Bloom Filter with f.p.p 0.0001','Single Bloom Filter with f.p.p 0.01'])
		draw_graph(name+"union",X,[Y[2],Y[4],Y[8],Y[10]],str(data["size"][i])+" Bytes unit dynamic  bloom filter with K = "+str(data["hash"][i]),['Union with 10% overlapp with f.p.p 0.0001', 'Union with 50% overlapp with f.p.p 0.0001','Union with 10% overlapp with f.p.p 0.01', 'Union with 50% overlapp with f.p.p 0.01'])
		draw_graph(name+"intersect",X,[Y[3],Y[5],Y[9],Y[11]],str(data["size"][i])+" Bytes unit dynamic  bloom filter with K = "+str(data["hash"][i]),['Intersection with 10% overlapp with f.p.p 0.0001',"Intersection with 50% overlapp with f.p.p 0.0001",'Intersection with 10% overlapp with f.p.p 0.01',"Intersection with 50% overlapp with f.p.p 0.01"])
		print "new set starts"
		i+=20;





make_graphs(data)

# def make_graphs(data):
# 	data_len = len(data['hash']);
# 	sample_len = 10;
# 	i = 0;
# 	while(i<data_len):
# 		X = [];
# 		Y = [[],[],[],[],[],[]]			#A_0.1 AUB_0.1 A-B_0.1 AUB_0.5 A-B_0.5
# 		for j in range(i,i+10):
# 			if(j%2==0):					#0.1 elem
# 				X += [data["numA"][j]];
# 				Y[0] += [data["fpA"][j]];
# 				Y[1] += [data["memA"][j]];
# 				Y[2] += [data[" fpUnion"][j]];
# 				Y[3] += [data[" fpIntersect"][j]];
# 				Y[4] += [data[" fpUnion"][j+1]];
# 				Y[5] += [data[" fpIntersect"][j+1]];
# 		name = str(data["size"][i])+"_"+str(data["hash"][i])+"_"+str(data["False_Pstv_Prob"][i])+"_"
# 		draw_graph_mem(name+"memory_bf",X,[Y[1]],str(data["size"][i])+"Bytes unit dynamic bloom filter with K = "+str(data["hash"][i])+" and threshold for false positive probability = "+str(data["False_Pstv_Prob"][i]),['Memory'])
# 		draw_graph(name+"bf",X,[Y[0]],str(data["size"][i])+"Bytes unit dynamic bloom filter with K = "+str(data["hash"][i]),['Single Bloom Filter'])
# 		draw_graph(name+"union",X,[Y[2],Y[4]],str(data["size"][i])+"Bytes unit dynamic bloom filter with K = "+str(data["hash"][i]),['Union with 10% overlapp', 'Union with 50% overlapp'])
# 		draw_graph(name+"intersect",X,[Y[3],Y[5]],str(data["size"][i])+"Bytes unit dynamic bloom filter with K = "+str(data["hash"][i]),['Intersection with 10% overlapp',"Intersection with 50% overlapp"])
# 		print "new set starts"
# 		i+=10;