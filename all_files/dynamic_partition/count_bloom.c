#include "count_bloom.h"
#include "dynamic_array/array.h"
#include "bloom_tree.h"
/*
tree based dynamic bloom filter --
* New components:
* Hash fn -- K level , level as argument
* partition fn -- grows a bloom filter into two based on which level it is
* structure of the dynamic_partition bloom filter
* Binary Tree of bloom filters with a pointer based structure
* Each leaf consists of a bloom filter
* rest is easy to handle
*/



int seed[10];
int aHash[10];//={3730, 9830, 3830, 2110, 4740, 9440, 5540, 2710, 690, 7470};
int bHash[10];//={269, 229, 929, 949, 624, 390, 64, 67, 945, 253};
int m[10];

int mPrime[10];

bloomParameters* bloomParam;
struct bloomTree* bloom_tree;
char* datasetPath;

void init_bloomParameters(UINT SIZE_BLOOM ,UINT K ,UINT COUNTER_CHUNK,UINT NUM_PARTITION,float FALSE_PSTV_PROB)
{
	bloomParam = (bloomParameters*)malloc(sizeof(bloomParameters));
	bloomParam->NUM_BITS     	= 8*sizeof(UINT);
	bloomParam->SIZE_BLOOM   	= SIZE_BLOOM;
	bloomParam->K 			  	= K;
	bloomParam->COUNTER_CHUNK  	= COUNTER_CHUNK;		//should be exponents of 2
	bloomParam->BIT_MAP 		= pow(2,COUNTER_CHUNK) -1 ;	//2^COUNTER_CHUNK - 1;

	bloomParam->FALSE_PSTV_PROB = FALSE_PSTV_PROB;
	bloomParam->FILL_THRESHOLD  = -1*((int)SIZE_BLOOM/((float)K*COUNTER_CHUNK))*log( 1 - pow(FALSE_PSTV_PROB,1.0/K));
	
	printf("FILL_THRESHOLD::%f\n",bloomParam->FILL_THRESHOLD);
	printf("FALSE_PSTV_PROB::%f\n",bloomParam->FALSE_PSTV_PROB);
	bloomParam->NUM_PARTITION	= NUM_PARTITION;
	bloomParam->TEST_RANGE 		= 500;
}

void print_bin(UINT n) 
{
  char buf[CHAR_BIT * sizeof n + 1];
  char *p = &buf[sizeof buf - 1];
  *p = '\0';

  int i = n;
  if (i > 0) {
    i = -i;
  }

  do {
    p--;
    *p = '0' - i%2;
    i /= 2;
  } while (i);

  if (n < 0) {
    p--;
    *p = '-';
  }
 
  puts(p);
}

void seiveInitial()
{
	int num_bits = bloomParam->SIZE_BLOOM/bloomParam->COUNTER_CHUNK;
	int *x = (int*)malloc(sizeof(int)*num_bits),i,j;
	for (i = 0; i < 10; ++i)
	{
		aHash[i] = rand() % num_bits;
		bHash[i] = rand() % num_bits;
	}

	for (i=0;i<num_bits;i++) 
		x[i] = i+1;
	for (i=1;i<num_bits;i++)
	{
		j = x[i];
		if (j==-1) 
			continue;

		int q = 1;
		while ((i + q*j) < num_bits)
		{
			x[i+q*j] = -1;
			q++;
		}
	}
	i = num_bits;
	for (j=0;j<10;j++)
	{
		for (i=i-1;i>0;i--)
		{
			if (x[i]!=-1)
			{
				m[j] = x[i];
				break;
			}
		}
	}
}

/*
* returns the value of the hash function based on the level
* provided as input to the function
*/ 
int partition_hash( UINT val, UINT level )
{
	switch(level)
	{
		case 0:
			return val%2;				//hash functions to be modified later
		case 1:
			return (val%7)%2;
		case 2:
			return (val%13)%2;
		case 3:
			return (val%17)%2;
		case 4:
			return (val%19)%2;
		case 5:
			return (val%23)%2;
		case 6:
			return (val%29)%2;
		case 7:
			return (val%31)%2;
		case 8:
			return (val%37)%2;
		case 9:
			return (val%43)%2;
	}

	printf("Value of level is larger than what we can handle\n");
	exit(0);
	return -1;
}

void init_node(bloom *bl)
{
	int size = (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);
	bl->flag_array = (UINT *)malloc(sizeof(UINT)*size);
	memset(bl->flag_array,0,sizeof(UINT)*size);
	bl->num_elems = 0;
}

/*
* initializes a single b_node to act as root of the tree
* call only for leaves as otherwise the bloom filters would get unnecessarily initialized
*/
void init(b_node *bloomf)
{
	bloomf->bf = (bloom *)malloc(sizeof(bloom *));
	init_node(bloomf->bf);								//call only for leaves
	bloomf->left = NULL;
	bloomf->right = NULL;
}

void init_without_bloom(b_node * a)
{
	a->bf = NULL;
	a->left = NULL;
	a->right = NULL;
}

/*
* splits a bloom filter based on the level on which it is present
* the level decides the hash function to use to split
*/
void partition_node(b_node *node,UINT level)
{
	// printf("size before reconstruction stage 0 %d\n",count_elems(node) );
	if(node->left!=NULL)
	{
		printf("only leaves can be partitioned\n");
		return;
	}
	my_array_long* reconstruct_array = (my_array_long *)malloc(sizeof(my_array_long));
	arrayLong_init(reconstruct_array);

	UINT size_array = 0;
	reconstructSet( bloom_tree ,node->bf, &reconstruct_array, &size_array);

	node->left  =  (b_node *)malloc(sizeof(b_node *));
	node->right =  (b_node *)malloc(sizeof(b_node *));
	init(node->left);
	init(node->right);
	// printf("size before reconstruction stage 1 %d to be inserted %d\n",count_elems_node(node->bf),size_array );
	free_bloom_node(node->bf);				//free the current bloom filter

	node->bf = NULL;

	UINT i = 0;
	for( i = 0 ; i < size_array ; i++)
	{
		UINT elem = reconstruct_array->elements[i];
		UINT bin = partition_hash(elem,level);
		if(bin<0)
		{
			printf("hash function not defined for the level\n");
			exit(0);
		}
		if(bin==0)
			insert_node(elem,node->left->bf);
		else
			insert_node(elem,node->right->bf);
	}
	arrayLong_free(reconstruct_array);
}

void insert_node(UINT val, bloom* bl)
{
	UINT i;
	UINT chunk_per_ind = bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK;
	bl->num_elems++;
	for (i=0;i<bloomParam->K;i++)
	{
		UINT a = hash(val,i);
		UINT loc = a/chunk_per_ind;
		UINT off = a%chunk_per_ind;

		UINT new = 1<<(off*bloomParam->COUNTER_CHUNK);
		UINT bitmap = (bloomParam->BIT_MAP<<(off*bloomParam->COUNTER_CHUNK));
		UINT chunk = bitmap & bl->flag_array[loc];
		if(chunk==bitmap)
		{
			// printf("overflow of the bucket for counter -- making no change\n");
			continue;
		}
		bl->flag_array[loc] = bl->flag_array[loc] + new;
	}
}

/*
*	traverses the tree and inserts at the right node
*/
void insert(UINT val, b_node * bloomf)
{
	int not_found 		= 1;
	b_node * curr_node  = bloomf;
	UINT curr_level 	= 0;


	while(not_found)
	{
		if(curr_node->left==NULL)				//identified as leaf
		{				
			insert_node(val, curr_node->bf);
			// if( (curr_node->bf->num_elems)*(bloomParam->K) > bloomParam->FILL_THRESHOLD*bloomParam->SIZE_BLOOM/bloomParam->COUNTER_CHUNK )
			if( curr_node->bf->num_elems > bloomParam->FILL_THRESHOLD )
			{
				partition_node(curr_node,curr_level);
				// printf("growing in insertion %d inserted number %d\n",curr_level,val );
			}
			not_found = 0;
		}
		else
		{
			if(partition_hash(val,curr_level)==0)
				curr_node = curr_node->left;
			else
				curr_node = curr_node->right;
			curr_level++;
		}
	}
}

void delete_node(UINT val, bloom* bl)
{
	UINT i;
	UINT chunk_per_ind = bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK;
	if(!is_in_node(val,bl))
	{
		return;
	}
	if(bl->num_elems>0)
		bl->num_elems--;

	for (i=0;i<bloomParam->K;i++)
	{
		UINT a = hash(val,i);
		UINT loc = a/chunk_per_ind;
		UINT off = a%chunk_per_ind;

		UINT new = 1<<(off*bloomParam->COUNTER_CHUNK);
		UINT bitmap = (bloomParam->BIT_MAP<<(off*bloomParam->COUNTER_CHUNK));
		UINT chunk = bitmap & bl->flag_array[loc];
		if(chunk>0)
		{
			bl->flag_array[loc] = bl->flag_array[loc] - new;
		}
	}
}

/*
*	traverses the tree and deletes at the right spot
*/
void delete(UINT val, b_node * bloomf)
{
	int not_found 		= 1;
	b_node * curr_node  = bloomf;
	UINT curr_level 	= 0;

	while(not_found)
	{
		if(curr_node->left==NULL)				//identified as leaf
		{
			if(is_in_node(val,curr_node->bf))
			{
				delete_node(val, curr_node->bf);
			}
			else
				return;							//element not present in the overall bloom filter
			not_found = 0;
		}
		else
		{
			if(partition_hash(val,curr_level)==0)
				curr_node = curr_node->left;
			else
				curr_node = curr_node->right;
			curr_level++;
		}
	}
}


UINT is_in_node(UINT val, bloom* bl)
{
	UINT i;
	UINT chunk_per_ind = bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK;
	for (i=0;i<bloomParam->K;i++){

		UINT a = hash(val,i);
		UINT loc = a/chunk_per_ind;
		UINT off = a%chunk_per_ind;

		UINT bitmap = (bloomParam->BIT_MAP<<(off*bloomParam->COUNTER_CHUNK));
		UINT chunk = bitmap & bl->flag_array[loc];
		if (!chunk) return 0;
	}
	return 1;
}

/*
*	traverses the tree and checks at the right spot
*/
UINT is_in(UINT val, b_node * bloomf)
{
	int not_found 		= 1;
	b_node * curr_node  = bloomf;
	UINT curr_level 	= 0;

	while(not_found)
	{
		if(curr_node->left==NULL)				//identified as leaf
		{
			if(is_in_node(val,curr_node->bf))
			{
				return 1;
			}
			else
				return 0;							//element not present in the overall bloom filter
			not_found = 0;
		}
		else
		{
			if(partition_hash(val,curr_level)==0)
				curr_node = curr_node->left;
			else
				curr_node = curr_node->right;
			curr_level++;
		}
	}
}

UINT hash(int a, int i)
{
	unsigned long z = aHash[i];
	z = z*a + bHash[i];
	UINT retVal = (z % m[i]);
	return retVal;
}

void free_bloom_node(bloom *bl)
{
	free(bl->flag_array);
	bl->num_elems = 0;
}

/*
* traverse the tree in a dfs fashion and free all memory
*/
void free_bloom(b_node * bloomf)
{
	if(bloomf == NULL)
		return;
	if(bloomf->left==NULL)
	{
		free_bloom_node(bloomf->bf);
		return;
	}
	else
	{
		free_bloom(bloomf->left);
		free_bloom(bloomf->right);
		free(bloomf);
	}
}

UINT * reconstruct_dict_attack(bloom* bl, UINT start, UINT end )
{
	UINT i;
	UINT num_elems,elem = 0;
	for(i = start; i <= end; i++)
	{
		if(is_in_node(i,bl))
			num_elems++;
	}

	UINT * val_array = (UINT *)malloc(sizeof(UINT)*num_elems);
	
	memset(val_array, 0, sizeof(UINT)*num_elems);
	for(i = start; i <= end; i++)
	{
		if(is_in_node(i,bl))
		{
			val_array[elem] = i;
			elem++;
		}
	}
	return val_array;
}

//slightly inefficient as twice time taken for now but for complete reconstruction use this 
void reconstruct_complete(b_node* a,my_array_long * return_array)	
{
	if(a->left==NULL)
	{
		my_array_long* reconstruct_array;
		UINT size_array,i = 0;
		reconstructSet( bloom_tree ,a->bf, &reconstruct_array, &size_array);
		for(i = 0 ; i < size_array ; i++)
		{
			arrayLong_addElement(return_array, reconstruct_array->elements[i]);
		}
		arrayLong_free(reconstruct_array);
	}
	else
	{
		reconstruct_complete(a->left,return_array);
		reconstruct_complete(a->right,return_array);
	}
}

void intersect_bloom_node(bloom *a, bloom *b, bloom *c)
{

	UINT i,j,num,check=0;
	for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			temp += MIN(a->flag_array[i]&new,b->flag_array[i]&new);
		}
		if(temp>0)
			check = 1;
		c->flag_array[i] = temp;
	}
	c->num_elems = count_elems_node(c);
}


void union_bloom_node(bloom *a, bloom *b, bloom *c)
{
	UINT i,j,num=0;
	for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			temp += MAX(a->flag_array[i]&new,b->flag_array[i]&new);
		}
		c->flag_array[i] = temp;
	}
	c->num_elems = count_elems_node(c);
}


// algorithm for intersection
// two important components
// compress a subtree into a single node -- easy and then intersect or subtract
// expand a node to a tree -- dictionary attack and then adjust or use the bloomTree



//compresses allof a into c with just one node
void compress_subtree(b_node *a, b_node *c)
{
	if(a->left!=NULL)		//not a leaf
	{
		compress_subtree(a->left,c);
		compress_subtree(a->right,c);
	}
	else
	{
		union_bloom_node(a->bf,c->bf,c->bf);
	}
}

void intersect_bloom(b_node *a, b_node *b, b_node* c)
{
	if(c==NULL)
	{
		c = (b_node *)malloc(sizeof(b_node));
		init_without_bloom(c);
	}
	//going recursive
	if(a->left==NULL && b->left==NULL)			//both are leaves and at same level
	{
		if(c->left!=NULL)					//as c may not necessarily be empty , can be possibly a or b even
		{
			free_bloom(c->left);
			free_bloom(c->right);
		}
		if(c->bf==NULL)
			init(c);
		intersect_bloom_node(a->bf, b->bf, c->bf);
	}
	else if(a->left==NULL && b->left!=NULL)
	{
		b_node * temp = (b_node *)malloc(sizeof(b_node));
		init(temp);
		compress_subtree(b,temp);
		if(c->left!=NULL )					//as c may not necessarily be empty , can be possibly a or b even
		{
			free_bloom(c->left);
			free_bloom(c->right);
		}
		if(c->bf==NULL)
			init(c);
		intersect_bloom_node(a->bf ,temp->bf, c->bf);
	}
	else if(a->left!=NULL && b->left==NULL)	//b ends and a continues so a needs to be compressed
	{
		b_node * temp = (b_node *)malloc(sizeof(b_node));
		init(temp);
		compress_subtree(a,temp);
		if(c->left!=NULL)					//as c may not necessarily be empty , can be possibly a or b even
		{
			free_bloom(c->left);
			free_bloom(c->right);
		}
		if(c->bf==NULL)
			init(c);
		intersect_bloom_node(temp->bf ,b->bf, c->bf);
	}
	else //both are not leaves so initializing both children of c and 
	{
		if(c->left==NULL)
		{
			c->left = (b_node *)malloc(sizeof(b_node));
			init_without_bloom(c->left);
		}
		if(c->right==NULL)
		{
			c->right = (b_node *)malloc(sizeof(b_node));
			init_without_bloom(c->right);
		}
		intersect_bloom(a->left, b->left, c->left);					//recursion continues
		intersect_bloom(a->right, b->right, c->right);
	}
}


void expand_tree(b_node *b, b_node *c)	//takes b and replicates it in c separately
{
	if(b->left==NULL)
	{
		c->bf = (bloom *)malloc(sizeof(bloom));
		int size = (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);
		init_node(c->bf);
		memcpy(c->bf->flag_array, b->bf->flag_array, size*sizeof(UINT)); 
	}
	else
	{
		c->left = (b_node *)malloc(sizeof(b_node));
		c->right = (b_node *)malloc(sizeof(b_node));
		init_without_bloom(c->left);
		expand_tree(b->left,c->left);

		init_without_bloom(c->right);
		expand_tree(b->right,c->right);
	}
}

void union_subtree(b_node *a, b_node *b, b_node *c)	//b represents the size in which it must be grown
{
	//first recursively expand then insert
	my_array_long* reconstruct_array = (my_array_long *)malloc(sizeof(my_array_long));
	arrayLong_init(reconstruct_array);
	UINT size_array = 0;
	printf("size of bloom tobe explored : %d\n",a->bf->num_elems );
	reconstructSet( bloom_tree ,a->bf, &reconstruct_array, &size_array);
	if(b!=c)
	{
		if(c->left!=NULL)					//as c may not necessarily be empty , can be possibly a or b even
		{
			free_bloom(c->left);
			free_bloom(c->right);
		}
		if(c->bf!=NULL)
		{
			free_bloom_node(c->bf);
			c->bf=NULL;
		}
		expand_tree(b,c);
		// exit(0);
	}
	UINT i = 0;
	for( i = 0 ; i < size_array ; i++)
	{
		if(!is_in(reconstruct_array->elements[i], c))
			insert(reconstruct_array->elements[i], c);
	}
	arrayLong_free(reconstruct_array);
}

void union_bloom(b_node *a, b_node *b, b_node *c)
{
		//going recursive
		if(a->left==NULL && b->left==NULL)			//both are leaves and at same level
		{
			if(c->left!=NULL)					//as c may not necessarily be empty , can be possibly a or b even
			{
				free_bloom(c->left);
				free_bloom(c->right);
			}
			if(c->bf==NULL)
				init(c);
			union_bloom_node(a->bf, b->bf, c->bf);
		}
		else if(a->left==NULL && b->left!=NULL)
		{
			union_subtree(a,b,c);
		}
		else if(a->left!=NULL && b->left==NULL)	//b ends and a continues so a needs to be compressed
		{
			union_subtree(b,a,c);
		}
		else //both are not leaves so initializing both children of c and 
		{
			if(c->left==NULL)
			{
				c->left = (b_node *)malloc(sizeof(b_node));
				init_without_bloom(c->left);
			}
			if(c->right==NULL)
			{
				c->right = (b_node *)malloc(sizeof(b_node));
				init_without_bloom(c->right);
			}
			union_bloom(a->left, b->left, c->left);					//recursion continues
			union_bloom(a->right, b->right, c->right);
		}
}


void bloom_test_suit1()
{
	b_node *a = (b_node*)malloc(sizeof(b_node));
	init(a);
	UINT i;
	printf("bloom graph insert/member/delete test initiated\n");
	// partition_node(a);
	for(i = 0 ; i < bloomParam->TEST_RANGE ; i++ )
	{
		UINT z = i;//rand()%bloomParam->TEST_RANGE;
		insert(z,a);
		if(!is_in(z,a))
		{
			printf("bloom graph not functioning properly - inserted val :: %d not found\n",z);
			exit(0);
		}
		// delete(z,a);
	}
	partition_node(a,0);

	printf("count of elems :: %u  %u \n",count_elems(a),bloomParam->TEST_RANGE );
	free_bloom(a);
	printf("bloom graph insert/member/delete test successful\n");
}

void bloom_test_suit2()
{
	b_node *a = (b_node*)malloc(sizeof(b_node));
	b_node *b = (b_node*)malloc(sizeof(b_node));

	init(a);	
	init(b);

	UINT i;
	printf("bloom graph union/intersect test initiated bloomParam->SIZE_BLOOM :: %u\n",bloomParam->SIZE_BLOOM);
	UINT count_a = 0,count_b = 0;
	for( i = 0 ; i < bloomParam->TEST_RANGE ; i++ )
	{
		if(i%2==0)
		{
			insert(i,a);
			if(!is_in(i,a))
				printf("element not found a %u\n",i);
			count_a++;
		}
		else
		{
			insert(i,b);
			if(!is_in(i,b))
				printf("element not found b  %u\n",i);
			count_b++;
		}
	}
	partition_node(a,0);
	partition_node(a,0);

	printf("insertion stage over\n");
	printf("size a :: %u actual:: %u\n",count_elems(a),count_a );	//the number of elements may be less as some counter may completely fill up
	printf("size b :: %u actual:: %u\n",count_elems(b),count_b );	//

	b_node * c = (b_node *)malloc(sizeof(b_node));
	init_without_bloom(c);
	
	b_node * d = (b_node *)malloc(sizeof(b_node));
	init_without_bloom(d);	

	intersect_bloom(a,b,c);
	union_bloom(a,b,d);



	printf("union/intersection operations executed -- size union size::%u intersect::%u \n",count_elems(d),count_elems(c));
	// printf("intersection count via-- current elemsIntersection::%f \n",elemsIntersection(a,b,num_ones(a),num_ones(b)));
	// exit(0);

	ULONG con_union = 0;
	ULONG con_inter = 0;
	for( i = 0 ; i < bloomParam->TEST_RANGE ; i++ )
	{

		if(!is_in(i,d))
		{
			con_union++;
			// exit(0);
		}
		if(is_in(i,c)==1)
		{
			con_inter++;
		}
	}
	printf("errors_union :: %llu errors_intersect :: %llu \n",con_union,con_inter );

	free_bloom(a);
	free_bloom(b);
	free_bloom(c);
	printf("bloom graph union/intersect test successful\n");
}

UINT count_elems_node(bloom *a)
{
	UINT count = num_ones(a)/bloomParam->K;
	return count;
}

/*
* traverse the tree in a dfs fashion and count all the elements
*/
UINT count_elems(b_node* bloomf)
{
	if(bloomf->left==NULL)
	{
		UINT count = bloomf->bf->num_elems;
		return count;
	}
	else
	{
		UINT count = 0;
		count += count_elems(bloomf->left);
		count += count_elems(bloomf->right);
		return count;
	}
}

UINT num_ones(bloom *a)
{
	UINT i = 0, n = 0;
	for (i = 0 ; i < bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS ; i++)
		n += count_ones(a->flag_array[i]);
	return n;
}

UINT numOnes_intersection_node(bloom *a, bloom *b)
{
	UINT i , j, num = 0, numCounts = 0;

	for (i=0;i<(bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			 UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			 UINT curr = 0;
			if(new<0)
			{
				curr = MIN( (a->flag_array[i]&new)>>bloomParam->COUNTER_CHUNK , (b->flag_array[i]&new)>>bloomParam->COUNTER_CHUNK );
				curr = curr<<bloomParam->COUNTER_CHUNK;
			}
			else
				curr = MIN( (a->flag_array[i]&new) , (b->flag_array[i]&new) );
			temp+=curr;
		}
		num += count_ones(temp);										//can be made efficient
	}
	return num;
}

double elemsIntersection_node(bloom *a, bloom *b, int ta, int tb)
{
	double t1 = (double)ta;
	double t2 = (double)tb;
	double th = (double)numOnes_intersection_node(a,b);
	return th/bloomParam->K;
}

UINT count_ones(UINT x)
{
	UINT i = 0;
	UINT count = 0;
	for(i = 0 ; i < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; i++)
	{
		UINT new = bloomParam->BIT_MAP<<(i*bloomParam->COUNTER_CHUNK);
		UINT intersect = x&new;
		count += intersect>>(i*bloomParam->COUNTER_CHUNK);
	}

	return count;
}

// Reads data from file and inserts them in the bloom filter
void read_and_insert(char * filename, b_node* filter)
{
	FILE * reader;
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,filename);
	reader = fopen(fullFilename,"r");
	UINT i,num_elems = 0,namespace = 1;
	fscanf(reader,"%d",&num_elems);
	fscanf(reader,"%d",&namespace);
	for(i = 0 ; i < num_elems ; i++)
	{
		int elem = 0;
		fscanf(reader,"%d",&elem);
		// printf("Inserting element::%d\n",elem);
		insert(elem,filter);
	}
	fclose(reader);
}

// Estimates memory usage by bloom filter for different set of points
void test_memory_usage(char** filenameList,int numFiles)
{
	FILE* logger ;
	if( access( "../logs/dyn_partition_individual.txt", F_OK ) == -1 ) 
	{
    	// file does not exist
    	logger = fopen("../logs/dyn_partition_individual.txt","w");
		fprintf(logger, "%s\n","File Name,No. of elements,Namespace,NumHashFunc,Size of BF,CtrSize,False_Pstv_Prob,Split_Threshold,NumPartitionFunc,Memory Usage,No. of false positives, Ratio of false positives to set size,NULL");
	}
	else
	{
    	logger = fopen("../logs/dyn_partition_individual.txt","a");
	}

	int i;
	for (i = 0; i < numFiles; ++i)
	{
		printf("File::%s\n",filenameList[i] );
		b_node * filter = (b_node*)malloc(sizeof(b_node));
		init(filter);

		read_and_insert(filenameList[i],filter);
		char fullFilename[1000];
		sprintf(fullFilename,"%s%s",datasetPath,filenameList[i]);
		FILE * reader = fopen(fullFilename,"r");
		int num_elems,namespace;
		fscanf(reader,"%d",&num_elems);
		fscanf(reader,"%d",&namespace);
		fclose(reader);

		fprintf(logger, "%s,",filenameList[i]);
		fprintf(logger, "%d,",num_elems);
		fprintf(logger, "%d,",namespace);
		fprintf(logger, "%d,",bloomParam->K);
		fprintf(logger, "%d,",bloomParam->SIZE_BLOOM/8);
		fprintf(logger, "%d,",bloomParam->COUNTER_CHUNK);
		fprintf(logger, "%.3f,",bloomParam->FALSE_PSTV_PROB);
		fprintf(logger, "%.3f,",bloomParam->FILL_THRESHOLD);
		fprintf(logger, "%d,",bloomParam->NUM_PARTITION);
		int tempCount = count_leaves(filter);
		printf("Filename::%s, count_elems()::%d, NumLeaves::%d\n",filenameList[i],count_elems(filter),tempCount );
		fprintf(logger, "%d,",tempCount*(bloomParam->SIZE_BLOOM + sizeof(b_node))/8);

		estimate_false_positive(filenameList[i],filter,logger);
		fprintf(logger, "NULL\n");
	}
	fclose(logger);
}

// Takes a bloom filter and estimates number of false positives in it.
void estimate_false_positive(char* filename,b_node* filter, FILE* logger)
{
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,filename);
	FILE * reader = fopen(fullFilename,"r");
	int num_elems,namespace;
	fscanf(reader,"%d",&num_elems);
	fscanf(reader,"%d",&namespace);
	fclose(reader);
	int i;
	int numPresent = 0;
	for (i = 1; i <= namespace; ++i)
	{
		if(is_in(i,filter))
		{
			numPresent++;
		}
	}
	fprintf(logger, "%d,",numPresent - num_elems);
	fprintf(logger, "%.3f,",(numPresent - num_elems)/(float)num_elems);
}

void test_union_intersection(char** filenameList,int numFiles)
{
	FILE* logger;
	
	if( access( "../logs/dyn_partition_set_ops.txt", F_OK ) == -1 ) 
	{
		logger = fopen("../logs/dyn_partition_set_ops.txt","w");
		fprintf(logger,   "%s\n","File A,File B,Union File,Intersection File,Namespace,NumHashFunc,Size of BF,CtrSize,False_Pstv_Prob,Split_Threshold,No. of Elems (A),No. of false positives, Ratio of false positives to set size,No. of Elems (B),No. of false positives, Ratio of false positives to set size,No. of Elems (A union B),No. of false positives, Ratio of false positives to set size,No. of Elems (A intersect B),No. of false positives, Ratio of false positives to set size,NULL");
	}
	else
	{
		logger = fopen("../logs/dyn_partition_set_ops.txt","a");
	}

	// fprintf(logger,   "%s\n","File A,File B,Union File,Intersection File,Namespace,NumHashFunc,Size of BF,CtrSize,
	// No. of Elems (A),No. of false positives, Ratio of false positives to set size,
	// No. of Elems (B),No. of false positives, Ratio of false positives to set size,
	// No. of Elems (A union B),No. of false positives, Ratio of false positives to set size,
	// No. of Elems (A intersect B),No. of false positives, Ratio of false positives to set size,NULL");

	int i,j;
	for (i = 0; i < numFiles; ++i)
	{
		for (j = i+1; j < numFiles; ++j)
		{
			char union_filename[1000];
			char intersect_filename[1000];
			sprintf(union_filename		,"%s_%s_union",		filenameList[i],filenameList[j]);
			sprintf(intersect_filename	,"%s_%s_intersect",	filenameList[i],filenameList[j]);

			b_node* filter_1 		= (b_node*)malloc(sizeof(b_node));
			b_node* filter_2 		= (b_node*)malloc(sizeof(b_node));
			b_node* filter_union 	= (b_node*)malloc(sizeof(b_node));
			b_node* filter_intersect = (b_node*)malloc(sizeof(b_node));
			init(filter_1);
			init(filter_2);
			init(filter_union);
			init(filter_intersect);
		
			FILE * reader;
			int namespace;
			int num_elems_1,num_elems_2, num_elems_union, num_elems_intersect;

			char* fullFilename = (char*)malloc(1000*sizeof(char));
			sprintf(fullFilename,"%s%s",datasetPath,filenameList[i]);
			reader = fopen(fullFilename,"r");
			fscanf(reader,"%d",&num_elems_1);
			fscanf(reader,"%d",&namespace);
			fclose(reader);
			free(fullFilename);

			fullFilename = (char*)malloc(1000*sizeof(char));
			sprintf(fullFilename,"%s%s",datasetPath,filenameList[j]);
			reader = fopen(fullFilename,"r");
			fscanf(reader,"%d",&num_elems_2);
			fclose(reader);
			free(fullFilename);

			fullFilename = (char*)malloc(1000*sizeof(char));
			sprintf(fullFilename,"%s%s",datasetPath,union_filename);
			reader = fopen(fullFilename,"r");
			fscanf(reader,"%d",&num_elems_union);
			fclose(reader);
			free(fullFilename);

			fullFilename = (char*)malloc(1000*sizeof(char));
			sprintf(fullFilename,"%s%s",datasetPath,intersect_filename);
			reader = fopen(fullFilename,"r");
			fscanf(reader,"%d",&num_elems_intersect);
			fclose(reader);
			free(fullFilename);

			fprintf(logger, "%s,", filenameList[i]);
			fprintf(logger, "%s,", filenameList[j]);
			fprintf(logger, "%s_%s_union,", filenameList[i],filenameList[j]);
			fprintf(logger, "%s_%s_intersect,", filenameList[i],filenameList[j]);

			fprintf(logger, "%d,",namespace);
			fprintf(logger, "%d,",bloomParam->K);
			fprintf(logger, "%d,",bloomParam->SIZE_BLOOM/8);
			fprintf(logger, "%d,",bloomParam->COUNTER_CHUNK);
			fprintf(logger, "%.3f,",bloomParam->FALSE_PSTV_PROB);
			fprintf(logger, "%.3f,",bloomParam->FILL_THRESHOLD);
			
			read_and_insert(filenameList[i],filter_1);
			read_and_insert(filenameList[j],filter_2);
			union_bloom(filter_1,filter_2,filter_union);
			intersect_bloom(filter_1,filter_2,filter_intersect);

			// Details for set A
			fprintf(logger, "%d,",num_elems_1 );
			estimate_false_positive(filenameList[i],filter_1,logger);

			// Details for set B
			fprintf(logger, "%d,",num_elems_2 );
			estimate_false_positive(filenameList[j],filter_2,logger);
		
			// Details for set (A union B)
			fprintf(logger, "%d,",num_elems_union );
			estimate_false_positive(union_filename, filter_union,logger);

			// Details for set (A intersect B)
			fprintf(logger, "%d,",num_elems_intersect );
			estimate_false_positive(intersect_filename,filter_intersect,logger);

			fprintf(logger, "NULL\n");
		}
	}
	fclose(logger);

}

UINT count_leaves(b_node *node)
{
	if(node->left==NULL)
		return 1;
	else
	{
		return count_leaves(node->left)+count_leaves(node->right);
	}
}

void run_memory_tests()
{
	// correct overwrite issues
	// int numFiles = 2;
	int i,j,k;
	int numFiles = 9;
	char** filenameList = (char**)malloc(numFiles*sizeof(char*));
	for (i = 0; i < numFiles; ++i)
	{
		filenameList[i] = (char*)malloc(sizeof(char)*100);
	}
	strcpy(filenameList[0],"rand::10");
	strcpy(filenameList[1],"rand::50");
	strcpy(filenameList[2],"rand::100");
	strcpy(filenameList[3],"rand::200");
	strcpy(filenameList[4],"rand::500");
	strcpy(filenameList[5],"rand::1000");
	strcpy(filenameList[6],"rand::2000");
	strcpy(filenameList[7],"rand::5000");
	strcpy(filenameList[8],"rand::10000");
	
	// char filenameList[10][1000] = {"rand::10","rand::50","rand::100","rand::200","rand::500","rand::1000","rand::2000","rand::5000","rand::10000"};
	int num_K_Vals 			= 2;
	int num_CtrSizeVals 	= 1;
	int num_filterSizeVals 	= 1;

	int K_Vals[2] 			= {3,4};
	int ctrSizeVals[1]	 	= {2};
	int filterSizeVals[1] 	= {10240};//,2048,4000,10000,20000,40000};

	for (i = 0; i < num_CtrSizeVals; ++i)
	{
		for (j = 0; j < num_K_Vals; ++j)
		{
			for (k = 0; k < num_filterSizeVals; ++k)
			{
				printf("Running test for K = %d, Ctr Size = %d, FilterSize = %d\n",K_Vals[j], ctrSizeVals[i], filterSizeVals[i]);
				init_bloomParameters(filterSizeVals[k],K_Vals[j],ctrSizeVals[i],10,0.5); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
				seiveInitial(); // Important to initialize it here to reset m[i] values to suit the changed values of SIZE_BLOOM
				bloom_tree = setupTreeRange(1,1000000);

				printf("GOing to test function\n");
				test_memory_usage(filenameList,numFiles);				
			}
		}
	}
	// init_bloomParameters(10240,3,2); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
	// seiveInitial(); // Important to initialize it here to reset m[i] values to suit the changed values of SIZE_BLOOM

	// test_memory_usage(filenameList,2);
}

void run_set_operation_tests()
{
	int numFiles = 10;
	char** filenameList = (char**)malloc(numFiles*sizeof(char*));
	filenameList[0] = (char*)malloc(sizeof(char)*100);
	strcpy(filenameList[0],"randomNum_1");
	filenameList[1] = (char*)malloc(sizeof(char)*100);
	strcpy(filenameList[1],"randomNum_2");
	
	init_bloomParameters(10240,3,2,10,0.5); // Sets SIZE_BLOOM, K, and COUNTER_SIZE, NUM_PARTITION_FUNC
	seiveInitial(); // Important to initialize it here to reset m[i] values to suit the changed values of SIZE_BLOOM

	test_union_intersection(filenameList,2);
}

int main()
{
	datasetPath = (char*)malloc(1000*sizeof(char));
	strcpy(datasetPath,"../datasets/");
	time_t t;
	srand((unsigned) time(&t));	

	// run_memory_tests();

	// return 1;
	init_bloomParameters(10240,3,2,10,0.5);
	seiveInitial(); // Important to initialize it here to reset m[i] values to suit the changed values of SIZE_BLOOM
	bloom_tree = setupTreeRange(1,1000000);
	b_node *letsdoit = (b_node *)malloc(sizeof(b_node));
	init(letsdoit );
	insert(343568,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(627732,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(366852,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(925727,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(284707,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(966697,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(487476,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(986678,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(50743,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(811068,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(800319,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(326210,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(149065,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(585803,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(279633,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(732757,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(123992,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(288355,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(519783,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(587889,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(332409,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(824446,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(738965,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(664217,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(961691,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(787621,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(347309,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(944824,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(669044,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(793792,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(833729,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(886467,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(867016,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(222922,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(127692,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(597709,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(982736,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(668881,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(216786,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(330984,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(308970,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(975087,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(844540,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(745730,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(323332,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(924936,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(339722,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(864530,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(882456,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(940315,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(642332,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(519457,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(560931,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(141605,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(867112,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(932140,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(311607,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(800569,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(649024,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(197953,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(996406,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(297287,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(603978,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(706381,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(894291,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(148321,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(592227,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(169324,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(173422,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(258415,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(443764,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(856952,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(91541,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(465796,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(206216,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(652173,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(627093,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(725399,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(746397,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(627105,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(837538,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(944547,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(92068,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(331178,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(459187,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(443828,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(573940,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(793018,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(393662,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(928198,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(889761,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(779219,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(764836,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(765605,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(746979,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(246764,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(694253,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(673263,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(572404,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));
	insert(821238,letsdoit);
	printf("number of elements in bloomfilter %d,    %d\n",count_elems(letsdoit),count_leaves(letsdoit));

	


	// init_bloomParameters();
	// seiveInitial();
	// // printTree(bloom_tree);
	// printf("tree initialized\n");
	// bloom_tree = setupTreeRange(1,1000000);
	// // exit(0);
	// b_node *filter = (b_node*)malloc(sizeof(b_node));
	// init(filter);
	// partition_node(filter);
	// printf("first partition done\n");
	// partition_node(filter);
	// bloom_test_suit1();
	// bloom_test_suit2();
	// printf("testing phase over\n");
	// read_and_insert("../datasets/randomNum.txt",filter);
}
