#include "bloom_tree.h"

UINT  THRESH = 200;
ULONG leafID = 0;
UINT  levelThreshold = 6;
UINT nMembership = 0;  // Not used in our code

ULONG MIN = 0;
ULONG MAX = 99999999999999;


//------------
UINT overlapThreshold;


ULONG totalRange = 0;
omp_lock_t m_totalRange;

UINT k_ones_query;	//Number of 1s in the query bloom filter

FILE *foutput;				
omp_lock_t m_output;

my_array_long **output;
ULONG numLeaves;
UINT *size_output;


ULONG nVertices;

bloom query;

struct bloomNode *taskQueue[QSIZE];
pthread_mutex_t m_taskQueue;
pthread_cond_t c_overflow = PTHREAD_COND_INITIALIZER;


int rear = -1;	//rear == front indicates empty queue
int front = -1;


//----------------------------------------------------------------------------------------//
/*
task at hand -
dynamic partition bloom filter which can grow-
Currently ready elements:
- simple insert/delete/is_in operations
to be done:
- set operations
- split a bloom filter into two
Requirements from bloomTree :
-- first get a simple reconstruction tree working
-- then get a partitioned tree working
-- then get a split operation based version working

- ****setup according to the K partition functions provided**almost done
- ****reconstruction function -- so that for now splitting can be done
- one more thing -- better if reconstruction called for specific subtree

- later make the improved splitting function
*/

// task is extracted -- each task is basically a bloomNode to be explored
struct bloomNode *extractTask()
{
	pthread_mutex_lock(&m_taskQueue);
	if (rear==front) 
	{
		pthread_mutex_unlock(&m_taskQueue);
		return NULL;
	}
	struct bloomNode *t = taskQueue[front];
	front = (front+1)%QSIZE;
	pthread_mutex_unlock(&m_taskQueue);
	pthread_cond_broadcast(&c_overflow);
	return t;
}

// no change required as simply adding tasks to the queue
void addTask(struct bloomNode *t)
{
	pthread_mutex_lock(&m_taskQueue);
	if ((rear+1)%QSIZE == front)
	{
		pthread_cond_wait(&c_overflow,&m_taskQueue);
		taskQueue[rear] = t;
		rear = (rear+1)%QSIZE;
		pthread_mutex_unlock(&m_taskQueue);
	}
	else if (rear==front) //empty queue
	{	
		rear = 0;
		front = 0;
		taskQueue[rear] = t;
		rear = (rear+1)%QSIZE;
		pthread_mutex_unlock(&m_taskQueue);
	}
	else
	{
		taskQueue[rear] = t;
		rear = (rear+1)%QSIZE;
		pthread_mutex_unlock(&m_taskQueue);
	}
}

// searching leaf -- in this case taking the simple reconstruction requirement and searching 
// from min to max -- but this is not correct if the partition is using hash functions as the 
// range of dictinary attack is not there suitably as it should have been there 

// but we can quickly check membership by using the set of the partition functions on the number
// and picking it out if required
void searchLeaf(struct bloomNode *t, double elemsIntersection)
{
	omp_set_lock(&m_totalRange);
	nMembership++;
	omp_unset_lock(&m_totalRange);
	// if(elemsIntersection < thresh_intersect)			//to trim off certain leaves if there is not enough intersection
	// 	return;
	ULONG i = 0;
	ULONG localCount = 0;
	ULONG currleafID = t->leafID;
	// printf("tree::exploring a leaf %d\n",currleafID);

	for (i=t->start;i<=t->end;i++){
		if (is_in_node(i,&query)){
			// printf("inserting %d %d\n",i,currleafID );
			arrayLong_addElement(output[currleafID],i);
			size_output[currleafID]++;
		}
	}
}


void descendLevels(struct bloomNode *root, UINT numLevels, UINT currLevel, double estimate)
{
	if (currLevel == numLevels)
	{
		root->prevEstimate = estimate;
		addTask(root);
	}
	else if ((root->lchild==NULL) && (root->rchild==NULL))
	{
		root->prevEstimate = estimate;
		addTask(root);
	}
	else
	{
		descendLevels(root->lchild,numLevels,currLevel+1,estimate);
		descendLevels(root->rchild,numLevels,currLevel+1,estimate);
	}
}



void *executeTask(void *x)
{
	while (1)
	{
		struct bloomNode *t = extractTask();
		if (t == NULL)
		{
			omp_set_lock(&m_totalRange);
			ULONG tR = totalRange;
			omp_unset_lock(&m_totalRange);
			if (tR>=nVertices)
				break;
		}
		else
		{
			double estimate = elemsIntersection_node(&(t->filter),&query,t->nOnes,k_ones_query);
			// printf("tree::estimate %f\n",estimate);
			if ((t->level >= levelThreshold) || (t->end == t->start ))
			{				
				// This estimate is intentionaly kept to be greater than 1.0 instead of using overlapThreshold
				if (estimate >= 1.0)
				{
					searchLeaf(t,estimate);
				}

				omp_set_lock(&m_totalRange);
				// totalRange += (t->end - t->start + 1);
				totalRange += 1;
				omp_unset_lock(&m_totalRange);
				continue;
			}


			int flag = (estimate >= overlapThreshold)?1:0;
			if (flag>0)
			{

				double ratio = estimate/t->prevEstimate;											//prevEstimate tells how much was found at parent and then check what fraction is there in the children as compared to their parent
				int numLevels;

				if (ratio <= 0.25) numLevels = 1;
				else if (ratio <= 0.5) numLevels = 2;
				else if (ratio <= 0.75) numLevels = 3;
				else numLevels = 4;

				descendLevels(t,numLevels,0,estimate);	
			}
			else
			{
				omp_set_lock(&m_totalRange);
                // totalRange += (t->end - t->start + 1);
                totalRange += 1;
                omp_unset_lock(&m_totalRange);
			}
		}
	}
}

//----------------------------------------------------------------------------------------//

//whatever level the bloom Tree is the hash function would be applied for the level below
struct bloomTree *getBloomTree(ULONG startVertex, ULONG endVertex)
{
	struct bloomTree *bT = (struct bloomTree*)malloc(sizeof(struct bloomTree));
	ULONG mid   = (startVertex+endVertex)/2;
	bT->left 	= createBloomTree(startVertex, mid, 1);
	bT->right 	= createBloomTree(mid+1, endVertex, 1);
	return bT;
}

/*
* min_start and max_end opposite so that they get updated on entering
*/
struct bloomNode *createBloomTree(ULONG startVertex, ULONG endVertex, UINT level)
{
	struct bloomNode *r = (struct bloomNode*)malloc(sizeof(struct bloomNode));
	r->level 		= level;
	r->start 		= startVertex;								
	r->end 			= endVertex;
	r->min_start 	= MAX;
	r->max_end 		= MIN;						
	r->lchild 		= NULL;
	r->rchild 		= NULL;

	if (level <= levelThreshold)
	{
		init_node(&r->filter);
		// r->trueValue = 0;	//TO BE DELETED
		r->nOnes = 0;
	}

	if (level < levelThreshold)
	{
		ULONG mid   = (startVertex+endVertex)/2;
		r->lchild = createBloomTree(startVertex, mid, level+1);
		r->rchild = createBloomTree(mid+1, endVertex, level+1);
	}
	else
	{
		r->leafID = leafID;
		leafID++;
	}
	return r;
}

struct bloomTree* setupTree(ULONG start, ULONG end, char* data_file_name)
{
	FILE *data_file = fopen(data_file_name,"r");

	struct bloomTree* bloom_tree = getBloomTree(start, end);

	UINT elem;

	ULONG to_insert = 0;
	while(!feof(data_file))
	{ 
		fscanf(data_file,"%u\n",&elem);
		insert_bloom_tree(bloom_tree,elem);
	}
	fclose(data_file);

	// printf("Going to update nOness\n");
	update_nones(bloom_tree->left);
	update_nones(bloom_tree->right);
	return bloom_tree;
}

/*
* This functions creates a bloom tree and enters all the numbers between start and end
*/
struct bloomTree* setupTreeRange(ULONG start, ULONG end)
{
	struct bloomTree* bloom_tree = getBloomTree(start, end);
	ULONG i = 0;
	for(i = start ; i <= end ; i++)
		insert_bloom_tree(bloom_tree,i);
	// printf("Going to update nOness\n");
	update_nones(bloom_tree->left);
	update_nones(bloom_tree->right);
	return bloom_tree;
}

void insert_bloom_node(struct bloomNode* node, ULONG elem, UINT level)
{
	struct bloomNode* currNode = node;
	UINT k = level;
	while( currNode!=NULL )
	{
		insert_node(elem, &(currNode->filter));
		
		if(elem > currNode->max_end )				//updating the actual ranges of elements
			currNode->max_end = elem;
		if(elem < currNode->min_start)
			currNode->min_start = elem;
		
		// if(currNode->lchild==NULL)
		// 	printf("node->min %d %d level -- %d \n",currNode->min_start,currNode->max_end,k );
		
		if(currNode->lchild==NULL)
		{
			break;
		}

		if( elem > (currNode->lchild->start)  && elem <= (currNode->lchild->end) )		//checking and entering if in the same 
			currNode = currNode->lchild;
		else
			currNode = currNode->rchild;
		k++;
	}
}

void insert_bloom_tree( struct bloomTree* tree, ULONG elem)
{
	if( elem > tree->left->start  && elem <= tree->left->end )
	{
		insert_bloom_node(tree->left ,elem ,1);
	}
	else
	{
		insert_bloom_node(tree->right ,elem ,1);
	}
}

void update_nones(struct bloomNode *tree)
{
	tree->nOnes = num_ones(&(tree->filter));
	struct  bloomNode * templ = tree->lchild;
	struct  bloomNode * tempr = tree->rchild;
	if(templ!=NULL)
		update_nones(templ);
	if(tempr!=NULL)
		update_nones(tempr);
}

/*Count the number of ones in the intersection of r and k*/
ULONG intersectNode(struct bloomNode *r,bloom *k)
{
	ULONG flag = 0;
	flag = numOnes_intersection_node(&r->filter,k);
	return flag;
}

//----------------------------------------------------------------------------------------//


/* Assuming bloomSampleTree is initialised with range being O-M
*  Need
*  nNodes : Number of nodes in query
*  simple traversal of tree, also adjust so that it begins from the exact point in tree where it is
*  by sending a binary path  
*  need to adjust the reconstruct so that the search is restricted to the node where it belongs 
*/
void reconstructSet( struct bloomTree* a, bloom* arg_query , my_array_long** return_array, UINT* return_array_size)
{
	UINT nNodes = arg_query->num_elems;
	k_ones_query = num_ones(arg_query);
	query = *arg_query;

	(a->left)->prevEstimate = 1.0*nNodes;										//prevEstimate stores the number of elements found in common with the parent
	(a->right)->prevEstimate = 1.0*nNodes;										//so that we can find the fraction found in their children

	addTask(a->left);															//adding tasks to the queue
	addTask(a->right);

	// ULONG M = nVertices;
	numLeaves = pow(2,levelThreshold);
	nVertices = numLeaves;

	output 		= (my_array_long**)malloc(sizeof(my_array_long*)*numLeaves);	//initialising numLeaves and output arrays corresponding to each leaves (may try to do per thread)
	size_output = (UINT*)malloc(sizeof(UINT)*numLeaves);

	ULONG i;
	// printf("numLeaves :: %d\n",numLeaves);
	for (i=0;i<numLeaves;i++)
	{
		output[i] 		= (my_array_long*)malloc(sizeof(my_array_long));
		arrayLong_init(output[i]);
		size_output[i] 	=  0;
	}


	// omp_init_lock(&m_numElements);
	pthread_mutex_init(&m_taskQueue, NULL);
	omp_init_lock(&m_totalRange);

	double start = omp_get_wtime();
	#pragma omp parallel
	{
		int a = omp_get_thread_num();
		executeTask((void *)a);
	}
	double elapsed = omp_get_wtime()-start;

	omp_destroy_lock(&m_totalRange);

	// Now combine output of each leaf node into a single array of output values 
	my_array_long* final_array;
	final_array = (my_array_long*)malloc(sizeof(my_array_long));
	arrayLong_init(final_array);
	UINT j;
	UINT ele_ctr = 0;
	for (i = 0; i < numLeaves; ++i)
	{
		for (j = 0; j < size_output[i]; ++j)
		{
			arrayLong_addElement(final_array,output[i]->elements[j]);
			ele_ctr += 1;
		}
	}
	*return_array = final_array;
	*return_array_size = ele_ctr;
	// printf("reconstruction is done\n");
	//freeing the memory
	for (i = 0; i < numLeaves; ++i)
		free(output[i]->elements);
	free(output);
	free(size_output);
}



void init_reconstruct(UINT arg_levelThreshold, UINT  arg_overlapThreshold)
{
	levelThreshold 		= arg_levelThreshold;
	overlapThreshold 	= arg_overlapThreshold;
	// nVertices 			= arg_nVertices;
}

//----------------------------------------------------------------------------------------//


void printNode(struct bloomNode* n)
{
	if(n==NULL)
		return;
	if(n->lchild==NULL)
		printf("ID %d -- start %lu end %lu min %lu max %lu size :: %lu\n",n->leafID,n->start,n->end,n->min_start,n->max_end, n->filter.num_elems );
	printNode(n->lchild);
	printNode(n->rchild);

}

void printTree(struct bloomTree* a)
{
	printNode(a->left);
	printNode(a->right);
}