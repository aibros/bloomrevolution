import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import glob
from pprint import pprint
import math
import copy

metrics = ["EffMemUsage", "FP_Ratio"];
metricsOrder = {"EffMemUsage":0, "FP_Ratio":1};

factors = ["UnitSize","NumElems","NumElemsA","NumElemsB","Overlap","Operation"]

database = { "CountBloom":{ "solo":{}, "pair":{} },"DynamicBloom":{ "solo":{}, "pair":{} },"DPBloom":{ "solo":{}, "pair":{} } }

soloFactors = ["UnitSize","NumElems"]
soloOrder = {"UnitSize":0,"NumElems":1,"BatchSize":2}
soloRange = {"UnitSize":[512,1024,2048,4096,8192],"NumElems":[10,100,200,500,1000,2000],"BatchSize":[5]}

pairFactors = ["UnitSize","NumElemsA","NumElemsB","Overlap","Operation"]
pairOrder = {"UnitSize":0,"NumElemsA":1,"NumElemsB":2,"Overlap":3,"Operation":4}
pairRange = {"UnitSize":[512,1024,2048,4096,8192],"NumElemsA":[10,100,200,500,1000,2000],"NumElemsB":[10,100,200,500,1000,2000],"Overlap":[0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9],"BatchSize":[5],"Operation":["Union","Intersection"]}

#types of graphs we are talking about

# sensitivity analysis:
# 1- for each unit size : metrics vs set sizes
# 2- for each batch size : metrics vs set sizes
# 3- for each set size A,%Overlap : metrics vs set size B  
# 4- for each set size A, set size B : metrics vs %Overlap
# 5- for each set size A, set size B, unit size : metrics vs %Overlap

# Comparison analysis:
# - 1 vs 1 
# - 3 vs 3
# - 4 vs 4
# - 5 vs 5


# one possible database combine all information as 

# function :for reading a folder
# function :for any graph data -- fixed factors , varying factors - metric
# function :given two graph data with same fixed factors and varying factors merge

# 
# BloomType : CountBloom(alias for standard bloom filter), DynamicBloom, DPBloom
# expType	: Type of experiment: 	a) solo : For getting graphs for set creation experiments
#									b) pair : For set union and intersection experiments
# foldername: Name of folder to be read 
# sampleSize: Size of sample over which each datapoint(plotted on graph) has to be averaged. 
def readFolder(BloomType, expType, folderName, sampleSize ):
	global database,soloFactors,soloOrder,pairFactors,pairOrder,metrics,factors,metricsOrder

	file_list = glob.glob(folderName+"/*.csv");
	for file_name in file_list:
		file_data = pd.read_csv(file_name)
		# print file_data
		# exit(0)
		file_len = len(file_data["UnitSize"]);		#assuming unit size is present in all
		attrs = [];

		if(expType=="solo"):
			for ind in range(len(soloFactors)):
				if(soloFactors[ind] in file_data.keys()):
					attrs += [soloFactors[ind]];
				else:
					print "Factor "+soloFactors[ind]+" not found. Stopping in folder : "+folderName+" file :"+file_name;
					exit(0)
		elif(expType=="pair"):
			for ind in range(len(pairFactors)):
				if(pairFactors[ind] in file_data.keys()):
					attrs += [pairFactors[ind]];
				else:
					print "Factor "+pairFactors[ind]+" not found. Stopping in folder : "+folderName+" file :"+file_name;
					exit(0)
		else:
			print "expType not correct :"+expType;
			exit(0)

		for block in range(0,file_len,sampleSize):
			lstKey = [];
			for fac_ind in range(len(attrs)):
				lstKey += [file_data[attrs[fac_ind]][block]];
			key = tuple(lstKey);

			val_list = []
			for r in range(len(metrics)):
				val_list += [[]];


			for elem in range(block,block+sampleSize):
				for met_ind in range(len(metrics)):
					met = metrics[met_ind];
					val_list[met_ind] += [file_data[met][elem]];

			val = [(0.0,0.0)]*len(metrics)
			for met_ind in range(len(metrics)):
				x = np.mean(val_list[met_ind]); 
				y = float(np.std(val_list[met_ind]))/float(math.sqrt(len(val_list[met_ind]))); 
				val[met_ind] = (x,y)
			database[BloomType][expType][key] = val;


# return an tuple of lists for a graph instance
# BloomType		: Type of bloom filter: CountBloom, DynamicBloom or DPBloom
# expType   	: "solo" (for set creation and membership query experiments) and "pair" for set union and intersection experiments
# fixed_fact	: Parameters that are not varying
# fixed_vals	: Value of parameters that are not varying
# vary_factor	: Parameters that are varying (X-Axis)
# metric 		: Value of be plotted on Y-Axis
def get_graph_instance(BloomType, expType, fixed_fact, fixed_vals, vary_factor, metric):
	global database,soloFactors,soloOrder,soloRange,pairFactors,pairOrder,pairRange,metrics,factors,metricsOrder

	dataset = database[BloomType][expType];
	factors = [];
	factorOrder = {};
	factorRange = [];
	if(expType == "solo"):
		factors = soloFactors;
		factorOrder = soloOrder;
		factorRange = soloRange;
	else:
		factors = pairFactors;
		factorOrder = pairOrder;
		factorRange = pairRange;

	start_lstKey  	= [0]*len(factors);
	vary_ind 		= factorOrder[vary_factor];
	vary_range 		= factorRange[vary_factor]

	for ind in range(len(fixed_fact)):
		fact = fixed_fact[ind];
		start_lstKey[factorOrder[fact]] = fixed_vals[ind];

	# print fixed_vals
	# print fixed_fact
	# print "first fixed",start_lstKey
	X = [];
	Y = [];
	Z = [];
	for val_ind in range(len(vary_range)):
		lstKey = copy.deepcopy(start_lstKey)
		val = vary_range[val_ind]
		lstKey[vary_ind] = val;
		tupKey = tuple(lstKey)
		# print val,vary_ind
		# print tupKey
		X += [val];
		if(tupKey in dataset.keys()):						#as only unique pairs present in data
			Y += [dataset[tupKey][metricsOrder[metric]][0]]
			Z += [dataset[tupKey][metricsOrder[metric]][1]]
			# print "varied",lstKey
		elif(expType=="pair"):
			temp = lstKey[factorOrder["NumElemsB"]]
			lstKey[factorOrder["NumElemsB"]] = lstKey[factorOrder["NumElemsA"]]
			lstKey[factorOrder["NumElemsA"]] = temp
			# print "varied reverse",lstKey
			tupKey = tuple(lstKey)
			Y += [dataset[tupKey][metricsOrder[metric]][0]]
			Z += [dataset[tupKey][metricsOrder[metric]][1]]
	return (X,Y,Z)


# draws multiple graphs on the same figure, provided the suitable data
# arguments in order -> title of the figure, X axis label , Y axis label
# data for different graphs as list of tuples ex [(X,Y),(X1,Y1)], labels
# for each graph instance, directory for saving figure, name of figure
def draw_graph( title, X_label, Y_label, graph_instances, instance_labels,directory,name):
	fig = plt.figure(figsize=(12, 9), dpi=80);
	ax = fig.add_subplot(111)

	for ind in range(len(graph_instances)):
		(X,Y,Z) = graph_instances[ind];		
		plt.errorbar(X,Y,yerr=Z,marker='o',ls='-');
		for xy in zip(X, Y):                                       
	  		ax.annotate('(%s, %s)' % xy, xy=xy, textcoords='data')

	plt.legend(instance_labels, loc='upper right')
	plt.suptitle(title);
	plt.xlabel(X_label);
	plt.ylabel(Y_label);
	# plt.show();
	fig.savefig(directory+"/"+name+".png")


def runMain():
	# simple test cases
	# draw_graph("ok","x","y",[([1,2,3,4],[5,8,9,10],[1,2,1,2]),([1,2,3,4],[3,6,13,26],[0.1,0.2,0.1,0.2])],["a","b"],"","");
	# readFolder("CountBloom", "solo", "sample",20 )

	# pprint(database)

	# print get_graph_instance("CountBloom","solo",["UnitSize"],[2048],"NumElems","EffMemUsage")
	# print get_graph_instance("CountBloom","solo",["UnitSize"],[2048],"NumElems","FP_Ratio")


	print("reading logs..")

	readFolder("CountBloom","pair","data/CB_INT",2)
	readFolder("CountBloom","solo","data/CB_SET",20)
	readFolder("CountBloom","pair","data/CB_UN",2)

	readFolder("DynamicBloom","pair","data/DB_INT",2)
	readFolder("DynamicBloom","solo","data/DB_SET",20)
	readFolder("DynamicBloom","pair","data/DB_UN",2)

	readFolder("DPBloom","pair","data/DBP_INT",2)
	readFolder("DPBloom","solo","data/DBP_SET",20)
	readFolder("DPBloom","pair","data/DBP_UN",2)

	print("logs read!!")

	# pprint(database)
	# correct the problem in count bloom which is still giving negatives

	# pprint(database["DPBloom"]["pair"])

	print("drawing graphs")

	print("all solo graphs creation started...")
	CB1 = get_graph_instance("CountBloom","solo",["UnitSize"],[1024],"NumElems","EffMemUsage")
	CB2 = get_graph_instance("CountBloom","solo",["UnitSize"],[1024],"NumElems","FP_Ratio")

	DB1 = get_graph_instance("DynamicBloom","solo",["UnitSize"],[1024],"NumElems","EffMemUsage")
	DB2 = get_graph_instance("DynamicBloom","solo",["UnitSize"],[1024],"NumElems","FP_Ratio")
	DB3 = get_graph_instance("DynamicBloom","solo",["UnitSize"],[2048],"NumElems","EffMemUsage")
	DB4 = get_graph_instance("DynamicBloom","solo",["UnitSize"],[2048],"NumElems","FP_Ratio")

	DPB1 = get_graph_instance("DPBloom","solo",["UnitSize"],[1024],"NumElems","EffMemUsage")
	DPB2 = get_graph_instance("DPBloom","solo",["UnitSize"],[1024],"NumElems","FP_Ratio")
	DPB3 = get_graph_instance("DPBloom","solo",["UnitSize"],[2048],"NumElems","EffMemUsage")
	DPB4 = get_graph_instance("DPBloom","solo",["UnitSize"],[2048],"NumElems","FP_Ratio")


	draw_graph( "DPB vs CB", "set size", "False Positive Probability", [DPB2,CB2], ["DPB","CB"],"graphs","DPB_CB_FP");

	draw_graph( "DPB vs DB", "set size", "Effective memory usage", [DPB1,DB1], ["DPB","DB"],"graphs","DPB_DB_EM");
	draw_graph( "DPB vs DB", "set size", "False Positive Probability", [DPB2,DB2], ["DPB","DB"],"graphs","DPB_DB_FP");

	draw_graph( "DPB vs CB vs DB", "set size", "False Positive Probability", [DPB2,CB2,DB2], ["DPB","CB","DB"],"graphs","DPB_CB_DB_FP");


	print("all solo graphs created!!")
	print("Creating all pair graphs")
	#3
	CB1_2 = get_graph_instance("CountBloom","pair",["UnitSize","NumElemsA","Overlap","Operation"],[1024,1000,0.4,"Intersection"],"NumElemsB","EffMemUsage")
	CB2_2 = get_graph_instance("CountBloom","pair",["UnitSize","NumElemsA","Overlap","Operation"],[1024,1000,0.4,"Intersection"],"NumElemsB","FP_Ratio")
	CB3_2 = get_graph_instance("CountBloom","pair",["UnitSize","NumElemsA","Overlap","Operation"],[1024,1000,0.4,"Union"],"NumElemsB","EffMemUsage")
	CB4_2 = get_graph_instance("CountBloom","pair",["UnitSize","NumElemsA","Overlap","Operation"],[1024,1000,0.4,"Union"],"NumElemsB","FP_Ratio")
	 
	DB1_2 = get_graph_instance("DynamicBloom","pair",["UnitSize","NumElemsA","Overlap","Operation"],[1024,1000,0.4,"Intersection"],"NumElemsB","EffMemUsage")
	DB2_2 = get_graph_instance("DynamicBloom","pair",["UnitSize","NumElemsA","Overlap","Operation"],[1024,1000,0.4,"Intersection"],"NumElemsB","FP_Ratio")
	DB3_2 = get_graph_instance("DynamicBloom","pair",["UnitSize","NumElemsA","Overlap","Operation"],[1024,1000,0.4,"Union"],"NumElemsB","EffMemUsage")
	DB4_2 = get_graph_instance("DynamicBloom","pair",["UnitSize","NumElemsA","Overlap","Operation"],[1024,1000,0.4,"Union"],"NumElemsB","FP_Ratio")
	 
	DPB1_2 = get_graph_instance("DPBloom","pair",["UnitSize","NumElemsA","Overlap","Operation"],[1024,1000,0.4,"Intersection"],"NumElemsB","EffMemUsage")
	DPB2_2 = get_graph_instance("DPBloom","pair",["UnitSize","NumElemsA","Overlap","Operation"],[1024,1000,0.4,"Intersection"],"NumElemsB","FP_Ratio")
	DPB3_2 = get_graph_instance("DPBloom","pair",["UnitSize","NumElemsA","Overlap","Operation"],[1024,1000,0.4,"Union"],"NumElemsB","EffMemUsage")
	DPB4_2 = get_graph_instance("DPBloom","pair",["UnitSize","NumElemsA","Overlap","Operation"],[1024,1000,0.4,"Union"],"NumElemsB","FP_Ratio")
	 




	DPB1_x = get_graph_instance("DPBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,1000,1000,"Intersection"],"Overlap","EffMemUsage")
	DPB2_x = get_graph_instance("DPBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,1000,1000,"Intersection"],"Overlap","FP_Ratio")
	DPB3_x = get_graph_instance("DPBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,1000,1000,"Union"],"Overlap","EffMemUsage")
	DPB4_x = get_graph_instance("DPBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,1000,1000,"Union"],"Overlap","FP_Ratio")

	DB1_x = get_graph_instance("DynamicBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,1000,1000,"Intersection"],"Overlap","EffMemUsage")
	DB2_x = get_graph_instance("DynamicBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,1000,1000,"Intersection"],"Overlap","FP_Ratio")
	DB3_x = get_graph_instance("DynamicBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,1000,1000,"Union"],"Overlap","EffMemUsage")
	DB4_x = get_graph_instance("DynamicBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,1000,1000,"Union"],"Overlap","FP_Ratio")

	CB1_x = get_graph_instance("CountBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,1000,1000,"Intersection"],"Overlap","EffMemUsage")
	CB2_x = get_graph_instance("CountBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,1000,1000,"Intersection"],"Overlap","FP_Ratio")
	CB3_x = get_graph_instance("CountBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,1000,1000,"Union"],"Overlap","EffMemUsage")
	CB4_x = get_graph_instance("CountBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,1000,1000,"Union"],"Overlap","FP_Ratio")



	# draw_graph( "DPB vs CB Intersection with size A as 1000 and overlap 40%", "set size of B", "False Positive Probability", [DPB2_2,CB2_2], ["DPB","CB"],"graphs","DPB_CB_FP_INT");
	# draw_graph( "DPB vs CB Union with size A as 1000 and overlap 40%", "set size of B", "False Positive Probability", [DPB4_2,CB4_2], ["DPB","CB"],"graphs","DPB_CB_FP_UN");

	# draw_graph( "DPB vs DB Intersection with size A as 1000 and overlap 40%", "set size", "Effective memory usage", [DPB1_2,DB1_2], ["DPB","DB"],"graphs","DPB_DB_EM_INT");
	# draw_graph( "DPB vs DB Intersection with size A as 1000 and overlap 40%", "set size", "False Positive Probability", [DPB2_2,DB2_2], ["DPB","DB"],"graphs","DPB_DB_FP_INT");
	# draw_graph( "DPB vs DB Union with size A as 1000 and overlap 40%", "set size", "Effective Memory usage", [DPB3_2,DB3_2], ["DPB","DB"],"graphs","DPB_DB_EM_UN");
	# draw_graph( "DPB vs DB Union with size A as 1000 and overlap 40%", "set size", "False Positive Probability", [DPB4_2,DB4_2], ["DPB","DB"],"graphs","DPB_DB_FP_UN");

	# draw_graph( "DPB vs DB Intersection analysis on overlap with set sizes 1000", "Overlap fraction", "Effective memory usage", [DPB1_x,DB1_x], ["DPB","DB"],"graphs","DPB_DB_O_INT_EM");
	# draw_graph( "DPB vs DB Intersection analysis on overlap with set sizes 1000", "Overlap fraction", "False Positive Probability", [DPB2_x,DB2_x], ["DPB","DB"],"graphs","DPB_DB_O_INT_FP");
	# draw_graph( "DPB vs DB Union analysis on overlap with set sizes 1000", "Overlap fraction", "Effective memory usage", [DPB3_x,DB3_x], ["DPB","DB"],"graphs","DPB_DB_O_UN_EM");
	# draw_graph( "DPB vs DB Union analysis on overlap with set sizes 1000", "Overlap fraction", "False Positive Probability", [DPB4_x,DB4_x], ["DPB","DB"],"graphs","DPB_DB_O_UN_FP");

	# draw_graph( "DPB vs CB Intersection analysis on overlap", "Overlap fraction", "False Positive Probability with set sizes 1000", [DPB2_x,CB2_x], ["DPB","CB"],"graphs","DPB_CB_O_INT_FP");
	# draw_graph( "DPB vs CB Union analysis on overlap", "Overlap fraction", "False Positive Probability with set sizes 1000", [DPB4_x,CB4_x], ["DPB","CB"],"graphs","DPB_CB_O_UN_FP");


	print("all graphs created")

	set_sizes = [10,100,200,500,1000,2000];
	for i in range(len(set_sizes)):
		for j in range(i,len(set_sizes)):
			A = set_sizes[i]
			B = set_sizes[j]
			DPB1_x = get_graph_instance("DPBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,A,B,"Intersection"],"Overlap","EffMemUsage")
			DPB2_x = get_graph_instance("DPBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,A,B,"Intersection"],"Overlap","FP_Ratio")
			DPB3_x = get_graph_instance("DPBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,A,B,"Union"],"Overlap","EffMemUsage")
			DPB4_x = get_graph_instance("DPBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,A,B,"Union"],"Overlap","FP_Ratio")

			DB1_x = get_graph_instance("DynamicBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,A,B,"Intersection"],"Overlap","EffMemUsage")
			DB2_x = get_graph_instance("DynamicBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,A,B,"Intersection"],"Overlap","FP_Ratio")
			DB3_x = get_graph_instance("DynamicBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,A,B,"Union"],"Overlap","EffMemUsage")
			DB4_x = get_graph_instance("DynamicBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,A,B,"Union"],"Overlap","FP_Ratio")

			CB1_x = get_graph_instance("CountBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,A,B,"Intersection"],"Overlap","EffMemUsage")
			CB2_x = get_graph_instance("CountBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,A,B,"Intersection"],"Overlap","FP_Ratio")
			CB3_x = get_graph_instance("CountBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,A,B,"Union"],"Overlap","EffMemUsage")
			CB4_x = get_graph_instance("CountBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,A,B,"Union"],"Overlap","FP_Ratio")

			draw_graph( "DPB vs DB Intersection analysis on overlap with set sizes A :"+str(A)+" B :"+str(B), "Overlap fraction", "Effective memory usage", [DPB1_x,DB1_x], ["DPB","DB"],"graphs2","DPB_DB_O_INT_EM"+str(A)+'_'+str(B));
			draw_graph( "DPB vs DB Intersection analysis on overlap with set sizes A :"+str(A)+" B :"+str(B), "Overlap fraction", "False Positive Probability", [DPB2_x,DB2_x], ["DPB","DB"],"graphs2","DPB_DB_O_INT_FP"+str(A)+'_'+str(B));
			draw_graph( "DPB vs DB Union analysis on overlap with set sizes A :"+str(A)+" B :"+str(B), "Overlap fraction", "Effective memory usage", [DPB3_x,DB3_x], ["DPB","DB"],"graphs2","DPB_DB_O_UN_EM"+str(A)+'_'+str(B));
			draw_graph( "DPB vs DB Union analysis on overlap with set sizes A :"+str(A)+" B :"+str(B), "Overlap fraction", "False Positive Probability", [DPB4_x,DB4_x], ["DPB","DB"],"graphs2","DPB_DB_O_UN_FP"+str(A)+'_'+str(B));

			draw_graph( "DPB vs CB Intersection analysis on overlap A :"+str(A)+" B :"+str(B), "Overlap fraction", "False Positive Probability with set sizes 1000", [DPB2_x,CB2_x], ["DPB","CB"],"graphs2","DPB_CB_O_INT_FP"+str(A)+'_'+str(B));
			draw_graph( "DPB vs CB Union analysis on overlap A :"+str(A)+" B :"+str(B), "Overlap fraction", "False Positive Probability with set sizes 1000", [DPB4_x,CB4_x], ["DPB","CB"],"graphs2","DPB_CB_O_UN_FP"+str(A)+'_'+str(B));

# Draws graph for varying unit size in Dynamic Partition Bloom Filter
# unitSizeVals: List of unit sizes for which graph has to be created
def drawUnitSizeGraphs(unitSizeVals):

	print("Reading logs...")
	readFolder("DPBloom","solo","data/DPB_SET",20)
	print("Logs read...")

	print("Drawing graphs")
	memUsageGraphList = []
	for unitSize in unitSizeVals:
		graph = get_graph_instance("DPBloom","solo",["UnitSize"],[unitSize],"NumElems","EffMemUsage")
		memUsageGraphList.append(graph)
		
	fpRatioGraphList = []
	for unitSize in unitSizeVals:
		graph = get_graph_instance("DPBloom","solo",["UnitSize"],[unitSize],"NumElems","FP_Ratio")
		fpRatioGraphList.append(graph)
		
	draw_graph( "FPP Analysis for Different Unit Bloom Filter Sizes", "Set Size", "Empirical False Positive Probability", fpRatioGraphList, unitSizeVals ,"graphs","DPB_UnitSize_FP");
	draw_graph( "Memory Usage Analysis for Different Unit Bloom Filter Sizes", "Set Size", "Effective Memory Usage", memUsageGraphList, unitSizeVals ,"graphs","DPB_UnitSize_MemUsage");

# This is to generate plots comparing memory taken by BFs when they have same false positive probability
def compareMemory():
	numSamples 	= 20;
	unitSize 	= 1024;
	readFolder("CountBloom","solo","data/SB_SET_SameFP",numSamples)
	readFolder("DynamicBloom","solo","data/DB_SET_SameFP",numSamples)
	readFolder("DPBloom","solo","data/DBP_SET",numSamples)

	DPB = get_graph_instance("DPBloom","solo",["UnitSize"],[unitSize],"NumElems","EffMemUsage")
	SB 	= get_graph_instance("CountBloom","solo",["UnitSize"],[unitSize],"NumElems","EffMemUsage")
	DB 	= get_graph_instance("DynamicBloom","solo",["UnitSize"],[unitSize],"NumElems","EffMemUsage")

	draw_graph( "Avg Memory usage for BFs", "Set Size", "Memory Usage", [SB,DB,DPB], ["Static BF","Dynamic BF","Dynamic Partition BF"] ,"graphs","MemUsage_With_Same_FP");

	DPB = get_graph_instance("DPBloom","solo",["UnitSize"],[unitSize],"NumElems","MembershipQueryTime")
	SB 	= get_graph_instance("CountBloom","solo",["UnitSize"],[unitSize],"NumElems","MembershipQueryTime")
	DB 	= get_graph_instance("DynamicBloom","solo",["UnitSize"],[unitSize],"NumElems","MembershipQueryTime")

	draw_graph( "Membership Query Time for BFs", "Set Size", "MembershipQueryTime", [SB,DB,DPB], ["Static BF","Dynamic BF","Dynamic Partition BF"] ,"graphs","MemQueryTime_With_Same_FP");

	DPB = get_graph_instance("DPBloom","solo",["UnitSize"],[unitSize],"NumElems","TimeTaken")
	SB 	= get_graph_instance("CountBloom","solo",["UnitSize"],[unitSize],"NumElems","TimeTaken")
	DB 	= get_graph_instance("DynamicBloom","solo",["UnitSize"],[unitSize],"NumElems","TimeTaken")

	draw_graph( "Set Creation Time for BFs", "Set Size", "BF Creation Time", [SB,DB,DPB], ["Static BF","Dynamic BF","Dynamic Partition BF"] ,"graphs","BF_CreationTime_With_Same_FP");


# This is to generate plots comparing false positive probability of BFs when they take same memory
def compareFP():
	numSamples 	= 20;
	unitSize 	= 1024;
	readFolder("CountBloom","solo","data/SB_SET_SameMemUsage",numSamples)
	readFolder("DynamicBloom","solo","data/DB_SET_SameMemUsage",numSamples)
	readFolder("DPBloom","solo","data/DBP_SET",numSamples)

	DPB = get_graph_instance("DPBloom","solo",["UnitSize"],[unitSize],"NumElems","FP_Ratio")
	SB 	= get_graph_instance("CountBloom","solo",["UnitSize"],[unitSize],"NumElems","FP_Ratio")
	DB 	= get_graph_instance("DynamicBloom","solo",["UnitSize"],[unitSize],"NumElems","FP_Ratio")

	draw_graph("Empirical False Positive Probability for BFs", "Set Size", "Empirical FPP", [SB,DB,DPB], ["Static BF","Dynamic BF","Dynamic Partition BF"] ,"graphs","FP_With_Same_MemUsage");

	DPB = get_graph_instance("DPBloom","solo",["UnitSize"],[unitSize],"NumElems","MembershipQueryTime")
	SB 	= get_graph_instance("CountBloom","solo",["UnitSize"],[unitSize],"NumElems","MembershipQueryTime")
	DB 	= get_graph_instance("DynamicBloom","solo",["UnitSize"],[unitSize],"NumElems","MembershipQueryTime")

	draw_graph("Membership Query Time for BFs", "Set Size", "Membership Query Time", [SB,DB,DPB], ["Static BF","Dynamic BF","Dynamic Partition BF"] ,"graphs","MembershipQueryTime_With_Same_MemUsage");

	DPB = get_graph_instance("DPBloom","solo",["UnitSize"],[unitSize],"NumElems","TimeTaken")
	SB 	= get_graph_instance("CountBloom","solo",["UnitSize"],[unitSize],"NumElems","TimeTaken")
	DB 	= get_graph_instance("DynamicBloom","solo",["UnitSize"],[unitSize],"NumElems","TimeTaken")

	draw_graph("Set Creation Time for BFs", "Set Size", "BF Creation Time", [SB,DB,DPB], ["Static BF","Dynamic BF","Dynamic Partition BF"] ,"graphs","BF_CreationTime_With_Same_MemUsage");


if __name__ == '__main__':
	drawUnitSizeGraphs([512,1024,2048])