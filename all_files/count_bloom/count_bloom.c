#include "count_bloom.h"

/*
two level bloom filter
MAX_RANGE = larger range over which small chunks built
bloomParam->SIZE_BLOOM = bloom filter for the small chunk
HASH_ONE = the hash function for first designating the chunks
then each function is modified preferably locality sensitive hashing
*/

int aHash[10]={3730, 9830, 3830, 2110, 4740, 9440, 5540, 2710, 690, 7470};
int bHash[10]={269, 229, 929, 949, 624, 390, 64, 67, 945, 253};
int m[10];

bloomParameters* bloomParam;
char* datasetPath;

void init_bloomParameters(UINT SIZE_BLOOM ,UINT K ,UINT COUNTER_CHUNK )
{
	bloomParam = (bloomParameters*)malloc(sizeof(bloomParameters));
	bloomParam->NUM_BITS     	= 8*sizeof(UINT);
	bloomParam->SIZE_BLOOM   	= SIZE_BLOOM;
	bloomParam->K 			  	= K;
	bloomParam->COUNTER_CHUNK  	= COUNTER_CHUNK;		//should be exponents of 2	
	bloomParam->BIT_MAP 		= (UINT)pow(2,COUNTER_CHUNK) - 1 ;	//2^COUNTER_CHUNK - 1;
	bloomParam->TEST_RANGE 		= 500;
	// print_bloomParameters();
}

void print_bloomParameters()
{
	printf("NUM_BITS:%d - SIZE_BLOOM:%d - K:%d - COUNTER_CHUNK:%d - BIT_MAP:%d\n",bloomParam->NUM_BITS,bloomParam->SIZE_BLOOM,bloomParam->K,bloomParam->COUNTER_CHUNK,bloomParam->BIT_MAP );
	// printf("bitmap :: %d-->%u\n",(UINT)pow(2,1)-1,(UINT)pow(2,2)-1);
}

void seiveInitial()
{
	int num_bits = bloomParam->SIZE_BLOOM/bloomParam->COUNTER_CHUNK;
	int *x = (int*)malloc(sizeof(int)*num_bits),i,j;
	
	// for (i = 0; i < 10; ++i)
	// {
	// 	aHash[i] = rand() % num_bits;
	// 	bHash[i] = rand() % num_bits;
	// }

	for (i = 0 ; i < num_bits ; i++) 
		x[i] = i + 1;

	for (i = 1 ; i < num_bits ; i++)
	{
		j = x[i];
		if (j== -1) 
			continue;

		int q = 1;
		while ((i + q*j) < num_bits)
		{
			x[i+q*j] = -1;
			q++;
		}
	}
	i = num_bits;
	for (j =  0 ; j < 10 ; j++)
	{
		for (i = i-1 ; i > 0 ; i--)
		{
			if (x[i] != -1)
			{
				m[j] = x[i];
				break;
			}
		}
	}
}

void init(bloom *bl)
{
	int size = (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);
	bl->flag_array = (UINT *)malloc(sizeof(UINT)*size);
	memset(bl->flag_array,0,sizeof(UINT)*size);
}

void insert(UINT val, bloom* bl)
{
	UINT i;
	UINT chunk_per_ind = bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK;
	for (i=0;i<bloomParam->K;i++)
	{
		UINT a = hash(val,i);
		UINT loc = a/chunk_per_ind;
		UINT off = a%chunk_per_ind;
		UINT new = 1<<(off*bloomParam->COUNTER_CHUNK);
		UINT bitmap = (bloomParam->BIT_MAP<<(off*bloomParam->COUNTER_CHUNK));
		UINT chunk = bitmap & bl->flag_array[loc];
		// printf(" elem ::%d   K:%d -- loc:%d off:%d chunk_per_ind::%d chunk:%d\n",val,i,loc,off,chunk_per_ind,chunk);

		if(chunk==bitmap)
		{
			// printf("overflow of the bucket for counter -- making no change\n");
			continue;
		}
		bl->flag_array[loc] = bl->flag_array[loc] + new;
	}
}

void delete(UINT val, bloom* bl)
{
	// UINT id = hash_prime(val);
	// if(bl->flag_array[id]==NULL)
	// 	return;

	UINT i;
	UINT chunk_per_ind = bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK;
	if(!is_in(val,bl))
	{
		return;
	}
	for (i=0;i<bloomParam->K;i++)
	{
		UINT a = hash(val,i);
		UINT loc = a/chunk_per_ind;
		UINT off = a%chunk_per_ind;

		UINT new = 1<<(off*bloomParam->COUNTER_CHUNK);
		UINT bitmap = (bloomParam->BIT_MAP<<(off*bloomParam->COUNTER_CHUNK));
		UINT chunk = bitmap & bl->flag_array[loc];
		if(chunk>0)
		{
			bl->flag_array[loc] = bl->flag_array[loc] - new;
		}
	}
}

UINT is_in(UINT val, bloom* bl)
{
	// UINT id = hash_prime(val);
	// if(bl->flag_array[id]==NULL)
	// 	return 0;

	UINT i;
	UINT chunk_per_ind = bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK;
	for (i = 0 ; i < bloomParam->K ; i++)
	{
		UINT a = hash(val,i);
		UINT loc = a/chunk_per_ind;
		UINT off = a%chunk_per_ind;

		UINT bitmap = (bloomParam->BIT_MAP<<(off*bloomParam->COUNTER_CHUNK));
		UINT chunk = bitmap & bl->flag_array[loc];
		// printf("is_in phase elem ::%d   K:%d -- loc:%d off:%d chunk_per_ind::%d chunk:%d\n",val,i,loc,off,chunk_per_ind,chunk);

		if (!chunk) return 0;
	}
	return 1;
}

UINT hash(int a, int i)
{
	unsigned long z = aHash[i];
	z = z*a + bHash[i];
	UINT retVal = (z % m[i]);
	return retVal;
}

void free_bloom(bloom *bl)
{
	free(bl->flag_array);
}

UINT * reconstruct_bloom(bloom* bl, UINT start, UINT end )
{
	UINT i;
	UINT num_elems,elem = 0;
	for(i = start; i <= end; i++)
	{
		if(is_in(i,bl))
			num_elems++;
	}

	UINT * val_array = (UINT *)malloc(sizeof(UINT)*num_elems);
	
	memset(val_array, 0, sizeof(UINT)*num_elems);
	for(i = start; i <= end; i++)
	{
		if(is_in(i,bl))
		{
			val_array[elem] = i;
			elem++;
		}
	}
	return val_array;
}

void intersect_bloom(bloom *a, bloom *b, bloom *c)
{
	UINT i,j,num,check=0;
	for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			temp += MIN(a->flag_array[i]&new,b->flag_array[i]&new);
		}
		if(temp>0)
			check = 1;
		c->flag_array[i] = temp;
	}
	// if(check==0)
	// 	return 0;			//freeing memory if no intersection at all
	// return 1;
}

void subtract_bloom(bloom *a, bloom *b, bloom *c)
{
	UINT i,j,check,num=0;
	for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			if ( (a->flag_array[i]&new) >= (b->flag_array[i]&new) )
				temp += (a->flag_array[i]&new)-(b->flag_array[i]&new);
		}
		if(temp!=0)
			check = 1;
		c->flag_array[i] = temp;
	}
	// if(check==0)
	// 	return 0;
	// return 1;
}

void add_bloom(bloom *a, bloom *b, bloom *c)
{
	UINT i,j,num=0;
	for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			UINT curr = (a->flag_array[i]&new)+(b->flag_array[i]&new);
			if(curr > new && j!=(bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK-1))
			{
				curr = new;
			}
			else if(j==(bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK-1))			//as this chunk stores the sign bit so it is dealt separately as in this case it can overflow
			{
				curr = (a->flag_array[i]&new)>>2+(b->flag_array[i]&new)>>2;
				if(curr > (new>>2))
					curr = new;
				else
					curr = curr<<2;
			}
			temp+=curr;
		}
		c->flag_array[i] = temp;
	}
	// return 1;
}

void union_bloom(bloom *a, bloom *b, bloom *c)
{
	UINT i,j,num=0;
	for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			temp += MAX(a->flag_array[i]&new,b->flag_array[i]&new);
		}
		c->flag_array[i] = temp;
	}
	// return 1;
}

void bloom_test_suit1()
{
	bloom *a = (bloom*)malloc(sizeof(bloom));
	init(a);
	UINT i;
	printf("bloom graph insert/member/delete test initiated\n");
	for(i = 0 ; i < bloomParam->TEST_RANGE ; i++ )
	{
		UINT z = i;//rand()%bloomParam->TEST_RANGE;
		insert(z,a);
		if(!is_in(z,a))
		{
			printf("bloom graph not functioning properly - inserted val :: %d not found\n",z);
			exit(0);
		}
		delete(z,a);
	}

	printf("count of elems :: %u  %u \n",count_elems(a),bloomParam->TEST_RANGE );
	free_bloom(a);
	free(a);
	printf("bloom graph insert/member/delete test successful\n");
}

void bloom_test_suit2()
{
	bloom *a = (bloom*)malloc(sizeof(bloom));
	bloom *b = (bloom*)malloc(sizeof(bloom));

	init(a);	
	init(b);

	UINT i;
	printf("bloom graph union/intersect test initiated bloomParam->SIZE_BLOOM :: %u\n",bloomParam->SIZE_BLOOM);
	UINT count_a = 0,count_b = 0;
	for( i = 0 ; i < bloomParam->TEST_RANGE ; i++ )
	{
		if(i%2==0)
		{
			insert(i,a);
			if(!is_in(i,a))
				printf("element not found a %u\n",i);
			// if(is_in(i,b))
			// 	printf("incorrect element found in b %u\n",i);
			count_a++;
		}
		else
		{
			insert(i,b);
			if(!is_in(i,b))
				printf("element not found b  %u\n",i);
			// if(is_in(i,a))
			// 	printf("incorrect element found in a  %u\n",i);
			count_b++;
		}
	}
	printf("insertion stage over\n");
	printf("size a :: %u actual:: %u\n",count_elems(a),count_a );	//the number of elements may be less as some counter may completely fill up
	printf("size b :: %u actual:: %u\n",count_elems(b),count_b );	//

	bloom * c = (bloom *)malloc(sizeof(bloom));
	init(c);
	intersect_bloom(a,b,c);
	
	bloom * d = (bloom *)malloc(sizeof(bloom));
	init(d);	
	union_bloom(a,b,d);

	bloom * e = (bloom *)malloc(sizeof(bloom));
	init(e);
	add_bloom(a,b,e);



	printf("union/intersection operations executed -- size union size::%u intersect::%u addition::%u\n",count_elems(d),count_elems(c),count_elems(e));
	printf("intersection count via-- current elemsIntersection::%f \n",elemsIntersection(a,b,num_ones(a),num_ones(b)));
	// exit(0);
	// if(is_in(bloomParam->TEST_RANGE*2,a)==1)
	// {
	// 	printf("insertion conflict found - %u\n",bloomParam->TEST_RANGE*2);
	// 	exit(0);
	// }
	ULONG con_union = 0;
	ULONG con_inter = 0;
	for( i = 0 ; i < bloomParam->TEST_RANGE ; i++ )
	{

		if(!is_in(i,d))
		{
			// printf("union not working correctly\n");
			con_union++;
			// exit(0);
		}
		if(is_in(i,c)==1)
		{
			// printf("intersect conflict found - %d\n",i);
			con_inter++;
		}
	}
	printf("errors_union :: %llu errors_intersect :: %llu \n",con_union,con_inter );

	free_bloom(a);
	free_bloom(b);
	free_bloom(c);
	free_bloom(e);
	free(a);
	free(b);
	free(c);
	free(e);
	printf("bloom graph union/intersect test successful\n");
}

UINT count_elems(bloom *a)
{
	UINT count = num_ones(a)/bloomParam->K;
	return count;
}

UINT num_ones(bloom *a)
{
	UINT i = 0, n = 0;
	for (i = 0 ; i < bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS ; i++)
		n += count_ones(a->flag_array[i]);
	return n;
}

UINT numOnes_intersection(bloom *a, bloom *b)
{
	UINT i , j, num = 0, numCounts = 0;

	for (i=0;i<(bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		UINT temp = 0;
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			 UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			 UINT curr = 0;
			if(new<0)
			{
				curr = MIN( (a->flag_array[i]&new)>>bloomParam->COUNTER_CHUNK , (b->flag_array[i]&new)>>bloomParam->COUNTER_CHUNK );
				curr = curr<<bloomParam->COUNTER_CHUNK;
			}
			else
				curr = MIN( (a->flag_array[i]&new) , (b->flag_array[i]&new) );
			temp+=curr;
		}
		num += count_ones(temp);										//can be made efficient
	}
	return num;
}

double elemsIntersection(bloom *a, bloom *b, int ta, int tb)
{
	double t1 = (double)ta;
	double t2 = (double)tb;
	double th = (double)numOnes_intersection(a,b);
	return th/bloomParam->K;
}

UINT count_ones(UINT x)
{
	UINT i = 0;
	UINT count = 0;
	for(i = 0 ; i < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; i++)
	{
		UINT new = bloomParam->BIT_MAP<<(i*bloomParam->COUNTER_CHUNK);
		UINT intersect = x&new;
		count += intersect>>(i*bloomParam->COUNTER_CHUNK);
	}

	return count;
}

// Reads data from file and inserts them in the bloom filter
void read_and_insert(char * filename, bloom* filter)
{
	FILE * reader;
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,filename);
	reader = fopen(fullFilename,"r");
	UINT i,num_elems = 0,namespace = 1;
	fscanf(reader,"%d",&num_elems);
	fscanf(reader,"%d",&namespace);
	for(i = 0 ; i < num_elems ; i++)
	{
		int elem = 0;
		fscanf(reader,"%d",&elem);
		insert(elem,filter);
	}
	fclose(reader);
}

// Estimates memory usage by bloom filter for different set of points
void test_memory_usage(char** filenameList,int numFiles)
{
	FILE* logger;
	if( access( "../logs/bloom_individual.txt", F_OK ) == -1 ) 
	{
		logger = fopen("../logs/bloom_individual.txt","w");
		fprintf(logger,   "%s\n","File A,File B,Namespace,hash,size,CtrSize,numA, fpA, numB, fpB ,numUnion, fpUnion , numIntersect, fpIntersect,NULL");

	}
	else
	{
		logger = fopen("../logs/bloom_individual.txt","a");
	}

	int i;
	for (i = 0; i < numFiles; ++i)
	{
		bloom* filter = (bloom*)malloc(sizeof(bloom));
		init(filter);
		read_and_insert(filenameList[i],filter);
		char fullFilename[1000];
		sprintf(fullFilename,"%s%s",datasetPath,filenameList[i]);
		FILE * reader = fopen(fullFilename,"r");
		int num_elems,namespace;
		fscanf(reader,"%d",&num_elems);
		fscanf(reader,"%d",&namespace);
		fclose(reader);

		fprintf(logger, "%s,",filenameList[i]);
		fprintf(logger, "%d,",num_elems);
		fprintf(logger, "%d,",namespace);
		fprintf(logger, "%d,",bloomParam->K);
		fprintf(logger, "%d,",bloomParam->SIZE_BLOOM/8);
		fprintf(logger, "%d,",bloomParam->COUNTER_CHUNK);

		estimate_false_positive(filenameList[i],filter,logger);
		fprintf(logger, "NULL\n");
	}
	fclose(logger);
}

// Takes a bloom filter and estimates number of false positives in it.
void estimate_false_positive(char* filename,bloom* filter, FILE* logger)
{
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,filename);
	FILE * reader = fopen(fullFilename,"r");
	int num_elems,namespace;
	fscanf(reader,"%d",&num_elems);
	fscanf(reader,"%d",&namespace);
	fclose(reader);
	int i;
	int numPresent = 0;
	for (i = 1; i <= namespace; ++i)
	{
		if(is_in(i,filter))
		{
			numPresent++;
			// printf("Element present::%d\n",i );
		}
	}
	fprintf(logger,"%.7f,",(numPresent - num_elems)/(float)namespace);
}


void test_union_intersection(char** filenameList_1,char** filenameList_2,int numFiles)
{
	FILE* logger;
	if( access( "../logs/bloom_set_ops.txt", F_OK ) == -1 ) 
	{
		logger = fopen("../logs/bloom_set_ops.txt","w");
		fprintf(logger,   "%s\n","File A,File B,Namespace,hash,size,CtrSize,numA, fpA, numB, fpB ,numUnion, fpUnion , numIntersect, fpIntersect,NULL");
	}
	else
	{
		logger = fopen("../logs/bloom_set_ops.txt","a");
	}

	// fprintf(logger,   "%s\n","File A,File B,Union File,Intersection File,Namespace,NumHashFunc,Size of BF,CtrSize,
	// No. of Elems (A),No. of false positives, Ratio of false positives to set size,
	// No. of Elems (B),No. of false positives, Ratio of false positives to set size,
	// No. of Elems (A union B),No. of false positives, Ratio of false positives to set size,
	// No. of Elems (A intersect B),No. of false positives, Ratio of false positives to set size,NULL");

	int i;
	for (i = 0; i < numFiles; ++i)
	{
		char union_filename[1000];
		char intersect_filename[1000];
		sprintf(union_filename		,"%s_%s_union",		filenameList_1[i],filenameList_2[i]);
		sprintf(intersect_filename	,"%s_%s_intersect",	filenameList_1[i],filenameList_2[i]);

		// printf("%s,  %s,  %s,  %s\n",filenameList_1[i],filenameList_2[i],union_filename, intersect_filename);
		bloom* filter_1 		= (bloom*)malloc(sizeof(bloom));
		bloom* filter_2 		= (bloom*)malloc(sizeof(bloom));
		bloom* filter_union 	= (bloom*)malloc(sizeof(bloom));
		bloom* filter_intersect = (bloom*)malloc(sizeof(bloom));
		init(filter_1);
		init(filter_2);
		init(filter_union);
		init(filter_intersect);
	
		FILE * reader;
		int namespace;
		int num_elems_1,num_elems_2, num_elems_union, num_elems_intersect;

		char* fullFilename = (char*)malloc(1000*sizeof(char));
		sprintf(fullFilename,"%s%s",datasetPath,filenameList_1[i]);
		reader = fopen(fullFilename,"r");
		fscanf(reader,"%d",&num_elems_1);
		fscanf(reader,"%d",&namespace);
		fclose(reader);
		free(fullFilename);

		fullFilename = (char*)malloc(1000*sizeof(char));
		sprintf(fullFilename,"%s%s",datasetPath,filenameList_2[i]);
		reader = fopen(fullFilename,"r");
		fscanf(reader,"%d",&num_elems_2);
		fclose(reader);
		free(fullFilename);

		fullFilename = (char*)malloc(1000*sizeof(char));
		sprintf(fullFilename,"%s%s",datasetPath,union_filename);
		reader = fopen(fullFilename,"r");
		fscanf(reader,"%d",&num_elems_union);
		fclose(reader);
		free(fullFilename);

		fullFilename = (char*)malloc(1000*sizeof(char));
		sprintf(fullFilename,"%s%s",datasetPath,intersect_filename);
		reader = fopen(fullFilename,"r");
		fscanf(reader,"%d",&num_elems_intersect);
		fclose(reader);
		free(fullFilename);

		fprintf(logger, "%s,", filenameList_1[i]);
		fprintf(logger, "%s,", filenameList_2[i]);
		// fprintf(logger, "%s_%s_union,", filenameList_1[i],filenameList_2[i]);
		// fprintf(logger, "%s_%s_intersect,", filenameList_1[i],filenameList_2[i]);

		fprintf(logger, "%d,",namespace);
		fprintf(logger, "%d,",bloomParam->K);
		fprintf(logger, "%d,",bloomParam->SIZE_BLOOM/8);
		fprintf(logger, "%d,",bloomParam->COUNTER_CHUNK);
		
		read_and_insert(filenameList_1[i],filter_1);
		read_and_insert(filenameList_2[i],filter_2);
		union_bloom(filter_1,filter_2,filter_union);
		intersect_bloom(filter_1,filter_2,filter_intersect);

		// Details for set A
		fprintf(logger, "%d,",num_elems_1 );
		estimate_false_positive(filenameList_1[i],filter_1,logger);

		// Details for set B
		fprintf(logger, "%d,",num_elems_2 );
		estimate_false_positive(filenameList_2[i],filter_2,logger);
	
		// Details for set (A union B)
		fprintf(logger, "%d,",num_elems_union );
		estimate_false_positive(union_filename, filter_union,logger);

		// Details for set (A intersect B)
		fprintf(logger, "%d,",num_elems_intersect );
		estimate_false_positive(intersect_filename,filter_intersect,logger);

		// print
		fprintf(logger, "NULL\n");
	}

	fclose(logger);
}

void print_bloom_filter(bloom * a)
{
	int i,j,k;
	for(i=0;i< (bloomParam->SIZE_BLOOM/bloomParam->NUM_BITS);i++)
	{
		for( j = 0 ; j < bloomParam->NUM_BITS/bloomParam->COUNTER_CHUNK ; j++)
		{
			UINT new = bloomParam->BIT_MAP<<(j*bloomParam->COUNTER_CHUNK);
			printf("%0*d ",2,(a->flag_array[i]&new)>>(j*bloomParam->COUNTER_CHUNK));
		}
	}
	printf("\n");
}

void run_memory_tests()
{
	int i,j,k;
	int numFiles = 5;
	char** filenameList = (char**)malloc(numFiles*sizeof(char*));
	for (i = 0; i < numFiles; ++i)
	{
		filenameList[i] = (char*)malloc(sizeof(char)*100);
	}
	strcpy(filenameList[0],"rand::10");
	strcpy(filenameList[1],"rand::100");
	strcpy(filenameList[2],"rand::500");
	strcpy(filenameList[3],"rand::1000");
	strcpy(filenameList[4],"rand::2000");

	int num_K_Vals 			= 2;
	int num_CtrSizeVals 	= 1;
	int num_filterSizeVals 	= 4;

	int K_Vals[2] 			= {3,5};
	int ctrSizeVals[1]	 	= {2};
	int filterSizeVals[4] 	= {1024*1,1024*2,1024*4,1024*8};//,1024*8,1024*10,1024*15,1024*20,1024*40,1024*60,1024*80};

	for (i = 0; i < num_filterSizeVals; ++i)
	{
		for (j = 0; j < num_K_Vals; ++j)
		{
			for (k = 0; k < num_CtrSizeVals; ++k)
			{
				printf("Running test for K = %d, Ctr Size = %d, FilterSize = %d\n",K_Vals[j], ctrSizeVals[k], filterSizeVals[i]);
				init_bloomParameters(filterSizeVals[i],K_Vals[j],ctrSizeVals[k]); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
				seiveInitial(); // Important to initialize it here to reset m[i] values to suit the changed values of SIZE_BLOOM

				test_memory_usage(filenameList,numFiles);				
			}
		}
	}
	// init_bloomParameters(10240,3,2); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
	// seiveInitial(); // Important to initialize it here to reset m[i] values to suit the changed values of SIZE_BLOOM
	// test_memory_usage(filenameList,2);
}

void getFilenames(char** filenameList_1,char** filenameList_2,int * numFiles)
{
	filenameList_1 = (char**)malloc(1000*sizeof(char*));
	filenameList_2 = (char**)malloc(1000*sizeof(char*));
	FILE* reader ;
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,"filenameList");
	reader = fopen(fullFilename,"r");
	
	int ctr  = 0;
	while(!feof(reader))
	{
		filenameList_1[ctr] = (char*)malloc(200*sizeof(char));
		filenameList_2[ctr] = (char*)malloc(200*sizeof(char));
		fscanf(reader,"%s %s",filenameList_1[ctr],filenameList_2[ctr]);
		ctr += 1;
	}
	*numFiles = ctr;
	printf("Filenamelist size::%d\n",ctr );
}

void run_set_operation_tests()
{
	// int numFiles = 10;
	// char** masterFilenameList = (char**)malloc(numFiles*sizeof(char*));
	// for (i = 0; i < numFiles; ++i)
	// {
	// 	masterFilenameList[i] = (char*)malloc(sizeof(char)*200);
	// }
	// strcpy(masterFilenameList[0],"rand::10");
	// strcpy(masterFilenameList[1],"rand::50");
	// strcpy(masterFilenameList[2],"rand::100");
	// strcpy(masterFilenameList[3],"rand::200");
	// strcpy(masterFilenameList[4],"rand::400");
	// strcpy(masterFilenameList[5],"rand::600");
	// strcpy(masterFilenameList[6],"rand::800");
	// strcpy(masterFilenameList[7],"rand::1000");
	// strcpy(masterFilenameList[8],"rand::1500");
	// strcpy(masterFilenameList[9],"rand::2000");
	
	int numFiles = 10;
	char ** filenameList_1;
	char **filenameList_2;
	filenameList_1 = (char**)malloc(1000*sizeof(char*));
	filenameList_2 = (char**)malloc(1000*sizeof(char*));
	FILE* reader ;
	char fullFilename[1000];
	sprintf(fullFilename,"%s%s",datasetPath,"filenameList");
	reader = fopen(fullFilename,"r");
	
	int ctr  = 0;
	while(!feof(reader))
	{
		filenameList_1[ctr] = (char*)malloc(200*sizeof(char));
		filenameList_2[ctr] = (char*)malloc(200*sizeof(char));
		fscanf(reader,"%s %s",filenameList_1[ctr],filenameList_2[ctr]);
		ctr += 1;
	}
	numFiles = ctr;

	int num_K_Vals 			= 1;
	int num_CtrSizeVals 	= 1;
	int num_filterSizeVals 	= 5;

	int K_Vals[1] 			= {5};
	int ctrSizeVals[1]	 	= {1};
	int filterSizeVals[5] 	= {512,1024*1,1024*2,1024*4,1024*8};//,1024*8,1024*10,1024*15,1024*20,1024*40,1024*60,1024*80};
	// int filterSizeVals[12] 	= {1024*1,1024*2,1024*3,1024*4,1024*6,1024*8,1024*10,1024*15,1024*20,1024*40,1024*60,1024*80};
	int i,j,k;
	for (i = 0; i < num_filterSizeVals; ++i)
	{
		for (j = 0; j < num_K_Vals; ++j)
		{
			for (k = 0; k < num_CtrSizeVals; ++k)
			{
				printf("Running test for K = %d, Ctr Size = %d, FilterSize = %d\n",K_Vals[j], ctrSizeVals[k], filterSizeVals[i]);
				init_bloomParameters(filterSizeVals[i],K_Vals[j],ctrSizeVals[k]); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
				seiveInitial(); // Important to initialize it here to reset m[i] values to suit the changed values of SIZE_BLOOM
				test_union_intersection(filenameList_1,filenameList_2,numFiles);
			}
		}
	}
	// init_bloomParameters(10240,3,2); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
	// seiveInitial(); // Important to initialize it here to reset m[i] values to suit the changed values of SIZE_BLOOM

	// test_union_intersection(filenameList,filenameList,2);
}

// This function reads bloom filter size from a file which contain results from dynamic_partition so that we can
// allocate a static bloom filter of the same size so that we can compare the performance
// unitSize is the size of unit bloom filter used in dynPartition bloom filter
void run_set_creation_tests(int unitSize)
{
	// It is important for referenceFileName to be tab separated data, somehow it makes things easier to parse
	char* filename;
	char referenceFilename[1000];
	char temp[1000];
	sprintf(referenceFilename,"../logs/SetCreationUnitSizeAnalysis_DynPart/%d.tsv",unitSize); // Name of file containing dynPartition's output

	FILE* refReader;
	refReader = fopen(referenceFilename,"r");
	fscanf(refReader,"%s\t%s\t%s\t%s\t%s\t%s\t%s",temp,temp,temp,temp,temp,temp,temp); // Reading header out

	int size,numBFs,memUsage;
	float fpRatio;
	char nullStr[100];
	UINT K 				= 3;
	UINT COUNTER_SIZE 	= 1;
	int tempUnitSize 		= 1;
	FILE* logger;
	char logFilename[1000];
	mkdir("../logs/SetCreationUnitSizeAnalysis_Static",S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	sprintf(logFilename,"../logs/SetCreationUnitSizeAnalysis_Static/%d.csv",unitSize);
	// if( access( logFilename, F_OK ) == -1 ) 
	// {
	// 	logger = fopen(logFilename,"w");
	// 	fprintf(logger,"%s","FileName,NumElems,EffMemUsage,FP_Ratio,NULL");
	// }
	// else
	// {
	// 	logger = fopen(logFilename,"a");
	// }
	logger = fopen(logFilename,"w");
	fprintf(logger,"%s","FileName,NumElems,UnitSize,EffMemUsage,FP_Ratio,NULL");

	while(!feof(refReader))
	{
		fprintf(logger, "\n");
		filename 	= (char*)malloc(1000*sizeof(char));
		fscanf(refReader,"%s\t%d\t%d\t%d\t%d\t%f\t%s",filename,&size,&tempUnitSize,&numBFs,&memUsage,&fpRatio,nullStr);
		
		if(tempUnitSize != unitSize)
		{
			printf("We are reading wrong file, unitSizes do not match::%d!=%d\n",tempUnitSize,unitSize );
			exit(0);
		}
		init_bloomParameters(memUsage,K,COUNTER_SIZE); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
		seiveInitial(); // Important to initialize it here to reset m[i] values to suit the changed values of SIZE_BLOOM

		///////////////////////////////Read file and insert in bloom filter///////////////////////////////

		bloom* filter = (bloom*)malloc(sizeof(bloom));
		init(filter);
		read_and_insert(filename,filter);

		char fullFilename[1000];
		sprintf(fullFilename,"%s%s",datasetPath,filename);
		FILE * reader = fopen(fullFilename,"r");
		int num_elems,namespace;
		fscanf(reader,"%d",&num_elems);
		fscanf(reader,"%d",&namespace);
		fclose(reader);

		fprintf(logger, "%s,",filename);
		fprintf(logger, "%d,",num_elems);
		fprintf(logger, "%d,",unitSize);
		fprintf(logger, "%d,",bloomParam->SIZE_BLOOM);
		estimate_false_positive(filename,filter,logger);
		fprintf(logger, "NULL");
		///////////////////////////////////////////////////////////////////////////////////////////////
		free(filename);
	}
	fclose(refReader);
}

void run_intersection_overlap()
{
	UINT K 				= 3;
	UINT COUNTER_SIZE 	= 1;
	int filesizes[]   = {10,100,200,500,1000,2000};
	int num_filesizes = 6;
	int num_samples   = 2; 
	UINT namespace    = 100000;
	UINT unitSize;
	float floatJunk;
	int num_elems_A;
	int num_elems_B;
	int num_elems_intersect;
	int intJunk;
	int memUsage;


	int i,j,l;
	float overlap;
	for(i = 0 ; i < num_filesizes ; i++)
	{
		for(j = i ; j < num_filesizes ; j++)
		{
			char referenceFilename[1000];
			char temp[1000];
			sprintf(referenceFilename,"../logs/intersection_dynPartition_overlap/%d_%d.tsv",filesizes[i],filesizes[j]); // Name of file containing dynPartition's output

			FILE* refReader;
			refReader = fopen(referenceFilename,"r");
			fscanf(refReader,"%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s",temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp); // Reading header out

			FILE* logger;
			char logFilename[1000];
			mkdir("../logs/intersection_Static_overlap",S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
			sprintf(logFilename,"../logs/intersection_Static_overlap/%d_%d.csv",filesizes[i],filesizes[j]);
			logger = fopen(logFilename,"w");
			fprintf(logger,"%s","FileA,FileB,ResultFile,Overlap,Operation,Namespace,NumHashFunc,UnitSize,NumElemsA,NumElemsB,NumElemsResult,EffMemUsage,FP_Ratio,NULL");

			while(!feof(refReader))
			{
				char* file_A,*file_B;
				char * intersect_filename;

				file_A = (char*)malloc(1000*sizeof(char));
				file_B = (char*)malloc(1000*sizeof(char));
				intersect_filename = (char*)malloc(1000*sizeof(char));

				fscanf(refReader,"%s\t%s\t%s\t%f\t%s\t%d\t%d\t%d\t%f\t%f\t%d\t%d\t%d\t%d\t%d\t%f\t%s",file_A,file_B,intersect_filename,&overlap,temp,&namespace,&K,&unitSize,&floatJunk,&floatJunk,&num_elems_A,&num_elems_B,&num_elems_intersect,&intJunk,&memUsage,&floatJunk,temp);

				init_bloomParameters(memUsage,K,COUNTER_SIZE); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
				seiveInitial();
			
				bloom* filter_1 		= (bloom*)malloc(sizeof(bloom));
				bloom* filter_2 		= (bloom*)malloc(sizeof(bloom));
				bloom* filter_intersect = (bloom*)malloc(sizeof(bloom));
				init(filter_1);
				init(filter_2);
				init(filter_intersect);

				FILE * reader;
				int namespace;
				int num_elems_1,num_elems_2, num_elems_union, num_elems_intersect;

				// printf("file_A::%s\n",file_A );
				// printf("file_B::%s\n",file_B );
				// printf("file_C::%s\n",intersect_filename );
				fprintf(logger, "\n%s,", file_A);
				fprintf(logger, "%s,", file_B);
				fprintf(logger, "%s,", intersect_filename);
				fprintf(logger, "%.5f,", overlap);
				fprintf(logger, "Intersection,");

				fprintf(logger, "%d,",namespace);
				fprintf(logger, "%d,",bloomParam->K);
				fprintf(logger, "%d,",unitSize);
				
				read_and_insert(file_A,filter_1);
				read_and_insert(file_B,filter_2);
				intersect_bloom(filter_1,filter_2,filter_intersect);

				// Details for set A
				fprintf(logger, "%d,",num_elems_A );

				// Details for set B
				fprintf(logger, "%d,",num_elems_B );

				// Details for set (A intersect B)
				fprintf(logger, "%d,",num_elems_intersect );
				fprintf(logger, "%d,",bloomParam->SIZE_BLOOM);
				estimate_false_positive(intersect_filename,filter_intersect,logger);

				// print
				fprintf(logger, "NULL");

			}
		}
	}

}

void run_union_overlap()
{
	UINT K 				= 3;
	UINT COUNTER_SIZE 	= 1;
	int filesizes[]   = {10,100,200,500,1000,2000};
	int num_filesizes = 6;
	int num_samples   = 20; 
	UINT namespace    = 100000;
	UINT unitSize;
	float floatJunk;
	int num_elems_A;
	int num_elems_B;
	int num_elems_union;
	int intJunk;
	int memUsage;

	mkdir("../logs/union_Static_overlap",S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

	int i,j,l;
	float overlap;
	for(i = 0 ; i < num_filesizes ; i++)
	{
		for(j = i ; j < num_filesizes ; j++)
		{
			char referenceFilename[1000];
			char temp[1000];
			sprintf(referenceFilename,"../logs/union_dynPartition_overlap/%d_%d.tsv",filesizes[i],filesizes[j]); // Name of file containing dynPartition's output

			FILE* refReader;
			refReader = fopen(referenceFilename,"r");
			fscanf(refReader,"%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s",temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp); // Reading header out

			FILE* logger;
			char logFilename[1000];
			sprintf(logFilename,"../logs/union_Static_overlap/%d_%d.csv",filesizes[i],filesizes[j]);
			logger = fopen(logFilename,"w");
			fprintf(logger,"%s","FileA,FileB,ResultFile,Overlap,Operation,Namespace,NumHashFunc,UnitSize,NumElemsA,NumElemsB,NumElemsResult,EffMemUsage,FP_Ratio,NULL");

			while(!feof(refReader))
			{
				char* file_A,*file_B;
				char * union_filename;

				file_A = (char*)malloc(1000*sizeof(char));
				file_B = (char*)malloc(1000*sizeof(char));
				union_filename = (char*)malloc(1000*sizeof(char));

				fscanf(refReader,"%s\t%s\t%s\t%f\t%s\t%d\t%d\t%d\t%f\t%f\t%d\t%d\t%d\t%d\t%d\t%f\t%s",file_A,file_B,union_filename,&overlap,temp,&namespace,&K,&unitSize,&floatJunk,&floatJunk,&num_elems_A,&num_elems_B,&num_elems_union,&intJunk,&memUsage,&floatJunk,temp);

				init_bloomParameters(memUsage,K,COUNTER_SIZE); // Sets SIZE_BLOOM, K, and COUNTER_SIZE
				seiveInitial();
			
				bloom* filter_1 		= (bloom*)malloc(sizeof(bloom));
				bloom* filter_2 		= (bloom*)malloc(sizeof(bloom));
				bloom* filter_union = (bloom*)malloc(sizeof(bloom));
				init(filter_1);
				init(filter_2);
				init(filter_union);

				FILE * reader;
				int namespace;
				int num_elems_1,num_elems_2, num_elems_union;

				// printf("file_A::%s\n",file_A );
				// printf("file_B::%s\n",file_B );
				// printf("file_C::%s\n",union_filename );
				fprintf(logger, "\n%s,", file_A);
				fprintf(logger, "%s,", file_B);
				fprintf(logger, "%s,", union_filename);
				fprintf(logger, "%.5f,", overlap);
				fprintf(logger, "Union,");

				fprintf(logger, "%d,",namespace);
				fprintf(logger, "%d,",bloomParam->K);
				fprintf(logger, "%d,",unitSize);
				
				read_and_insert(file_A,filter_1);
				read_and_insert(file_B,filter_2);
				union_bloom(filter_1,filter_2,filter_union);

				// Details for set A
				fprintf(logger, "%d,",num_elems_A );

				// Details for set B
				fprintf(logger, "%d,",num_elems_B );

				// Details for set (A intersect B)
				fprintf(logger, "%d,",num_elems_union );
				fprintf(logger, "%d,",bloomParam->SIZE_BLOOM);
				estimate_false_positive(union_filename,filter_union,logger);

				// print
				fprintf(logger, "NULL");

			}
		}
	}

}

int main()
{
	datasetPath = (char*)malloc(1000*sizeof(char));
	strcpy(datasetPath,"../datasets/");
	time_t t;
	srand((unsigned) time(&t));	

	// init_bloomParameters( 4096 ,2 ,1 );
	// print_bloomParameters();
	// seiveInitial();
	// printf("%u\n",32/1);
	// run_memory_tests();
	// run_set_operation_tests();
	// bloom_test_suit1();
	// bloom_test_suit2();

	// run_set_creation_tests(1024);
	// run_set_creation_tests(2048);
	run_intersection_overlap();
	run_union_overlap();
}