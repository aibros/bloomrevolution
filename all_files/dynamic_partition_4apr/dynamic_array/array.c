#include "array.h"

void arrayInt_init(my_array* array)
{
	array->elements 	= (unsigned int*)malloc(8*sizeof(unsigned int));
	array->curr_size 	= 0;
	array->max_size 	= 8;
}

void arrayInt_addElement(my_array* array,unsigned int element)
{
	if (array == NULL)
	{
		array = (my_array*)malloc(sizeof(my_array));
		arrayInt_init(array);
	}
	if (array->curr_size < array->max_size)
	{
		array->elements[array->curr_size] = element;
		array->curr_size++;
	}
	else // Need to double array size
	{	
		unsigned int* new_list = (unsigned int*)malloc(2*(array->max_size)*sizeof(unsigned int));
		unsigned int i;
		for (i = 0; i < array->max_size; ++i)
		{
			new_list[i] = array->elements[i];
		}
		new_list[array->curr_size] = element;
		array->curr_size++;

		array->max_size *= 2;
		free(array->elements);
		array->elements  = new_list;
	}
}

void arrayInt_free(my_array* array)
{
	free(array->elements);
}

int arrayInt_find(my_array* array,unsigned int element)
{
	unsigned int i;
	for (i = 0; i < array->curr_size; ++i)
	{
		if (array->elements[i] == element)
			return 1;
	}
	return 0;
}

void arrayLong_init(my_array_long* array)
{
	array->elements 	= (unsigned long*)malloc(8*sizeof(unsigned long));
	array->curr_size 	= 0;
	array->max_size 	= 8;
}

void arrayLong_addElement(my_array_long* array,unsigned long element)
{
	if (array == NULL)
	{
		array = (my_array_long*)malloc(sizeof(my_array_long));
		arrayLong_init(array);
		printf("Array was NULL\n");
	}
	if (array->curr_size < array->max_size)
	{
		array->elements[array->curr_size] = element;
		array->curr_size++;
	}
	else // Need to double array size
	{	
		unsigned long* new_list = (unsigned long*)malloc(2*(array->max_size)*sizeof(unsigned long));
		unsigned int i;
		for (i = 0; i < array->max_size; ++i)
		{
			new_list[i] = array->elements[i];
		}
		new_list[array->curr_size] = element;
		array->curr_size++;

		array->max_size *= 2;
		free(array->elements);
		array->elements  = new_list;
	}
}

void arrayLong_free(my_array_long* array)
{
	free(array->elements);
}

int arrayLong_find(my_array_long* array,unsigned long element)
{
	unsigned int i;
	for (i = 0; i < array->curr_size; ++i)
	{
		if (array->elements[i] == element)
			return 1;
	}
	return 0;
}

void print_elems(my_array* array)
{
	int i = 0;
	for(i = 0 ; i < array->curr_size ; i++)
	{
		printf("%d ",array->elements[i] );
	}
	printf("\n");
}