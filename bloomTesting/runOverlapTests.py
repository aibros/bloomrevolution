#!/usr/bin/python

import os,sys

def run():
	# Generate datasets
	namespace 		= 1000000
	fp_rate 		= 0.01
	K 				= 3
	unitSize 		= 1024
	numSamples 		= 10
	numSets			= 810
	overlapSizes 	= [10,20,50,100,200,400,1000]
	overlapSizes 	= [100]
	numOverlapSizes = len(overlapSizes)
	overlapSizesStr = " "
	for overlap in overlapSizes:
		overlapSizesStr += str(overlap) + " "

	print "Creating datasets..."
	command = "./setGenerator.py 2 datasets 1 " + str(namespace) + " " + str(numSamples) + " " + str(numOverlapSizes) + overlapSizesStr
	os.system(command) 	
	print "Created datasets"

	print "Running tests for Dynamic partition bloom"
	os.chdir("../DP_bloom/test_folder/bloomFilterTests")
	command = "make"
	os.system(command)
	command = "./test 2 " + str(namespace) + " " + str(K) + " " + str(fp_rate) + " " + str(unitSize)
	command += " " + str(numSets) + " " + str(numOverlapSizes) + overlapSizesStr
	print command
	os.system(command)

	print "Running tests for dynamic_bloom"
	os.chdir("../../../dynamic_bloom/test_folder/bloomFilterTests")
	command = "make"
	os.system(command)
	command = "./test 2 " + str(namespace) + " " + str(K) + " " + str(fp_rate) + " " + str(unitSize)
	command += " " + str(numSets) + " " + str(numOverlapSizes) + overlapSizesStr
	print command
	os.system(command)

	print "Running tests for standard BF"
	os.chdir("../../../count_bloom/test_folder/bloomFilterTests")
	command = "make"
	os.system(command)
	command = "./test 2 " + str(K) + " " + str(fp_rate) + " " + str(numSets) + " " + str(numOverlapSizes) + overlapSizesStr
	print command
	os.system(command)
	os.chdir("../../../bloomTesting")

	print "Copying log files to correct folder"
	# Copy log files to graph_generator folder
	os.system("cp logs/intersection_dynPartition_fixedOverlap/* graph_generator/data/DPB_INTER")
	os.system("cp logs/union_dynPartition_fixedOverlap/* graph_generator/data/DPB_UNION")
	os.system("cp logs/intersection_dynBloom_fixedOverlap/* graph_generator/data/DB_INTER")
	os.system("cp logs/union_dynBloom_fixedOverlap/* graph_generator/data/DB_UNION")
	os.system("cp logs/intersection_Static_fixedOverlap/* graph_generator/data/SB_INTER")
	os.system("cp logs/union_Static_fixedOverlap/* graph_generator/data/SB_UNION")

	print "Generating plots"
	os.chdir("graph_generator")
	command = "./graph_creator.py 5 " + str(unitSize) + " " + str(numOverlapSizes) + " " + overlapSizesStr
	os.system(command);

	print "Running gnuplot"
	os.chdir("gnuPlot")
	command = "gnuplot createPlot.gnu"
	os.system(command);

def cleanUpDatasets():
	os.system("rm datasets/*")

def cleanUpLogs():
	os.system("rm logs/intersection_dynPartition_fixedOverlap/*")
	os.system("rm logs/intersection_dynBloom_fixedOverlap/*")
	os.system("rm logs/intersection_Static_fixedOverlap/*")
	os.system("rm logs/union_dynPartition_fixedOverlap/*")
	os.system("rm logs/union_dynBloom_fixedOverlap/*")
	os.system("rm logs/union_Static_fixedOverlap/*")

def cleanUpAll():
	cleanUpDatasets();
	cleanUpLogs();
	os.system("rm graph_generator/data/DB_INTER/*")
	os.system("rm graph_generator/data/DB_UNION/*")
	os.system("rm graph_generator/data/SB_INTER/*")
	os.system("rm graph_generator/data/SB_UNION/*")
	os.system("rm graph_generator/data/DPB_INTER/*")
	os.system("rm graph_generator/data/DPB_UNION/*")

if __name__ == '__main__':
	if len(sys.argv) >= 2 :
		if (int(sys.argv[1]) == 1):
			run()
		elif (int(sys.argv[1]) == 2): 
			cleanUpDatasets();
		elif (int(sys.argv[1]) == 3): 
			cleanUpLogs();
		elif (int(sys.argv[1]) == 4): 
			cleanUpAll();
	else:
		print "Usage: For running tests : ./runOverLapTest.py 1"
		print "Usage: For cleaning datasets: ./runOverLapTest.py 2"
		print "Usage: For cleaning logs: ./runOverLapTest.py 3"
		print "Usage: For cleaning datasets,logs,plots: ./runOverLapTest.py 4"
