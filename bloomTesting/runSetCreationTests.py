#!/usr/bin/python

import os,sys

def run_SameMemUsage():
	# Generate datasets
	namespace 		= 1000000
	fp_rate 		= 0.01
	K 				= 3
	numSamples 		= 10
	unitSizes 		= [1024]
	numUnitSizes 	= len(unitSizes) 
	setSizes 	    = [10,50,100,200,500,1000,2000,5000,10000,20000]
	setSizes 	    = [10,50,100,200,500,1000,2000,5000,10000,20000]
	numSetSizes     = len(setSizes)
	setSizeStr 		= " "
	unitSizesStr 	= " "
	for setSize in setSizes:
		setSizeStr += str(setSize) + " "

	for unitSize in unitSizes:
		unitSizesStr += str(unitSize) + " "
	
	print "Creating datasets..."
	command = "./setGenerator.py 1 datasets 1 " + str(namespace) + " " + str(numSamples) + " " + str(numSetSizes) + setSizeStr
	os.system(command) 	
	print "Created datasets"

	print "Running tests for Dynamic partition bloom"
	os.chdir("../DP_bloom/test_folder/bloomFilterTests")
	command = "make"
	os.system(command)
	command = "./test 1 fileList " + str(namespace) + " " + str(K) + " " + str(fp_rate) + " " + str(numUnitSizes) + unitSizesStr
	print "DPBF command::",command
	os.system(command)

	print "Running tests for dynamic_bloom"
	os.chdir("../../../dynamic_bloom/test_folder/bloomFilterTests")
	command = "make"
	os.system(command)
	command = "./test 3 fileList " + str(namespace) + " " + str(K) + " " + str(fp_rate) + " " + str(numUnitSizes) + unitSizesStr
	print "DBF command::",command
	os.system(command)

	print "Running tests for standard BF"
	os.chdir("../../../count_bloom/test_folder/bloomFilterTests")
	command = "make"
	os.system(command)
	command = "./test 3 "  + str(K) + " " + str(fp_rate) + " " + str(numUnitSizes) + unitSizesStr
	print "SB command::",command
	os.system(command)
	os.chdir("../../../bloomTesting")

	print "Copying log files to correct folder"
	# Copy log files to graph_generator folder
	os.system("cp logs/SetCreationUnitSizeAnalysis_DynPart/* graph_generator/data/DPB_SET")
	os.system("cp logs/SetCreationUnitSizeAnalysis_DynBloom/* graph_generator/data/DB_SET_SameMemUsage")
	os.system("cp logs/SetCreationUnitSizeAnalysis_Static/* graph_generator/data/SB_SET_SameMemUsage")

	print "Generating plots"
	os.chdir("graph_generator")
	command = "./graph_creator.py 2 " + str(unitSizes[0]) + " " + str(numSamples)
	print "Plot command::",command
	os.system(command);

def run_SameFP():
	# Generate datasets
	namespace 		= 1000000
	fp_rate 		= 0.01
	K 				= 3
	numSamples 		= 10
	unitSizes 		= [1024]
	numUnitSizes 	= len(unitSizes) 
	setSizes 	    = [10,50,100,200,500,1000,2000,5000,10000,20000]
	numSetSizes     = len(setSizes)
	setSizeStr 		= " "
	unitSizesStr 	= " "
	for setSize in setSizes:
		setSizeStr += str(setSize) + " "

	for unitSize in unitSizes:
		unitSizesStr += str(unitSize) + " "
	
	print "Creating datasets..."
	command = "./setGenerator.py 1 datasets 1 " + str(namespace) + " " + str(numSamples) + " " + str(numSetSizes) + setSizeStr
	os.system(command) 	
	print "Created datasets"

	print "Running tests for Dynamic partition bloom"
	os.chdir("../DP_bloom/test_folder/bloomFilterTests")
	command = "make"
	os.system(command)
	command = "./test 1 fileList " + str(namespace) + " " + str(K) + " " + str(fp_rate) + " " + str(numUnitSizes) + unitSizesStr
	print "DPBF command::",command
	os.system(command)

	print "Running tests for dynamic_bloom"
	os.chdir("../../../dynamic_bloom/test_folder/bloomFilterTests")
	command = "make"
	os.system(command)
	command = "./test 4 fileList " + str(namespace) + " " + str(K) + " " + str(fp_rate) + " " + str(numUnitSizes) + unitSizesStr
	print "DBF command::",command
	os.system(command)

	print "Running tests for standard BF"
	os.chdir("../../../count_bloom/test_folder/bloomFilterTests")
	command = "make"
	os.system(command)
	command = "./test 4 "  + str(K) + " " + str(fp_rate) + " " + str(numUnitSizes) + unitSizesStr
	print "SB command::",command
	os.system(command)
	os.chdir("../../../bloomTesting")

	print "Copying log files to correct folder"
	# Copy log files to graph_generator folder
	os.system("cp logs/SetCreationUnitSizeAnalysis_DynPart/* graph_generator/data/DPB_SET")
	os.system("cp logs/SetCreationUnitSizeAnalysis_DynBloom/* graph_generator/data/DB_SET_SameFP")
	os.system("cp logs/SetCreationUnitSizeAnalysis_Static/* graph_generator/data/SB_SET_SameFP")

	print "Generating plots"
	os.chdir("graph_generator")
	command = "./graph_creator.py 3 " + str(unitSizes[0]) + " " + str(numSamples)
	print "Plot command::",command
	os.system(command);

def cleanUpDatasets():
	os.system("rm datasets/*")

def cleanUpLogs():
	os.system("rm logs/SetCreationUnitSizeAnalysis_DynPart/*")
	os.system("rm logs/SetCreationUnitSizeAnalysis_DynBloom/*")
	os.system("rm logs/SetCreationUnitSizeAnalysis_Static/*")

def cleanUpAll():
	cleanUpDatasets();
	cleanUpLogs();
	os.system("rm graph_generator/data/DB_SET_SameFP/*")
	os.system("rm graph_generator/data/DB_SET_SameMemUsage/*")
	os.system("rm graph_generator/data/SB_SET_SameFP/*")
	os.system("rm graph_generator/data/SB_SET_SameMemUsage/*")
	os.system("rm graph_generator/data/DPB_SET/*")

if __name__ == '__main__':

	if len(sys.argv) >= 2 :
		if (int(sys.argv[1]) == 1): # Plot for case when we compare FP (keeping memUsage same)
			print "Running tests for the case when we have same memUsage"
			run_SameMemUsage()
		elif (int(sys.argv[1]) == 2): # Plot for case when we compare Memusage (keeping overall FP same)
			print "Running tests for the case when we have same effective overall FP"
			run_SameFP()
		elif (int(sys.argv[1]) == 3): 
			cleanUpDatasets();
		elif (int(sys.argv[1]) == 4): 
			cleanUpLogs();
		elif (int(sys.argv[1]) == 5): 
			cleanUpAll();
		else:
			print "Usage: For running tests where we compare FP rate keeping memUsage same : ./runOverLapTest.py 1"
			print "Usage: For running tests where we compare memUsage keeping FP rate same : ./runOverLapTest.py 2"
			print "Usage: For cleaning datasets: ./runOverLapTest.py 3"
			print "Usage: For cleaning logs: ./runOverLapTest.py 4"
			print "Usage: For cleaning datasets,logs,plots: ./runOverLapTest.py 5"
	else:
		print "Usage: For running tests where we compare FP rate keeping memUsage same : ./runOverLapTest.py 1"
		print "Usage: For running tests where we compare memUsage keeping FP rate same : ./runOverLapTest.py 2"
		print "Usage: For cleaning datasets: ./runOverLapTest.py 3"
		print "Usage: For cleaning logs: ./runOverLapTest.py 4"
		print "Usage: For cleaning datasets,logs,plots: ./runOverLapTest.py 5"
