import math
import sys
import random
import sets

def getPowerLaw(xmin, xmax, gamma,numValues):
	if (xmin < 0) or (xmax < 0) or (gamma > 0):
		print('Usage: xmin >= 0, xmax >=0, gamma < 0!');
		return [];
	numEntries = (xmax - xmin + 1);
	if (numEntries <= 0):
		print('Usage: xmax must be greater than xmin!');
		return [];
	probValues = [0 for i in range(numEntries)];
	sum = 0;
	for i in range(numEntries):
		probValues[i] = pow(xmax - i, gamma);
		sum += probValues[i];
	
	for i in range(numEntries):
		probValues[i] /= sum;
	vals = [];
	random.seed();
	for i in range(numValues):
		r = random.random();
		t = 0;
		flag = 0;
		for j in range(numEntries):
			t += probValues[j];
			if (r <= t):
				vals.append(xmax - j);
				flag = 1;
				break;
		if (flag == 0): vals.append(xmin);
	return vals;


def save_file(file_name, elems, namespace_size):
	file = open(file_name,'w');
	file.write(str(len(elems))+"\n");
	file.write(str(namespace_size)+"\n");
	for i in range(len(elems)):
		if( i < len(elems)-1 ):
			file.write(str(elems[i])+"\n");
		else:
			file.write(str(elems[i]));

	file.close();

def generate_sets(xmin, xmax, gamma, numValues, namespace_size, folder_name):
	set_sizes = getPowerLaw(xmin, xmax, gamma,numValues);
	fileList = open(folder_name+"/filelist",'w');
	# fileList.write(str(numValues)+"\n");
	for ind in range(len(set_sizes)):
		items = random.sample(range(1, namespace_size), set_sizes[ind])
		save_file(folder_name+"/"+str(ind), items, namespace_size);
		if(ind<len(set_sizes)-1):
			fileList.write(str(ind)+"\n");
		else:
			fileList.write(str(ind));
	fileList.close();



if __name__ == "__main__":
	print("Format for commandline arguments ::")
	print("<Min set size> <Max set size> <Number of Sets> <Namespace Size> <Folder Name>")

	if len(sys.argv) < 5:
			print("Not enough arguments, please see the format")
	else:
		xmin = sys.argv[1];
		xmax = sys.argv[2];
		numValues = sys.argv[3];
		namespace_size = sys.argv[4];
		folder_name = sys.argv[5];

		generate_sets(int(xmin), int(xmax), -2.5, int(numValues), int(namespace_size), folder_name)
