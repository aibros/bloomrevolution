#!/usr/bin/python
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import glob
from pprint import pprint
import math
import copy
import sys,os

metrics = ["EffMemUsage", "FP_Ratio","MembershipQueryTime","TimeTaken"];
metricsOrder = {"EffMemUsage":0, "FP_Ratio":1, "MembershipQueryTime":2, "TimeTaken":3};
# metrics = ["EffMemUsage", "FP_Ratio"];
# metricsOrder = {"EffMemUsage":0, "FP_Ratio":1};

# factors = ["UnitSize","NumElems","NumElemsA","NumElemsB","Overlap","Operation"]
factors = ["UnitSize","NumElems","NumElemsA","NumElemsB","Overlap","Operation","SymmetricDiffSize"]

database = { "CountBloom":{ "solo":{}, "pair":{} },"DynamicBloom":{ "solo":{}, "pair":{} },"DPBloom":{ "solo":{}, "pair":{} } }

soloFactors = ["UnitSize","NumElems"]
soloOrder = {"UnitSize":0,"NumElems":1,"BatchSize":2}
soloRange = {"UnitSize":[512,1024],"NumElems":[10,50,100,200,500,1000,2000,5000,10000,20000],"BatchSize":[5]}

# pairFactors = ["UnitSize","NumElemsA","NumElemsB","Overlap","Operation"]
# pairOrder = {"UnitSize":0,"NumElemsA":1,"NumElemsB":2,"Overlap":3,"Operation":4}
pairMetrics = ["EffMemUsage", "FP_Ratio"];
pairMetricsOrder = {"EffMemUsage":0, "FP_Ratio":1};
pairFactors = ["UnitSize","Overlap","Operation","SymmetricDiffSize"]
pairOrder = {"UnitSize":0,"Overlap":1,"Operation":2,"SymmetricDiffSize":3}
pairRange = {"UnitSize":[1024],"Overlap":[200],"Operation":["Union","Intersection"],"SymmetricDiffSize":range(400,3601,200)}
# pairRange = {"UnitSize":[512,1024,2048,4096,8192],"NumElemsA":[10,100,200,500,1000,2000],"NumElemsB":[10,100,200,500,1000,2000],"Overlap":[0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9],"BatchSize":[5],"Operation":["Union","Intersection"]}

#types of graphs we are talking about

# sensitivity analysis:
# 1- for each unit size : metrics vs set sizes
# 2- for each batch size : metrics vs set sizes
# 3- for each set size A,%Overlap : metrics vs set size B  
# 4- for each set size A, set size B : metrics vs %Overlap
# 5- for each set size A, set size B, unit size : metrics vs %Overlap

# Comparison analysis:
# - 1 vs 1 
# - 3 vs 3
# - 4 vs 4
# - 5 vs 5


# one possible database combine all information as 

# function :for reading a folder
# function :for any graph data -- fixed factors , varying factors - metric
# function :given two graph data with same fixed factors and varying factors merge


# BloomType : CountBloom(alias for standard bloom filter), DynamicBloom, DPBloom
# expType	: Type of experiment: 	a) solo : For getting graphs for set creation experiments
#									b) pair : For set union and intersection experiments
# foldername: Name of folder to be read 
# sampleSize: Size of sample over which each datapoint(plotted on graph) has to be averaged. 
def readFolder(BloomType, expType, folderName, sampleSize ):
	global database,soloFactors,soloOrder,pairFactors,pairOrder,metrics,factors,metricsOrder

	file_list = glob.glob(folderName+"/*.csv");
	for file_name in file_list:
		tempDB = {}
		file_data = pd.read_csv(file_name)
		# print file_data
		# exit(0)
		file_len = len(file_data["UnitSize"]);		#assuming unit size is present in all
		attrs = [];

		if(expType=="solo"):
			for ind in range(len(soloFactors)):
				if(soloFactors[ind] in file_data.keys()):
					attrs += [soloFactors[ind]];
				else:
					print "Factor "+soloFactors[ind]+" not found. Stopping in folder : "+folderName+" file :"+file_name;
					exit(0)
		elif(expType=="pair"):
			for ind in range(len(pairFactors)):
				if(pairFactors[ind] in file_data.keys()):
					attrs += [pairFactors[ind]];
				else:
					print "Factor "+pairFactors[ind]+" not found. Stopping in folder : "+folderName+" file :"+file_name;
					exit(0)
		else:
			print "expType not correct :"+expType;
			exit(0)

		for block in range(0,file_len,sampleSize):
			lstKey = [];
			for fac_ind in range(len(attrs)):
				lstKey += [file_data[attrs[fac_ind]][block]];
			key = tuple(lstKey);

			val_list = []
			for r in range(len(metrics)):
				val_list += [[]];

			for elem in range(block,block+sampleSize):
				for met_ind in range(len(metrics)):
					met = metrics[met_ind];
					val_list[met_ind] += [file_data[met][elem]];

			try:
				for met_ind in range(len(pairMetrics)):
					tempDB[key][met_ind] += val_list[met_ind]
			except:
				tempDB[key] = [0]*len(pairMetrics)
				for met_ind in range(len(pairMetrics)):
					tempDB[key][met_ind] = val_list[met_ind]

			val = [(0.0,0.0)]*len(metrics)
			for met_ind in range(len(metrics)):
				x = np.mean(val_list[met_ind]); 
				y = float(np.std(val_list[met_ind]))/float(math.sqrt(len(val_list[met_ind]))); 
				val[met_ind] = (x,y)

			database[BloomType][expType][key] = val;

# This one is specifically for plotting results of set operation tests where we have fixed overlap
# BloomType : CountBloom(alias for standard bloom filter), DynamicBloom, DPBloom
# foldername: Name of folder to be read 
# sampleSize: Size of sample over which each datapoint(plotted on graph) has to be averaged. 
def readFolder_setOps(BloomType, folderName, sampleSize ):
	global database,pairFactors,pairOrder,pairMetrics,factors,pairMetricsOrder

	expType = "pair"
	file_list = glob.glob(folderName+"/*.csv");
	for file_name in file_list:
		tempDB = {}
		# Read data from csv file
		file_data = pd.read_csv(file_name)
		file_len = len(file_data["UnitSize"]);		#assuming unit size is present in all
		attrs = [];

		# Get a list of attributes from pairFactors
		for ind in range(len(pairFactors)):
			if(pairFactors[ind] in file_data.keys()):
				attrs += [pairFactors[ind]];
			else:
				print "Factor "+pairFactors[ind]+" not found. Stopping in folder : "+folderName+" file :"+file_name;
				exit(0)

		# Populate tempDB with value of metrics for each possible combination of values of attributes "attrs"
		for block in range(0,file_len,sampleSize):
			lstKey = [];

			# Make a "key" using values of attributes, in the same order as in "attrs"
			for fac_ind in range(len(attrs)):
				lstKey += [file_data[attrs[fac_ind]][block]];
			key = tuple(lstKey);

			val_list = []
			for r in range(len(pairMetrics)):
				val_list += [[]];

			# For value of attributes as present in "key", accumulate values of all metrics in a list in "val_list"
			for elem in range(block,block+sampleSize):
				for met_ind in range(len(pairMetrics)):
					met = pairMetrics[met_ind];
					val_list[met_ind] += [file_data[met][elem]];

			try:
				for met_ind in range(len(pairMetrics)):
					tempDB[key][met_ind] += val_list[met_ind]
			except:
				tempDB[key] = [0]*len(pairMetrics)
				for met_ind in range(len(pairMetrics)):
					tempDB[key][met_ind] = val_list[met_ind]
		
		# Now, for each combination of values of attributes, find mean and variance of all metrics
		for key in tempDB.keys():
			val_list = tempDB[key]
			val = [(0.0,0.0)]*len(pairMetrics)  # A tuple of (0,0) for (mean and variance)
			for met_ind in range(len(pairMetrics)):
				x = np.mean(val_list[met_ind]); 
				y = float(np.std(val_list[met_ind]))/float(math.sqrt(len(val_list[met_ind]))); 
				val[met_ind] = (x,y)

			database[BloomType][expType][key] = val;


# return an tuple of lists for a graph instance
# BloomType		: Type of bloom filter: CountBloom, DynamicBloom or DPBloom
# expType   	: "solo" (for set creation and membership query experiments) and "pair" for set union and intersection experiments
# fixed_fact	: Parameters that are not varying
# fixed_vals	: Value of parameters that are not varying
# vary_factor	: Parameters that are varying (X-Axis)
# metric 		: Value of be plotted on Y-Axis
def get_graph_instance(BloomType, expType, fixed_fact, fixed_vals, vary_factor, metric):
	global database,soloFactors,soloOrder,soloRange,pairFactors,pairOrder,pairRange,metrics,factors,metricsOrder

	dataset = database[BloomType][expType];
	factors = [];
	factorOrder = {};
	factorRange = [];
	if(expType == "solo"):
		factors = soloFactors;
		factorOrder = soloOrder;
		factorRange = soloRange;
	elif (expType == "pair"):
		factors = pairFactors;
		factorOrder = pairOrder;
		factorRange = pairRange;
		# Adjust range of SymmetricDiffSize according to value of overlap specified
		for i in range(len(fixed_fact)):
			if (fixed_fact[i] == "Overlap"):
				overlap = fixed_vals[i]
				break;
		factorRange["SymmetricDiffSize"] = range(2*overlap,18*overlap+1 ,overlap);
	else:
		print "Wrong experiment type",expType

	start_lstKey  	= [0]*len(factors);
	vary_ind 		= factorOrder[vary_factor];
	vary_range 		= factorRange[vary_factor]

	for ind in range(len(fixed_fact)):
		fact = fixed_fact[ind];
		start_lstKey[factorOrder[fact]] = fixed_vals[ind];

	# print fixed_vals
	# print fixed_fact
	# print "first fixed",start_lstKey
	X = [];
	Y = [];
	Z = [];
	for val_ind in range(len(vary_range)):
		lstKey 	= copy.deepcopy(start_lstKey)
		val 	= vary_range[val_ind]
		lstKey[vary_ind] 	= val;
		tupKey 	= tuple(lstKey)
		# print val,vary_ind
		# print tupKey
		X += [val];
		if(tupKey in dataset.keys()):						#as only unique pairs present in data
			Y += [dataset[tupKey][metricsOrder[metric]][0]]
			Z += [dataset[tupKey][metricsOrder[metric]][1]]
			# print "varied",lstKey
		elif(expType=="pair"):
			temp = lstKey[factorOrder["NumElemsB"]]
			lstKey[factorOrder["NumElemsB"]] = lstKey[factorOrder["NumElemsA"]]
			lstKey[factorOrder["NumElemsA"]] = temp
			# print "varied reverse",lstKey
			tupKey = tuple(lstKey)
			# print tupKey
			Y += [dataset[tupKey][metricsOrder[metric]][0]]
			Z += [dataset[tupKey][metricsOrder[metric]][1]]
	return (X,Y,Z)


# draws multiple graphs on the same figure, provided the suitable data
# arguments in order -> title of the figure, X axis label , Y axis label
# data for different graphs as list of tuples ex [(X,Y),(X1,Y1)], labels
# for each graph instance, directory for saving figure, name of figure
def draw_graph( title, X_label, Y_label, graph_instances, instance_labels,directory,name):
	fig = plt.figure(figsize=(16, 9), dpi=80);
	ax = fig.add_subplot(111)

	for ind in range(len(graph_instances)):
		(X,Y,Z) = graph_instances[ind];		
		plt.errorbar(X,Y,yerr=Z,marker='o',ls='-');
		# for xy in zip(X, Y):                                       
		# 	ax.annotate('(%s, %s)' % xy, xy=xy, textcoords='data')

	plt.xscale('log')
	plt.legend(instance_labels, loc='upper left')
	plt.suptitle(title);
	plt.xlabel(X_label);
	plt.ylabel(Y_label);
	# plt.show();
	fig.savefig(directory+"/"+name+".png")

def createGNUPlotDataFile(graph_instances,filename):
	
	strList = {}
	for ctr in range(len(graph_instances[0][0])):
		strList[ctr] = str(graph_instances[0][0][ctr]) + "\t" 

	for ind in range(len(graph_instances)):
		(X,Y,Z) = graph_instances[ind];
		for ctr in range(len(graph_instances[ind][0])):
			strList[ctr] += str(Y[ctr]) + "\t" + str(Z[ctr]) + "\t" 

	dataWriter = open(filename + ".txt","w")
	for key in sorted(strList.keys()):
		# print strList[key]
		dataWriter.write(strList[key]+"\n")
	
	dataWriter.close()

	# gnuWriter = open("temp.gn","w")
	# gnuWriter.write("set terminal pdf enhanced font 'Helvetica,20'\n")
	# gnuWriter.write("set output '" + directory+"/"+name+".pdf'\n")

	# gnuWriter.write("set key outside vertical right\n")
	# gnuWriter.write("set ylabel '" + Y_label + "'\n")
	# gnuWriter.write("set xlabel '" + X_label + "'\n")
	# gnuWriter.write("set logscale y 2\n")
	# gnuWriter.write("set grid y\n")
	# gnuWriter.write("set xtics 5000\n")
	# gnuWriter.write("set title '" + title + "'\n")

	# gnuWriter.write("plot 'plotData' using 1:2:3 title 'SBF' with yerrorlines lw 3, '' using 1:4:5 title 'DBF' with yerrorlines lw 3, '' using 1:6:7 title 'DPBF' with yerrorlines lw 3\n")
	# # gnuWriter.write("plot 'plotData' using 1:2 title 'SBF' with lw 3, '' using 1:4:5 title 'DBF' with yerrorlines lw 3, '' using 1:6:7 title 'DPBF' with yerrorlines lw 3\n")
	# gnuWriter.close();
	# os.system("gnuplot > load temp.gn")

def draw_graph_withSec_Axis( title, X_label, Y_label,Y_label_sec, graph_instances_prim,graph_instances_sec, instance_labels,instance_labels_sec, directory,name):
	
	fig = plt.figure(figsize=(16, 9), dpi=80);
	ax1 = fig.add_subplot(111)

	colorList = ['r','g','b','y']
	ctr = 0;
	for (X,Y,Y_err) in graph_instances_prim:
		ax1.errorbar(X,Y,yerr=Y_err,marker='*',ls='-',color=colorList[ctr]);
		ctr += 1

	ax2 =  ax1.twinx()
	ctr = 0;
	for (X,Y,Y_err) in graph_instances_sec:
		ax2.errorbar(X,Y,yerr=Y_err,marker='D',ls='--',color=colorList[ctr]);
		ctr += 1

	ax1.set_xlabel(X_label)
	ax1.set_ylabel(Y_label)
	ax2.set_ylabel(Y_label_sec)

	ax1.legend(instance_labels, loc='upper left')
	ax2.legend(instance_labels_sec, loc='upper right')

	# plt.xscale('log')
	# plt.xlim(500,17000)
	plt.suptitle(title);
	# plt.show();
	fig.savefig(directory+"/"+name+".png")

def oldMainFunction():
	# simple test cases
	# draw_graph("ok","x","y",[([1,2,3,4],[5,8,9,10],[1,2,1,2]),([1,2,3,4],[3,6,13,26],[0.1,0.2,0.1,0.2])],["a","b"],"","");
	# readFolder("CountBloom", "solo", "sample",20 )

	# pprint(database)

	# print get_graph_instance("CountBloom","solo",["UnitSize"],[2048],"NumElems","EffMemUsage")
	# print get_graph_instance("CountBloom","solo",["UnitSize"],[2048],"NumElems","FP_Ratio")


	print("reading logs..")

	readFolder("CountBloom","pair","data/CB_INT",2)
	readFolder("CountBloom","solo","data/CB_SET",20)
	readFolder("CountBloom","pair","data/CB_UN",2)

	readFolder("DynamicBloom","pair","data/DB_INT",2)
	readFolder("DynamicBloom","solo","data/DB_SET",20)
	readFolder("DynamicBloom","pair","data/DB_UN",2)

	readFolder("DPBloom","pair","data/DBP_INT",2)
	readFolder("DPBloom","solo","data/DBP_SET",20)
	readFolder("DPBloom","pair","data/DBP_UN",2)

	print("logs read!!")

	# pprint(database)
	# correct the problem in count bloom which is still giving negatives

	# pprint(database["DPBloom"]["pair"])

	print("drawing graphs")

	print("all solo graphs creation started...")
	CB1 = get_graph_instance("CountBloom","solo",["UnitSize"],[1024],"NumElems","EffMemUsage")
	CB2 = get_graph_instance("CountBloom","solo",["UnitSize"],[1024],"NumElems","FP_Ratio")

	DB1 = get_graph_instance("DynamicBloom","solo",["UnitSize"],[1024],"NumElems","EffMemUsage")
	DB2 = get_graph_instance("DynamicBloom","solo",["UnitSize"],[1024],"NumElems","FP_Ratio")
	DB3 = get_graph_instance("DynamicBloom","solo",["UnitSize"],[2048],"NumElems","EffMemUsage")
	DB4 = get_graph_instance("DynamicBloom","solo",["UnitSize"],[2048],"NumElems","FP_Ratio")

	DPB1 = get_graph_instance("DPBloom","solo",["UnitSize"],[1024],"NumElems","EffMemUsage")
	DPB2 = get_graph_instance("DPBloom","solo",["UnitSize"],[1024],"NumElems","FP_Ratio")
	DPB3 = get_graph_instance("DPBloom","solo",["UnitSize"],[2048],"NumElems","EffMemUsage")
	DPB4 = get_graph_instance("DPBloom","solo",["UnitSize"],[2048],"NumElems","FP_Ratio")


	draw_graph( "DPB vs CB", "set size", "False Positive Probability", [DPB2,CB2], ["DPB","CB"],"graphs","DPB_CB_FP");

	draw_graph( "DPB vs DB", "set size", "Effective memory usage", [DPB1,DB1], ["DPB","DB"],"graphs","DPB_DB_EM");
	draw_graph( "DPB vs DB", "set size", "False Positive Probability", [DPB2,DB2], ["DPB","DB"],"graphs","DPB_DB_FP");

	draw_graph( "DPB vs CB vs DB", "set size", "False Positive Probability", [DPB2,CB2,DB2], ["DPB","CB","DB"],"graphs","DPB_CB_DB_FP");


	print("all solo graphs created!!")
	print("Creating all pair graphs")
	#3
	CB1_2 = get_graph_instance("CountBloom","pair",["UnitSize","NumElemsA","Overlap","Operation"],[1024,1000,0.4,"Intersection"],"NumElemsB","EffMemUsage")
	CB2_2 = get_graph_instance("CountBloom","pair",["UnitSize","NumElemsA","Overlap","Operation"],[1024,1000,0.4,"Intersection"],"NumElemsB","FP_Ratio")
	CB3_2 = get_graph_instance("CountBloom","pair",["UnitSize","NumElemsA","Overlap","Operation"],[1024,1000,0.4,"Union"],"NumElemsB","EffMemUsage")
	CB4_2 = get_graph_instance("CountBloom","pair",["UnitSize","NumElemsA","Overlap","Operation"],[1024,1000,0.4,"Union"],"NumElemsB","FP_Ratio")
	 
	DB1_2 = get_graph_instance("DynamicBloom","pair",["UnitSize","NumElemsA","Overlap","Operation"],[1024,1000,0.4,"Intersection"],"NumElemsB","EffMemUsage")
	DB2_2 = get_graph_instance("DynamicBloom","pair",["UnitSize","NumElemsA","Overlap","Operation"],[1024,1000,0.4,"Intersection"],"NumElemsB","FP_Ratio")
	DB3_2 = get_graph_instance("DynamicBloom","pair",["UnitSize","NumElemsA","Overlap","Operation"],[1024,1000,0.4,"Union"],"NumElemsB","EffMemUsage")
	DB4_2 = get_graph_instance("DynamicBloom","pair",["UnitSize","NumElemsA","Overlap","Operation"],[1024,1000,0.4,"Union"],"NumElemsB","FP_Ratio")
	 
	DPB1_2 = get_graph_instance("DPBloom","pair",["UnitSize","NumElemsA","Overlap","Operation"],[1024,1000,0.4,"Intersection"],"NumElemsB","EffMemUsage")
	DPB2_2 = get_graph_instance("DPBloom","pair",["UnitSize","NumElemsA","Overlap","Operation"],[1024,1000,0.4,"Intersection"],"NumElemsB","FP_Ratio")
	DPB3_2 = get_graph_instance("DPBloom","pair",["UnitSize","NumElemsA","Overlap","Operation"],[1024,1000,0.4,"Union"],"NumElemsB","EffMemUsage")
	DPB4_2 = get_graph_instance("DPBloom","pair",["UnitSize","NumElemsA","Overlap","Operation"],[1024,1000,0.4,"Union"],"NumElemsB","FP_Ratio")
	 




	DPB1_x = get_graph_instance("DPBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,1000,1000,"Intersection"],"Overlap","EffMemUsage")
	DPB2_x = get_graph_instance("DPBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,1000,1000,"Intersection"],"Overlap","FP_Ratio")
	DPB3_x = get_graph_instance("DPBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,1000,1000,"Union"],"Overlap","EffMemUsage")
	DPB4_x = get_graph_instance("DPBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,1000,1000,"Union"],"Overlap","FP_Ratio")

	DB1_x = get_graph_instance("DynamicBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,1000,1000,"Intersection"],"Overlap","EffMemUsage")
	DB2_x = get_graph_instance("DynamicBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,1000,1000,"Intersection"],"Overlap","FP_Ratio")
	DB3_x = get_graph_instance("DynamicBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,1000,1000,"Union"],"Overlap","EffMemUsage")
	DB4_x = get_graph_instance("DynamicBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,1000,1000,"Union"],"Overlap","FP_Ratio")

	CB1_x = get_graph_instance("CountBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,1000,1000,"Intersection"],"Overlap","EffMemUsage")
	CB2_x = get_graph_instance("CountBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,1000,1000,"Intersection"],"Overlap","FP_Ratio")
	CB3_x = get_graph_instance("CountBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,1000,1000,"Union"],"Overlap","EffMemUsage")
	CB4_x = get_graph_instance("CountBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,1000,1000,"Union"],"Overlap","FP_Ratio")



	# draw_graph( "DPB vs CB Intersection with size A as 1000 and overlap 40%", "set size of B", "False Positive Probability", [DPB2_2,CB2_2], ["DPB","CB"],"graphs","DPB_CB_FP_INT");
	# draw_graph( "DPB vs CB Union with size A as 1000 and overlap 40%", "set size of B", "False Positive Probability", [DPB4_2,CB4_2], ["DPB","CB"],"graphs","DPB_CB_FP_UN");

	# draw_graph( "DPB vs DB Intersection with size A as 1000 and overlap 40%", "set size", "Effective memory usage", [DPB1_2,DB1_2], ["DPB","DB"],"graphs","DPB_DB_EM_INT");
	# draw_graph( "DPB vs DB Intersection with size A as 1000 and overlap 40%", "set size", "False Positive Probability", [DPB2_2,DB2_2], ["DPB","DB"],"graphs","DPB_DB_FP_INT");
	# draw_graph( "DPB vs DB Union with size A as 1000 and overlap 40%", "set size", "Effective Memory usage", [DPB3_2,DB3_2], ["DPB","DB"],"graphs","DPB_DB_EM_UN");
	# draw_graph( "DPB vs DB Union with size A as 1000 and overlap 40%", "set size", "False Positive Probability", [DPB4_2,DB4_2], ["DPB","DB"],"graphs","DPB_DB_FP_UN");

	# draw_graph( "DPB vs DB Intersection analysis on overlap with set sizes 1000", "Overlap fraction", "Effective memory usage", [DPB1_x,DB1_x], ["DPB","DB"],"graphs","DPB_DB_O_INT_EM");
	# draw_graph( "DPB vs DB Intersection analysis on overlap with set sizes 1000", "Overlap fraction", "False Positive Probability", [DPB2_x,DB2_x], ["DPB","DB"],"graphs","DPB_DB_O_INT_FP");
	# draw_graph( "DPB vs DB Union analysis on overlap with set sizes 1000", "Overlap fraction", "Effective memory usage", [DPB3_x,DB3_x], ["DPB","DB"],"graphs","DPB_DB_O_UN_EM");
	# draw_graph( "DPB vs DB Union analysis on overlap with set sizes 1000", "Overlap fraction", "False Positive Probability", [DPB4_x,DB4_x], ["DPB","DB"],"graphs","DPB_DB_O_UN_FP");

	# draw_graph( "DPB vs CB Intersection analysis on overlap", "Overlap fraction", "False Positive Probability with set sizes 1000", [DPB2_x,CB2_x], ["DPB","CB"],"graphs","DPB_CB_O_INT_FP");
	# draw_graph( "DPB vs CB Union analysis on overlap", "Overlap fraction", "False Positive Probability with set sizes 1000", [DPB4_x,CB4_x], ["DPB","CB"],"graphs","DPB_CB_O_UN_FP");


	print("all graphs created")

	set_sizes = [10,100,200,500,1000,2000];
	for i in range(len(set_sizes)):
		for j in range(i,len(set_sizes)):
			A = set_sizes[i]
			B = set_sizes[j]
			DPB1_x = get_graph_instance("DPBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,A,B,"Intersection"],"Overlap","EffMemUsage")
			DPB2_x = get_graph_instance("DPBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,A,B,"Intersection"],"Overlap","FP_Ratio")
			DPB3_x = get_graph_instance("DPBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,A,B,"Union"],"Overlap","EffMemUsage")
			DPB4_x = get_graph_instance("DPBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,A,B,"Union"],"Overlap","FP_Ratio")

			DB1_x = get_graph_instance("DynamicBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,A,B,"Intersection"],"Overlap","EffMemUsage")
			DB2_x = get_graph_instance("DynamicBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,A,B,"Intersection"],"Overlap","FP_Ratio")
			DB3_x = get_graph_instance("DynamicBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,A,B,"Union"],"Overlap","EffMemUsage")
			DB4_x = get_graph_instance("DynamicBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,A,B,"Union"],"Overlap","FP_Ratio")

			CB1_x = get_graph_instance("CountBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,A,B,"Intersection"],"Overlap","EffMemUsage")
			CB2_x = get_graph_instance("CountBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,A,B,"Intersection"],"Overlap","FP_Ratio")
			CB3_x = get_graph_instance("CountBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,A,B,"Union"],"Overlap","EffMemUsage")
			CB4_x = get_graph_instance("CountBloom","pair",["UnitSize","NumElemsA","NumElemsB","Operation"],[1024,A,B,"Union"],"Overlap","FP_Ratio")

			draw_graph( "DPB vs DB Intersection analysis on overlap with set sizes A :"+str(A)+" B :"+str(B), "Overlap fraction", "Effective memory usage", [DPB1_x,DB1_x], ["DPB","DB"],"graphs2","DPB_DB_O_INT_EM"+str(A)+'_'+str(B));
			draw_graph( "DPB vs DB Intersection analysis on overlap with set sizes A :"+str(A)+" B :"+str(B), "Overlap fraction", "False Positive Probability", [DPB2_x,DB2_x], ["DPB","DB"],"graphs2","DPB_DB_O_INT_FP"+str(A)+'_'+str(B));
			draw_graph( "DPB vs DB Union analysis on overlap with set sizes A :"+str(A)+" B :"+str(B), "Overlap fraction", "Effective memory usage", [DPB3_x,DB3_x], ["DPB","DB"],"graphs2","DPB_DB_O_UN_EM"+str(A)+'_'+str(B));
			draw_graph( "DPB vs DB Union analysis on overlap with set sizes A :"+str(A)+" B :"+str(B), "Overlap fraction", "False Positive Probability", [DPB4_x,DB4_x], ["DPB","DB"],"graphs2","DPB_DB_O_UN_FP"+str(A)+'_'+str(B));

			draw_graph( "DPB vs CB Intersection analysis on overlap A :"+str(A)+" B :"+str(B), "Overlap fraction", "False Positive Probability with set sizes 1000", [DPB2_x,CB2_x], ["DPB","CB"],"graphs2","DPB_CB_O_INT_FP"+str(A)+'_'+str(B));
			draw_graph( "DPB vs CB Union analysis on overlap A :"+str(A)+" B :"+str(B), "Overlap fraction", "False Positive Probability with set sizes 1000", [DPB4_x,CB4_x], ["DPB","CB"],"graphs2","DPB_CB_O_UN_FP"+str(A)+'_'+str(B));

# Draws graph for varying unit size in Dynamic Partition Bloom Filter
# unitSizeVals: List of unit sizes for which graph has to be created
def drawUnitSizeGraphs(unitSizeVals,numSamples):

	print("Reading logs...")
	readFolder("DPBloom","solo","data/DPB_SET",numSamples)
	print("Logs read...")

	print("Drawing graphs")
	memUsageGraphList = []
	for unitSize in unitSizeVals:
		graph = get_graph_instance("DPBloom","solo",["UnitSize"],[unitSize],"NumElems","EffMemUsage")
		memUsageGraphList.append(graph)
		print graph
		
	fpRatioGraphList = []
	for unitSize in unitSizeVals:
		graph = get_graph_instance("DPBloom","solo",["UnitSize"],[unitSize],"NumElems","FP_Ratio")
		fpRatioGraphList.append(graph)
		print graph
		
	draw_graph( "FPP Analysis for Different Unit Bloom Filter Sizes", "Set Size", "Empirical False Positive Probability", fpRatioGraphList, unitSizeVals ,"graphs","DPB_UnitSize_FP");
	draw_graph( "Memory Usage Analysis for Different Unit Bloom Filter Sizes", "Set Size", "Effective Memory Usage", memUsageGraphList, unitSizeVals ,"graphs","DPB_UnitSize_MemUsage");

# This is to generate plots comparing memory taken by BFs when they have same false positive probability
def compareMemory(numSamples,unitSize,foldername):
	# numSamples 	= 20;
	# unitSize 	= 1024;

	readFolder("CountBloom","solo","data/SB_SET_SameFP",numSamples)
	readFolder("DynamicBloom","solo","data/DB_SET_SameFP",numSamples)
	readFolder("DPBloom","solo","data/DPB_SET",numSamples)

	DPB = get_graph_instance("DPBloom","solo",["UnitSize"],[unitSize],"NumElems","EffMemUsage")
	SB 	= get_graph_instance("CountBloom","solo",["UnitSize"],[unitSize],"NumElems","EffMemUsage")
	DB 	= get_graph_instance("DynamicBloom","solo",["UnitSize"],[unitSize],"NumElems","EffMemUsage")

	createGNUPlotDataFile([SB,DB,DPB],"gnuPlot/memUsage_sameFP");
	
	# draw_graph( "Memory usage for Storing Sets", "Set Size", "Memory Usage(in Bits)", [SB,DB,DPB], ["Static BF","Dynamic BF","Dynamic Partition BF"] ,foldername,"MemUsage_With_Same_FP_All");
	# draw_graph( "Memory usage for Storing Sets", "Set Size", "Memory Usage(in Bits)", [SB,DB], ["Static BF","Dynamic BF"] ,foldername,"MemUsage_With_Same_FP_SB_DB");
	# draw_graph( "Memory usage for Storing Sets", "Set Size", "Memory Usage(in Bits)", [SB,DPB], ["Static BF","Dynamic Partition BF"] ,foldername,"MemUsage_With_Same_FP_SB_vs_DPB");
	# draw_graph( "Memory usage for Storing Sets", "Set Size", "Memory Usage(in Bits)", [DB,DPB], ["Dynamic BF","Dynamic Partition BF"] ,foldername,"MemUsage_With_Same_FP_DB_DPB");

	DPB = get_graph_instance("DPBloom","solo",["UnitSize"],[unitSize],"NumElems","MembershipQueryTime")
	SB 	= get_graph_instance("CountBloom","solo",["UnitSize"],[unitSize],"NumElems","MembershipQueryTime")
	DB 	= get_graph_instance("DynamicBloom","solo",["UnitSize"],[unitSize],"NumElems","MembershipQueryTime")
	createGNUPlotDataFile([SB,DB,DPB],"gnuPlot/memQueryTime_sameFP");

	# draw_graph( "Membership Query Time for BFs", "Set Size", "MembershipQueryTime", [SB,DB,DPB], ["Static BF","Dynamic BF","Dynamic Partition BF"] ,foldername,"MemQueryTime_With_Same_FP_All");
	# draw_graph( "Membership Query Time for BFs", "Set Size", "MembershipQueryTime", [SB,DB], ["Static BF","Dynamic BF"] ,foldername,"MemQueryTime_With_Same_FP_SB_DB");
	# draw_graph( "Membership Query Time for BFs", "Set Size", "MembershipQueryTime", [SB,DPB], ["Static BF","Dynamic Partition BF"] ,foldername,"MemQueryTime_With_Same_FP_SB_vs_DPB");
	# draw_graph( "Membership Query Time for BFs", "Set Size", "MembershipQueryTime", [DB,DPB], ["Dynamic BF","Dynamic Partition BF"] ,foldername,"MemQueryTime_With_Same_FP_DB_DPB");

	# DPB = get_graph_instance("DPBloom","solo",["UnitSize"],[unitSize],"NumElems","TimeTaken")
	# SB 	= get_graph_instance("CountBloom","solo",["UnitSize"],[unitSize],"NumElems","TimeTaken")
	# DB 	= get_graph_instance("DynamicBloom","solo",["UnitSize"],[unitSize],"NumElems","TimeTaken")

	# draw_graph( "Set Creation Time for BFs", "Set Size", "BF Creation Time", [SB,DB,DPB], ["Static BF","Dynamic BF","Dynamic Partition BF"] ,foldername,"BF_CreationTime_With_Same_FP_All");
	# draw_graph( "Set Creation Time for BFs", "Set Size", "BF Creation Time", [SB,DB], ["Static BF","Dynamic BF"] ,foldername,"BF_CreationTime_With_Same_FP_SB_DB");
	# draw_graph( "Set Creation Time for BFs", "Set Size", "BF Creation Time", [SB,DPB], ["Static BF","Dynamic Partition BF"] ,foldername,"BF_CreationTime_With_Same_FP_SB_vs_DPB");
	# draw_graph( "Set Creation Time for BFs", "Set Size", "BF Creation Time", [DB,DPB], ["Dynamic BF","Dynamic Partition BF"] ,foldername,"BF_CreationTime_With_Same_FP_DB_DPB");

# This is to generate plots comparing false positive probability of BFs when they take same memory
def compareFP(numSamples,unitSize,foldername):
	# numSamples 	= 20;
	# unitSize 	= 1024;

	readFolder("CountBloom","solo","data/SB_SET_SameMemUsage",numSamples)
	readFolder("DynamicBloom","solo","data/DB_SET_SameMemUsage",numSamples)
	readFolder("DPBloom","solo","data/DPB_SET",numSamples)

	DPB = get_graph_instance("DPBloom","solo",["UnitSize"],[unitSize],"NumElems","FP_Ratio")
	SB 	= get_graph_instance("CountBloom","solo",["UnitSize"],[unitSize],"NumElems","FP_Ratio")
	DB 	= get_graph_instance("DynamicBloom","solo",["UnitSize"],[unitSize],"NumElems","FP_Ratio")
	
	createGNUPlotDataFile([SB,DB,DPB],"gnuPlot/FP_sameMemUsage");
	
	# draw_graph("Empirical False Positive Probability for BFs", "Set Size", "False Positive Rate", [SB,DB,DPB], ["Static BF","Dynamic BF","Dynamic Partition BF"] ,foldername,"FP_With_Same_MemUsage_All");
	# draw_graph("Empirical False Positive Probability for BFs", "Set Size", "False Positive Rate", [SB,DB], ["Static BF","Dynamic BF"] ,foldername,"FP_With_Same_MemUsage_SB_DB");
	# draw_graph("Empirical False Positive Probability for BFs", "Set Size", "False Positive Rate", [SB,DPB], ["Static BF","Dynamic Partition BF"] ,foldername,"FP_With_Same_MemUsage_SB_vs_DPB");
	# draw_graph("Empirical False Positive Probability for BFs", "Set Size", "False Positive Rate", [DB,DPB], ["Dynamic BF","Dynamic Partition BF"] ,foldername,"FP_With_Same_MemUsage_DB_DPB");

	DPB = get_graph_instance("DPBloom","solo",["UnitSize"],[unitSize],"NumElems","MembershipQueryTime")
	SB 	= get_graph_instance("CountBloom","solo",["UnitSize"],[unitSize],"NumElems","MembershipQueryTime")
	DB 	= get_graph_instance("DynamicBloom","solo",["UnitSize"],[unitSize],"NumElems","MembershipQueryTime")
	createGNUPlotDataFile([SB,DB,DPB],"gnuPlot/memQueryTime_sameMemUsage");

	# draw_graph("Membership Query Time for BFs", "Set Size", "Membership Query Time", [SB,DB,DPB], ["Static BF","Dynamic BF","Dynamic Partition BF"] ,foldername,"MembershipQueryTime_With_Same_MemUsage_All");
	# draw_graph("Membership Query Time for BFs", "Set Size", "Membership Query Time", [SB,DB], ["Static BF","Dynamic BF"] ,foldername,"MembershipQueryTime_With_Same_MemUsage_SB_DB");
	# draw_graph("Membership Query Time for BFs", "Set Size", "Membership Query Time", [SB,DPB], ["Static BF","Dynamic Partition BF"] ,foldername,"MembershipQueryTime_With_Same_MemUsage_SB_vs_DPB");
	# draw_graph("Membership Query Time for BFs", "Set Size", "Membership Query Time", [DB,DPB], ["Dynamic BF","Dynamic Partition BF"] ,foldername,"MembershipQueryTime_With_Same_MemUsage_DB_DPB");

	# DPB = get_graph_instance("DPBloom","solo",["UnitSize"],[unitSize],"NumElems","TimeTaken")
	# SB 	= get_graph_instance("CountBloom","solo",["UnitSize"],[unitSize],"NumElems","TimeTaken")
	# DB 	= get_graph_instance("DynamicBloom","solo",["UnitSize"],[unitSize],"NumElems","TimeTaken")

	# draw_graph("Set Creation Time for BFs", "Set Size", "BF Creation Time", [SB,DB,DPB], ["Static BF","Dynamic BF","Dynamic Partition BF"] ,foldername,"BF_CreationTime_With_Same_MemUsage_All");
	# draw_graph("Set Creation Time for BFs", "Set Size", "BF Creation Time", [SB,DB], ["Static BF","Dynamic BF"] ,foldername,"BF_CreationTime_With_Same_MemUsage_SB_DB");
	# draw_graph("Set Creation Time for BFs", "Set Size", "BF Creation Time", [SB,DPB], ["Static BF","Dynamic Partition BF"] ,foldername,"BF_CreationTime_With_Same_MemUsage_SB_vs_DPB");
	# draw_graph("Set Creation Time for BFs", "Set Size", "BF Creation Time", [DB,DPB], ["Dynamic BF","Dynamic Partition BF"] ,foldername,"BF_CreationTime_With_Same_MemUsage_DB_DPB");

# Comparing memory usage and space taken for a real-graph
def createGraphComparisonPlots(folderName):

	numNodes 		= 149700 
	AdjListMemUsage = [44326.0/1024,44326.0/1024,44326.0/1024,44326.0/1024,44326.0/1024,44326.0/1024]
	BF_Size 		= [512,1024,2048,4096,8192,8192*2];
	StaticMemUsage 	= [9356.0/1024,18712.0/1024,37425.0/1024,74850.0/1024,149701.0/1024,299402.0/1024]
	StaticAvgFP 	= [7170.0/numNodes,3756.0/numNodes,1860.0/numNodes,792.0/numNodes,276.0/numNodes,91.0/numNodes]
	StaticMaxFP 	= [148836.0/numNodes,147828.0/numNodes,145425.0/numNodes,140900.0/numNodes,133893.0/numNodes,122161.0/numNodes]
	Static_Error 	= [0,0,0,0,0,0]

	DPB_UnitSize	= [512,1024,2048,4096,8192,8192*2]
 	# DPB_MemUsage  	= [36966.0/1024,39828.0/1024,54567.0/1024,89078.0/1024,161173.0/1024,308263.0/1024] # With internal nodes
 	DPB_MemUsage  	= [25814.0/1024,33265.0/1024,50063.0/1024,85490.0/1024,157991.0/1024,305248.0/1024] # Without internal nodes 
 	DPB_AvgFP 		= [406.0/numNodes,181.0/numNodes,87.0/numNodes,47.0/numNodes,25.0/numNodes,15.0/numNodes]
 	DPB_MaxFP 		= [2498.0/numNodes,2200.0/numNodes,2099.0/numNodes,1989.0/numNodes,1839.0/numNodes,1851.0/numNodes]
	DPB_Error 		= [0,0,0,0,0,0]

	DB_UnitSize		= [512,1024,2048,4096,8192,8192*2]
 	DB_MemUsage  	= [29576.0/1024,36276.0/1024,53076.0/1024,88627.0/1024,161383.0/1024,308654.0/1024] # Without internal nodes 
 	DB_AvgFP 		= [1048.0/numNodes,486.0/numNodes,232.0/numNodes,119.0/numNodes,53.0/numNodes,29.0/numNodes]
 	DB_MaxFP 		= [117648.0/numNodes,102534.0/numNodes,90807.0/numNodes,88094.0/numNodes,76298.0/numNodes,65696.0/numNodes]
	DB_Error 		= [0,0,0,0,0,0]

	graphDetails 	= "Catster Network,\n numNodes:149700 numEdges:5449275 AvgDegree:72.8"
	title 		= "Using Bloom Filters to store Graphs:" + graphDetails
	X_label 	= "Size of Unit Bloom Filter"
	Y_label 	= "Memory(in MB)"
	Y_label_sec = "Avg False Positive"
	prim_graph_inst = [(BF_Size,StaticMemUsage,Static_Error), (DPB_UnitSize,DPB_MemUsage,DPB_Error),(DB_UnitSize,DB_MemUsage,DB_Error),(BF_Size,AdjListMemUsage,Static_Error)]
	sec_graph_inst  = [(BF_Size,StaticAvgFP,Static_Error), (DPB_UnitSize, DPB_AvgFP,DPB_Error), (DB_UnitSize, DB_AvgFP, DB_Error)]
	prim_inst_label = ["SBF","DPBF","DBF","Adj List"]
	sec_inst_label  = ["SBF","DPBF","DBF"]

	# Create graph with Memory Usage and FPP on the same plot(using secondary Y-Axis)
	# draw_graph_withSec_Axis(title,X_label,Y_label,Y_label_sec,prim_graph_inst,sec_graph_inst,prim_inst_label,sec_inst_label,folderName,"Graph storage comparison")
	createGNUPlotDataFile(prim_graph_inst,"gnuPlot/bloomGraph_memUsage")


	title 		= "Using Bloom Filters to store Graphs:" + graphDetails
	X_label 	= "Size of Unit Bloom Filter"
	Y_label 	= "Memory(in MB)"
	prim_graph_inst = [(BF_Size,StaticMemUsage,Static_Error), (DPB_UnitSize,DPB_MemUsage,DPB_Error), (DB_UnitSize,DB_MemUsage,DB_Error)]
	prim_inst_label = ["SBF","DPBF","DBF"]
	# draw_graph(title,X_label,Y_label,prim_graph_inst,prim_inst_label,folderName,"MemUsage Comparison for graphs")

	title 		= "Using Bloom Filters to store Graphs:" + graphDetails
	X_label 	= "Size of Unit Bloom Filter"
	Y_label 	= "Avg Num False Positives"
	prim_graph_inst = [(BF_Size,StaticAvgFP,Static_Error), (DPB_UnitSize,DPB_AvgFP,DPB_Error), (DB_UnitSize,DB_AvgFP,DB_Error)]
	prim_inst_label = ["SBF","DPBF","DBF"]
	# draw_graph(title,X_label,Y_label,prim_graph_inst,prim_inst_label,folderName,"Avg FP Comparison for graphs")
	createGNUPlotDataFile(prim_graph_inst,"gnuPlot/bloomGraph_Avg_FP")

	title 		= "Using Bloom Filters to store Graphs:" + graphDetails
	X_label 	= "Size of Unit Bloom Filter"
	Y_label 	= "Max Num False Positives"
	prim_graph_inst = [(BF_Size,StaticMaxFP,Static_Error), (DPB_UnitSize,DPB_MaxFP,DPB_Error), (DB_UnitSize,DB_MaxFP,DB_Error)]
	prim_inst_label = ["SBF","DPBF","DBF"]
	# draw_graph(title,X_label,Y_label,prim_graph_inst,prim_inst_label,folderName,"Max FP Comparison for graphs")
	createGNUPlotDataFile(prim_graph_inst,"gnuPlot/bloomGraph_Max_FP")

	title 		= "Using Bloom Filters to store Graphs:" + graphDetails
	X_label 	= "Size of Unit Bloom Filter"
	Y_label 	= "Number of False Positive"
	prim_graph_inst = [(BF_Size,StaticMaxFP,Static_Error), (DPB_UnitSize,DPB_MaxFP,DPB_Error),(BF_Size,StaticAvgFP,Static_Error), (DPB_UnitSize,DPB_AvgFP,DPB_Error)]
	prim_inst_label = ["SBF(Max)","DPBF(Max)","SBF(Avg)","DPBF(Avg)"]
	# draw_graph(title,X_label,Y_label,prim_graph_inst,prim_inst_label,folderName,"FP Comparison for graphs")

# Create plots comparing 3 variants of Bloom filters for set operations, when we have fixed overlap between set
# being contant and varying set sizes
def createSetOpsPlots_fixedOverlap(unitSize, overlapList,foldername):

	readFolder_setOps("CountBloom","data/SB_INTER",1)
	readFolder_setOps("DynamicBloom","data/DB_INTER",1)
	readFolder_setOps("DPBloom","data/DPB_INTER",1)

	for overlap in overlapList:
		SB_F 	= get_graph_instance("CountBloom","pair",["UnitSize","Overlap","Operation"],[unitSize,overlap,"Intersection"],"SymmetricDiffSize","FP_Ratio")
		
		DB_F 	= get_graph_instance("DynamicBloom","pair",["UnitSize","Overlap","Operation"],[unitSize,overlap,"Intersection"],"SymmetricDiffSize","FP_Ratio")
		
		DPB_F 	= get_graph_instance("DPBloom","pair",["UnitSize","Overlap","Operation"],[unitSize,overlap,"Intersection"],"SymmetricDiffSize","FP_Ratio")

		SB_M 	= get_graph_instance("CountBloom","pair",["UnitSize","Overlap","Operation"],[unitSize,overlap,"Intersection"],"SymmetricDiffSize","EffMemUsage")
		
		DB_M 	= get_graph_instance("DynamicBloom","pair",["UnitSize","Overlap","Operation"],[unitSize,overlap,"Intersection"],"SymmetricDiffSize","EffMemUsage")
		
		DPB_M 	= get_graph_instance("DPBloom","pair",["UnitSize","Overlap","Operation"],[unitSize,overlap,"Intersection"],"SymmetricDiffSize","EffMemUsage")

		title 	= "Intersection Results"
		X_label	= "Size of Symmetric Difference of A & B"
		Y_label = "False Positive Rate"
		Y_label_sec 	= "Memory Usage( in bits)"
		prim_graph_inst = [SB_F,DB_F,DPB_F]
		sec_graph_inst 	= [SB_M,DB_M,DPB_M]
		prim_inst_label = ["SB_F","DB_F","DPB_F"]
		sec_inst_label 	= ["SB_M","DB_M","DPB_M"]

		createGNUPlotDataFile(prim_graph_inst,"gnuPlot/FP_intersection_overlap_"+ str(overlap))
		createGNUPlotDataFile(sec_graph_inst,"gnuPlot/memUsage_intersection_overlap_"+ str(overlap))
		
		# draw_graph_withSec_Axis(title,X_label,Y_label,Y_label_sec, prim_graph_inst,sec_graph_inst,prim_inst_label,sec_inst_label ,foldername,"Set Intersection_All_Overlap_" + str(overlap))
		# draw_graph("False Positive Rate After Intersection for BFs", "Size of Symmetric Diference", "False Positive Rate", [SB,DB,DPB], ["Static BF","Dynamic BF","Dynamic Partition BF"] ,foldername,"Set_Intersection_Fixed_Overlap_"+str(overlap)+"_All");
		# draw_graph("False Positive Rate After Intersection for BFs", "Size of Symmetric Diference", "False Positive Rate", [SB,DB], ["Static BF","Dynamic BF"] ,foldername,"Set_Intersection_Fixed_Overlap_"+str(overlap)+"_SB_DB");
		# draw_graph("False Positive Rate After Intersection for BFs", "Size of Symmetric Diference", "False Positive Rate", [SB,DPB], ["Static BF","Dynamic Partition BF"] ,foldername,"Set_Intersection_Fixed_Overlap_"+str(overlap)+"_SB_DPB");
		# draw_graph("False Positive Rate After Intersection for BFs", "Size of Symmetric Diference", "False Positive Rate", [DB,DPB], ["Dynamic BF","Dynamic Partition BF"] ,foldername,"Set_Intersection_Fixed_Overlap_"+str(overlap)+"_DB_DPB");


	readFolder_setOps("CountBloom","data/SB_UNION",1)
	readFolder_setOps("DynamicBloom","data/DB_UNION",1)
	readFolder_setOps("DPBloom","data/DPB_UNION",1)

	for overlap in overlapList:
		SB_F 	= get_graph_instance("CountBloom","pair",["UnitSize","Overlap","Operation"],[unitSize,overlap,"Union"],"SymmetricDiffSize","FP_Ratio")
		
		DB_F 	= get_graph_instance("DynamicBloom","pair",["UnitSize","Overlap","Operation"],[unitSize,overlap,"Union"],"SymmetricDiffSize","FP_Ratio")
		
		DPB_F 	= get_graph_instance("DPBloom","pair",["UnitSize","Overlap","Operation"],[unitSize,overlap,"Union"],"SymmetricDiffSize","FP_Ratio")

		SB_M 	= get_graph_instance("CountBloom","pair",["UnitSize","Overlap","Operation"],[unitSize,overlap,"Union"],"SymmetricDiffSize","EffMemUsage")
		
		DB_M 	= get_graph_instance("DynamicBloom","pair",["UnitSize","Overlap","Operation"],[unitSize,overlap,"Union"],"SymmetricDiffSize","EffMemUsage")
		
		DPB_M 	= get_graph_instance("DPBloom","pair",["UnitSize","Overlap","Operation"],[unitSize,overlap,"Union"],"SymmetricDiffSize","EffMemUsage")

		title 	= "Union Results"
		X_label	= "Size of Symmetric Difference of A & B"
		Y_label = "False Positive Rate"
		Y_label_sec 	= "Memory Usage( in bits)"
		prim_graph_inst = [SB_F,DB_F,DPB_F]
		sec_graph_inst 	= [SB_M,DB_M,DPB_M]
		prim_inst_label = ["SB_F","DB_F","DPB_F"]
		sec_inst_label 	= ["SB_M","DB_M","DPB_M"]

		# draw_graph_withSec_Axis(title,X_label,Y_label,Y_label_sec,prim_graph_inst,sec_graph_inst,prim_inst_label,sec_inst_label,foldername,"Set Union_All_Overlap_" + str(overlap))
		createGNUPlotDataFile(prim_graph_inst,"gnuPlot/FP_union_overlap_"+ str(overlap))
		createGNUPlotDataFile(sec_graph_inst,"gnuPlot/memUsage_union_overlap_"+ str(overlap))



if __name__ == '__main__':
	
	font = {'size'   : 20}
	matplotlib.rc('font', **font)

	if len(sys.argv) >= 2:
		expType = int(sys.argv[1])
		if expType == 1: # For drawing plots for varying unitSize of DPBF
			drawUnitSizeGraphs([512,1024],20)
		elif expType == 2: # For Comparing FP rate for BFs when all three consume same memory

			unitSize 	= int(sys.argv[2])
			numSamples 	= int(sys.argv[3])
			compareFP(numSamples,unitSize,"graphs_SameMemUsage")
			
		elif expType == 3: # For Comparing memoryUsage for BFs when all three have same FP rate
			unitSize 	= int(sys.argv[2])
			numSamples 	= int(sys.argv[3])
			compareMemory(numSamples,unitSize,"graphs_SameFP")
		elif expType == 4: # For plotting data for bloomGraph
			createGraphComparisonPlots("plotsForRealWorldGraphs")
		elif expType == 5: # For set ops with fixed overlap

			foldername = "plots_SetOps_FixOverlap"
			unitSize 		= int(sys.argv[2])
			numOverlapSizes = int(sys.argv[3])
			overlapSizes    = []
			for i in xrange(numOverlapSizes):
				overlapSizes += [int(sys.argv[4 + i])]

			os.system("mkdir " + foldername)
			createSetOpsPlots_fixedOverlap(unitSize,overlapSizes,foldername)
		else:
			print "expType 1: For drawing plots for varying unitSize of DPBF"
			print "expType 2: For Comparing FP rate for BFs when all three consume same memory"
			print "expType 3: For Comparing memoryUsage for BFs when all three have same FP rate"
			print "expType 4: For plotting data for bloomGraph"
			print "expType 5: For set ops with fixed overlap"
			print "Usage: ./graph_creator <expType>"
			print "Usage: ./graph_creator 5 <unitSize> <numOverlapSizes> <overlap_1> ... <overlap_n>"
	else:
		print "expType 1: For drawing plots for varying unitSize of DPBF"
		print "expType 2: For Comparing FP rate for BFs when all three consume same memory"
		print "expType 3: For Comparing memoryUsage for BFs when all three have same FP rate"
		print "expType 4: For plotting data for bloomGraph"
		print "expType 5: For set ops with fixed overlap"
		print "Usage: ./graph_creator <expType>"
		print "Usage: ./graph_creator 5 <unitSize> <numOverlapSizes> <overlap_1> ... <overlap_n>"

