set terminal pdf enhanced font 'Helvetica,20'

set format y "10^{%L}"
################################################## Plots for Storing Set using Bloom Filter #######################################
set logscale xy
set grid y
set xtics (10,100,1000,10000)

set output 'FP_sameMemUsage.pdf'
set key outside vertical right
set ylabel 'FP Rate'
set xlabel 'Set Size'
set title 'False Positive Rate for Bloom Filters' 
plot 'FP_sameMemUsage.txt' using 1:2:3 title 'SBF' with yerrorlines lw 2, '' using 1:4:5 title 'DBF' with yerrorlines lw 2, '' using 1:6:7 title 'DPBF' with yerrorlines lw 2


set output 'memUsage_sameFP.pdf'
set key outside vertical right
set ylabel 'Memory Usage(in Bits)'
set xlabel 'Set Size'
set title 'Memory Usage for Bloom Filters' 
plot 'memUsage_sameFP.txt' using 1:2:3 title 'SBF' with yerrorlines lw 2, '' using 1:4:5 title 'DBF' with yerrorlines lw 2, '' using 1:6:7 title 'DPBF' with yerrorlines lw 2	

set output 'memQueryTime_sameFP.pdf'
set key outside vertical right
set ylabel 'Query Time(in s)'
set xlabel 'Set Size'
set title 'Time take for 10^6 Membership queries'
plot 'memQueryTime_sameFP.txt' using 1:2:3 title 'SBF' with yerrorlines lw 2, '' using 1:4:5 title 'DBF' with yerrorlines lw 2, '' using 1:6:7 title 'DPBF' with yerrorlines lw 2	

set output 'memQueryTime_sameMemUsage.pdf'
set key outside vertical right
set ylabel 'Query Time(in s)'
set xlabel 'Set Size'
set title 'Time take for 10^6 Membership queries'
plot 'memQueryTime_sameMemUsage.txt' using 1:2:3 title 'SBF' with yerrorlines lw 2, '' using 1:4:5 title 'DBF' with yerrorlines lw 2, '' using 1:6:7 title 'DPBF' with yerrorlines lw 2	

unset logscale x

################################################## Plots for Set Intersection  ######################################
set logscale y
set xtics 500

set output 'FP_intersection_overlap_100.pdf'
set key outside vertical right
set ylabel 'FP Rate'
set xlabel 'Size of Symmetric Difference b/w A & B'
set title 'FP Rate after Intersection' 
plot 'FP_intersection_overlap_100.txt' using 1:2:3 title 'SBF' with yerrorlines lw 2, '' using 1:4:5 title 'DBF' with yerrorlines lw 2, '' using 1:6:7 title 'DPBF' with yerrorlines lw 2

set output 'memUsage_intersection_overlap_100.pdf'
set key outside vertical right
set ylabel 'Memory Usage(in bits)'
set xlabel 'Size of Symmetric Difference b/w A & B'
set title 'Memory Usage after Intersection' 
plot 'memUsage_intersection_overlap_100.txt' using 1:2:3 title 'SBF' with yerrorlines lt 4 lw 2, '' using 1:4:5 title 'DBF' with yerrorlines lw 2, '' using 1:6:7 title 'DPBF' with yerrorlines lt 7 ps 0.5 lw 2

unset logscale y
################################################## Plots for Set Union  ####################################################
set logscale y
set xtics 500

set output 'FP_union_overlap_100.pdf'
set key outside vertical right
set ylabel 'FP Rate'
set xlabel 'Size of Symmetric Difference b/w A & B'
set title 'FP Rate after Union' 
plot 'FP_union_overlap_100.txt' using 1:2:3 title 'SBF' with yerrorlines lw 2, '' using 1:4:5 title 'DBF' with yerrorlines lw 2, '' using 1:6:7 title 'DPBF' with yerrorlines lw 2

set output 'memUsage_union_overlap_100.pdf'
set key outside vertical right
set ylabel 'Memory Usage(in bits)'
set xlabel 'Size of Symmetric Difference b/w A & B'
set title 'Memory Usage after Union' 
plot 'memUsage_union_overlap_100.txt' using 1:2:3 title 'SBF' with yerrorlines lw 2, '' using 1:4:5 title 'DBF' with yerrorlines lw 2, '' using 1:6:7 title 'DPBF' with yerrorlines lw 2

unset logscale y
################################################## Plots for BloomGraph  ######################################
set logscale xy
set xtics (512,1024,2048,4096,8192,16384)

set output 'bloomGraph_Avg_FP.pdf'
set key outside vertical right
set ylabel 'Avg FP Rate'
set xlabel 'Size of Bloom Filter'
set title 'Average FP Rate : Catster Network' 
plot 'bloomGraph_Avg_FP.txt' using 1:2 title 'SBF' with linespoints lw 2, '' using 1:4 title 'DPBF' with linespoints lw 2 , '' using 1:6 title 'DBF' with linespoints lw 2 

set output 'bloomGraph_Max_FP.pdf'
set key outside vertical right
set ylabel 'Max FP Rate'
set xlabel 'Size of Bloom Filter'
set title 'Max FP Rate : Catster Network' 
plot 'bloomGraph_Max_FP.txt' using 1:2 title 'SBF' with linespoints lw 2, '' using 1:4 title 'DPBF' with linespoints lw 2 , '' using 1:6 title 'DBF' with linespoints lw 2 

set output 'bloomGraph_memUsage.pdf'
set key outside vertical right
set ylabel 'Memory Usage(in MB)'
set xlabel 'Size of Bloom Filter'
set title 'Overall Memory Usage : Catster Network' 
plot 'bloomGraph_memUsage.txt' using 1:2 title 'SBF' with linespoints lw 2, '' using 1:4 title 'DPBF' with linespoints lw 2 , '' using 1:6 title 'DBF' with linespoints lw 2, '' using 1:8 title 'Adj' with linespoints lw 2

set output 'bloomGraph_FP.pdf'
set key outside vertical right
set ylabel 'Max FP Rate'
set y2label 'Avg FP Rate'
set xlabel 'Size of Bloom Filter'
set title 'Average FP Rate : Catster Network' 
plot 'bloomGraph_FP.txt' using 1:2 title 'SBF' axes x1y1 with linespoints lw 2, '' using 1:4 title 'DPBF' axes x1y1 with linespoints lw 2 , '' using 1:6 title 'DBF' axes x1y1  with linespoints lw 2 , '' using 1:8 title 'sbf' axes x1y2 with linespoints pt 2 lt 0 lc rgb "red" lw 2, '' using 1:10 title 'dpbf' axes x1y2 with linespoints lt 0 lw 2 , '' using 1:12 title 'dbf' axes x1y2  with linespoints lt 0 lw 2 

unset logscale xy

