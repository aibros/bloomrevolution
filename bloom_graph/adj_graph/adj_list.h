#ifndef ADJ_LIST
#define ADJ_LIST

#include <stdio.h>
#include <stdlib.h>
#include "array.h"
#include "sys/time.h"
#include <unistd.h>



struct graph_node{
	int id;
	int size;
	my_array *ngbrs;
};

typedef struct graph_node graph_node;

extern int max_id;
extern int numNodes_gbl;
extern int numEdges_gbl;

int search_max(char *filename);
graph_node* init_stdGraph(char *filename);
static inline float get_time_diff(struct timeval t1, struct timeval t2);

#endif