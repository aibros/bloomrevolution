#include <stdio.h>
#include <stdlib.h>
#include "array.h"
#include "sys/time.h"
#include <unistd.h>

static inline float get_time_diff(struct timeval t1, struct timeval t2);
void performMembershipTest_Sampled(char* sampleFilename,FILE* logger);

