#include <stdio.h>
#include <stdlib.h>
#include "adj_list.h"
#include <assert.h>

//format for the graph for reading
//first line "n v" -- number of edges and vertices
//followed by edges in "v1 v2" manner
//assuming node numbering starts at 0

struct graph_node* graph;
int max_id;
int numNodes_gbl;
int numEdges_gbl;

void performMembershipTest_Sampled(char* sampleFilename,FILE* logger)
{
	FILE * reader;
	reader = fopen(sampleFilename,"r");
	int src, dest;
	float numPositives = 0, numSampledEdges = 0;
	struct timeval start,end;

	gettimeofday(&start,NULL);
	while(!feof(reader))
	{
		fscanf(reader,"%d\t%d\n",&src,&dest);	
		if (arrayInt_find(graph[src].ngbrs,dest))
		{
			numPositives++;
		}
		numSampledEdges++;
	}
	gettimeofday(&end,NULL);
	fclose(reader);
 	
 	// For undirected graph
	double totalMemory = numEdges_gbl*2*sizeof(int) + numNodes_gbl*(sizeof(struct my_array) + sizeof(struct graph_node)); 
	
	// For directed graph, notice 2 not multiplied with num_edges
	// double totalMemory = numEdges_gbl*sizeof(int) + numNodes_gbl*(sizeof(struct my_array) + sizeof(struct graph_node));
	
	totalMemory /= 1024;
	totalMemory /= 1024;
	float fp_ratio = numPositives/numSampledEdges;
	fprintf(logger, "%s,%d,%d,%d,%f,%.0f,%f,%f\n",sampleFilename,numNodes_gbl,numEdges_gbl,0,totalMemory,numSampledEdges,fp_ratio,get_time_diff(start,end));
}

static inline float get_time_diff(struct timeval t1, struct timeval t2)
{
	return t2.tv_sec - t1.tv_sec + 1e-6 * (t2.tv_usec - t1.tv_usec);
}

int main(int argc, char const *argv[])
{
	struct timeval start,end;

	if(argc >= 3)
	{
		char* graphFilename = malloc(1000*sizeof(char));
		sprintf(graphFilename,"%s",argv[1]);
		
		gettimeofday(&start,NULL);
		graph = init_stdGraph(graphFilename);
		gettimeofday(&end,NULL);
		printf("Created Graph in time::%f\n",get_time_diff(start,end));

		FILE* logger;
		if( access( "../Results/ADJ.csv", F_OK ) != -1 ) 
		{
    		// file exists
    		logger = fopen("../Results/ADJ.csv","a");
		} 
		else 
		{
    		// file doesn't exist
    		logger = fopen("../Results/ADJ.csv","w");
    		fprintf(logger, "graphFilename,testFilename,numNodes,numEdges,SIZE_BLOOM,MemUsage(inMB),totalSampledEdges,fp_ratio,timeTaken\n");
		}
		int numTestFiles 	= atoi(argv[2]);
		char* testFilename;
		for (int i = 0; i < numTestFiles; ++i)
		{
			testFilename = malloc(1000*sizeof(char));
			sprintf(testFilename,"%s",argv[3+i]);
			
			fprintf(logger, "%s,",graphFilename );
			gettimeofday(&start,NULL);
			performMembershipTest_Sampled(testFilename,logger);
			gettimeofday(&end,NULL);
			
			printf("Membership tests for file %s done in time::%f\n",testFilename,get_time_diff(start,end));
			free(testFilename);
		}
	}
	else
	{
		printf("Incorrect usage:\n");
		printf("Usage: ./bloom_graph <graph filename> <numSampledEdgeFiles> <filename_1> ... <filename_n\n");
	}
	/* code */
	return 0;
}