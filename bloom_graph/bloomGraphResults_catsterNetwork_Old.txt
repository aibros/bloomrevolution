Memory Footprint : numLeaves*UnitSize/8 Bytes
More detailed footprint:  numLeaves*UnitSize/8 + (numLeaves - numNodes)*12 + numLeaves*20 Bytes
	<1st term: For actual bloom filters>
	<2nd term: For internal nodes, we have 3 pointers, so 12 Bytes>
	<3rd term: For leaves, we have 12 bytes for left,right and bf pointer, 8 bytes for BF's count of elements & pointer to actual BF vector>

Adj List Memory Footprint : (numEdges*2 + numNodes*3)*4 (for undirected graph)
Adj List Memory Footprint : (numEdges + numNodes*3)*4 (for directed graph)
<2nd term: For each node, we have 12 bytes for additional memory for storing numElements, maxArraySize, pointer to array)

Graph:
Catster: 
	NumNodes	:	149 700 
	NumEdges	:	5 449 275
	Avg Degree	:	72.8
	Adj List Mem Footprint: 45 390 600 Bytes
	BloomParameters:
		K 				: 	3
		FPP threshold	:	0.01

Static Bloom Filter:

	Total memory should be divided by 8 to get correct value
	for static case, it is now corrected in the code but these results are for older version of code

	SIZE_BLOOM::512	K::3	COUNTER_SIZE::1

	Size of graph::10897472
	Total memory(in KB)				::74850
	Created Graph in time::2.476778
	totalFalsePositives::1073418348
	AvgFalsePositives::7170
	maxFalsePositives::148836
	Membership tests done in time::2378.424561

	SIZE_BLOOM::1024	K::3	COUNTER_SIZE::1

	Size of graph::10897472
	Total memory(in KB)				::149701
	Created Graph in time::2.910194
	totalFalsePositives::562369183
	AvgFalsePositives::3756
	maxFalsePositives::147828
	Membership tests done in time::2186.261475
	SIZE_BLOOM::2048	K::3	COUNTER_SIZE::1

	Size of graph::10897472
	Total memory(in KB)				::299402
	Created Graph in time::3.984483
	totalFalsePositives::278590093
	AvgFalsePositives::1860
	maxFalsePositives::145425
	Membership tests done in time::2259.382812

	SIZE_BLOOM::4096	K::3	COUNTER_SIZE::1

	Size of graph::10897472
	Total memory(in KB)				::598804
	Created Graph in time::5.865776
	totalFalsePositives::118641690
	AvgFalsePositives::792
	maxFalsePositives::140900
	Membership tests done in time::2208.535400


	SIZE_BLOOM::8192	K::3	COUNTER_SIZE::1

	Size of graph::10897472
	Total memory(in KB)				::1197608
	Created Graph in time::8.116232
	totalFalsePositives::41384512
	AvgFalsePositives::276
	maxFalsePositives::133893
	Membership tests done in time::2176.601562

	SIZE_BLOOM::16384	K::3	COUNTER_SIZE::1

	Size of graph::10897472
	Total memory(in KB)				::2395216
	Created Graph in time::7.974340
	totalFalsePositives::13733840
	AvgFalsePositives::91
	maxFalsePositives::122161
	Membership tests done in time::2132.329590


Dynamic Partition Bloom Filter:

	Number of edges::5449275, Number of nodes::149701
	SIZE_BLOOM::512	K::3	COUNTER_SIZE::1
	FALSE_PSTV_PROB::0.010000
	FILL_THRESHOLD::41
	NUM_PARTITION::11
	Gotten adjacency list based graph. Enter zero to continue
	Initialised graph data structure and now going to create graph
	Successfully created graph
	Size of bloom_graph(numLeaves)::413026
	Number of nodes to free::7745042
	BF memory(in KB)				::25814
	Total memory(in KB)				::36966
	Memory overhead for tree(in KB)		::11152
	Memory overhead for tree(in percent)	::30.168263
	AdjList memory				::44326
	Memory Gain 				::18512
	Memory Gain(with overhead)			::7360
	Memory Gain(in percent)			::0.417633
	Memory Gain(with overhead)(in prcnt)	::0.166043
	Gotten graph1
	Created Graph in time::11.187047
	totalFalsePositives::60847393
	AvgFalsePositives  ::406.459496
	MaxFalsePositives  ::2498
	Membership tests done in time::3067.729248
	Enter zero to exit

	Number of edges::5449275, Number of nodes::149701
	SIZE_BLOOM::1024	K::3	COUNTER_SIZE::1
	FALSE_PSTV_PROB::0.010000
	FILL_THRESHOLD::82
	NUM_PARTITION::10
	Gotten adjacency list based graph. Enter zero to continue
	Initialised graph data structure and now going to create graph
	Successfully created graph
	Size of bloom_graph(numLeaves)::266127
	Number of nodes to free::2192381
	BF memory(in KB)				::33265
	Total memory(in KB)				::39828
	Memory overhead for tree(in KB)		::6563
	Memory overhead for tree(in percent)	::16.478357
	AdjList memory				::44326
	Memory Gain 				::11061
	Memory Gain(with overhead)			::4498
	Memory Gain(in percent)			::0.249538
	Memory Gain(with overhead)(in prcnt)	::0.101475
	Gotten graph1
	Created Graph in time::10.441603
	totalFalsePositives::27173510
	AvgFalsePositives  ::181.518560
	MaxFalsePositives  ::2200
	Membership tests done in time::2953.598633
	Enter zero to exit


	Number of edges::5449275, Number of nodes::149701
	SIZE_BLOOM::2048	K::3	COUNTER_SIZE::1
	FALSE_PSTV_PROB::0.010000
	FILL_THRESHOLD::165
	NUM_PARTITION::9
	Gotten adjacency list based graph. Enter zero to continue
	Initialised graph data structure and now going to create graph
	Successfully created graph
	Size of bloom_graph(numLeaves)::200254
	Number of nodes to free::503416
	BF memory(in KB)				::50063
	Total memory(in KB)				::54567
	Memory overhead for tree(in KB)		::4504
	Memory overhead for tree(in percent)	::8.254073
	AdjList memory				::44326
	Memory Gain 				::-5737
	Memory Gain(with overhead)			::-10241
	Memory Gain(in percent)			::-0.129427
	Memory Gain(with overhead)(in prcnt)	::-0.231038
	Gotten graph1
	Created Graph in time::2.695155
	totalFalsePositives::13062021
	AvgFalsePositives  ::87.254066
	MaxFalsePositives  ::2099
	Membership tests done in time::2921.647949
	Enter zero to exit


	Number of edges::5449275, Number of nodes::149701
	SIZE_BLOOM::4096	K::3	COUNTER_SIZE::1
	FALSE_PSTV_PROB::0.010000
	FILL_THRESHOLD::331
	NUM_PARTITION::8
	Gotten adjacency list based graph. Enter zero to continue
	Initialised graph data structure and now going to create graph
	Successfully created graph
	Size of bloom_graph(numLeaves)::170980
	Number of nodes to free::94058
	BF memory(in KB)				::85490
	Total memory(in KB)				::89078
	Memory overhead for tree(in KB)		::3588
	Memory overhead for tree(in percent)	::4.027931
	AdjList memory				::44326
	Memory Gain 				::-41164
	Memory Gain(with overhead)			::-44752
	Memory Gain(in percent)			::-0.928665
	Memory Gain(with overhead)(in prcnt)	::-1.009611
	Gotten graph1
	Created Graph in time::2.368918
	totalFalsePositives::7180551
	AvgFalsePositives  ::47.965952
	MaxFalsePositives  ::1989
	Membership tests done in time::2862.660889
	Enter zero to exit

	Number of edges::5449275, Number of nodes::149701
	SIZE_BLOOM::8192	K::3	COUNTER_SIZE::1
	FALSE_PSTV_PROB::0.010000
	FILL_THRESHOLD::662
	NUM_PARTITION::7
	Gotten adjacency list based graph. Enter zero to continue
	Initialised graph data structure and now going to create graph
	Successfully created graph
	Size of bloom_graph(numLeaves)::157991
	Number of nodes to free::12874
	BF memory(in KB)				::157991
	Total memory(in KB)				::161173
	Memory overhead for tree(in KB)		::3182
	Memory overhead for tree(in percent)	::1.974276
	AdjList memory				::44326
	Memory Gain 				::-113665
	Memory Gain(with overhead)			::-116847
	Memory Gain(in percent)			::-2.564296
	Memory Gain(with overhead)(in prcnt)	::-2.636083
	Gotten graph1
	Created Graph in time::2.967997
	totalFalsePositives::3780070
	AvgFalsePositives  ::25.250800
	MaxFalsePositives  ::1839
	Membership tests done in time::2875.443604
	Enter zero to exit

	Number of edges::5449275, Number of nodes::149701
	SIZE_BLOOM::16384	K::3	COUNTER_SIZE::1
	FALSE_PSTV_PROB::0.010000
	FILL_THRESHOLD::1325
	NUM_PARTITION::6
	Gotten adjacency list based graph. Enter zero to continue
	Initialised graph data structure and now going to create graph
	Successfully created graph
	Size of bloom_graph(numLeaves)::152624
	Number of nodes to free::2432
	BF memory(in KB)				::305248
	Total memory(in KB)				::308263
	Memory overhead for tree(in KB)		::3015
	Memory overhead for tree(in percent)	::0.978061
	AdjList memory				::44326
	Memory Gain 				::-260922
	Memory Gain(with overhead)			::-263937
	Memory Gain(in percent)			::-5.886432
	Memory Gain(with overhead)(in prcnt)	::-5.954451
	Gotten graph1
	Created Graph in time::3.069394
	totalFalsePositives::2268312
	AvgFalsePositives  ::15.152284
	MaxFalsePositives  ::1851
	Membership tests done in time::2815.440674
	Enter zero to exit


Tried to have Same MemUsage
Dynamic  Bloom Filter:
	
	SIZE_BLOOM::512	K::3	COUNTER_SIZE::1
	FALSE_PSTV_PROB::0.010000
	FILL_THRESHOLD::27.606659
	createBloomGraph()::Initialised graph data structure and now going to create graph
	maxNodeId::149700
	createBloomGraph()::Successfully created graph
	createBloomGraph()::Size of bloom_graph(numUnitBFs)::473225
	createBloomGraph()::Total memory(in KB)				::29576
	Created Graph in time::2.791833
	totalFalsePositives::156995738
	AvgFalsePositives  ::1048.728719
	MaxFalsePositives  ::117648
	Membership tests done in time::5431.308594

	SIZE_BLOOM::1024	K::3	COUNTER_SIZE::1
	FALSE_PSTV_PROB::0.010000
	FILL_THRESHOLD::55.213318
	createBloomGraph()::Initialised graph data structure and now going to create graph
	maxNodeId::149700
	createBloomGraph()::Successfully created graph
	createBloomGraph()::Size of bloom_graph(numUnitBFs)::290212
	createBloomGraph()::Total memory(in KB)				::36276
	Created Graph in time::3.176199
	totalFalsePositives::72828018
	AvgFalsePositives  ::486.489856
	MaxFalsePositives  ::102534
	Membership tests done in time::4281.100586

	SIZE_BLOOM::2048	K::3	COUNTER_SIZE::1
	FALSE_PSTV_PROB::0.010000
	FILL_THRESHOLD::110.426636
	createBloomGraph()::Initialised graph data structure and now going to create graph
	maxNodeId::149700
	createBloomGraph()::Successfully created graph
	createBloomGraph()::Size of bloom_graph(numUnitBFs)::212306
	createBloomGraph()::Total memory(in KB)				::53076
	Created Graph in time::5.547638
	totalFalsePositives::34771680
	AvgFalsePositives  ::232.274200
	MaxFalsePositives  ::90807
	Membership tests done in time::3649.792969

	SIZE_BLOOM::4096	K::3	COUNTER_SIZE::1
	FALSE_PSTV_PROB::0.010000
	FILL_THRESHOLD::220.853271
	createBloomGraph()::Initialised graph data structure and now going to create graph
	maxNodeId::149700
	createBloomGraph()::Successfully created graph
	createBloomGraph()::Size of bloom_graph(numUnitBFs)::177254
	createBloomGraph()::Total memory(in KB)				::88627
	Created Graph in time::5.963548
	totalFalsePositives::17860010
	AvgFalsePositives  ::119.304547
	MaxFalsePositives  ::88094
	Membership tests done in time::3283.781006

	SIZE_BLOOM::8192	K::3	COUNTER_SIZE::1
	FALSE_PSTV_PROB::0.010000
	FILL_THRESHOLD::441.706543
	createBloomGraph()::Initialised graph data structure and now going to create graph
	maxNodeId::149700
	createBloomGraph()::Successfully created graph
	createBloomGraph()::Size of bloom_graph(numUnitBFs)::161383
	createBloomGraph()::Total memory(in KB)				::161383
	Created Graph in time::7.963394
	totalFalsePositives::8069827
	AvgFalsePositives  ::53.906300
	MaxFalsePositives  ::76298
	Membership tests done in time::3069.011230

	SIZE_BLOOM::16384	K::3	COUNTER_SIZE::1
	FALSE_PSTV_PROB::0.010000
	FILL_THRESHOLD::883.413086
	createBloomGraph()::Initialised graph data structure and now going to create graph
	maxNodeId::149700
	createBloomGraph()::Successfully created graph
	createBloomGraph()::Size of bloom_graph(numUnitBFs)::154327
	createBloomGraph()::Total memory(in KB)				::308654
	Created Graph in time::9.004403
	totalFalsePositives::4437617
	AvgFalsePositives  ::29.643202
	MaxFalsePositives  ::65696
	Membership tests done in time::2970.380615

