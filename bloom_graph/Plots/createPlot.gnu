set terminal pdf enhanced font 'Helvetica,20'

############################################ FP RATIO PLOT ##################################################################
set output 'bloomGraph_2_FP.pdf'
set key outside vertical right
set ylabel 'FP Rate'
set title "False Positive Rate for Bloom Filters"
set boxwidth 0.9 relative
set style data histograms
set style histogram cluster
set style histogram errorbars gap 1 lw 2
set style fill solid 1.0 border lt -1
set bars front

plot 'bloomGraph_2_FP.txt' using 2:3:xticlabels(1) title 'SBF','' using 4:5:xticlabels(1) title 'DBF' ,'' using 6:7:xticlabels(1) title 'DPBF'

############################################ MEMBERSHIP QUERY TIME  PLOT ####################################################
set output 'bloomGraph_2_MemQuery.pdf'
set key outside vertical right
set ylabel 'Query time(in s)'
set title "Time taken for 2*|E| Membership Queries"
set boxwidth 0.9 relative
set style data histograms
set style histogram cluster
set style histogram errorbars gap 1 lw 2
set style fill solid 1.0 border lt -1
set bars front

plot 'bloomGraph_2_MemQuery.txt' using 2:3:xticlabels(1) title 'SBF','' using 4:5:xticlabels(1) title 'DBF' ,'' using 6:7:xticlabels(1) title 'DPBF' ,'' using 8:9:xticlabels(1) title 'ADJ'	

############################################ MEMORY USAGE PLOT ###############################################################
set output 'bloomGraph_2_MemUsage.pdf'
set key outside vertical right
set ylabel 'Memory Usage(in MB)'
set title "Memory Usage to store graph"
set boxwidth 0.9 relative
set style data histograms
set style histogram cluster
set style histogram errorbars gap 1 lw 2
set style fill solid 1.0 border lt -1
set bars front

plot 'bloomGraph_2_MemUsage.txt' using 2:3:xticlabels(1) title 'SBF','' using 4:5:xticlabels(1) title 'DBF' ,'' using 6:7:xticlabels(1) title 'DPBF' ,'' using 8:9:xticlabels(1) title 'ADJ'	

