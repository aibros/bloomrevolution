import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import glob
from pprint import pprint
import math
import copy
import os,sys


metrics = ["timeTaken", "fp_ratio","MemUsage(inMB)"];
factor = "SIZE_BLOOM";


def correct_data(tp, pqdata, num_tests):
	tot_size = pqdata["fp_ratio"].size;
	rep = int(tot_size/num_tests);
	frames = [];
	for ind in range(rep):
		frames += [tp];
	concat_correct = pd.concat(frames, ignore_index=True);
	return pqdata;


def get_graph_instance(metric, factor, data, num_tests):
	X = [];
	Y = [];
	Z = [];
	i = 0;
	while(i<data[factor].size):
		j = i;
		temp = [];
		X += [data[factor][i]];
		while( j < i+num_tests ):
			temp += [data[metric][j]];
			j+=1;
		Y += [np.mean(temp)];
		Z += [float(np.std(temp))/float(math.sqrt(len(temp)))]; 
		i += num_tests;
	return (X,Y,Z);


def draw_graph( title, X_label, Y_label, graph_instances, instance_labels,directory,name):
	fig = plt.figure(figsize=(12, 9), dpi=80);
	ax = fig.add_subplot(111)

	for ind in range(len(graph_instances)):
		(X,Y,Z) = graph_instances[ind];		
		plt.errorbar(X,Y,yerr=Z,marker='o',ls='-');
		for xy in zip(X, Y):                                       
	  		ax.annotate('(%s, %s)' % xy, xy=xy, textcoords='data')

	plt.legend(instance_labels, loc='upper right')
	plt.suptitle(title);
	plt.xlabel(X_label);
	plt.ylabel(Y_label);
	plt.show();
	fig.savefig(directory+"/"+name+".png")

def plot(graphs, num_tests, graph_labels, directory):
	global factor, metrics;
	for metric in metrics:
		instances = [];
		for graph in graphs:
			instance 	= get_graph_instance(metric, factor, graph, num_tests);
			instances 	+= [instance];

		print("metric",metric,instances)
		print("\n\n")
		createGNUPlotDataFile(instances,metric,directory)
		# draw_graph(metric+" vs "+factor, factor, metric, instances, graph_labels, directory, metric);

def createGNUPlotDataFile(graph_instances,filename,directory):
	
	strList = {}
	for ctr in range(len(graph_instances[0][0])):
		strList[ctr] = str(graph_instances[0][0][ctr]) + "\t" 


	for ind in range(len(graph_instances)):
		(X,Y,Z) = graph_instances[ind];
		for ctr in range(len(graph_instances[ind][0])):
			strList[ctr] += str(Y[ctr]) + "\t" + str(Z[ctr]) + "\t" 

	dataWriter = open(directory+ "/"+ filename + "_temp.txt","w")
	for key in sorted(strList.keys()):
		# print(strList[key])
		dataWriter.write(strList[key]+"\n")
	
	# print("---------------------------------------------------------")
	dataWriter.close()

def make_dir(path):
	try: 
	    os.makedirs(path)
	except OSError:
	    if not os.path.isdir(path):
	        raise

if __name__ == '__main__':

	try:
		graphName = str(sys.argv[1])
	except:
		print("Pass graph name as command line argument. There must be folder with the same name containing .csv files")
	else:
		DB 	= pd.read_csv( graphName+"/DB.csv" , sep = "," )
		ADJ = pd.read_csv( graphName+"/ADJ.csv", sep = "," )
		DP 	= pd.read_csv( graphName+"/DP.csv" , sep = "," )
		SB 	= pd.read_csv( graphName+"/SB.csv" , sep = "," )
		num_tests = 10 # Number of test files created for each real-world graph in order to estimate its fp ratio

		# Get correct false positive values for each BF, originally the values are just true positives. It is converted into false
		# positives by subtracting true positve rate from it

		DB = correct_data(ADJ, DB, num_tests);
		SB = correct_data(ADJ, SB, num_tests);
		DP = correct_data(ADJ, DP, num_tests);

		plot([SB,DB,DP, ADJ], num_tests, ["Static Bloom","Dynamic Bloom","Dynamic Partition Bloom","Adjacency List"], graphName)
