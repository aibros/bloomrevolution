#!/usr/bin/python

import os, sys, glob

def runTests(graphFilename,c,numTestFiles,BF_Sizes):
	# BF_Sizes = [256,512,1024,2048,4096,8192]
	# BF_Sizes = [512,1024]

	testFileList = []
	# Create test files containing sampled edges
	os.system("mkdir testFiles")
	for ctr in range(numTestFiles):
		command = "./edgeSampler.py " + graphFilename + " " + str(c) + " testFiles/file_" + str(ctr)
		testFileList += ["../testFiles/file_" + str(ctr)]
		# print os.getcwd()
		# print command + "\n"
		os.system(command)

	print "Created all test files for graph\t", graphFilename
	print os.getcwd()
	# Make all executbles freshly
	os.chdir("../count_bloom")
	os.system("make clean > tempFile && make > tempFile")
	os.system("rm tempFile")

	os.chdir("../dynamic_bloom")
	os.system("make clean > tempFile && make > tempFile")
	os.system("rm tempFile")

	os.chdir("../DP_bloom")
	os.system("make clean > tempFile && make > tempFile")
	os.system("rm tempFile")

	os.chdir("../bloom_graph/bloom_graph_SB")
	os.system("make clean > tempFile && make > tempFile")
	os.system("rm tempFile")

	os.chdir("../bloom_graph_DB")
	os.system("make clean > tempFile && make > tempFile")
	os.system("rm tempFile")

	os.chdir("../bloom_graph_DP")
	os.system("make clean > tempFile && make > tempFile")
	os.system("rm tempFile")

	os.chdir("../adj_graph")
	os.system("make clean > tempFile && make > tempFile")
	os.system("rm tempFile")	

	print "Created all executables freshly files for graph"

	# Run memQuery test on all
	for size in BF_Sizes:
		command = "./bloom_graph " + str(size) + " ../" + graphFilename + " " + str(len(testFileList)) 
		
		print "------------------------------Running test for BF Size",size,"-----------------------------------\n"
		for filename in testFileList:
			command += " " + filename

		# Static BF
		print "\nRunning test for Static BF"
		os.chdir("../bloom_graph_SB")
		# print os.getcwd()
		# print command + "\n"
		os.system(command)

		# Dynamic BF
		print "\nRunning test for  DBF "
		os.chdir("../bloom_graph_DB")
		# print os.getcwd()
		# print command + "\n"
		os.system(command)

		# Dynamic Partition BF
		print "\nRunning test for DPBF"
		os.chdir("../bloom_graph_DP")
		# print os.getcwd()
		# print command + "\n"
		os.system(command)
		print "------------------------------Test for BF Size ",size," done--------------------------------------\n"


	# AdjList Graph
	adjCommand = "./adj_graph ../" + graphFilename + " " + str(len(testFileList)) 
	print "\nRunning test on Adjacency list representation",size
	for filename in testFileList:
		adjCommand += " " + filename

	os.chdir("../adj_graph")
	# print os.getcwd()
	# print adjCommand + "\n"
	os.system(adjCommand)

	# Delete test files containing sampled edges
	os.chdir("..")
	# print os.getcwd()
	# print "rm testFiles -R\n"
	os.system("rm testFiles -R")

	# Remove all executables
	os.chdir("../count_bloom")
	os.system("make clean > tempFile ")
	os.system("rm tempFile")

	os.chdir("../dynamic_bloom")
	os.system("make clean > tempFile ")
	os.system("rm tempFile")

	os.chdir("../DP_bloom")
	os.system("make clean > tempFile ")
	os.system("rm tempFile")

	os.chdir("../bloom_graph/bloom_graph_SB")
	os.system("make clean > tempFile ")
	os.system("rm tempFile")

	os.chdir("../bloom_graph_DB")
	os.system("make clean > tempFile ")
	os.system("rm tempFile")

	os.chdir("../bloom_graph_DP")
	os.system("make clean > tempFile ")
	os.system("rm tempFile")

	os.chdir("../adj_graph")
	os.system("make clean > tempFile ")
	os.system("rm tempFile")

	os.chdir("..")

def addMetaData(graphFilename):
	
	reader 	= open(graphFilename,"r");
	writer	= open(graphFilename+"_New","w")

	
	numEdges = 0
	numNodes = 0;
	nodes 	= {}
	maxNodeId = 1;
	for line in reader:
		
		if line.startswith("%"):
			continue;

		numEdges += 1;
		lineV = line.strip().split();
		node1 = int(lineV[0])
		node2 = int(lineV[1])
		nodes[node1] = 1	
		nodes[node2] = 1
		if maxNodeId <= node1:
			maxNodeId  = node1

		if maxNodeId <= node2:
			maxNodeId  = node2

	reader.close();
	numNodes = len(nodes.keys())
	# print "numNodes",numNodes
	# print "maxNodeId", maxNodeId
	# print "numEdges",numEdges

	writer.write(str(numEdges) + " " + str(numNodes+1) + "\n")
	reader 	= open(graphFilename,"r");

	ctr = 0;
	for line in reader:
		
		if line.startswith("%"):
			continue;
		
		ctr += 1
		if ctr  == numEdges:
			writer.write(line.strip())
		else:
			writer.write(line)

	return 2*numEdges*1.0/numNodes

if __name__ == '__main__':

	try:
		numGraphs 		= int(sys.argv[1])
		graphFiles 		= []
		for x in xrange(numGraphs):
			graphFiles += [str(sys.argv[2+x])]
		
		c 				= float(sys.argv[2+numGraphs])
		numTestFiles 	= int(sys.argv[3+numGraphs])
	except Exception, e:
		print "Incorrect usage"
		print "./runBloomGraphTests <numGraphFiles> <graphFilename_1> ... <graphFilename_n> <c> <numTestFiles>"
		print "where each testfile will contain c*|E| edges,|E| = Number of edges in given graph"
		raise e

	try:

		ctr = 0;
		for graphFilename in graphFiles:

			avgDegree = addMetaData(graphFilename)
			print "avgDegree",avgDegree
			bloomSize_1 = int(.4*avgDegree)*32
			bloomSize_2 = int(.6*avgDegree)*32
			bloomSize_3 = int(.8*avgDegree)*32
			tempD = {}
			tempD[bloomSize_1] =1
			tempD[bloomSize_2] =1
			tempD[bloomSize_3] =1
			bloomSizes = tempD.keys()
			runTests(graphFilename+"_New",c,numTestFiles,bloomSizes)
			ctr += 1
			os.system("rm " + graphFilename + "_New")
			# Put results in a separate folder
			os.chdir("Results");
			os.system("mkdir Graph_"+str(ctr));
			fileList =  glob.glob("*.csv");
			for f in fileList:
				command = "mv " + f + " Graph_" + str(ctr) + "/" + f;
				# print command
				os.system(command)
			os.chdir("..");

	except Exception, e:
		print "Error in runTests function"
		raise e
		