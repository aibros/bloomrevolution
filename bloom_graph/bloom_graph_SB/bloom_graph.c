#include "bloom_graph.h"



bloom_graph* createBloomGraphFromFile(char* filename)
{

	// Initialise graph data structure
	FILE *fp = fopen(filename,"r");
	UINT num_nodes,num_edges;
	fscanf(fp,"%u %u\n",&num_edges,&num_nodes);
	
	bloom_graph* graph 	= (bloom_graph*)malloc(sizeof(bloom_graph));	
	graph->node_list 	= (bloom_node*)malloc(num_nodes*sizeof(bloom_node));
	graph->graph_size 	= num_nodes;
	graph->graph_volume = num_edges;
	for (int i = 1; i < num_nodes; ++i)
	{
		graph->node_list[i].num_ngbrs 	= 0;
		graph->node_list[i].ngbrs = (bloom*)malloc(sizeof(bloom));
		init(graph->node_list[i].ngbrs);
	}
	printf("createBloomGraph()::Creating graph from file::%s\n",filename );
	printf("createBloomGraph()::Initialised graph data structure and now going to create graph\n");
	
	// Read graph from file and create a graph
	UINT node1,node2;
	int maxNodeId = 0;
	while(!feof(fp))
	{		
		fscanf(fp,"%u %u\n",&node1,&node2);
		graph->node_list[node1].num_ngbrs 	+= 1;
		insert(node2,graph->node_list[node1].ngbrs);
		if(node1 > maxNodeId)
		{
			maxNodeId =  node1;
		}

		if(node1 != node2) // To avoid self-loops to be added twice
		{
			graph->node_list[node2].num_ngbrs 	+= 1;
			insert(node1,graph->node_list[node2].ngbrs);
			if(node2 > maxNodeId)
			{
				maxNodeId =  node2;
			}
		}
	}
	
	if(maxNodeId != num_nodes - 1)
	{
		printf("numNodes::%d\tnumEdges::%d\n",num_nodes,num_edges );
		printf("numNodes should be exactly 1 more than maxNodeId\n");
		printf("maxNodeId::%d\n",maxNodeId);
	}
	int totalNgbrs = 0;
	for (int i = 1; i < num_nodes; ++i)
	{
		totalNgbrs += graph->node_list[i].num_ngbrs;
	}
	printf("createBloomGraph()::Successfully created graph\n");
	// printf("createBloomGraph()::Size of graph::%d\n",totalNgbrs );
	// long int totalMemory = bloomParam->SIZE_BLOOM*num_nodes/8;
	// totalMemory /= 1024;
	// printf("createBloomGraph()::Total memory(in KB)\t\t\t\t::%ld\n",totalMemory);

	return graph;
}

bloom_graph* createBloomGraph(graph_node* stdGraph,char* filename)
{
	// Initialise graph data structure
	FILE *fp = fopen(filename,"r");
	UINT num_nodes,num_edges;
	fscanf(fp,"%u %u\n",&num_edges,&num_nodes);

	bloom_graph* graph 		= (bloom_graph*)malloc(sizeof(bloom_graph));	
	graph->node_list 		= (bloom_node*)malloc(num_nodes*sizeof(bloom_node));
	graph->graph_size 		= num_nodes;
	graph->graph_volume 	= num_edges;
	fclose(fp);

	for (int i = 1; i < num_nodes; ++i)
	{
		graph->node_list[i].num_ngbrs 	= 0;
		graph->node_list[i].ngbrs 		= (bloom*)malloc(sizeof(bloom));
		init(graph->node_list[i].ngbrs);

		for (int j = 0; j < stdGraph[i].size; ++j)
		{
			int node = stdGraph[i].ngbrs->elements[j];
			insert(node,graph->node_list[i].ngbrs);
			graph->node_list[i].num_ngbrs++;
		}
		arrayInt_free(stdGraph[i].ngbrs);
		free(stdGraph[i].ngbrs);
	}
	free(stdGraph);
	printf("createBloomGraph()::Successfully created graph\n");
	return graph;
}

void initParametersForGraph(char* filename,UINT SIZE_BLOOM )
{
	FILE * reader;
	reader = fopen(filename,"r");
	int numEdges, numNodes;
	fscanf(reader,"%d %d\n",&numEdges,&numNodes);
	fclose(reader);
	// UINT SIZE_BLOOM 	= 8192*2;
	UINT K 				= 3;
	UINT COUNTER_SIZE 	= 1;
	int M 				= numNodes;

	// printf("SIZE_BLOOM::%u\tK::%u\tCOUNTER_SIZE::%u\n",SIZE_BLOOM,K,COUNTER_SIZE);
	init_bloomParameters(SIZE_BLOOM,K,COUNTER_SIZE);
}

void performMembershipTest(bloom_graph* graph)
{
	long int totalFalsePositives = 0;
	long int numFalsePositives = 0, maxFalsePositives = 0;
	for (int node1 = 1; node1 < graph->graph_size; ++node1)
	{
		long int numBloomNgbrs = 0;
		for (int node2 = 1; node2 < graph->graph_size ; ++node2)
		{
			if (is_in(node2,graph->node_list[node1].ngbrs))
			{
				numBloomNgbrs++;
			}
		}
		numFalsePositives = numBloomNgbrs - graph->node_list[node1].num_ngbrs;
		totalFalsePositives += numFalsePositives;
		if( numFalsePositives > maxFalsePositives)
			maxFalsePositives = numFalsePositives;
		// printf("Node::%d::%5ld\t- %5d\t = %5ld\n", node1, numBloomNgbrs, graph->node_list[node1].num_ngbrs, numBloomNgbrs - graph->node_list[node1].num_ngbrs);
	}
	printf("totalFalsePositives::%ld\n",totalFalsePositives);
	printf("AvgFalsePositives::%ld\n",totalFalsePositives/graph->graph_size);
	printf("maxFalsePositives::%ld\n",maxFalsePositives);
}

// It tries to estimate false positive probability by using testing bloomGraph for some randomly sampled edges
void performMembershipTest_Sampled(bloom_graph* graph, char* sampleFilename,FILE* logger)
{
	FILE * reader;
	reader = fopen(sampleFilename,"r");
	int src, dest;
	float numPositives = 0, numSampledEdges = 0;
	struct timeval start,end;

	gettimeofday(&start,NULL);
	while(!feof(reader))
	{
		fscanf(reader,"%d\t%d\n",&src,&dest);	
		if (is_in(dest,graph->node_list[src].ngbrs))
		{
			numPositives++;
		}
		numSampledEdges++;
	}
	gettimeofday(&end,NULL);
	fclose(reader);

	double totalMemory = bloomParam->SIZE_BLOOM*graph->graph_size/8;
	totalMemory /= 1024;
	totalMemory /= 1024;
	float fp_ratio = numPositives/numSampledEdges;
	fprintf(logger, "%s,%ld,%ld,%d,%f,%.0f,%f,%f\n",sampleFilename,graph->graph_size,graph->graph_volume,bloomParam->SIZE_BLOOM,totalMemory,numSampledEdges,fp_ratio,get_time_diff(start,end));
}

static inline float get_time_diff(struct timeval t1, struct timeval t2)
{
	return t2.tv_sec - t1.tv_sec + 1e-6 * (t2.tv_usec - t1.tv_usec);
}

int main(int argc, char const *argv[])
{
	struct timeval start,end;
	char filename[1000];

	if(argc >= 4)
	{
		int SIZE_BLOOM = atoi(argv[1]);
		if(SIZE_BLOOM % (8*sizeof(int)) != 0)
		{
			printf("SIZE_BLOOM should be a multiple of %ld\n", 8*sizeof(int));
			exit(0);
		}
		char* graphFilename = malloc(1000*sizeof(char));
		sprintf(graphFilename,"%s",argv[2]);
		initParametersForGraph(graphFilename,SIZE_BLOOM);

		graph_node* stdGraph = init_stdGraph(graphFilename);
		printf("Gotten adjacency list for graph\n");

		gettimeofday(&start,NULL);
		bloom_graph* graph  = createBloomGraph(stdGraph,graphFilename);
		// bloom_graph* graph  = createBloomGraphFromFile(graphFilename);
		gettimeofday(&end,NULL);
		printf("Created Graph in time::%f\n",get_time_diff(start,end));

		FILE* logger;
		if( access( "../Results/SB.csv", F_OK ) != -1 ) 
		{
    		// file exists
    		logger = fopen("../Results/SB.csv","a");
		} 
		else 
		{
    		// file doesn't exist
    		logger = fopen("../Results/SB.csv","w");
    		fprintf(logger, "graphFilename,testFilename,numNodes,numEdges,SIZE_BLOOM,MemUsage(inMB),totalSampledEdges,fp_ratio,timeTaken\n");
		}
		int numTestFiles 	= atoi(argv[3]);
		char* testFilename;
		for (int i = 0; i < numTestFiles; ++i)
		{

			testFilename = malloc(1000*sizeof(char));
			sprintf(testFilename,"%s",argv[4+i]);
			
			fprintf(logger, "%s,",graphFilename );
			gettimeofday(&start,NULL);
			performMembershipTest_Sampled(graph,testFilename,logger);
			gettimeofday(&end,NULL);
			
			printf("Membership tests for file %s done in time::%f\n",testFilename,get_time_diff(start,end));
			free(testFilename);
		}
	}
	else
	{
		printf("Incorrect usage:\n");
		printf("Usage: ./bloom_graph <SIZE OF BloomFilter> <graph filename> <numSampledEdgeFiles> <filename_1> ... <filename_n\n");
	}
	// sprintf(filename,"../../../Data/orkut-links/out.orkut-links");
	// sprintf(filename,"../../../Data/petster-friendships-cat/out.petster-friendships-cat-uniq");
	// sprintf(filename,"../../../Data/reactome/out.reactome");
}