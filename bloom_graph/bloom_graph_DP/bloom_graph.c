#include "bloom_graph.h"

double totalMemory_gbl = 1; // Stores Overall memUsage in Bytes

bloom_graph* createBloomGraphFromFile(char* filename)
{
	// Initialise graph data structure
	struct timeval start,end;
	FILE *fp = fopen(filename,"r");
	UINT num_nodes,num_edges;
	fscanf(fp,"%u %u\n",&num_edges,&num_nodes);

	bloom_graph* graph 	= (bloom_graph*)malloc(sizeof(bloom_graph));	
	graph->node_list 	= (bloom_node*)malloc(num_nodes*sizeof(bloom_node));
	graph->graph_size 	= num_nodes;
	graph->graph_volume = num_edges;

	gettimeofday(&start,NULL);	
	for (int i = 1; i < num_nodes; ++i)
	{
		graph->node_list[i].num_ngbrs 	= 0;
		graph->node_list[i].ngbrs = (b_node*)malloc(sizeof(b_node));
		// init(graph->node_list[i].ngbrs);
		init_without_bloom(graph->node_list[i].ngbrs);
	}
	printf("createBloomGraph()::Creating graph from file::%s\n",filename );
	printf("createBloomGraph()::Initialised graph data structure and now going to create graph\n");
	
	for (int i = 1; i < num_nodes; ++i)
	{
		grow_full_tree(graph->node_list[i].ngbrs,0);
	}
	gettimeofday(&end,NULL);	
	printf("Grown all bloomFilters to fullest in time::%f\n",get_time_diff(start,end));

	gettimeofday(&start,NULL);
	// Read graph from file and create a graph
	UINT node1,node2;
	int maxNodeId = 0;
	while(!feof(fp))
	{		
		fscanf(fp,"%u %u\n",&node1,&node2);
		graph->node_list[node1].num_ngbrs 	+= 1;
		insert_new(node2,graph->node_list[node1].ngbrs,0);
		// printf("Inserting (%d,%d)\n",node1,node2 );
		if(node1 > maxNodeId)
		{
			maxNodeId =  node1;
		}

		if(node1 != node2) // To avoid self-loops to be added twice, we must check if node1 and node2 are different
		{
			graph->node_list[node2].num_ngbrs 	+= 1;
			insert_new(node1,graph->node_list[node2].ngbrs,0);
			// printf("Inserting (%d,%d)\n",node2,node1);
			if(node2 > maxNodeId)
			{
				maxNodeId =  node2;
			}
		}
	}
	gettimeofday(&end,NULL);	
	printf("maxNodeId::%d\n. Read graph in time::%f\n",maxNodeId,get_time_diff(start,end));
	int totalNumLeaves = 0;

	gettimeofday(&start,NULL);
	for (int i = 1; i < num_nodes; ++i)
	{
		finish_insertion(graph->node_list[i].ngbrs);
		int numLeaves  = 0;// count_leaves(graph->node_list[i].ngbrs);
		totalNumLeaves += numLeaves;
		// printf("Done with vertex::%d\n", i);
	}
	gettimeofday(&end,NULL);	

	printf("createBloomGraph()::Successfully created graph in time::%f\n",get_time_diff(start,end));
	printf("createBloomGraph()::Size of bloom_graph(numLeaves)::%d\n",totalNumLeaves );
	return graph;
}

// Uses a standard adjacency list based graph to get the bloom graph
bloom_graph* createBloomGraph(graph_node* stdGraph,char* filename)
{
	// Initialise graph data structure
	FILE *fp = fopen(filename,"r");
	long int num_nodes,num_edges;
	fscanf(fp,"%ld %ld\n",&num_edges,&num_nodes);

	bloom_graph* graph 		= (bloom_graph*)malloc(sizeof(bloom_graph));	
	graph->node_list 		= (bloom_node*)malloc(num_nodes*sizeof(bloom_node));
	graph->graph_size 		= num_nodes;
	graph->graph_volume 	= num_edges;
	fclose(fp);

	long int totalNumLeaves = 0;
	long int totalNumNgbrs = 0;
	long int numNodesToFree = 0;
	for (int i = 1; i < num_nodes; ++i)
	{
		graph->node_list[i].num_ngbrs 	= 0;
		graph->node_list[i].ngbrs 		= (b_node*)malloc(sizeof(b_node));
		init_without_bloom(graph->node_list[i].ngbrs);

		int estimatedLevel = (int)((stdGraph[i].size)/bloomParam->FILL_THRESHOLD); 
		// estimatedLevel *= estimatedLevel;
		if (estimatedLevel > bloomParam->NUM_PARTITION)
		{
			estimatedLevel = bloomParam->NUM_PARTITION;
		}

		// printf("Growing for %d upto level %d as it has %d elements\n",i,estimatedLevel,stdGraph[i].size);
		grow_full_tree_upto_level(graph->node_list[i].ngbrs,0,estimatedLevel);
		for (int j = 0; j < stdGraph[i].size; ++j)
		{
			int node = stdGraph[i].ngbrs->elements[j];
			insert_at_level(node,graph->node_list[i].ngbrs,0,estimatedLevel);
			graph->node_list[i].num_ngbrs++;
		}
		finish_insertion(graph->node_list[i].ngbrs);
		int numLeaves  = count_leaves(graph->node_list[i].ngbrs);
		numNodesToFree += pow(2,estimatedLevel) - numLeaves;
		totalNumLeaves += numLeaves;
		totalNumNgbrs  += stdGraph[i].size; 
		// printf("Node::%d\tNgbrs::%d\tTotalNgbrs::%d\tnumLeaves::%d\tTotalLeaves::%d\n",i,stdGraph[i].size,totalNumNgbrs,numLeaves,totalNumLeaves );
		arrayInt_free(stdGraph[i].ngbrs);
		free(stdGraph[i].ngbrs);
	}
	free(stdGraph);
	printf("createBloomGraph()::Successfully created graph\n");
	// printf("createBloomGraph()::Size of bloom_graph(numLeaves)::%d\n",totalNumLeaves );
	// printf("createBloomGraph()::Number of nodes to free::%d\n",numNodesToFree );
	long int totalMemory = num_nodes*sizeof(bloom_node) +  totalNumLeaves*bloomParam->SIZE_BLOOM/8 + (totalNumLeaves - num_nodes)*12 + totalNumLeaves*20;
	totalMemory_gbl = totalMemory;
	long int BFMemory 	= totalNumLeaves*bloomParam->SIZE_BLOOM/8;
	long int adjListMemory = (num_edges*2 + num_nodes*3)*4 + num_nodes*sizeof(struct graph_node);; 
	totalMemory /= 1024;
	BFMemory /= 1024;
	adjListMemory /= 1024;
	// printf("createBloomGraph()::BF memory(in KB)\t\t\t\t::%ld\n",BFMemory);
	// printf("createBloomGraph()::Total memory(in KB)\t\t\t\t::%ld\n",totalMemory);
	// printf("createBloomGraph()::Memory overhead for tree(in KB)\t\t::%ld\n",totalMemory - BFMemory);
	// printf("createBloomGraph()::Memory overhead for tree(in percent)\t::%f\n",(totalMemory - BFMemory)*100.0/totalMemory);
	// printf("createBloomGraph()::AdjList memory\t\t\t\t::%ld\n",adjListMemory);
	// printf("createBloomGraph()::Memory Gain \t\t\t\t::%ld\n",adjListMemory - BFMemory);
	// printf("createBloomGraph()::Memory Gain(with overhead)\t\t\t::%ld\n",adjListMemory - totalMemory);
	// printf("createBloomGraph()::Memory Gain(in percent)\t\t\t::%f\n",(adjListMemory - BFMemory)*1.0/adjListMemory);
	// printf("createBloomGraph()::Memory Gain(with overhead)(in prcnt)\t::%f\n",(adjListMemory - totalMemory)*1.0/adjListMemory);
	return graph;
}

void initParametersForGraph(char* filename, UINT SIZE_BLOOM)
{
	FILE * reader;
	reader = fopen(filename,"r");
	int numEdges, numNodes;
	fscanf(reader,"%d %d\n",&numEdges,&numNodes);
	// printf("Number of edges::%d, Number of nodes::%d\n",numEdges,numNodes);

	// UINT SIZE_BLOOM 	= 1024;
	UINT K 				= 3;
	UINT COUNTER_SIZE 	= 1;
	int M 				= numNodes;
	float FALSE_PSTV_PROB = 0.01;

	UINT FILL_THRESHOLD  = -1*((int)(SIZE_BLOOM)/((float)K*COUNTER_SIZE))*log( 1 - pow(FALSE_PSTV_PROB,1.0/K));
	UINT NUM_PARTITION   = (UINT)(log2(ceil(M*1.0/FILL_THRESHOLD)));

	printf("SIZE_BLOOM::%u\tK::%u\tCOUNTER_SIZE::%u\t",SIZE_BLOOM,K,COUNTER_SIZE);
	printf("FALSE_PSTV_PROB::%f\t",FALSE_PSTV_PROB );
	printf("FILL_THRESHOLD::%u\t",FILL_THRESHOLD );
	printf("NUM_PARTITION::%u\n",NUM_PARTITION );
	init_bloomParameters(SIZE_BLOOM,K,COUNTER_SIZE,NUM_PARTITION,FALSE_PSTV_PROB);
}

void performMembershipTest(bloom_graph* graph)
{
	long int totalFalsePositives 	= 0;
	long int maxFalsePositives 		= 0;
	long int numFalsePositives 		= 0;
	for (int node1 = 1; node1 < graph->graph_size; ++node1)
	{
		long int numBloomNgbrs = 0;
		for (int node2 = 1; node2 < graph->graph_size ; ++node2)
		{
			if (is_in(node2,graph->node_list[node1].ngbrs))
			{
				numBloomNgbrs++;
			}
		}
		numFalsePositives = numBloomNgbrs - graph->node_list[node1].num_ngbrs;
		totalFalsePositives += numFalsePositives;
		if( numFalsePositives > maxFalsePositives)
			maxFalsePositives = numFalsePositives;
		// printf("Node::%d::%5ld\t- %5d\t = %5ld\n", node1, numBloomNgbrs, graph->node_list[node1].num_ngbrs, numFalsePositives);
	}
	printf("totalFalsePositives::%ld\n",totalFalsePositives);
	printf("AvgFalsePositives  ::%f\n",totalFalsePositives*1.0/graph->graph_size);
	printf("MaxFalsePositives  ::%ld\n",maxFalsePositives);
}

// It tries to estimate false positive probability by using testing bloomGraph for some randomly sampled edges
void performMembershipTest_Sampled(bloom_graph* graph, char* sampleFilename,FILE* logger)
{
	FILE * reader;
	reader = fopen(sampleFilename,"r");
	int src, dest;
	float numPositives = 0, numSampledEdges = 0;
	struct timeval start,end;

	gettimeofday(&start,NULL);
	while(!feof(reader))
	{
		fscanf(reader,"%d\t%d\n",&src,&dest);	
		if (is_in(dest,graph->node_list[src].ngbrs))
		{
			numPositives++;
		}
		numSampledEdges++;
	}
	gettimeofday(&end,NULL);
	fclose(reader);

	double totalMemory_temp = totalMemory_gbl;
	totalMemory_temp /= 1024; // Convert into KB
	totalMemory_temp /= 1024; // Convert into MB
	float fp_ratio = numPositives/numSampledEdges;
	fprintf(logger, "%s,%ld,%ld,%d,%f,%.0f,%f,%f\n",sampleFilename,graph->graph_size,graph->graph_volume,bloomParam->SIZE_BLOOM,totalMemory_temp,numSampledEdges,fp_ratio,get_time_diff(start,end));
}

void compareGraphs(bloom_graph* graph1,bloom_graph* graph2)
{
	long int totalFalsePositives = 0,one =1,zero= 0;
	printf("Size::Graph1::%ld\tGraph2::%ld\n",graph1->graph_size,graph2->graph_size);
	for (int node1 = 1; node1 < graph1->graph_size; ++node1)
	{
		int ctr1 = 0,ctr2 = 0;
		for (int node2 = 1; node2 < graph1->graph_size ; ++node2)
		{
			int is_in_1 = is_in(node2,graph1->node_list[node1].ngbrs);
			int is_in_2 = is_in(node2,graph2->node_list[node1].ngbrs);


			if ( (is_in_1 == 1)   && (is_in_2 == 1) )
			{
				// printf("Node2::%d graph1::%d and graph2::%d\n",node1,1,1 );
				ctr1++;
				ctr2++;	
			}
			else if ( (is_in_1 == 0)   && (is_in_2 == 1) )
			{
				printf("Node2::%d graph1::%d and graph2::%d\n",node1,0,1 );
				// ctr1++;
				ctr2++;
			}
			else if ( (is_in_1 == 1)   && (is_in_2 == 0) )
			{
				printf("Node2::%d graph1::%d and graph2::%d\n",node1,1,0 );
				ctr1++;
				// ctr2++;
			}
			else if ( (is_in_1 == 0)   && (is_in_2 == 0) )
			{
				// printf("Node2::%d graph1::%d and graph2::%d\n",node1,0,0 );
				// ctr1++;
				// ctr2++;
			}
		}
		printf("Node::%d\tctr1::%d\tActual1::%d\tctr2::%d\tActual2::%d\n",node1,ctr1,graph1->node_list[node1].num_ngbrs,ctr2,graph2->node_list[node1].num_ngbrs);
		// totalFalsePositives += numBloomNgbrs - graph1->node_list[node1].num_ngbrs;
		// printf("Node::%d::%5ld\t- %5d\t = %5ld\n", node1, numBloomNgbrs, graph->node_list[node1].num_ngbrs, numBloomNgbrs - graph->node_list[node1].num_ngbrs);
	}
	// printf("totalFalsePositives::%ld\n",totalFalsePositives);
	// printf("AvgFalsePositives::%ld\n",totalFalsePositives/graph->graph_size);
}

static inline float get_time_diff(struct timeval t1, struct timeval t2)
{
	return t2.tv_sec - t1.tv_sec + 1e-6 * (t2.tv_usec - t1.tv_usec);
}

void debug()
{
	char filename[1000];
	sprintf(filename,"../../../Data/reactome/out.reactome");
	// // sprintf(filename,"../../../Data/orkut-links/out.orkut-links");
	initParametersForGraph(filename,256);
	int ctr = 0;
	int numElems =  171;
	// int* elems;
	// elems =(int*)malloc(numElems*sizeof(int));
	// FILE* reader = fopen("tempList.csv","r");
	// while(!feof(reader))
	// {
	// 	int temp;
	// 	fscanf(reader,"%d",&temp);
	// 	elems[ctr] = temp;
	// 	ctr++;
	// }
	// printf("Ctr::%d\n",ctr );
	b_node** tempNodeArray = (b_node**)malloc(5000*sizeof(b_node*));
	ctr = 0;
	while(ctr < 1000)
	{
		ctr++;
		int elems[] = {12,103,689,710,2368,71,65,1994,706,3116,3121,614,42,3124,290,120,18,50,1896,19,54,1995,3915,673,686,1903,2151,728,724,738,4801,57,97,696,604,620,72,644,4804,75,14,16,1891,603,1892,21,22,695,29,30,31,3118,38,37,40,45,47,48,52,51,58,1992,60,64,1904,68,69,73,397,77,76,82,84,88,90,1895,91,93,92,96,100,104,105,2803,106,1898,111,112,731,3122,116,1900,117,121,1901,1997,123,679,125,127,126,70,646,5104,1902,17,56,3644,712,107,5286,5297,114,5353,33,35,1993,5450,1996,115,119,619,5218,62,1897,3117,3120,83,3123,5105,1890,55,735,3916,610,1899,4130,2661,675,85,122,63,46,1991,99,3668,611,661,677,79,5217,3646,1990,2379,23,94,723,2889,20,67,701,3119,13,6269,615,627,113,4802,2380,95,101};

		b_node* tempNode;
		tempNode = (b_node*)malloc(sizeof(b_node));
		init_without_bloom(tempNode);
		printf("fraction::%f\n", numElems/bloomParam->FILL_THRESHOLD);
		int estimatedLevel = (int)(numElems/bloomParam->FILL_THRESHOLD); 
		estimatedLevel *= estimatedLevel;
		if (estimatedLevel > bloomParam->NUM_PARTITION)
		{
			estimatedLevel = bloomParam->NUM_PARTITION;
		}
		printf("Growing upto level %d as no. of elements is %d\n",estimatedLevel,numElems);
		grow_full_tree_upto_level(tempNode,0,estimatedLevel);
		for (int j = 0; j < numElems; ++j)
		{
			insert_at_level(elems[j],tempNode,0,estimatedLevel);
		}
		printf("numLeaves(before compressing)::%d\n",count_leaves(tempNode));
		finish_insertion(tempNode);
		int numLeaves  = count_leaves(tempNode);
		int numNodesToFree = pow(2,estimatedLevel) - numLeaves;
		printf("numLeaves::%d\n",numLeaves);
		printf("numNodesToFree::%d\n",numNodesToFree);
		tempNodeArray[ctr-1] = tempNode;
	}
	ctr = 0;
	printf("Freeing memory\n");
	while(ctr < 1000)
	{
		free_bloom(tempNodeArray[ctr]);
		free(tempNodeArray[ctr]);
		ctr++;
		printf("Freeing::%d\n",ctr );
	}
	free(tempNodeArray);
	int choice = 1;
	while(choice)
	{	
		scanf("%d",&choice);
		int tCtr = 0;
		// while(tCtr < 5000)
		// {
		// 	int* d= (int*)malloc(500000*sizeof(int));
		// 	free(d);
		// 	tCtr++;	
		// }
		
	}

	// exit(0);

}


int main(int argc, char const *argv[])
{
	struct timeval start,end;

	if(argc >= 4)
	{
		int SIZE_BLOOM = atoi(argv[1]);
		if(SIZE_BLOOM % (8*sizeof(int)) != 0)
		{
			printf("SIZE_BLOOM should be a multiple of %ld\n", 8*sizeof(int));
			exit(0);
		}
		char* graphFilename = malloc(1000*sizeof(char));
		sprintf(graphFilename,"%s",argv[2]);
		initParametersForGraph(graphFilename,SIZE_BLOOM);
		graph_node* stdGraph = init_stdGraph(graphFilename);
		printf("Gotten adjacency list for graph\n");

		gettimeofday(&start,NULL);
		bloom_graph* graph  = createBloomGraph(stdGraph,graphFilename);
		gettimeofday(&end,NULL);
		printf("Created Graph in time::%f\n",get_time_diff(start,end));

		FILE* logger;
		if( access( "../Results/DP.csv", F_OK ) != -1 ) 
		{
    		// file exists
    		logger = fopen("../Results/DP.csv","a");
		} 
		else 
		{
    		// file doesn't exist
    		logger = fopen("../Results/DP.csv","w");
    		fprintf(logger, "graphFilename,testFilename,numNodes,numEdges,SIZE_BLOOM,MemUsage(inMB),totalSampledEdges,fp_ratio,timeTaken\n");
		}
		
		int numTestFiles 	= atoi(argv[3]);
		char* testFilename;
		for (int i = 0; i < numTestFiles; ++i)
		{
			testFilename = malloc(1000*sizeof(char));
			sprintf(testFilename,"%s",argv[4+i]);

			fprintf(logger, "%s,",graphFilename );
			gettimeofday(&start,NULL);
			performMembershipTest_Sampled(graph,testFilename,logger);
			gettimeofday(&end,NULL);
			
			printf("Membership tests for file %s done in time::%f\n",testFilename,get_time_diff(start,end));
			free(testFilename);
		}
	}
	else
	{
		printf("Incorrect usage:\n");
		printf("Usage: ./bloom_graph <SIZE OF BloomFilter> <graph filename> <numSampledEdgeFiles> <filename_1> ... <filename_n\n");
	}
	// sprintf(filename,"../../../Data/orkut-links/out.orkut-links");
	// sprintf(filename,"../../../Data/petster-friendships-cat/out.petster-friendships-cat-uniq");
	// sprintf(filename,"../../../Data/reactome/out.reactome");
}