import sys

def addMetaData(graphFilename):
	
	reader 	= open(graphFilename,"r");
	writer	= open(graphFilename+"_New","w")

	
	numEdges = 0
	numNodes = 0;
	nodes 	= {}
	maxNodeId = 1;
	for line in reader:
		
		if line.startswith("%"):
			continue;

		numEdges += 1;
		lineV = line.strip().split();
		node1 = int(lineV[0])
		node2 = int(lineV[1])
		nodes[node1] = 1	
		nodes[node2] = 1
		if maxNodeId <= node1:
			maxNodeId  = node1

		if maxNodeId <= node2:
			maxNodeId  = node2

		
	reader.close();
	numNodes = len(nodes.keys())
	
	# print "numNodes",numNodes
	# print "maxNodeId", maxNodeId
	# print "numEdges",numEdges

	writer.write(str(numEdges) + " " + str(numNodes+1) + "\n")
	reader 	= open(graphFilename,"r");

	ctr = 0;
	for line in reader:
		
		if line.startswith("%"):
			continue;
		
		ctr += 1
		if ctr  == numEdges:
			writer.write(line.strip())
		else:
			writer.write(line)

	return 2*numEdges*1.0/numNodes


def createOnlyEdgeFile(graphFilename):

	reader = open(graphFilename,'r')

	edgeList = {}
	nodes 	= {}
	for line in reader:
		if line.startswith("%"):
			continue;

		lineV = line.strip().split();
		node1 = int(lineV[0]);
		node2 = int(lineV[1]);

		nodes[node1] = 1	
		nodes[node2] = 1

		smallerNode = min(node1,node2)
		largerNode 	= max(node1,node2)
		edgeList[str(smallerNode) + " " + str(largerNode)] = 1;

	print("Number of unique edges::",len(edgeList.keys()))

	reader.close();
	writer = open(graphFilename+"_uniq",'w');
	numEdges = len(edgeList.keys());
	numNodes = len(nodes.keys())

	print("numEdges",numEdges)
	print("numNodes",numNodes)
	# writer.write(str(numEdges) + " " + str(numNodes + 1)+"\n");

	ctr = 0;
	for edge in edgeList.keys():
		ctr += 1
		if ctr == numEdges :  
			writer.write(str(edge))
		else:
			writer.write(str(edge) + "\n")



if __name__ == '__main__':
	filename = sys.argv[1];
	# print(addMetaData(filename))
	createOnlyEdgeFile(filename)


