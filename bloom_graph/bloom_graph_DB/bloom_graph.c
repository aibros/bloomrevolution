#include "bloom_graph.h"

double totalMemory_gbl = 1; // Stores Overall memUsage in Bytes

bloom_graph* createBloomGraphFromFile(char* filename)
{

	// Initialise graph data structure
	FILE *fp = fopen(filename,"r");
	UINT num_nodes,num_edges;
	fscanf(fp,"%u %u\n",&num_edges,&num_nodes);

	bloom_graph* graph 	= (bloom_graph*)malloc(sizeof(bloom_graph));	
	graph->node_list 	= (bloom_node*)malloc(num_nodes*sizeof(bloom_node));
	graph->graph_size 	= num_nodes;
	graph->graph_volume = num_edges;
	int i;
	for (i = 1; i < num_nodes; ++i)
	{
		graph->node_list[i].num_ngbrs 	= 0;
		graph->node_list[i].ngbrs = (dyn_bloom*)malloc(sizeof(dyn_bloom));
		dyn_init(graph->node_list[i].ngbrs);
	}
	printf("createBloomGraph()::Initialised graph data structure and now going to create graph\n");
	
	// Read graph from file and create a graph
	UINT node1,node2;
	int maxNodeId = 0;
	while(!feof(fp))
	{		
		fscanf(fp,"%u %u\n",&node1,&node2);
		graph->node_list[node1].num_ngbrs 	+= 1;
		dyn_insert(node2,graph->node_list[node1].ngbrs);
		if(node1 > maxNodeId)
		{
			maxNodeId =  node1;
		}

		if(node1 != node2) // To avoid self-loops to be added twice
		{
			graph->node_list[node2].num_ngbrs 	+= 1;
			dyn_insert(node1,graph->node_list[node2].ngbrs);
			if(node2 > maxNodeId)
			{
				maxNodeId =  node2;
			}
		}
	}
	// printf("maxNodeId::%d\n",maxNodeId);
	int totalBloomFilters = 0;
	for (int node = 1; node < num_nodes; ++node)
	{
		totalBloomFilters += graph->node_list[node].ngbrs->currNum + 1;
	}
	// printf("createBloomGraph()::Successfully created graph\n");
	// printf("createBloomGraph()::Size of bloom_graph(numUnitBFs)::%d\n",totalBloomFilters);
	long int totalMemory = totalBloomFilters*bloomParam->SIZE_BLOOM/8;
	totalMemory_gbl = totalMemory;
	totalMemory /= 1024;
	// printf("createBloomGraph()::Total memory(in KB)\t\t\t\t::%ld\n",totalMemory);
	return graph;
}

// Uses a standard adjacency list based graph to get the bloom graph
bloom_graph* createBloomGraph(graph_node* stdGraph,char* filename)
{
	// Initialise graph data structure
	FILE *fp = fopen(filename,"r");
	long int num_nodes,num_edges;
	fscanf(fp,"%ld %ld\n",&num_edges,&num_nodes);

	bloom_graph* graph 		= (bloom_graph*)malloc(sizeof(bloom_graph));	
	graph->node_list 		= (bloom_node*)malloc(num_nodes*sizeof(bloom_node));
	graph->graph_size 		= num_nodes;
	graph->graph_volume 	= num_edges;
	fclose(fp);
	long int totalBloomFilters = 0;
	for (int i = 1; i < num_nodes; ++i)
	{
		graph->node_list[i].num_ngbrs 	= 0;
		graph->node_list[i].ngbrs 		= (dyn_bloom*)malloc(sizeof(dyn_bloom));
		dyn_init(graph->node_list[i].ngbrs);

		for (int j = 0; j < stdGraph[i].size; ++j)
		{
			int node = stdGraph[i].ngbrs->elements[j];
			dyn_insert(node,graph->node_list[i].ngbrs);
			graph->node_list[i].num_ngbrs++;
		}
		totalBloomFilters += graph->node_list[i].ngbrs->currNum + 1;
		arrayInt_free(stdGraph[i].ngbrs);
		free(stdGraph[i].ngbrs);
	}
	free(stdGraph);
	printf("createBloomGraph()::Successfully created graph\n");
	totalMemory_gbl = num_nodes*sizeof(bloom_node) + totalBloomFilters*bloomParam->SIZE_BLOOM/8;
	
	return graph;
}

void initParametersForGraph(char* filename, UINT SIZE_BLOOM)
{
	FILE * reader;
	reader = fopen(filename,"r");
	int numEdges, numNodes;
	fscanf(reader,"%d %d\n",&numEdges,&numNodes);

	// UINT SIZE_BLOOM 	= 8192*2;
	UINT K 				= 3;
	UINT COUNTER_SIZE 	= 1;
	int M 				= numNodes + 1;
	float FALSE_PSTV_PROB = 0.01;

	printf("SIZE_BLOOM::%u\tK::%u\tCOUNTER_SIZE::%u\t",SIZE_BLOOM,K,COUNTER_SIZE);
	printf("FALSE_PSTV_PROB::%f\t",FALSE_PSTV_PROB );
	init_dyn_parameters(SIZE_BLOOM,K,COUNTER_SIZE,FALSE_PSTV_PROB);
	bloomParam->FILL_THRESHOLD /= 1.5;
	printf("FILL_THRESHOLD::%f\n",bloomParam->FILL_THRESHOLD );
}

void performMembershipTest(bloom_graph* graph)
{
	long int totalFalsePositives 	= 0;
	long int maxFalsePositives 		= 0;
	long int numFalsePositives 		= 0;
	for (int node1 = 1; node1 < graph->graph_size; ++node1)
	{
		long int numBloomNgbrs = 0;
		for (int node2 = 1; node2 < graph->graph_size ; ++node2)
		{
			if (dyn_is_in(node2,graph->node_list[node1].ngbrs))
			{
				numBloomNgbrs++;
			}
		}
		numFalsePositives = numBloomNgbrs - graph->node_list[node1].num_ngbrs;
		totalFalsePositives += numFalsePositives;
		if( numFalsePositives > maxFalsePositives)
			maxFalsePositives = numFalsePositives;
		// printf("Node::%d::%5ld\t- %5d\t = %5ld\n", node1, numBloomNgbrs, graph->node_list[node1].num_ngbrs, numBloomNgbrs - graph->node_list[node1].num_ngbrs);
	}
	printf("totalFalsePositives::%ld\n",totalFalsePositives);
	printf("AvgFalsePositives  ::%f\n",totalFalsePositives*1.0/graph->graph_size);
	printf("MaxFalsePositives  ::%ld\n",maxFalsePositives);
}

// It tries to estimate false positive probability by using testing bloomGraph for some randomly sampled edges
void performMembershipTest_Sampled(bloom_graph* graph, char* sampleFilename,FILE* logger)
{
	FILE * reader;
	reader = fopen(sampleFilename,"r");
	int src, dest;
	float numPositives = 0, numSampledEdges = 0;
	struct timeval start,end;

	gettimeofday(&start,NULL);
	while(!feof(reader))
	{
		fscanf(reader,"%d\t%d\n",&src,&dest);	
		if (dyn_is_in(dest,graph->node_list[src].ngbrs))
		{
			numPositives++;
		}
		numSampledEdges++;
	}
	gettimeofday(&end,NULL);
	fclose(reader);

	double totalMemory_temp = totalMemory_gbl;
	totalMemory_temp /= 1024; // Convert in KB
	totalMemory_temp /= 1024; // Convert in MB
	float fp_ratio = numPositives/numSampledEdges;
	fprintf(logger, "%s,%ld,%ld,%d,%f,%.0f,%f,%f\n",sampleFilename,graph->graph_size,graph->graph_volume,bloomParam->SIZE_BLOOM,totalMemory_temp,numSampledEdges,fp_ratio,get_time_diff(start,end));
}

static inline float get_time_diff(struct timeval t1, struct timeval t2)
{
	return t2.tv_sec - t1.tv_sec + 1e-6 * (t2.tv_usec - t1.tv_usec);
}

int main(int argc, char const *argv[])
{
	struct timeval start,end;
	char filename[1000];

	if(argc >= 4)
	{
		int SIZE_BLOOM = atoi(argv[1]);
		if(SIZE_BLOOM % (8*sizeof(int)) != 0)
		{
			printf("SIZE_BLOOM should be a multiple of %ld\n", 8*sizeof(int));
			exit(0);
		}
		char* graphFilename = malloc(1000*sizeof(char));
		sprintf(graphFilename,"%s",argv[2]);
		initParametersForGraph(graphFilename,SIZE_BLOOM);

		graph_node* stdGraph = init_stdGraph(graphFilename);
		printf("Gotten adjacency list for graph\n");

		gettimeofday(&start,NULL);
		bloom_graph* graph  = createBloomGraph(stdGraph,graphFilename);
		// bloom_graph* graph  = createBloomGraphFromFile(graphFilename);
		gettimeofday(&end,NULL);
		printf("Created Graph in time::%f\n",get_time_diff(start,end));

		FILE* logger;
		if( access( "../Results/DB.csv", F_OK ) != -1 ) 
		{
    		// file exists
    		logger = fopen("../Results/DB.csv","a");
		} 
		else 
		{
    		// file doesn't exist
    		logger = fopen("../Results/DB.csv","w");
    		fprintf(logger, "graphFilename,testFilename,numNodes,numEdges,SIZE_BLOOM,MemUsage(inMB),totalSampledEdges,fp_ratio,timeTaken\n");
		}
		int numTestFiles 	= atoi(argv[3]);
		char* testFilename;
		for (int i = 0; i < numTestFiles; ++i)
		{

			testFilename = malloc(1000*sizeof(char));
			sprintf(testFilename,"%s",argv[4+i]);
			
			fprintf(logger, "%s,",graphFilename );
			gettimeofday(&start,NULL);
			performMembershipTest_Sampled(graph,testFilename,logger);
			gettimeofday(&end,NULL);
			
			printf("Membership tests for file %s done in time::%f\n",testFilename,get_time_diff(start,end));
			free(testFilename);
		}
	}
	else
	{
		printf("Incorrect usage:\n");
		printf("Usage: ./bloom_graph <SIZE OF BloomFilter> <graph filename> <numSampledEdgeFiles> <filename_1> ... <filename_n\n");
	}
	// sprintf(filename,"../../../Data/orkut-links/out.orkut-links");
	// sprintf(filename,"../../../Data/petster-friendships-cat/out.petster-friendships-cat-uniq");
	// sprintf(filename,"../../../Data/reactome/out.reactome");
}