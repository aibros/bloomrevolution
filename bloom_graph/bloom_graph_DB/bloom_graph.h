#ifndef DYNBLOOMGRAPH_H
#define DYNBLOOMGRAPH_H

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>
#include "sys/time.h"

#include "../../dynamic_bloom/dynamic_bloom.h"
#include "../adj_graph/adj_list.h"

/*
* NOTE 	   : structure capturing the node of dyn_bloom graph
* CONTENTS : dyn_bloom filters containing neighbours, size of neighbours
*/
typedef struct bloom_node{
	dyn_bloom* ngbrs;
	int num_ngbrs;
}bloom_node;

/*
* NOTE 	   : structure capturing dyn_bloom graph
* CONTENTS : array of all bloom_nodes part of graph, size or no. of vertices in graph, 
* volume or no. of edges in graph
*/
typedef struct bloom_graph
{
	bloom_node* node_list;
	long int graph_size; 	
	long int graph_volume;	
}bloom_graph;

/*
INPUT 	: Name of file containing graph
WORK	: Creates bloomGrapgh reprsentation for the graph
OUTPUT 	: Returns graph so formed
*/
bloom_graph* createBloomGraphFromFile(char* filename);
static inline float get_time_diff(struct timeval t1, struct timeval t2);

#endif
