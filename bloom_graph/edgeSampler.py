#!/usr/bin/python
import random,sys

# This functions generates c*numEdges edges by sampling from a complete graph with numNodes vertices
# where numNodes and numEdges is number of edges and vertices in the actual graph
def sampleEdges(graphFilename,c,outFilename):

	reader = open(graphFilename,"r");

	for line in reader:
		temp = line.strip().split();
		numEdges = int(temp[0])
		numNodes = int(temp[1]) - 1; # This is because if the graphfile numNodes is the largest nodeId + 1
		break;
	reader.close();

	numEdgesToSample = c*numEdges;
	ctr = 0;
	writer = open(outFilename,"w");
	while ctr < numEdgesToSample:
		ctr+=1
		src 	= random.randint(1,numNodes);
		dest 	= src;
		while dest == src:
			dest = random.randint(1,numNodes);

		if ctr < numEdgesToSample:
			writer.write(str(src) + "\t" + str(dest) + "\n");
		else:
			writer.write(str(src) + "\t" + str(dest));
	writer.close();


if __name__ == '__main__':

	if len(sys.argv) >=4:
		graphFilename 	= str(sys.argv[1])
		c 				= float(sys.argv[2])
		outFilename 	= str(sys.argv[3])
		sampleEdges(graphFilename, c, outFilename)
	else:
		print "Usage: python edgeSampler.py <graphFilename> <c> <outFilename>"
